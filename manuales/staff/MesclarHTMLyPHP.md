![CCFavicon](http://www.brontobytemx.com/CCI/favicon_cact.png)

# Cash Control

## Como mezclar HTML y PHP (Concatenar) 
----------------
Podemos decir que hay 2 maneras de mezclar **HTML** y **PHP**. Aunque ambas formas dan el mismo resultado final en el navegador es recomendable usar cada uno en cierto conexto para facilitarnos escribir el código y sea mas fácil entenderlo.
1. **PHP incrustado en el HTML**
 Este lo vamos a usar generalmente con el view de html para incrustar valores estaticos al cargar el documento y mostrarlo en el navegador.
*Prestar atencion a: "Abrir y cerrar las tags **<?php ?>**" . "Usar la sentencia **echo**  para imprimir el valor en nuestro view no nos marcara ningun error sino lo ponemos pero simplemente no mostrará en el navegador". "Cerrar todas las sentencias con punto y coma **;**  sino lo hacemos si nos mostrara un error fatal en nuestro view".* **<?php echo ; ?>**
*Ejemplos:*
```html
<!-- Una cadena de texto simple-->
<h2><?php echo"Hola mundo";?></h2>

<!-- Una variable simple-->

<p><?php echo $parrafo; ?></p>

<!-- Una variable de un array 
**NOTA si usaras simplemente echo $lista; nos marcaria un error porque no se le puede hacer echo a una variable que es un array sin especificar su indice con los corchetes ["indice"]-->
<li><?php echo $lista["Elemento1"]; ?></li>


<!-- Una funcion. **NOTA una funcion siempre va tener la siguiente estructura "funcion()" 
pero las funciones pueden tener "X" numero de parametros dentro de los parentesis "funcion($parametro1,$parametro2,$parametro3)"y pueden verse asi "funcion(otraFuncion(),$variable,FALSE)" para no hacerse bolas hay que identificar la primer funcion despues del "echo" y ver donde abren y cierran los parentesis"()"-->

<!-- Esta funcion nos devuelve el texto en mayusculas "HOLA MUNDO"-->
<p><?php echo strtoupper("hola mundo"); ?></p>

<!-- Esta funcion nos imprime la "URL" principal de nuestro servidor -->
<a href="<?php echo base_url(); ?>">Pagina principal</a>

<!-- Una variable objeto de una función
**NOTA Estas son variables muy similares a los arrays pero usan "->" en vez de [""] para acceder a la funcion de su objeto y con la cualidad de tener "()" y parametros tal cual una funcion -->

<span><?php echo $this->lang->("mi_variable_de_lengua"); ?></span>
<!--Entre lineas del HTML
Esto ya es poco mas complicado porque hay que usar varias sentencias de "PHP" en diferentes lineas y el editor de texto no especifica los limites pero muy util y usado comunmente  nos puede servir por ejemplo para mostrar u ocultar bloques de "HTML" -->
<!-NOTA Esta sentencia no se imprime (no lleva echo) pero todo el HTML que estara despues osea debajo solo se imprimira si la condicion se cumple"-->
<?php if($var==1){ ?>
<article>
<h3><?php echo $titulo;?></h3>
</article>
<?php } ?><!--Limite de la condicion-->

```
**NOTA** Si algun valor no se muestra en el view no significa que esta mal nuesto código, puede haber otras razones por las cuales no se muestran. Mientras no nos marque algun error del **PHP**, se puede decir que nuestro codigo estan bien. Para ver si nuestras variables contienen algo o estan vacias o ver de que tipo son se pueden usar la funcion **var_dump($var)**, poniendo nuestra variable a testear dentro de los corchetes.
2. **HTML inscrustado en el PHP**
 Este lo vamos a usar generalmente con los helpers es html dinamico que regresa alguna funcion de **PHP** y encapsulado siempre en una variable.
*Prestar atencion: 
*Aqui no se necesita usar las tags **<?php ?>** ni el **echo** para poder usar la variables aqui solo hay que poner mucha atencion en el uso de **" "** y **' '*** para escapar las variables y siempre  usar un punto **.** para unirlas a la cadena de texto. El punto nos sirve inclusive para concatenar mas texto a una variable si se pone antes del operador de asignación **=** de la siguiente forma:*
```php
$var="Algo de texto";
$var.="mas texto para la variable";
echo $var; //Algo de texto mas texto para la variable
```

*Ejemplos de HTML inscrustado en el PHP:*
```php
//Una variable simple
$mi_html="<p>".$parrafo."</p>";

//Una variable de un array 
$mi_html="<li>".$lista["Elemento1"]."</li>";

//Una funcion
$mi_html="<p>".strtoupper("hola mundo")."</p>";
$mi_html="<a href='".base_url()."'>Pagina principal</a>";
//Una variable objeto de una función **NOTA las comillas dentro de la funcion no afectan la cadena de texto
$mi_html="<span>".$this->lang->("mi_variable_de_lengua")."</span>";

//En una condicion
if($var==1){ 
$mi_html="<article>
<h3>".$titulo."</h3>
</article>";
 } //Limite de la condicion-->
```


