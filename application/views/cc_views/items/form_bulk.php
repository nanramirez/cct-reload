<?php
echo form_open_multipart('items/bulk_update/', array('id'=>'item_form','class'=>'forms_module'));
?>
<div id="item_basic_info" class="fieldset_form_popup">
<legend class="title_forms_popup"><?php echo $this->lang->line('module_items_editon');
?></legend>
<input type="hidden" id="jsonerrors" value='<?php echo $json_form_errors; ?>'name="jsonerrors"/>
<div id="cover_field_row_popup">
<?php if($config_info["user_cfdi"]!="")
	{ ?>
	<div class="box_field_row">
	<?php echo form_label($this->lang->line('module_item_ClaveProdServ').':', 'ClaveProdServ',array('class'=>'required','id'=>'ClaveProdServ_label')); ?>
		<div class='box_row'>
		<?php echo form_input(array(
			'name'=>'ClaveProdServ',
			'id'=>'ClaveProdServ',
			'value'=>'')
		);?>
		</div>
	</div>
	<div class="box_field_row">
	<?php echo form_label($this->lang->line('module_item_ClaveUnidad').':', 'ClaveUnidad',array('class'=>'required','id'=>'ClaveUnidad_label')); ?>
		<div class='box_row'>
		<?php echo form_input(array(
			'name'=>'ClaveUnidad',
			'id'=>'ClaveUnidad',
			'value'=>'')
		);?>
		</div>
	</div>
<?php } ?>
<div class="box_field_row">
<?php echo form_label($this->lang->line('module_item_brand').':', 'brand',array('class'=>'required' ,'id'=>'brand_label')); ?>
<div class='box_row'>
<div id="ui-widget-brand">
<?php echo form_input(array(
'name'=>'brand',
'id'=>'brand',
'val'=>''
));?>
</div>
</div>
</div>
<div class="box_field_row">
<?php echo form_label($this->lang->line('common_category').':', 'category',array('class'=>'required','id'=>'category_label')); ?>
<div class='box_row'>
<div id="ui-widget-category">
<?php echo form_input(array(
'name'=>'category',
'id'=>'category',
'value'=>'')
);?>
</div>
</div>
</div>
<div class="box_field_row">
<?php echo form_label($this->lang->line('module_supplier').':', 'supplier',array('class'=>'required')); ?>
<div class='box_row'>
<?php echo form_dropdown('supplier_id', $suppliers, '',array('id'=>'supplier_id'));?>
</div>
</div>
<div class="box_field_row">
<?php echo form_label($this->lang->line('common_cost').':', 'cost_price',array('class'=>'required','id'=>'cost_price_label')); ?>
<div class='box_row'>
<?php echo form_input(array(
'name'=>'cost_price',
'id'=>'cost_price',
'val'=>'')
);?>
</div>
</div>
<div class="box_field_row">
<?php echo form_label($this->lang->line('common_price').':', 'unit_price',array('class'=>'required','id'=>'unit_price_label')); ?>
<div class='box_row'>
<?php echo form_input(array(
'name'=>'unit_price',
'id'=>'unit_price',
'val'=>'')
);?>
</div>
</div>
<div class="box_field_row">
<?php echo form_label($this->lang->line('common_price_special').':', 'other_price',array('class'=>'required' ,'id'=>'other_price_label')); ?>
<div class='box_row'>
<?php echo form_input(array(
'name'=>'other_price',
'id'=>'other_price',
'val'=>'')
);?>
</div>
</div>
<div class="box_field_row">
<?php echo form_label($this->lang->line('module_item_stock_minimum').':', 'reorder_level',array('class'=>'required','id'=>'reorder_level_label')); ?>
<div class='box_row'>
<?php echo form_input(array(
'name'=>'reorder_level',
'id'=>'reorder_level',
'val'=>'')
);?>
</div>
</div>
<div class="box_field_row">
<div class="field_row_half">
<?php echo form_label($this->lang->line('common_taxe').' 1:', 'tax_percent_1',array('class'=>'required','id'=>'tax_percent_name_1_label')); ?>
<div class='box_row'>
<?php echo form_input(array(
'name'=>'tax_names[]',
'id'=>'tax_name_1',
'value'=>'')
);?>
</div>
</div>
<div class="field_row_half">
<label class="percent-ico">%</label>
<div class='box_row'>
<?php echo form_input(array(
'name'=>'tax_percents[]',
'id'=>'tax_percents_1',
'val'=>''
));?>
</div>
</div>
</div>
<div class="box_field_row">
<div class="field_row_half">
<?php echo form_label($this->lang->line('common_taxe').' 2:', 'tax_percent_2',array('class'=>'required','id'=>'tax_percent_name_2_label')); ?>
<div class='box_row'>
<?php echo form_input(array(
'name'=>'tax_names[]',
'id'=>'tax_name_2',
'val'=>''));?>
</div>
</div>
<div class="field_row_half">
<label class="percent-ico">%</label>
<div class='box_row'>
<?php echo form_input(array(
'name'=>'tax_percents[]',
'id'=>'tax_percents_2',
'val'=>''));?>
</div>
</div>
</div>
<div class="box_field_row">
<?php echo form_label($this->lang->line('common_location').':', 'location',array('class'=>'required')); ?>
<div class='box_row'>
<?php echo form_input(array(
'name'=>'location',
'id'=>'location',
'val'=>'')
);?>
</div>
</div>
<div class='box_field_row bfr_checkbox'>
<?php echo form_label($this->lang->line('common_publish').':', 'publicar',array('class'=>'required')); ?>
<div class='box_row'>
<?php echo form_dropdown('publicar', $publish_choices, '',array('id'=>'publicar'));?>
</div>
</div>
<div class='box_field_row bfr_checkbox'>
<?php echo form_label($this->lang->line('module_item_destacado').':', 'destacar',array('class'=>'required')); ?>
<div class='box_row'>
<?php echo form_dropdown('destacar', $destacar_choices, '',array('id'=>'destacar'));?>
</div>
</div>
<div class='box_field_row bfr_checkbox'>
<?php echo form_label($this->lang->line('module_item_show_price').':', 'show_price',array('class'=>'required')); ?>
<div class='box_row'>
<?php echo form_dropdown('show_price', $show_price_choices, '',array('id'=>'show_price'));?>
</div>
</div>
<div class='box_field_row bfr_checkbox'>
<?php echo form_label($this->lang->line('module_item_show_stock').':', 'show_stock',array('class'=>'required')); ?>
<div class='box_row'>
<?php echo form_dropdown('show_stock', $show_stock_choices, '',array('id'=>'show_stock'));?>
</div>
</div>
<div class='box_field_row bfr_checkbox'>
<?php echo form_label($this->lang->line('common_promo').':', 'promocionar',array('class'=>'required')); ?>
<div class='box_row'>
<?php echo form_dropdown('promocionar', $promocionar_choices, '',array('id'=>'promocionar'));?>
</div>
</div>
<div class='box_field_row bfr_checkbox'>
<?php echo form_label($this->lang->line('common_allow').' '.$this->lang->line('common_description').':', 'allow_alt_description',array('class'=>'required')); ?>
<div class='box_row'>
<?php echo form_dropdown('allow_alt_description', $allow_alt_desciption_choices, '',array('id'=>'allow_alt_description'));?>
</div>
</div>
<div class='box_field_row bfr_checkbox'>
<?php echo form_label($this->lang->line('common_use').' '.$this->lang->line('module_item_sn').':', 'is_serialized',array('class'=>'required')); ?>
</div>
<div class='box_row'>
<?php echo form_dropdown('is_serialized', $serialization_choices,'',array('id'=>'is_serialized')); ?>
</div>
<div class="box_field_row"> 
<div class="quantity">
	<?php echo form_label($this->lang->line('module_item_qtyimg').':', 'img_qty',array('class'=>'required')); ?>
		<?php echo form_input(array(
			'type'=>'number',
			'name'=>'img_qty',
			'id'=>'img_qty',
			'value'=>'',
			'min'=>0,
			'max'=>99,
			'step'=>1
			)
		);?>
		</div>
		</div>
<?php
echo form_button(array(
			'name' => 'button',
			'type' => 'submit',
			'id' => 'button',
			'content' => $this->lang->line('common_submit'),
			'class'=>'submit_button_pop_2'));
?>
</div>
</div>
<?php 
echo form_close();
?>