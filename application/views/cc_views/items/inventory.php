<?php
echo form_open('items/save_inventory/'.$item_info->item_id,array('id'=>'item_inventory_form','class'=>'forms_module'));
?>
<fieldset id="" class="fieldset_form_popup">
	<legend class="title_forms_popup"><?php echo $this->lang->line('common_inventory_form');
	?></legend>
		<input type="hidden" id="json_msj" value='<?php echo $json_msjs; ?>'name="json_msj"/>
		<input type="hidden" id="id_input" value="<?php echo $item_info->item_id; ?>" name="id_input"/>
		<input type="hidden" id="base_url" value="<?php echo base_url(); ?>" name="base_url"/>
		<div id="cover_field_row_popup_info">
		<div class="box_field_row" id="box_info_title_middle">
		<?php echo form_label($this->lang->line('module_item').'#'.$item_info->item_id.': <b>'.$item_info->name.'</b> '.$item_info->modelo.'', 'name',array('class'=>'required')); ?>
		</div>	
		<div class="box_field_row_info" id="box_inv_quantity">
		<?php echo form_label($this->lang->line('module_item_stock').': <b>'.$item_info->quantity.'</b>', 'quantity',array('class'=>'required')); ?>
		</div>
		<div class="box_field_row ">
		<?php echo form_label($this->lang->line('common_inventory_label').':', 'newquantity',array('class'=>'required','id'=>'newquantity_label')); ?>
			<div class='box_row'>
			<?php echo form_input(array(
				'name'=>'newquantity',
				'id'=>'newquantity',
				'value'=>''
				)
			);?>
			</div>
		</div>
		<div class="box_field_row">
		<?php echo form_label($this->lang->line('common_inventory_justify').':', 'justify',array('class'=>'required','id'=>'justify_label')); ?>
			<div class='box_row'>
			<?php echo form_textarea(array(
				'name'=>'trans_comment',
				'id'=>'trans_comment',
				'value'=>'')
			);?>
			</div>
		</div>
		<?php
		echo form_button(array(
						'name' => 'button',
						'type' => 'submit',
						'id' => 'button',
						'content' => $this->lang->line('common_submit'),
						'class'=>'submit_button_inventory'));
		?>
		</div>
</fieldset>
<?php 
echo form_close();
?>