<form id="import_items" class="forms_module">
	<fieldset id="item_basic_info" class="fieldset_form_popup">
		<legend class="title_forms_popup">Excel</legend>
		<div id="cover_field_row_popup">
			<input type="hidden" id="xlsxsheet" value="" name="xlsxsheet[]"/>
			<input type="hidden" id="error_msj" value="<?php echo $this->lang->line("module_items_import_fail"); ?>" name="error_msj"/>
			<div class="box_field_row">	
				<?php echo form_label($this->lang->line('common_upload_file').':', 'uploadfile',array('class'=>'wide')); ?>
				<div id="box_row_uploadfile" class='box_row'>
					<?php echo form_upload(array(
					'multiple'=>'multiple',
					'accept'=>'xls|XLSX',
					'class'=>'form-control',
					'name'=>'xlfile',
					'id'=>'xlf',
					'size'=>'18'));?>
				</div>
			</div>
			<div class="box_field_row">	
				<div class="drop" id="drop">
					<?php echo $this->lang->line('common_drag_excel');?>
				</div>
				<pre id="out"></pre>
			</div>
			<?php echo form_button(array(
			'name' => 'button',
			'type' => 'button',
			'id' => 'button_import',
			'content' => $this->lang->line('common_import'),
			'class'=>'submit_import_button'));?>
			<div class="box_field_row">
				<?php echo form_label('<a  href="'.base_url().'formats/items.xlsx">'.$this->lang->line('common_download').'</a> '.$this->lang->line('common_excel_format').' ', 'formats',array('class'=>'required','id'=>'formats_label')); ?>
			</div>
			<?php echo form_button(array(
			'name' => 'button',
			'type' => 'button',
			'id' => 'button_export',
			'content' => $this->lang->line('common_export'),
			'class'=>'submit_export_button'));?>
		</div>
	</fieldset>
</form>