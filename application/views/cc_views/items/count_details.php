<form id="item_inventory_form" class="forms_module">
	<div  class="fieldset_form_popup_info">
		<legend class="title_forms_popup_info"><?php echo $this->lang->line("items_basic_information"); ?></legend>
		<input type="hidden" id="id_input" value="<?php echo $item_info->item_id; ?>" name="id_input"/>
		<div id="cover_field_row_popup_info_start">
			<div class="box_field_row_info" id="box_info_title_middle">	
				<?php echo form_label($this->lang->line('module_item').'#'.$item_info->item_id.': <b>'.$item_info->name.'</b> '.$item_info->modelo.'', 'name',array('class'=>'required')); ?>
			</div>
			<div>
				<!--Inicio IMG-->
				<?php 
				if(is_array($images)){
					foreach($images as $image){
						$img_info=getimagesize($image);
						$chars = array('.jpg','.png','.JPG','.JPEG','.jpeg');
						$img_name = explode("/".$item_info->item_id."/", $image);
						if(isset($img_name[1])){
							$no_ext_name=str_replace($chars,"",$img_name[1]);
						}
						else{
							$img_name = explode(chr(92).$item_info->item_id.chr(92), $image);
							$no_ext_name=str_replace($chars,"",$img_name[1]);
						}
						if($no_ext_name=="main"){	?>
							
							<div id="detail_image_item"> 
								<img id="main_img" class="image-list"src="<?php echo base_url().'images/f_images/img_catalogo/productos/'.$item_info->item_id.'/'.$img_name[1];?>"/>
							</div>
							<input type="hidden" id="src_main" value='<?php echo base_url().'images/f_images/img_catalogo/productos/'.$item_info->item_id.'/'.$img_name[1];?>'name="ext_name"/>
							<?php
						}
						
					}
				}?>
				<!--fin IMG-->
				
			</div>
		</div>
		<div id="cover_field_row_popup_info">
			<div class="box_field_row_info_small">
			<?php 
			echo form_label( '<b>'.$this->lang->line('module_item') .': </b>  '.$item_info->name, 'name',array('class'=>'required')); ?>
			</div>
			<div class="box_field_row_info_small">
			<?php 
			echo form_label('<b>'.$this->lang->line('module_item_brand').': </b>  '.$item_info->brand, 'brand',array('class'=>'required')); ?>
			</div>
			<div class="box_field_row_info_small">
			<?php 
			echo form_label('<b>'.$this->lang->line('module_item_modelo').': </b>'.$item_info->modelo, 'modelo',array('class'=>'required')); ?>
			</div>
			<div class="box_field_row_info_small">
			<?php 
			echo form_label('<b>'.$this->lang->line('common_category').': </b>'.$item_info->category, 'category',array('class'=>'required')); ?>
			</div>
			<div class="box_field_row_info_small">
			<?php 
			echo form_label('<b>'.$this->lang->line('module_supplier').': </b>'.$supplier, 'supplier',array('class'=>'required')); ?>
			</div>
			<div class="box_field_row_info_small">
			<?php 
			echo form_label('<b>'.$this->lang->line('common_cost').': </b>'.$item_info->cost_price, 'cost_price',array('class'=>'required')); ?>
			</div>
			<div class="box_field_row_info_small">
			<?php 
			echo form_label('<b>'.$this->lang->line('common_price_sale').': </b>'.$item_info->unit_price, 'unit_price',array('class'=>'required')); ?>	
			</div>
			<div class="box_field_row_info_small">
			<?php 
			echo form_label('<b>'.$this->lang->line('common_price_special').': </b>'.$item_info->other_price, 'other_price',array('class'=>'required')); ?>
			</div>
			<div class="box_field_row_info_small">
			<?php 
			echo form_label('<b>'.$this->lang->line('common_barcode').': </b>'.$item_info->item_number, 'item_number',array('class'=>'required')); ?>
			</div>
			<div class="box_field_row_info_small">
			<?php 
			echo form_label('<b>'.$this->lang->line('common_taxe').': </b>'.!is_null($item_tax_info).!is_null($default_tax_1_rate) .'', 'tax_percents',array('class'=>'required')); ?>
			</div>
			<div class="box_field_row_info_small">
			<?php 
			echo form_label('<b>'.$this->lang->line('common_location').': </b>'.$item_info->location, 'location',array('class'=>'required')); ?>
			</div>
			<div class="box_field_row_info_small">
			<?php 
			echo form_label('<b>'.$this->lang->line('common_description').': </b>'.$item_info->description, 'description',array('class'=>'required')); ?>
			</div>
			<div class="box_field_row_info_small">
			<?php 
			if($item_info->publicar==0){$resp=$this->lang->line('common_no');}else{$resp=$this->lang->line('common_si');}
			echo form_label('<b>'.$this->lang->line('common_published').': </b>'.$resp, 'publish',array('class'=>'required')); ?>
			</div>
			<div class="box_field_row_info_small">
			<?php 
			if($item_info->allow_alt_description==0){$resp=$this->lang->line('common_no');}else{$resp=$this->lang->line('common_si');}
			echo form_label('<b>'.$this->lang->line('common_allow').' '.$this->lang->line('common_description').': </b>'.$resp, 'allow_alt_description',array('class'=>'required')); ?>
			</div>
			<div class="box_field_row_info_small">
			<?php 
			if($item_info->is_serialized==0){$resp=$this->lang->line('common_no');}else{$resp=$this->lang->line('common_si');}
			echo form_label('<b>'.$this->lang->line('common_use').' '.$this->lang->line('module_item_sn').$resp.': </b>', 'is_serialized',array('class'=>'required')); ?>
			</div>
			<div class="box_field_row_info_small">
			<?php echo form_label('<b>'.$this->lang->line('common_category').': </b>'.$item_info->category, 'category',array('class'=>'required')); ?>
			</div>
			<div class="box_field_row_info_small">
			<?php echo form_label('<b>'.$this->lang->line('module_item_stock').': </b>'.$item_info->quantity, 'quantity',array('class'=>'required')); ?>
			</div>
			<div id="table_detail_item">
				<span><?php echo $this->lang->line('items_tracking_inventory_data'); ?></span>
				<div id="cover_title_detail_item">
					<div class="title_detail_item" id="inventoryFecha"><span><?php echo $this->lang->line('common_date'); ?></span></div>
					<div class="title_detail_item" id="inventoryEmpleado"><span><?php echo $this->lang->line('module_employee'); ?></span></div>
					<div class="title_detail_item" id="inventoryCantidad"><span><?php echo $this->lang->line('common_unidades'); ?></span></div>
					<div class="title_detail_item" id="inventoryRazon"><span><?php echo $this->lang->line('common_motivo'); ?></span></div>
				</div>
				<div id="cover_result_detail_item">
					<?php
					foreach($this->Inventory->get_inventory_data_for_item($item_info->item_id)->result_array() as $key=>$row)
					{
					?>
					<div class="file_det_item">
					<div class="detail_item" id="result_fecha">
					<div class="soloVisibleResponsivamente_det"><span><?php echo $this->lang->line('common_date'); ?></span></div>
					<span class="span_r_d"><?php echo $row['trans_date'];?></span>
					</div>
					<div class="detail_item" id="result_empleado">
					<div class="soloVisibleResponsivamente_det">
					<span><?php echo $this->lang->line('module_employee'); ?></span>
					</div>
					<span class="span_r_d"><?php
					$person_id = $row['trans_user'];
					$employee = $this->Employee->get_info($person_id);
					echo $employee->first_name." ".$employee->last_name;
					?>
					</span>
					</div>
					<div class="detail_item" id="result_cantidad">
					<div class="soloVisibleResponsivamente_det"><span><?php echo $this->lang->line('common_unidades'); ?></span>
					</div>
					<span class="span_r_d"><?php echo $row['trans_inventory'];?></span>
					</div>
					<div class="detail_item" id="result_razon">
					<div class="soloVisibleResponsivamente_det">
					<span><?php echo $this->lang->line('common_motivo'); ?></span>
					</div>
					<span class="span_r_d"><a class="ver_mas_<?php echo $key; ?>">Ver Motivo</a></span>
					</div>
					<div class="detail_razon" id="result_razon_detalle_<?php echo $key; ?>">
					<span><?php echo $row['trans_comment'];?></span>
					</div>
					<input type="hidden" id="detalle_input_<?php echo $key; ?>" value="<?php echo $key; ?>" name="result_razon_detalle_"/>
					<?php
					}
					?>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
