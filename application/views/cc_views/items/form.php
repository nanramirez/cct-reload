<?php
echo form_open_multipart('items/upload_image/'.$item_info->item_id,array('id'=>'item_form','class'=>'forms_module'));
?>
<div id="item_basic_info" class="fieldset_form_popup">
<legend class="title_forms_popup"><?php echo $title_form; ?></legend>
<input type="hidden" id="jsonerrors" value='<?php echo $json_form_errors; ?>' name="jsonerrors"/>	
<input type="hidden" id="id_input" value="<?php if($id!="menosuno") echo $id; ?>" name="id_input"/>
<input type="hidden" id="loading_txt" value="<?php echo $loading_txt; ?>" name="loading_txt"/>
	<div id="cover_field_row_popup">
	<div class="box_field_row">
		<?php echo form_label($this->lang->line('module_item').':', 'name',array('class'=>'required','id'=>'items_name_label')); ?>
		<div class='box_row'>
			<?php echo form_input(array(
				'name'=>'name',
				'id'=>'name',
				'value'=>$item_info->name,
				'title'=>$this->lang->line('module_item_title'))
			);?>
		</div>
	</div>
	<div class="box_field_row">
	<?php echo form_label($this->lang->line('module_item_brand').':', 'brand',array('class'=>'required' ,'id'=>'brand_label')); ?>
		<div class='box_row'>
		<div id="ui-widget-brand">
		<?php echo form_input(array(
			'name'=>'brand',
			'id'=>'brand',
			'value'=>$item_info->brand,
			'title'=>$this->lang->line('module_item_brand_title'))
		);?>
		</div>
		</div>
	</div>
	<div class="box_field_row">
	<?php echo form_label($this->lang->line('module_item_modelo').':', 'modelo',array('class'=>'required','id'=>'modelo_label')); ?>
		<div class='box_row'>
		<?php echo form_input(array(
			'name'=>'modelo',
			'id'=>'modelo',
			'value'=>$item_info->modelo,
			'title'=>$this->lang->line('module_item_modelo_title'))
		);?>
		</div>
	</div>
	<?php if($config_info["user_cfdi"]!="")
	{ ?>
	<div class="box_field_row">
	<?php echo form_label($this->lang->line('module_item_ClaveProdServ').':', 'ClaveProdServ',array('class'=>'required','id'=>'ClaveProdServ_label')); ?>
		<div class='box_row'>
		<?php echo form_input(array(
			'name'=>'ClaveProdServ',
			'id'=>'ClaveProdServ',
			'value'=>$item_cfdi_info->c_ClaveProdServ,
		    'title'=>$this->lang->line('module_item_ClaveProdServ_title'))
		);?>
		</div>
	</div>
<?php } ?>
	<div class="box_field_row">
	<?php echo form_label($this->lang->line('common_category').':', 'category_label',array('class'=>'required','id'=>'category_label')); ?>
		<div class='box_row'>
		<div id="ui-widget-category">
		<?php echo form_input(array(
			'name'=>'category',
			'id'=>'category',
			'value'=>$item_info->category,
		    'title'=>$this->lang->line('common_category_title'))
		);?>
		</div>
		</div>
	</div>
	<div class="box_field_row">
	<?php echo form_label($this->lang->line('module_supplier').':', 'supplier',array('class'=>'required')); ?>
		<div class='box_row'>
		<?php echo form_dropdown('supplier', $suppliers, $selected_supplier,array('id'=>'supplier'));?>
		</div>
	</div>
	<div class="box_field_row">
	<?php echo form_label($this->lang->line('common_cost').' '.$config_info['coin'].':', 'cost_price',array('class'=>'required','id'=>'cost_price_label')); ?>
		<div class='box_row'>
		<?php echo form_input(array(
			'name'=>'cost_price',
			'id'=>'cost_price',
			'value'=>$item_info->cost_price,
		    'title'=>$this->lang->line('common_cost_title'))
		);?>
		</div>
	</div>
	<div class="box_field_row">
	<?php echo form_label($this->lang->line('common_price').' '.$config_info['coin'].':', 'unit_price',array('class'=>'required','id'=>'unit_price_label')); ?>
		<div class='box_row'>
		<?php echo form_input(array(
			'name'=>'unit_price',
			'id'=>'unit_price',
			'value'=>$item_info->unit_price,
		    'title'=>$this->lang->line('common_price_title'))
		);?>
		</div>
	</div>
	<div class="box_field_row">
	<?php echo form_label($this->lang->line('common_price_special').' '.$config_info['coin'].':', 'other_price',array('class'=>'required' ,'id'=>'other_price_label')); ?>
		<div class='box_row'>
		<?php echo form_input(array(
			'name'=>'other_price',
			'id'=>'other_price',
			'value'=>$item_info->other_price,
		    'title'=>$this->lang->line('common_price_special_title'))
		);?>
		</div>
	</div>
	<div class="box_field_row">
	<?php echo form_label($this->lang->line('module_item_unit_stock').':', 'quantity',array('class'=>'required','id'=>'quantity_label')); ?>
		<div class='box_row'>
		<?php if(empty($item_info->item_id)){echo form_input(array(
			'name'=>'quantity',
			'id'=>'quantity',
			'value'=>$item_info->quantity,
		    'title'=>$this->lang->line('module_item_unit_stock'))
		);}else{
			echo form_label("<b>".$item_info->quantity."</b>", 'quantity',array('class'=>'required','id'=>'quantity_label_'));?>
			<input type="hidden" id="quantity" value="<?php echo $item_info->quantity; ?>" name="quantity"/>
			<?php
			}?>
		</div>
	</div>
	<?php if($config_info["user_cfdi"]!="")
	{ ?>
	<div class="box_field_row">
	<?php echo form_label($this->lang->line('module_item_ClaveUnidad').':', 'ClaveUnidad',array('class'=>'required','id'=>'ClaveUnidad_label')); ?>
		<div class='box_row'>
		<?php echo form_input(array(
			'name'=>'ClaveUnidad',
			'id'=>'ClaveUnidad',
			'value'=>$item_cfdi_info->c_ClaveUnidad,
		    'title'=>$this->lang->line('module_item_ClaveUnidad_title'))
		);?>
		</div>
	</div>
<?php } ?>
	<div class="box_field_row">
	<?php echo form_label($this->lang->line('module_item_stock_minimum').':', 'reorder_level',array('class'=>'required','id'=>'reorder_level_label')); ?>
		<div class='box_row'>
		<?php echo form_input(array(
			'name'=>'reorder_level',
			'id'=>'reorder_level',
			'value'=>$item_info->reorder_level,
		    'title'=>$this->lang->line('module_item_stock_minimum_title'))
		);?>
		</div>
	</div>
	<div class="box_field_row">
	<?php echo form_label($this->lang->line('common_barcode').':', 'item_number',array('class'=>'required','id'=>'item_number_label')); ?>
		<div class='box_row'>
		<?php echo form_input(array(
			'name'=>'item_number',
			'id'=>'item_number',
			'value'=>$item_info->item_number,
		    'title'=>$this->lang->line('common_barcode_title'))
		);?>
		</div>
	</div>
	<div class="box_field_row">
		<div class="field_row_half">
		<?php echo form_label($this->lang->line('common_taxe').' 1:', 'tax_percent_1',array('class'=>'required','id'=>'tax_percent_name_1_label')); ?>
			<div class='box_row'>
			<?php echo form_input(array(
				'name'=>'tax_names[]',
				'id'=>'tax_name_1',
				'value'=> isset($item_tax_info[0]['name']) ? $item_tax_info[0]['name'] : $this->config->item('default_tax_1_name'),
			    'title'=>$this->lang->line('common_taxe_title'))
			);?>
			</div>
		</div>
		<div class="field_row_half">
			<label class="percent-ico">%</label>
			<div class='box_row'>
			<?php echo form_input(array(
				'name'=>'tax_percents[]',
				'id'=>'tax_percents_1',
				'value'=> isset($item_tax_info[0]['percent']) ? $item_tax_info[0]['percent'] : $default_tax_1_rate)
			);?>
			</div>
		</div>
	</div>
	<div class="box_field_row">
		<div class="field_row_half">
		<?php echo form_label($this->lang->line('common_taxe').' 2:', 'tax_percent_2',array('class'=>'required','id'=>'tax_percent_name_2_label')); ?>
			<div class='box_row'>
			<?php echo form_input(array(
				'name'=>'tax_names[]',
				'id'=>'tax_name_2',
				'value'=> isset($item_tax_info[1]['name']) ? $item_tax_info[1]['name'] : $this->config->item('default_tax_2_name'),
			    'title'=>$this->lang->line('common_taxe_title'))
			);?>
			</div>
		</div>
		<div class="field_row_half">
			<label class="percent-ico">%</label>
			<div class='box_row'>
			<?php echo form_input(array(
				'name'=>'tax_percents[]',
				'id'=>'tax_percents_2',
				'value'=> isset($item_tax_info[1]['percent']) ? $item_tax_info[1]['percent'] : $default_tax_2_rate)
			);?>
			</div>
		</div>
	</div>
	<div class="box_field_row">	
	<?php echo form_label($this->lang->line('common_location').':', 'location',array('class'=>'required')); ?>
		<div class='box_row'>
		<?php echo form_input(array(
			'name'=>'location',
			'id'=>'location',
			'value'=>$item_info->location,
		    'title'=>$this->lang->line('common_location_title'))
		);?>
		</div>
	</div>
	<div class="box_field_row">
	<?php echo form_label($this->lang->line('common_description').':', 'description',array('class'=>'required')); ?>
		<div class='box_row'>
		<?php echo form_textarea(array(
			'name'=>'description',
			'id'=>'description',
			'value'=>$item_info->description,
			'rows'=>'5',
			'cols'=>'17',
		    'title'=>$this->lang->line('common_description_title'))
		);?>
		</div>
	</div>
	<div class='box_field_row bfr_checkbox'>
	<?php echo form_label($this->lang->line('common_publish').':', 'publish',array('class'=>'required')); ?>
		<?php 
		echo form_checkbox(array(
			'name'=>'publish',
			'id'=>'publish',
			'class'=> 'checkbox_form_n',
			'value'=>1,
			'checked'=>$item_info->publicar,
		    'title'=>$this->lang->line('common_publish_title'))
		);?>
	</div>
	<div class='box_field_row bfr_checkbox'>
	<?php echo form_label($this->lang->line('module_item_destacado').':', 'destacar',array('class'=>'required')); ?>
		<?php 
		echo form_checkbox(array(
			'name'=>'destacar',
			'id'=>'destacar',
			'class'=> 'checkbox_form_n',
			'value'=>1,
			'checked'=>$item_info->destacar,
		    'title'=>$this->lang->line('module_item_destacado_title'))
		);?>
	</div>
	<div class='box_field_row bfr_checkbox'>
	<?php echo form_label($this->lang->line('module_item_show_price').':', 'show_price',array('class'=>'required')); ?>
		<?php 
		echo form_checkbox(array(
			'name'=>'show_price',
			'id'=>'show_price',
			'class'=> 'checkbox_form_n',
			'value'=>1,
			'checked'=>$item_info->show_price,
		    'title'=>$this->lang->line('module_item_show_price_title'))
		);?>
	</div>
	<div class='box_field_row bfr_checkbox'>
	<?php echo form_label($this->lang->line('module_item_show_stock').':', 'show_stock',array('class'=>'required')); ?>
		<?php 
		echo form_checkbox(array(
			'name'=>'show_stock',
			'id'=>'show_stock',
			'class'=> 'checkbox_form_n',
			'value'=>1,
			'checked'=>$item_info->show_stock,
		    'title'=>$this->lang->line('module_item_show_stock_title'))
		);?>
	</div>
	<div class="box_field_row  bfr_checkbox">
	<?php echo form_label($this->lang->line('common_promo').':', 'promocionar',array('class'=>'required')); ?>
		<?php echo form_checkbox(array(
			'name'=>'promocionar',
			'class'=> 'checkbox_form_n',
			'id'=>'promocionar',
			'value'=>1,
			'checked'=>$item_info->promocionar,
		    'title'=>$this->lang->line('common_promo_title'))
		);?>
	</div>
	<div class="box_field_row  bfr_checkbox">
	<?php echo form_label($this->lang->line('common_allow').' '.$this->lang->line('common_description').':', 'allow_alt_description',array('class'=>'required')); ?>
		<?php echo form_checkbox(array(
			'name'=>'allow_alt_description',
			'class'=> 'checkbox_form_n',
			'id'=>'allow_alt_description',
			'value'=>1,
			'checked'=>$item_info->allow_alt_description,
		    'title'=>$this->lang->line('common_allow_title'))
		);?>
	</div>
	<div class="box_field_row  bfr_checkbox">
	<?php echo form_label($this->lang->line('common_use').' '.$this->lang->line('module_item_sn').':', 'is_serialized',array('class'=>'required')); ?>
		<?php echo form_checkbox(array(
			'name'=>'is_serialized',
			'class'=> 'checkbox_form_n',
			'id'=>'is_serialized',
			'value'=>1,
			'checked'=>$item_info->is_serialized,
		    'title'=>$this->lang->line('common_use_title'))
		);?>
	</div>
	<div class="box_field_row"> 
				<div class='quantity'>
	<?php echo form_label($this->lang->line('module_item_qtyimg').':', 'img_qty',array('class'=>'required')); ?>
		<?php echo form_input(array(
			'type'=>'number',
			'name'=>'img_qty',
			'id'=>'img_qty',
			'value'=>$item_info->img_qty==null ? 0:$item_info->img_qty,
			'min'=>0,
			'max'=>99,
			'step'=>1
			)
		);?>
		</div>
		</div>
	<?php 
	
		if($images!=FALSE && $id!="menosuno"){
		foreach($images as $key=>$image)
		{
			if($key<=$item_info->img_qty)
			{
				$img_info=getimagesize($image);
				
				$chars = array('.jpg','.png','.JPG','.JPEG','.jpeg','/',chr(92));
				$chars2 = array('/',chr(92));
				$img_name = explode('/'.$id.'/', $image);
				if(isset($img_name[1]))
				{
					$no_ext_name=str_replace($chars,"",$img_name[1]);
					$ext_name=str_replace($chars2,"",$img_name[1]);
				}
				else
				{
					$img_name = explode(chr(92).$id.chr(92), $image);
					$no_ext_name=str_replace($chars,"",$img_name[1]);
					$ext_name=str_replace($chars2,"",$img_name[1]);
				}
			/**/
?>
			<input type="hidden" id="ext_name_<?php echo $no_ext_name;?>" value='<?php echo $ext_name; ?>'name="ext_name"/>
			<input type="hidden" id="size_img_w<?php echo $no_ext_name;?>" value='512'name="ext_name"/>
			<input type="hidden" id="size_img_h<?php echo $no_ext_name;?>" value='512'name="ext_name"/>
			<input type="hidden" id="src_<?php echo $no_ext_name;?>" value='<?php echo base_url().'images/f_images/img_catalogo/productos/'.$id.'/'.$ext_name;?>'name="ext_name"/>
			<div class="box_field_row"> 
			<?php echo form_label($this->lang->line('common_img').": <b>".$no_ext_name."</b>", 'uploadfile_label',array('class'=>'label_img','id'=>'uploadfile_label_'.$no_ext_name)); ?>
				<div class="response" id="response_<?php echo $no_ext_name;?>" ></div>
					<ul class="image-list" id="image-list_<?php echo $no_ext_name;?>">
					<li id="first_li_<?php echo $no_ext_name;?>"><img id="content_thumbail_<?php echo $no_ext_name;?>" class="image-list" src="<?php echo base_url().'images/f_images/img_catalogo/productos/'.$id.'/'.$ext_name;?>"/></li>
					</ul>
				</div>
			<div class="box_field_row">
			<?php echo form_label($img_info[0]."px x ".$img_info[1]."px", 'uploadfile_label',array('class'=>'wide','id'=>'uploadfile_label_'.$no_ext_name)); ?>
				<div class='box_row'>
				 <?php echo form_upload(array(
					'multiple'=>'multiple',
					'accept'=>'gif|jpg|png|jpeg',
					'class'=>'form-control',
					'name'=>$no_ext_name.'[]',
					'id'=>$no_ext_name,
					'size'=>'18')
				 );?>
			</div>
			</div>
			<div class="box_field_row">	
			<div class="drop" id="drop_<?php echo $no_ext_name;?>"><?php echo $this->lang->line('common_drag');?></div>
			<pre id="out"></pre>
			</div>
		<?php 
			}
		} 
		}
echo form_button(array(
			'name' => 'button',
			'type' => 'submit',
			'id' => 'button',
			'content' => $this->lang->line('common_submit'),
			'class'=>'submit_button_pop'));
?>
</div>
</div>
<?php
echo form_close();
?>