<?php
echo form_open('',array('id'=>'item_form','class'=>'forms_module'));
?>
<div id="pagina_content_info" class="fieldset_form_popup">
<legend class="title_forms_popup"></legend>
<div id="cover_field_row_popup">
<span class="name_item_detail"><?php echo '<b>'.$item_info->name.'</b> '.$idioma;?></span>
<input type="hidden" id="id_input" value='<?php echo $item_info->item_id; ?>'name="id_input"/>
<input type="hidden" id="loading_txt" value='<?php echo $loading_txt; ?>'name="loading_txt"/>
<?php
echo form_hidden('name_lang',url_title($item_info->name, '_'));
/* echo form_hidden('current_idiom',"asd );*/
 foreach($lang as $clave =>$campo) { 
$vals_lang=explode('_', $clave);
$format_lang=$vals_lang[2];
$field =explode('.', $campo);
?>
<div class="box_field_row">
<?php echo form_label(ucwords($format_lang), 'name',array('class'=>'wide')); ?>
<div class='box_row'>
 <?php 	echo form_textarea(array(
		'name'=>$clave,
		'id'=>$clave,
		'class'=>"content",
		'value'=>$campo
		));
		?>
	</div>
</div>
<?php }
echo form_button(array(
			'name' => 'button',
			'type' => 'submit',
			'id' => 'button',
			'content' => $this->lang->line('common_submit'),
			'class'=>'submit_content_button'));
?>
</div>
</div>
<?php
echo form_close();
?>