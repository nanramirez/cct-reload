<form id="supplier_form" class="forms_module">
	<div id="supplier_basic_info" class="fieldset_form_popup">
		<legend class="title_forms_popup"><?php echo $title_form;
		?></legend>
		<input type="hidden" id="jsonerrors" value='<?php echo $json_form_errors; ?>'name="jsonerrors"/>
		<input type="hidden" id="json_fiscal_errors" value='<?php echo $json_fiscal_errors; ?>'name="json_fiscal_errors"/>
		<input type="hidden" id="id_input" value="<?php if($id!="menosuno") echo $id; ?>" name="id_input"/>
		<input type="hidden" id="loading_txt" value="<?php echo $loading_txt; ?>" name="loading_txt"/>
		<div id="cover_field_row_popup">
			<div class="box_field_row">
				<?php echo form_label($this->lang->line('common_company').':', 'company_name', array('class'=>'required')); ?>
				<div class='box_row'>
					<?php echo form_input(array(
					'name'=>'company',
					'id'=>'company',
					'value'=>$person_info->company,
					'title'=>$this->lang->line('common_company_title'))
					);?>
				</div>
			</div>
			<?php $this->load->view("cc_views/people/form_basic_info"); ?>
			<div class="box_field_row">
				<?php echo form_label($this->lang->line('common_accounts_bank').':', 'accounts_number',array('class'=>'required')); ?>
				<div class='box_row'>
					<?php echo form_textarea(array(
					'name'=>'accounts_number',
					'id'=>'accounts_number',
					'value'=>$person_info->accounts_number,
					'rows'=>'5',
					'cols'=>'17',
					'title'=>$this->lang->line('common_accounts_bank_title'))
					);?>
				</div>
			</div>
			<div class="box_field_row bfr_checkbox">
				<?php echo form_label($this->lang->line('common_taxeable').':', 'taxable'); ?>
				<?php echo form_checkbox(array(
				'name'        => 'taxable',
				'id'          => 'taxable',
				'class'       => 'checkbox_form',
				'value'       => 1,
				'checked'     => $taxable,
				'title'       =>$this->lang->line('common_taxeable_title'))
				);
				?>
			</div>
			<?php $this->load->view("cc_views/people/form_fiscal_info"); ?>
			<?php
			echo form_button(array(
			'name' => 'button',
			'type' => 'submit',
			'id' => 'button',
			'content' => $this->lang->line('common_submit'),
			'class'=>'submit_button_pop'));
			?>
		</div>
	</div>
</form>