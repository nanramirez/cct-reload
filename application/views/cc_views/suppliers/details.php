<form id="customers_form" class="forms_module">
	<div class="fieldset_form_popup_info">
		<legend class="title_forms_popup_info"><?php echo $this->lang->line("module_supplier_info"); ?></legend>
		<input type="hidden" id="id_input" value='<?php echo $person_info->person_id; ?>'name="id_input"/>	
		<div id="cover_field_row_popup_info_start">
		<div>
		<!--Inicio IMG-->
		<?php 
		if(is_array($images)){
			foreach($images as $image){
				$img_info=getimagesize($image);
				$chars = array('.jpg','.png','.JPG','.JPEG','.jpeg');
				$img_name = explode("/".$person_info->person_id."/", $image);
				if(isset($img_name[1])){
					$no_ext_name=str_replace($chars,"",$img_name[1]);
				}
				else{
					$img_name = explode(chr(92).$person_info->person_id.chr(92), $image);
					$no_ext_name=str_replace($chars,"",$img_name[1]);
				}
				if($no_ext_name=="person"){
				?>
				<div id="info_img_detail"> 
					<img id="main_img" class="image-list" src="<?php echo base_url().'images/cc_images/people/'.$person_info->person_id.'/'.$img_name[1];?>"/>
				</div>
				<input type="hidden" id="src_main" value='<?php echo base_url().'images/cc_images/people/'.$person_info->person_id.'/'.$img_name[1];?>'name="ext_name"/>
				<?php
				}
			}
		}?>
		<!--fin IMG-->
		</div>
			<div class="box_field_row_info" id="box_info_title">
				<?php echo form_label($person_info->first_name.' '.$person_info->last_name, 'first_name',array('class'=>'required','id'=>'first_name_label')); ?>
			</div>
		</div>
		<div id="cover_field_row_popup_info">
			<div id="info_detail">
				<div class="box_field_row_info ">
					<?php echo form_label('<b>'.$this->lang->line('common_company').':</b> '.$person_info->company, 'company') ?>
				</div>
				<?php $this->load->view("cc_views/people/basic_info_detail"); ?>
				<div class="box_field_row_info">
					<?php echo form_label('<b>'.$this->lang->line('common_account_bank').':</b> '.$person_info->accounts_number, 'account_number');?>
					<div id="drop"></div>
				</div>
			</div>
		</div>
		<div id="box_print">
			<a href="#"></a>
		</div>
	</div>
</form>