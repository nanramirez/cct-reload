<!DOCTYPE html>
<html lang="es">
  <head>
    <title><?php echo $this->lang->line("manual_".$controller_name."_title");?></title>    
    <meta charset="UTF-8">
    <meta name="title" content="<?php echo $this->lang->line("manual_".$controller_name."_title");?>">
    <meta name="description" content="<?php echo $this->lang->line("manual_".$controller_name."_description");?>">    
    <link href="<?php echo base_url();?>css/cc_css/.css" rel="stylesheet" type="text/css"/>    
  </head>
  <body>
    <header>
      <h1 title="<?php echo $this->lang->line("manual_".$controller_name."_title");?>"><?php echo $this->lang->line("manual_".$controller_name."_title");?></h1>
		<div id="main_img_help"><img src="<?php echo base_url();?>images/cc_images/.jpg"></div>
    </header>
    <nav>
    <?php ?>
    </nav>
	<p title="<?php echo $this->lang->line("manual_".$controller_name."_description");?>"><?php echo $this->lang->line("manual_".$controller_name."_description");?></p>
    <section>
		<article>
			<h2 title="<?php echo $this->lang->line("manual_".$controller_name."_subtitle");?>"><?php echo $this->lang->line("manual_".$controller_name."_subtitle");?></h2>
			<p title="<?php echo $this->lang->line("manual_".$controller_name."_description2");?>"><?php echo $this->lang->line("manual_".$controller_name."_description2");?></p>
			<div id="second_img_help" alt="" title="">
				<img src="<?php echo base_url();?>images/cc_images/">
			</div>
		</article>
    </section>
	<section>
		<article>
			<h2 title="<?php echo $this->lang->line("manual_".$controller_name."_subtitle2");?>"><?php echo $this->lang->line("manual_".$controller_name."_subtitle2");?></h2>
			<p title="<?php echo $this->lang->line("manual_".$controller_name."_descriptio3");?>"><?php echo $this->lang->line("manual_".$controller_name."_description3");?></p>
			<div id="second_img_help" alt="" title="">
				<img src="<?php echo base_url();?>images/cc_images/">
			</div>
		</article>
    </section>
	<section>
		<article>
			<h2 title="<?php echo $this->lang->line("manual_".$controller_name."_subtitle3");?>"><?php echo $this->lang->line("manual_".$controller_name."_subtitle3");?></h2>
			<p title="<?php echo $this->lang->line("manual_".$controller_name."_descriptio4");?>"><?php echo $this->lang->line("manual_".$controller_name."_description4");?></p>
			<div id="second_img_help" alt="" title="">
				<img src="<?php echo base_url();?>images/cc_images/">
			</div>
		</article>
    </section>
	<section>
		<article>
			<h2 title="<?php echo $this->lang->line("manual_".$controller_name."_subtitle4");?>"><?php echo $this->lang->line("manual_".$controller_name."_subtitle4");?></h2>
			<p title="<?php echo $this->lang->line("manual_".$controller_name."_description5");?>"><?php echo $this->lang->line("manual_".$controller_name."_description5");?></p>
			<div id="second_img_help" alt="<?php echo $this->lang->line("manual_".$controller_name."_description5");?>" title="<?php echo $this->lang->line("manual_".$controller_name."_description5");?>">
				<img src="<?php echo base_url();?>images/cc_images/">
			</div>
		</article>
    </section>
	<section>
		<article>
			<h2 title="<?php echo $this->lang->line("manual_".$controller_name."_subtitle5");?>"><?php echo $this->lang->line("manual_".$controller_name."_subtitle5");?></h2>
			<div id="second_img_help" alt="<?php echo $this->lang->line("manual_".$controller_name."_subtitle5");?>" title="<?php echo $this->lang->line("manual_".$controller_name."_subtitle5");?>">
				<img src="<?php echo base_url();?>images/cc_images/">
			</div>
			<ul>

                <!---
				<li title="<?php echo $this->lang->line("manual_".$controller_name."_TitleLista1")." ".$this->lang->line("manual_".$controller_name."_lista1");?>"><span><?php echo $this->lang->line("manual_".$controller_name."_TitleLista1");?>:</span><?php echo $this->lang->line("manual_".$controller_name."_lista1");?></li>
				<li title="<?php echo $this->lang->line("manual_".$controller_name."_TitleLista2")." ".$this->lang->line("manual_".$controller_name."_lista2");?>"><span><?php echo $this->lang->line("manual_".$controller_name."_TitleLista2");?>:</span><?php echo $this->lang->line("manual_".$controller_name."_lista2");?></li>
				<li title="<?php echo $this->lang->line("manual_".$controller_name."_TitleLista3")." ".$this->lang->line("manual_".$controller_name."_lista3");?>"><span><?php echo $this->lang->line("manual_".$controller_name."_TitleLista3");?>:</span><?php echo $this->lang->line("manual_".$controller_name."_lista3");?></li>
				<li title="<?php echo $this->lang->line("manual_".$controller_name."_TitleLista4")." ".$this->lang->line("manual_".$controller_name."_lista4");?>"><span><?php echo $this->lang->line("manual_".$controller_name."_TitleLista4");?>:</span><?php echo $this->lang->line("manual_".$controller_name."_lista4");?></li>
				<li title="<?php echo $this->lang->line("manual_".$controller_name."_TitleLista5")." ".$this->lang->line("manual_".$controller_name."_lista5");?>"><span><?php echo $this->lang->line("manual_".$controller_name."_TitleLista5");?>:</span><?php echo $this->lang->line("manual_".$controller_name."_lista5");?></li>
				<li title="<?php echo $this->lang->line("manual_".$controller_name."_TitleLista6")." ".$this->lang->line("manual_".$controller_name."_lista16");?>"><span><?php echo $this->lang->line("manual_".$controller_name."_TitleLista6");?>:</span><?php echo $this->lang->line("manual_".$controller_name."_lista6");?></li>
				<li title="<?php echo $this->lang->line("manual_".$controller_name."_TitleLista7")." ".$this->lang->line("manual_".$controller_name."_lista7");?>"><span><?php echo $this->lang->line("manual_".$controller_name."_TitleLista7");?>:</span><?php echo $this->lang->line("manual_".$controller_name."_lista7");?></li>--->
				<?php echo $contenido; ?>
			</ul>
		</article>
    </section>
    <footer>
    </footer>
  </body>  
</html>