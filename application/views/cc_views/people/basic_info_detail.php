<div class="box_field_row_info ">
<?php echo form_label('<b>'.$this->lang->line('common_email').': </b>  '.$person_info->email, 'email',array('class'=>'required','id'=>'email_label')); ?>
</div>
<div class="box_field_row_info ">
<?php echo form_label('<b>'.$this->lang->line('common_phone_number').': </b>  '.$person_info->phone_number, 'phone_number'); ?>
</div>
<div class="box_field_row_info ">
<?php echo form_label('<b>'.$this->lang->line('common_address_1').': </b>  '.$person_info->address_1, 'address_1'); ?>
</div>
<div class="box_field_row_info ">
<?php echo form_label('<b>'.$this->lang->line('common_city').': </b>  '.$person_info->city, 'city'); ?>
</div>
<div class="box_field_row_info ">
<?php echo form_label('<b>'.$this->lang->line('common_state').': </b>  '.$person_info->state, 'state'); ?>
</div>
<div class="box_field_row_info ">
<?php echo form_label('<b>'.$this->lang->line('common_zip').': </b>  '.$person_info->zip, 'zip'); ?>
</div>
<div class="box_field_row_info ">
<?php echo form_label('<b>'.$this->lang->line('common_country').': </b>  '.$person_info->country, 'country'); ?>
</div>
<div class="box_field_row_info ">
<?php echo form_label('<b>'.$this->lang->line('common_maps').': </b>  '.$person_info->address_2, 'address_2'); ?>
</div>