<?php $this->load->view("cc_views/partial/header"); ?>
<div id="content_area">
<div id="cover_interaction_bar">
<div id="interaction_bar_top">
<div id="title_interaction_bar" > <?php echo $this->lang->line('common_list_of').' '.$this->lang->line('module_'.$controller_name); ?>
</div>
<div id="cover_buttons_interaction_bar">
<?php echo anchor("$controller_name/view/","<div class='buttons_interaction_bar' > <span>".$this->lang->line('common_new_M')."</span></div>",array('class'=>'ajax-popup-link','title'=>$this->lang->line($controller_name.'_new')));
?>
</div>
</div>
<div id="interaction_bar_bottom">
<ul id="list_interaction_bar">
<li  class="li_look_interaction_bar" id="li_look_ib_one"  >
<?php  form_open("$controller_name/search",array('id'=>'search_form')); ?>
<div class="ui-widget">
<input type="text" name ="search" id="search"  placeholder="<?php echo $this->lang->line('common_suggest_search');?>"/>
</div>
</li>
<div id="cover_opt_one">
<li class="li_inte_bar" id="li_delete_interaction_bar">
<a id ="delete" > </a>
</li>
</div>
</ul>
</div>
</div>
<div class="cover_more_options_look">
<div id="titleTextImg">
<span id="title_opc_look"><?php echo $this->lang->line('common_more_search_options'); ?></span>
<a id="imageDivLink">
<img src="<?php echo base_url().'images/cc_images/menubar/plus.png';?>" id="f_options">
</a>
</div>
<div id="search_filter_section">
<div class="ind_opt">
<?php echo form_label($this->lang->line('common_company').' '.':', 'company');?>
<?php echo form_checkbox(array('name'=>'company','id'=>'company','value'=>'company','class'=>'sf')).' <span class="line-medium">| </span>';?>
</div>
<div class="ind_opt">
<?php echo form_label($this->lang->line('common_last_name').' '.':', 'last_name');?>
<?php echo form_checkbox(array('name'=>'last_name','id'=>'last_name','value'=>'last_name','class'=>'sf')).' <span class="line-medium">|</span> ';?>
</div>
<div class="ind_opt">
<?php echo form_label($this->lang->line('common_rfc').' '.':', 'rfc');?>
<?php echo form_checkbox(array('name'=>'rfc','id'=>'rfc','value'=>'rfc','class'=>'sf')).' <span class="line-medium">|</span> ';?>
</div>
<div class="ind_opt">
<?php echo form_label($this->lang->line('common_email').' '.':', 'email');?>
<?php echo form_checkbox(array('name'=>'email','id'=>'email','value'=>'email','class'=>'sf')).' <span class="line-medium">|</span> ';?>
</div>
<div class="ind_opt">
<?php echo form_label($this->lang->line('module_customer_id').' '.':', 'person_id');?>
<?php echo form_checkbox(array('name'=>'person_id','id'=>'person_id','value'=>'person_id','class'=>'sf')).' <span class="line-medium">|</span> ';?>
</div>
</div>
</div>
<div id="table_holder">
<?php echo $manage_table; ?>
</div>
<div class="box-pagination">
<?php echo $this->pagination->create_links();?>
</div>
</div>
<?php $this->load->view("cc_views/partial/footer"); ?>