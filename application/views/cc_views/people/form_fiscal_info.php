<div class="fiscales">
	<legend class="sub_title_forms_popup"><?php echo $this->lang->line("common_title_rfc"); ?></legend>
	<input type="hidden" id="fiscalerrors" value='<?php echo $json_fiscal_errors; ?>'name="fiscalerrors"/>
	<div class="fiscales bfr_checkbox">
		<?php echo form_label($this->lang->line('common_taxes_same_address').':', 'same_address'); ?>
		<?php echo form_checkbox(array(
			'name'=> 'same_address',
			'id'=> 'same_address',
			'class'=> 'fiscales',
			'value'=> '1',
			'checked'=> '',
			'title'=>$this->lang->line('common_taxes_same_address_title')));	?>
	</div>
	<div class="fiscales">
		<?php echo form_label($this->lang->line('common_rfc').':', 'rfc', array('class'=>'fiscales','id'=>'rfc_label')); ?>
		<div class='box_row'>
		<?php echo form_input(array(
			'name'=>'rfc',
			'id'=>'rfc',
			'class'=>'fiscales',
			'value'=>$fiscal_info->rfc,
			'title'=>$this->lang->line('common_rfc_title')));?>
		</div>
	</div>
	<div class="fiscales">
	<input type="hidden" id="field_razon_social" value='<?php echo $this->lang->line('common_razon_social'); ?>'name="field_razon_social"/>
	<?php echo form_label($this->lang->line('common_razon_social').':', 'razon_social', array('class'=>'fiscales','id'=>'razon_social_label')); ?>
		<div class='box_row'>
		<?php echo form_input(array(
			'name'=>'razon_social',
			'id'=>'razon_social',
			'class'=>'fiscales',
			'value'=>$fiscal_info->razon,
			'title'=>$this->lang->line('common_razon_social_title')
			)
		);?>
		</div>
	</div>
	<div class="fiscales">
	<input type="hidden" id="field_ce" value='<?php echo $this->lang->line('common_ce'); ?>'name="field_ce"/>
	<?php echo form_label($this->lang->line('common_ce').':', 'ce', array('class'=>'fiscales','id'=>'ce_label')); ?>
		<div class='box_row'>
		<?php echo form_input(array(
			'name'=>'ce',
			'id'=>'ce',
			'class'=>'fiscales',
			'value'=>$fiscal_info->ce,
			'title'=>$this->lang->line('common_ce_title')
			)
		);?>
		</div>
	</div>
	<div class="fiscales">
	<input type="hidden" id="field_no_exterior" value='<?php echo $this->lang->line('common_no_exterior'); ?>'name="field_no_exterior"/>
	<div class="field_row_half">
	<?php echo form_label($this->lang->line('common_no_exterior').':', 'no_exterior', array('class'=>'fiscales','id'=>'no_exterior_label')); ?>
		<div class='box_row'>
		<?php echo form_input(array(
			'name'=>'no_exterior',
			'id'=>'no_exterior',
			'class'=>'fiscales',
			'value'=>$fiscal_info->noExterior,
			'title'=>$this->lang->line('common_no_exterior_title')
			)
		);?>
	</div>
	</div>
	<div class="field_row_half">
	<?php echo form_label($this->lang->line('common_no_interior').':', 'no_interior', array('class'=>'fiscales','id'=>'no_interior_label')); ?>
		<div class='box_row'>
		<?php echo form_input(array(
			'name'=>'no_interior',
			'id'=>'no_interior',
			'class'=>'fiscales',
			'value'=>$fiscal_info->noInterior,
			'title'=>$this->lang->line('common_no_interior_title')
			)
		);?>
	</div>
	</div>
	</div>
	<div class="fiscales">
	<input type="hidden" id="field_colonia" value='<?php echo $this->lang->line('common_colonia'); ?>'name="field_colonia"/>
	<?php echo form_label($this->lang->line('common_colonia').':', 'colonia', array('class'=>'fiscales','id'=>'colonia_label')); ?>
		<div class='box_row'>
		<?php echo form_input(array(
			'name'=>'colonia',
			'id'=>'colonia',
			'class'=>'fiscales',
			'value'=>$fiscal_info->colonia,
			'title'=>$this->lang->line('common_colonia_title')
			)
		);?>
	</div>
	</div>
	<div class="fiscales">	
	<?php echo form_label($this->lang->line('common_localidad').':', 'localidad', array('class'=>'fiscales')); ?>
		<div class='box_row'>
		<?php echo form_input(array(
			'name'=>'localidad',
			'id'=>'localidad',
			'class'=>'fiscales',
			'value'=>$fiscal_info->localidad,
			'title'=>$this->lang->line('common_localidad_title')
			)
		);?>
	</div>
	</div>
	<div class="fiscales">
	<input type="hidden" id="field_municipio" value='<?php echo $this->lang->line('common_municipio'); ?>'name="field_municipio"/>
	<?php echo form_label($this->lang->line('common_municipio').':', 'municipio', array('class'=>'fiscales','id'=>'municipio_label')); ?>
		<div class='box_row'>
		<?php echo form_input(array(
			'name'=>'municipio',
			'id'=>'municipio',
			'class'=>'fiscales',
			'value'=>$fiscal_info->municipio,
			'title'=>$this->lang->line('common_municipio_title')
			)
		);?>
	</div>
	</div>
	<div class="fiscales">
	<input type="hidden" id="field_estado" value='<?php echo $this->lang->line('common_state'); ?>'name="field_estado"/>
	<?php echo form_label($this->lang->line('common_state').':', 'estado', array('class'=>'fiscales','id'=>'estado_label')); ?>
	<div class='box_row'>
		<?php echo form_input(array(
			'name'=>'estado',
			'id'=>'estado',
			'class'=>'fiscales',
			'value'=>$fiscal_info->estado,
			'title'=>$this->lang->line('common_state_title')
			)
		);?>
	</div>
	</div>
	<div class="fiscales">
	<input type="hidden" id="field_pais" value='<?php echo $this->lang->line('common_country'); ?>'name="field_pais"/>
	<?php echo form_label($this->lang->line('common_country').':', 'pais', array('class'=>'fiscales','id'=>'pais_label')); ?>
	<div class='box_row'>
		<?php echo form_input(array(
			'name'=>'pais',
			'id'=>'pais',
			'class'=>'fiscales',
			'value'=>$fiscal_info->pais,
			'title'=>$this->lang->line('common_country_title')
			)
		);?>
	</div>
	</div>
	<div class="fiscales">
	<?php echo form_label($this->lang->line('common_zip').':', 'cp', array('class'=>'fiscales','id'=>'cp_label')); ?>
	<div class='box_row'>
		<?php echo form_input(array(
			'name'=>'cp',
			'id'=>'cp',
			'class'=>'fiscales',
			'value'=>$fiscal_info->codigoPostal,
			'title'=>$this->lang->line('common_zip_title')
			)
		);?>
	</div>
	</div>
</div>