<form id="fiscal_form" class="forms_module">
	<div class="fieldset_form_popup_info">
		<legend class="title_forms_popup_info"><?php echo $this->lang->line("common_title_rfc"); ?>
		</legend>
		<input type="hidden" id="id_input" value='<?php echo $person_info->person_id; ?>'name="id_input"/>
		<div id="cover_field_row_popup_info_start">
			<div class="box_field_row_info" id="box_info_title">
				<?php echo form_label($person_info->razon, 'razon',array('class'=>'required','id'=>'razon_label')); ?>
			</div>
		</div>
		<div id="cover_field_row_popup_info">
			<div id="info_detail">
				<div class="box_field_row_info ">
					<?php echo form_label('<b>'.$this->lang->line('common_rfc').': </b>  '.$person_info->rfc, 'rfc',array('class'=>'required','id'=>'rfc_label')); ?>
				</div>
				<div class="box_field_row_info ">
					<?php echo form_label('<b>'.$this->lang->line('common_ce').': </b>  '.$person_info->ce, 'ce'); ?>
				</div>
				<div class="box_field_row_info ">
					<?php echo form_label('<b>'.$this->lang->line('common_no_exterior').': </b>  '.$person_info->noExterior, 'noExterior'); ?>
				</div>
				<div class="box_field_row_info ">
					<?php echo form_label('<b>'.$this->lang->line('common_no_interior').': </b>  '.$person_info->noInterior, 'noInterior'); ?>
				</div>
				<div class="box_field_row_info ">
					<?php echo form_label('<b>'.$this->lang->line('common_colonia').': </b>  '.$person_info->colonia, 'colonia'); ?>
				</div>
				<div class="box_field_row_info ">
					<?php echo form_label('<b>'.$this->lang->line('common_localidad').': </b>  '.$person_info->localidad, 'localidad'); ?>
				</div>
				<div class="box_field_row_info ">
					<?php echo form_label('<b>'.$this->lang->line('common_municipio').': </b>  '.$person_info->municipio, 'municipio'); ?>
				</div>
				<div class="box_field_row_info ">
					<?php echo form_label('<b>'.$this->lang->line('common_state').': </b>  '.$person_info->estado, 'estado'); ?>
				</div>
				<div class="box_field_row_info ">
					<?php echo form_label('<b>'.$this->lang->line('common_country').': </b>  '.$person_info->pais, 'pais'); ?>
				</div>
				<div class="box_field_row_info ">
					<?php echo form_label('<b>'.$this->lang->line('common_zip').': </b>  '.$person_info->codigoPostal, 'codigoPostal'); ?>
				</div>
			</div>
			<div id="aditional_info_detail">
			</div>
			<div id="box_print">
			<a href="#"></a>
			</div>
		</div>
		<div id="drop" style="display:none"></div>
	</div>
</form>