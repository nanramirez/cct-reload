<div class="box_field_row ">
	<?php echo form_label($this->lang->line('common_first_name').':', 'first_name',array('class'=>'required','id'=>'first_name_label')); ?>
	<div class='box_row'>
		<?php echo form_input(array(
		'name'=>'first_name',
		'id'=>'first_name',
		'class'=>'first_name',
		'value'=>$person_info->first_name,
		'title'=>$this->lang->line('common_first_name_title'))
		);?>
	</div>
</div>
<div class="box_field_row ">
	<?php echo form_label($this->lang->line('common_last_name').':', 'last_name',array('class'=>'required','id'=>'last_name_label')); ?>
	<div class='box_row'>
		<?php echo form_input(array(
		'name'=>'last_name',
		'id'=>'last_name',
		'class'=>'last_name',
		'value'=>$person_info->last_name,
		'title'=>$this->lang->line('common_last_name_title'))
		);?>
	</div>
</div>
<div class="box_field_row ">
	<?php echo form_label($this->lang->line('common_email').':', 'email',array('class'=>'required','id'=>'email_label')); ?>
	<div class='box_row'>
		<?php echo form_input(array(
		'name'=>'email',
		'id'=>'email',
		'class'=>'email',
		'value'=>$person_info->email,
		'title'=>$this->lang->line('common_email_title'))
		);?>
	</div>
</div>
<div class="box_field_row ">
	<?php echo form_label($this->lang->line('common_phone_number').':', 'phone_number'); ?>
	<div class='box_row'>
		<?php echo form_input(array(
		'name'=>'phone_number',
		'id'=>'phone_number',
		'value'=>$person_info->phone_number,
		'title'=>$this->lang->line('common_phone_number_title'))
		);?>
	</div>
</div>
<div class="box_field_row ">
	<?php echo form_label($this->lang->line('common_zip').':', 'zip'); ?>
	<div id="box_row_uploadfile" class='box_row'>
		<?php echo form_input(array(
		'name'=>'zip',
		'id'=>'zip',
		'value'=>$person_info->zip,
		'title'=>$this->lang->line('common_zip_title'))
		);?>
	</div>
</div>
<div class="box_field_row">
	<?php echo form_label($this->lang->line('common_address_2').':', 'address_2'); ?>
	<div class='box_row'>
		<?php echo form_textarea(array(
		'name'=>'address_2',
		'id'=>'address_2',
		'value'=>$person_info->address_2,
		'title'=>$this->lang->line('common_address_2'))
		);?>
	</div>
</div>
<div class="box_field_row ">
	<?php echo form_label($this->lang->line('common_colonia').':', 'address_1'); ?>
	<div class='box_row'>
		<?php echo form_textarea(array(
		'name'=>'address_1',
		'id'=>'address_1',
		'value'=>$person_info->address_1,
		'title'=>$this->lang->line('common_address_1_title'))
		);?>
	</div>
</div>
<div class="box_field_row ">
	<?php echo form_label($this->lang->line('common_city').':', 'city'); ?>
	<div class='box_row'>
		<?php echo form_input(array(
		'name'=>'city',
		'id'=>'city',
		'value'=>$person_info->city,
		'title'=>$this->lang->line('common_city_title'))
		);?>
	</div>
</div>
<div class="box_field_row ">
	<?php echo form_label($this->lang->line('common_state').':', 'state'); ?>
	<div class='box_row'>
		<?php echo form_input(array(
		'name'=>'state',
		'id'=>'state',
		'value'=>$person_info->state,
		'title'=>$this->lang->line('common_state_title'))
		);?>
	</div>
</div>
<div class="box_field_row ">
	<?php echo form_label($this->lang->line('common_country').':', 'country'); ?>
	<div class='box_row'>
		<?php echo form_input(array(
		'name'=>'country',
		'id'=>'country',
		'value'=>$person_info->country,
		'title'=>$this->lang->line('common_country_title'))
		);?>
	</div>
</div>
<div class="box_field_row ">
	<?php echo form_label($this->lang->line('common_maps').':', 'address_2'); ?>
	<div class='box_row'>
		<?php echo form_textarea(array(
		'name'=>'address_2',
		'id'=>'address_2',
		'value'=>$person_info->address_2,
		'title'=>$this->lang->line('common_maps_title'))
		);?>
	</div>
</div>
<div class="box_field_row ">
	<?php echo form_label($this->lang->line('common_extra_info').':', 'comments'); ?>
	<div class='box_row'>
		<?php echo form_textarea(array(
		'name'=>'comments',
		'id'=>'comments',
		'value'=>$person_info->comments,
		'rows'=>'5',
		'cols'=>'17',
		'title'=>$this->lang->line('common_extra_info_title'))
		);?>
	</div>
</div>
<?php 
if($images!=FALSE && $id!="menosuno"){
	foreach($images as $key=>$image){
		$img_info=getimagesize($image);
		$chars = array('.jpg','.png','.JPG','.JPEG','.jpeg','/',chr(92));
		$chars2 = array('/',chr(92));
		$img_name = explode('/'.$id.'/', $image);
		if(isset($img_name[1])){
			$no_ext_name=str_replace($chars,"",$img_name[1]);
			$ext_name=str_replace($chars2,"",$img_name[1]);
		}
		else{
			$img_name = explode(chr(92).$id.chr(92), $image);
			$no_ext_name=str_replace($chars,"",$img_name[1]);
			$ext_name=str_replace($chars2,"",$img_name[1]);
		}	?>
		<input type="hidden" id="ext_name_<?php echo $no_ext_name;?>" value='<?php echo $ext_name; ?>'name="ext_name"/>
		<input type="hidden" id="size_img_w<?php echo $no_ext_name;?>" value='512'name="ext_name"/>
		<input type="hidden" id="size_img_h<?php echo $no_ext_name;?>" value='512'name="ext_name"/>
		<input type="hidden" id="src_<?php echo $no_ext_name;?>" value='<?php echo base_url().'images/cc_images/people/'.$id.'/'.$ext_name;?>'name="ext_name"/>
		<div class="box_field_row"> 
			<?php echo form_label($this->lang->line('common_img').": <b>".$no_ext_name."</b>", 'uploadfile_label',array('class'=>'label_img','id'=>'uploadfile_label_'.$no_ext_name)); ?>
			<div class="response" id="response_<?php echo $no_ext_name;?>" ></div>
			<ul class="image-list" id="image-list_<?php echo $no_ext_name;?>">
				<li id="first_li_<?php echo $no_ext_name;?>"><img id="content_thumbail_<?php echo $no_ext_name;?>" class="image-list" src="<?php echo base_url().'images/cc_images/people/'.$id.'/'.$ext_name;?>"/></li>
			</ul>
		</div>
		<div class="box_field_row">
			<?php echo form_label($img_info[0]."px x ".$img_info[1]."px", 'uploadfile_label',array('class'=>'wide','id'=>'uploadfile_label_'.$no_ext_name)); ?>
			<div class='box_row'>
				<?php echo form_upload(array(
				'multiple'=>'multiple',
				'accept'=>'gif|jpg|png|jpeg',
				'class'=>'form-control',
				'name'=>$no_ext_name.'[]',
				'id'=>$no_ext_name,
				'size'=>'18')
				);?>
			</div>
		</div>
		<div class="box_field_row">
			<div class="drop" id="drop_<?php echo $no_ext_name;?>"><?php echo $this->lang->line('common_drag');?></div>
			<pre id="out"></pre>
		</div>
		<?php 
	} 
}
?>