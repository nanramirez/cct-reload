<?php $this->load->view("cc_views/partial/header"); ?>
<div id="content_area">
<div id="cover_interaction_bar">
	<div id="interaction_bar_top">
		<div id="title_interaction_bar" >
			<?php echo $this->lang->line('common_list_of').' '.$this->lang->line('module_'.$controller_name); ?>
		</div>
		<div id="title_interaction_bar" >
			<?php echo anchor($controller_name."/help/",
				"<img src='".base_url()."images/cc_images/icon_cct/manual.png'>",
				array('class'=>'ajax-popup-link','title'=>$this->lang->line("common_handbook")));
				?>
		</div>
		<div id="cover_buttons_interaction_bar">
			<?php echo anchor("$controller_name/view/",
			"<div class='buttons_interaction_bar'><span>".$this->lang->line('common_new_M')."</span></div>",
			array('class'=>'ajax-popup-link','title'=>$this->lang->line($controller_name.'_new')));
			?>
		</div>
	</div>
	<div id="interaction_bar_bottom">
		<ul id="list_interaction_bar">
			<li  class="li_look_interaction_bar" id="li_look_ib_one" >
			<div class="ui-widget">
			<input type="text" name ="search" id="search"  placeholder="<?php echo $this->lang->line('common_suggest_search');?>"/>
			</div>
			</li>
			<div id="cover_opt_one">
				<li class="li_inte_bar" id="li_delete_interaction_bar">
					<a id ="delete" > </a>
				</li>
			</div>
			<div id="cover_li_gastos">
			<li class="li_txt_gastos_interaction_bar" >
				<div class="txt_gastos_int_bar" id="txt_gastos_int_bar_undone">
					<b class="title_gastos_opt"><?php echo $this->lang->line('common_all_month_pending').": "; ?></b>
					<span  class="total_undone_coin"><?php echo '<b>'.to_currency($total_undone_mes_coin).'</b>&nbsp;'.$config_info["coin"]; ?></span>
					<span  class="total_undone_coin"><?php echo '<b>'.to_currency($total_undone_mes_coin2).'</b>&nbsp;'.$config_info["coin2"]; ?></span>
				</div>
				<div class="txt_gastos_int_bar" id="txt_gastos_int_bar_done">
					<b class="title_gastos_opt"><?php echo $this->lang->line('common_all_month_done').": "; ?></b>
					<span class="total_done_coin"><?php echo '<b>'.to_currency($total_done_mes_coin).'</b>&nbsp;'.$config_info["coin"]; ?></span>
					<span class="total_done_coin"><?php echo '<b>'.to_currency($total_done_mes_coin2).'</b>&nbsp;'.$config_info["coin2"]; ?></span>
				</div>
			</li>
			<li class="li_txt_gastos_interaction_bar" >
				<div class="txt_gastos_int_bar" id="txt_gastos_int_bar_undone">
					<b class="title_gastos_opt"><?php echo $this->lang->line('common_all_pending').": "; ?></b>
					<span  class="total_undone_coin"><?php echo '<b>'.to_currency($total_undone_coin).'</b>&nbsp;'.$config_info["coin"]; ?></span>
					<span  class="total_undone_coin"><?php echo '<b>'.to_currency($total_undone_coin2).'</b>&nbsp;'.$config_info["coin2"]; ?></span>
				</div>
				<div class="txt_gastos_int_bar" id="txt_gastos_int_bar_done">
					<b class="title_gastos_opt"><?php echo $this->lang->line('common_all_done').": "; ?></b>
					<span class="total_done_coin"><?php echo '<b>'.to_currency($total_done_coin).'</b>&nbsp;'.$config_info["coin"]; ?></span>
					<span class="total_done_coin"><?php echo '<b>'.to_currency($total_done_coin2).'</b>&nbsp;'.$config_info["coin2"]; ?></span>
				</div>
			</li> 
			</div>
		</ul>
	</div>
</div>
<div class="cover_more_options_look">
			<div id="titleTextImg">
				<span id="title_opc_look"><?php echo $this->lang->line('common_more_search_options'); ?></span>
				<a id="imageDivLink">
				<img src="
				<?php echo base_url().'images/cc_images/menubar/plus.png';?>" id="f_options"></a>
			</div>
				<div id="search_filter_section">
					<div class="ind_opt">
						<?php echo form_label($this->lang->line('common_concepto').' '.':', 'concepto');?>
						<?php echo form_checkbox(array('name'=>'concepto','id'=>'concepto','value'=>'concepto','class'=>'sf')).' <span class="line-medium">| </span>';?>
					</div>
					<div class="ind_opt">
						<?php echo form_label($this->lang->line('common_category').' '.':', 'category');?>
						<?php echo form_checkbox(array('name'=>'category','id'=>'category','value'=>'category','class'=>'sf')).' <span class="line-medium">| </span>';?>
					</div>
					<div class="ind_opt">
					<?php echo form_label($this->lang->line('common_date').' '.':', 'fecha');?>
							<?php echo form_checkbox(array('name'=>'fecha','id'=>'fecha','value'=>'fecha','class'=>'sf')).' <span class="line-medium">|</span> ';?>
					</div>
					<div class="ind_opt" id="ind_opt_standar_two">	
					<div id="li_select_standar_two_interaction_bar" >
					<?php 
						$jsm = array(
								'id'       => 'meses',
								'class'	   =>'filtro_changer'
						);
						echo form_dropdown('meses',$meses, $selected_month,$jsm); ?>
						<?php 
						$jse = array(
								'id'       => 'estado',
								'class'	   =>'filtro_changer'
						);
						echo form_dropdown('estado',$estado, $estado_selected,$jse);
						?>
					</div>
				</div>
			</div>
	</div>
<div id="table_holder">
<?php echo $manage_table; ?>
</div>
<div class="box-pagination">
<?php echo $this->pagination->create_links();?>
</div>
</div>
<?php $this->load->view("cc_views/partial/footer"); ?>