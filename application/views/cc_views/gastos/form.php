<form id="gastos_form" class="forms_module">
	<div id="gastos_basic_info" class="fieldset_form_popup">
		<legend class="title_forms_popup"><?php echo $title;?></legend>
		<input type="hidden" id="jsonerrors" value='<?php echo $json_form_errors; ?>'name="jsonerrors"/>
		<input type="hidden" id="gasto_recurrente" value='<?php echo $gasto_recurrente; ?>'name="gasto_recurrente"/>
		<input type="hidden" id="id_input" value='<?php echo $info->gasto_id; ?>'name="id_input"/>
		<div id="cover_field_row_popup">
			<div class="box_field_row">
			<?php echo form_label($this->lang->line('common_date').':', 'fecha',array('class'=>'required')); ?>
				<div id='report_date_range_complex'>
					<div class="box_row">
						<?php echo form_dropdown('month',$months, $selected_month,array('class'=>'required','id'=>'month')); ?>
					<?php echo form_dropdown('day',$days, $selected_day,array('class'=>'required','id'=>'day')); ?>
					<?php echo form_dropdown('year',$years, $selected_year,array('class'=>'required','id'=>'year') ); ?>
					<?php echo form_label($this->lang->line('common_hour').':', 'fecha',array('class'=>'required')); ?>
					<?php echo form_dropdown('hour',$hours, $selected_hour,array('class'=>'required','id'=>'hour')); ?>
					<?php echo form_dropdown('minute',$minutes, $selected_minute,array('class'=>'required','id'=>'minute')); ?>
					</div>
				</div>
			</div>
			<?php if($info->gasto_id==null){ ?>
			<div class="box_field_row bfr_checkbox">	
					<?php echo form_label($this->lang->line('module_gasto_fijo').':', 'suscrito'); ?>
					<div class='box_row'>
						<?php 
						echo form_dropdown('suscrito_box',$suscritos,$info->suscrito,array('class'=>'required','id'=>'suscrito_box'));
						?>
					</div>
					</div>
			<?php }elseif(isset($info->gasto_id)&&$info->suscrito=='0'){
			echo form_input(array(
					'name'=>'suscrito_box',
					'id'=>'suscrito_box',
					'type'=>'hidden',
					'value'=>0)
				);
			}elseif(isset($info->gasto_id)&&$info->suscrito!='0') { ?>
			 <div class="box_field_row  bfr_checkbox">
				<?php echo form_label($this->lang->line('module_gasto_fijo').':', 'gasto_fijo',array('class'=>'required')); ?>
					<?php echo form_checkbox(array(
						'name'=>'gasto_fijo',
						'class'=>'checkbox_form_n',
						'id'=>'gasto_fijo',
						'checked'=>'checked',
						'value'=>1));?>
				</div>
			<?php
			echo form_input(array(
					'name'=>'suscrito_box',
					'id'=>'suscrito_box',
					'type'=>'hidden',
					'value'=>$info->suscrito)
				);
			 }?>
			<div class="box_field_row">
			<?php echo form_label($this->lang->line('common_monto').':', 'cantidad',array('class'=>'required','id'=>'cantidad_label')); ?>
				<div class='box_row'>
			<?php
					echo form_input(array(
					'name'=>'cantidad',
					'id'=>'cantidad',
					'type'=>'number',
					'class'=>'required',
					'value'=>$info->cantidad));?>
				</div>
			</div>
			<div class="box_field_row">
			<?php echo form_label($this->lang->line('common_concepto').':', 'concepto',array('class'=>'required','id'=>'concepto_label')); ?>
			<div class='box_row'>
				<?php echo form_input(array(
					'name'=>'concepto',
					'id'=>'concepto',
					'class'=>'required',
					'value'=>$info->concepto)
				);?>
				</div>
			</div>
			<div class="box_field_row">
			<?php echo form_label($this->lang->line('common_category').':', 'category',array('class'=>'required','id'=>'category_label')); ?>
				<div class='box_row'>
				<div id="ui-widget-category">
						<?php echo form_input(array(
							'name'=>'category',
							'id'=>'category',
							'class'=>'required',
							'value'=>$info->category)
						);?>
						</div>
				</div>
			</div>
			<div class="box_field_row">
			<?php echo form_label($this->lang->line('module_supplier').':', 'supplier',array('class'=>'required','id'=>'supplier_label')); ?>
				<div class="ui-widget" id="ui-widget-suppliers">
				<div class='box_row'>
				<?php echo form_input(array(
					'name'=>'search_suppliers',
					'id'=>'search_suppliers',
					'class'=>'required',
					'value'=>'')
				);?>
				</div>
				</div>
			</div>
			<!--<div class="box_field_row">
			<?php //echo form_label($this->lang->line('module_recurso').':', 'recurso',array('class'=>'required')); ?>
				<div class='box_row'>
				<?php //echo form_dropdown('recurso',$recursos,'','id="recurso"');?>
				</div>
			</div>-->
			<div class="box_field_row">
			  <?php  echo form_label($this->lang->line('common_payments_methods').':', 'modo',array('class'=>'wide'));?>  
				<div class='box_row'>
				<?php echo form_dropdown('modo',$modo,'','id="modo",class="required"');?>
				</div>
			</div>
			<div class="box_field_row">
			  <?php  echo form_label($this->lang->line('common_state').':', 'estado',array('class'=>'wide','id'=>'estado_label'));?>  
				<div class='box_row'>
				<?php echo form_dropdown('estado',$estado,'','id="estado",class="required"');?>
				</div>
			</div>
			<div class="box_field_row  bfr_checkbox">
				<?php echo form_label($this->lang->line('common_deducible').':', 'deducible',array('class'=>'required')); ?>
					<?php echo form_checkbox(array(
						'name'=>'deducible',
						'class'=>'checkbox_form_n',
						'id'=>'deducible',
						'value'=>1));?>
				</div>
				<!--<div class="box_field_row">
				<?php form_label($this->lang->line('common_payments_ticket_upload').':', 'uploadfile',array('class'=>'required')); ?>
					<div id="box_row_uploadfile" class='box_row'>
					 <?php  form_upload(array(
						'multiple'=>'multiple',
						'accept'=>'gif|jpg|png',
						'class'=>'form-control',
						'name'=>'uploadfile[]',
						'id'=>'uploadfile',
						'size'=>'18')
					 );?>
					</div>
				</div>
				<div class="box_field_row">	
			<div id="drop"><?php  $this->lang->line('common_drag');?></div>
			<pre id="out"></pre>
			</div>
				<div id="box_field_row">	 
				<div id="response"></div>
					<ul id="image-list">
					<?php 
				if (isset($image_properties))
			{
			 img($image_properties);
			}
					?>
					</ul>
				</div>-->
			<div id="drop"></div>
			<?php
			echo form_input(array(
						'name' => 'button',
						'type' => 'button',
						'id' => 'button',
						'value' => $this->lang->line('common_submit'),
						'class'=>'submit_button_pop'));
			?>
		</div>
	</div>
</form>