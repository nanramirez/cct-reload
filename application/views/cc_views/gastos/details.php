<form id="form_gastos">
	<div  class="fieldset_form_popup_info">
		<legend class="title_forms_popup_info"><?php echo $this->lang->line("module_gasto_details"); ?></legend>
		<legend class="title_forms_popup"><?php echo $this->lang->line("module_gasto")." #".$gasto_id;?></legend>
		<input type="hidden" id="id_input" value='<?php echo $info_gasto->gasto_id; ?>'name="id_input"/>
		<div id="cover_field_row_popup_info">
			<div class="box_field_row_info ">
			<?php echo form_label("<b>".$this->lang->line("common_date").":</b> ".$info_gasto->fecha, 'cantidad', array('class'=>'items_category')); ?>
			</div>
			<div class="box_field_row_info">
				<?php echo form_label("<b>".$this->lang->line("common_state").":</b> ".$info_gasto->estado, 'concepto', array('class'=>'items_category')); ?>
			</div>
			<div class="box_field_row_info">
				<?php echo form_label("<b>".$this->lang->line("common_category").":</b> ".$info_gasto->category, 'concepto', array('class'=>'items_category')); ?>
			</div>
			<div class="box_field_row_info">
				<?php echo form_label("<b>".$this->lang->line("common_concepto").":</b> ".$info_gasto->concepto, 'concepto', array('class'=>'items_category')); ?>
			</div>
			<div class="box_field_row_info" id="box_info_title">
			<?php echo form_label("<b>".$this->lang->line("common_monto").":</b> ".to_currency($info_gasto->cantidad)." ".$info_gasto->coin, 'cantidad', array('class'=>'items_category')); ?>
			</div>
			<div class="box_field_row  bfr_checkbox">
				<?php echo form_label($this->lang->line('common_deducible').':', 'deducible',array('class'=>'required')); ?>
					<?php echo form_checkbox(array(
						'name'=>'deducible',
						'class'=>'checkbox_form_n',
						'id'=>'deducible',
						'checked'=>$info_gasto->deducible,
						'value'=>$info_gasto->deducible));?>
				</div>
			
			<div class="box_field_row">
				<?php echo form_label($this->lang->line('common_payments_ticket_upload').':', 'uploadfile',array('class'=>'required')); ?>
				<div id="box_row_uploadfile" class='box_row'>
					<?php echo form_upload(array(
					'multiple'=>'multiple',
					'accept'=>'gif|jpg|png',
					'class'=>'form-control',
					'name'=>'uploadfile[]',
					'id'=>'uploadfile',
					'size'=>'18'));?>
				</div>
			</div>
			<div class="box_field_row">	
				<div id="drop"><?php echo $this->lang->line('common_drag');?></div>
				<pre id="out"></pre>
			</div>
			<div id="box_field_row">	 
				<div id="response"></div>
				<div>
				<!--Inicio IMG-->
				<?php 
				if(is_array($images)){
					foreach($images as $image){
						$img_info=getimagesize($image);
						$chars = array('.jpg','.png','.JPG','.JPEG','.jpeg');
						$img_name = explode("/".$gasto_id."/", $image);
						if(isset($img_name[1])){
							$no_ext_name=str_replace($chars,"",$img_name[1]);
						}
						else{
							$img_name = explode(chr(92).$gasto_id.chr(92), $image);
							$no_ext_name=str_replace($chars,"",$img_name[1]);
						}
						if($no_ext_name=="main"){	?>
						<div id="detail_image_item"> 
							<img id="" class="" src="<?php echo base_url().'images/cc_images/gastos/'.$gasto_id.'/'.$img_name[1];?>"/></li>
						</div>
						<?php
						}
					}
				}?>
				<!--fin IMG-->
				</div>
			</div>
		<?php echo form_input(array(
			'name' => 'button',
			'type' => 'button',
			'id' => 'button_close',
			'value' => $this->lang->line('common_close'),
			'class'=>'submit_button_pop'));?>
		</div>
	</div>
</form>