<?php
echo form_open('recursos/save/'.$info->recurso_id,array('id'=>'recursos_form','class'=>'forms_module'));
?>
<div id="recursos_basic_info" class="fieldset_form_popup">
<legend class="title_forms_popup"><?php echo $title_form; ?></legend>
<input type="hidden" id="jsonerrors" value='<?php echo $json_form_errors; ?>' name="jsonerrors"/>
<input type="hidden" id="id_input" value='<?php echo $info->recurso_id; ?>'name="id_input"/>
<input type="hidden" id="hidde_coin" value='<?php echo $info->coin; ?>'name="hidde_coin"/>
<div id="cover_field_row_popup">
<div class="box_field_row">
<?php echo form_label($this->lang->line('module_recursos_nombre_cuenta').':', 'nombre_cuenta_label',array('class'=>'required', 'id'=>'nombre_cuenta_label')); ?>
	<div class='box_row'>
	<?php echo form_input(array(
		'name'=>'nombre_cuenta',
		'id'=>'nombre_cuenta',
		'value'=>$info->nombre)
	);?>
	</div>
</div>
<?php if($recurso_id==-1){ ?>
<div class="box_field_row">
<?php echo form_label($this->lang->line('common_monto').':', 'cantidad',array('class'=>'required','id'=>'cantidad_label')); ?>
	<div class='box_row'>
	<?php echo form_input(array(
		'name'=>'cantidad',
		'id'=>'cantidad',
		'value'=>$info->recurso)
	);?>
	</div>
</div>
<?php } else?>
<?php { ?>

<input type="hidden" id="cantidad" value='<?php echo $info->recurso; ?>'name="cantidad"/>
<?php } ?>
<div class="box_field_row">
<div class='field_row_half'>
<?php echo form_label($this->lang->line('common_coin').':', 'coin',array('class'=>' required','id'=>'coin_label')); ?>

<div class='box_row'>
<?php echo form_dropdown("coin",array(-1=>$this->lang->line("module_recursos_coin_msj")),-1,array('id'=>'coin'));?>
</div>
</div>
<div class='field_row_half'>
<?php echo form_label($payment_method.':', 'forma_pago',array('class'=>'required', 'id'=>'formas_pago_label')); ?>

	<div class='box_row'>
	<?php echo form_dropdown("formas_pago",$payment_methods,$select_method,array(
		'id'=>'formas_pago')
	);?>
	</div>
	</div>
</div>
<div id="table_recursos_pagos">
		<div id="cover_add_title_recursos_pagos">
			<div class="title_add_recursos_pagos" id="name_recursos_pagos">
			<span><?php echo $this->lang->line('common_payments_methods');?></span>
			</div>
			<div class="title_add_recursos_pagos">
			<span><?php echo $this->lang->line('common_delete');?></span>
			</div>
		</div>
		<div id="cover_add_result_recursos_pagos">
		<?php 
				foreach ($metodos as $metodo) {
				?>
			<div class="file_add_result_recursos_pagos" id="<?php if(isset($metodo['formas_pago_id']))echo 'result_name_recursos_pagos_'.$metodo['formas_pago_id']; ?>">
				<div class="detail_recursos_pagos" id="result_name_recursos_pagos">
				<span><?php if(isset($metodo['nombre'])) echo $metodo['nombre']; ?></span>
				</div>
				<div class="detail_recursos_pagos">
				<?php if(isset($metodo['formas_pago_id'])) { ?>
				<span><a class="delete" name="delete[]" id="<?php echo 'del_'.$metodo['formas_pago_id']; ?>">X</a></span>
				<?php	}?>
				</div>
			</div>
			<?php } ?>
		</div>
	</div>
<div id="drop"></div>
<?php
	echo form_input(array(
			'name' => 'button',
			'type' => 'button',
			'id' => 'button',
			'value' => $this->lang->line('common_submit'),
			'class'=>'submit_button_pop'));
?>
</div>
</div>
<?php
echo form_close();
?>