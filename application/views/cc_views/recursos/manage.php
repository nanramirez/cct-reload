<?php $this->load->view("cc_views/partial/header"); ?>
<script>
$(function() {
show_hidde_operations();
});
</script>
<div id="content_area">
<div id="cover_interaction_bar">
	<div id="interaction_bar_top">
		<div id="title_interaction_bar" >
			<?php echo $this->lang->line('common_list_of').' '.$this->lang->line('module_'.$controller_name); ?>
		</div>
		<div id="title_interaction_bar" >
			<?php echo anchor($controller_name."/help/",
				"<img src='".base_url()."images/cc_images/icon_cct/manual.png'>",
				array('class'=>'ajax-popup-link','title'=>$this->lang->line("common_handbook")));
				?>
		</div>
	<div id="cover_buttons_interaction_bar">
		<?php echo anchor("$controller_name/view/",
			"<div class='buttons_interaction_bar' > <span>".$this->lang->line('common_new_M')."</span></div>",
			array('class'=>'ajax-popup-link','title'=>$this->lang->line($controller_name.'_new')));
			?>
	</div>
</div>
	<div id="interaction_bar_bottom">
		<ul id="list_interaction_bar">
			<li  class="li_look_interaction_bar" id="li_look_ib_one" >
			<?php form_open("$controller_name/search",array('id'=>'search_form')); ?>
			<div class="ui-widget">
			<input type="text" name ="search" id="search"  placeholder="<?php echo $this->lang->line('common_suggest_search');?>"/>
			</div>
			</li>
			<div id="cover_opt_one">
				<li class="li_inte_bar" id="li_delete_interaction_bar">
					<a id ="delete" > </a>
				</li>
			</div>
	  </ul>
	</div>
</div>

	<div class="cover_more_options_look">
			<div id="titleTextImg">
				<span id="title_opc_look"><?php echo $this->lang->line('common_more_search_options'); ?></span>
				<a id="imageDivLink" href="javascript:show_hide_search_filter('search_filter_section', 'imageDivLink');">
				<img src="
				<?php echo isset($search_section_state)?  ( ($search_section_state)? base_url().'images/cc_images/menubar/minus.png' : base_url().'images/cc_images/menubar/plus.png') : base_url().'images/cc_images/menubar/plus.png';?>"></a>
			</div>
			<div id="search_filter_section">
				<!--
					<?php /*echo form_label($this->lang->line('module_item_modelo').' '.':', 'modelo');*/?>
					<?php /*echo form_checkbox(array('name'=>'modelo','id'=>'modelo','value'=>'modelo','class'=>'sf')).' <span class="line-medium">|</span> ';*/?>
					<?php /*echo form_label($this->lang->line('module_item').' '.':', 'name');*/?>
					<?php /*echo form_checkbox(array('name'=>'name','id'=>'name','value'=>'name','class'=>'sf')).' <span class="line-medium">|</span>';*/?>
					<?php /* echo form_label($this->lang->line('module_item_brand').' '.':', 'brand'); */?>
					<?php/* echo form_checkbox(array('name'=>'brand','id'=>'brand','value'=>'brand','class'=>'sf')).' <span class="line-medium">|</span>';*/?>
					<?php/* echo form_label($this->lang->line('common_category').' '.':', 'category'); */?>
					<?php/* echo form_checkbox(array('name'=>'category','id'=>'category','value'=>'category','class'=>'sf')).' <span class="line-medium">|</span> ';*/?>
					<?php/* echo form_label($this->lang->line('common_barcode').' '.':', 'item_number'); */?>
					<?php /*echo form_checkbox(array('name'=>'item_number','id'=>'item_number','value'=>'item_number','class'=>'sf')).' <span class="line-medium">|</span> '; */?>
					<?php/*echo form_label($this->lang->line('module_item_id').' '.':', 'item_id');*/?>
					<?php /*echo form_checkbox(array('name'=>'item_id','id'=>'item_id','value'=>'item_id','class'=>'sf')).' <span class="line-medium">|</span>'; */?>
				-->
		</div>
	</div>
<div id="table_holder">
<?php echo $manage_table; ?>
</div>
<div class="box-pagination">
<?php echo $this->pagination->create_links();?>
</div>
</div>
<?php $this->load->view("cc_views/partial/footer"); ?>