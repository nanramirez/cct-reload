<div class="forms_module">
	<fieldset class="fieldset_form_popup" >
		<input type="hidden" id="jsonerrors" value='<?php echo $json_form_errors; ?>'name="jsonerrors"/>
		<input type="hidden" id="id_input" value='<?php echo $recurso_info->recurso_id; ?>'name="id_input"/>
		<input type="hidden" id="recurso" value='<?php echo $recurso_info->recurso; ?>'name="recurso"/>
		<input type="hidden" id="recurso_name" value='<?php echo $recurso_info->nombre; ?>'name="recurso_name"/>
		<input type="hidden" id="coin" value='<?php echo $recurso_info->coin; ?>'name="coin"/>
		<input type="hidden" id="concept_transfer_to" value='<?php echo $concept_transfer_to; ?>'name="concept_transfer_to"/>
		<input type="hidden" id="concept_transfer_from" value='<?php echo $concept_transfer_from; ?>'name="concept_transfer_from"/>
		<legend class="title_forms_popup"><?php echo $this->lang->line('module_recursos_new_operation')."<br>".$recurso_info->nombre;
		?></legend>
		<div id="cover_field_row_popup_info">
		
		<div class="box_field_row" id="box_info_title_middle_b">
		<?php echo form_label(to_currency($recurso_info->recurso).' <b>'.$recurso_info->coin.'</b>', 'recurso_label',array('class'=>'required',"id"=>"recurso_label")); ?>
		</div>
		
		<div class="box_field_row">
		<div class='field_row_half'>
		<?php echo form_label($this->lang->line('module_recursos_operation_transfer').':', 'operation_transfer',array('class'=>' required','id'=>'operation_transfer_label')); ?>

		<div class='box_row'>
		<?php echo form_checkbox('transfer',"transfer",true,array('id'=>'transfer'));?>
		</div>
		</div>

		<div class='field_row_half'>
		<?php echo form_label($this->lang->line('module_recursos_operation_manual').':', 'forma_pago',array('class'=>'required', 'id'=>'operation_manual_label')); ?>

		<div class='box_row'>
		<?php echo form_checkbox('operation_manual',"operation_manual",false,array('id'=>'operation_manual'));?>
		</div>
		</div>
		</div>
		
		<div class="box_field_row" id="trans">
		<?php echo form_label($this->lang->line('module_recursos_nombre_cuenta').':', 'nombre_cuenta_label',array('class'=>'required','id'=>'nombre_cuenta_label')); ?>
			<div class='box_row'>
			<?php echo form_dropdown('recursos', $recursos, '-1',array('id'=>'recursos'));?>
			</div>
		</div>
		
		
		
		<div class="box_field_row" id="manual_op">
		<?php echo form_label($this->lang->line('common_concepto').':', 'concepto_label',array('class'=>'required','id'=>'concepto_label')); ?>
			<div class='box_row'>
			<?php echo form_textarea(array(
				'name'=>'concepto',
				'id'=>'concepto',
				'value'=>'')
			);?>
			</div>
		</div>
		<div class="box_field_row" id="xchange_rate_row">
		<?php echo form_label($this->lang->line('common_xchange_rate').':', 'xchange_rate_label',array('class'=>'required','id'=>'xchange_rate_label')); ?>
			<div class='box_row'>
			<?php echo form_input(array(
				'name'=>'xchange_rate',
				'id'=>'xchange_rate',
				'value'=>''
				)
			);?>
			</div>
		</div>
		<div class="box_field_row ">
		<?php echo form_label($this->lang->line('module_recursos_operation_monto').':', 'operation_monto_label',array('class'=>'required','id'=>'operation_monto_label')); ?>
			<div class='box_row'>
			<?php echo form_input(array(
				'name'=>'monto',
				'id'=>'monto',
				'value'=>''
				)
			);?>
			</div>
		</div>
		<?php
		echo form_button(array(
						'name' => 'button',
						'type' => 'submit',
						'id' => 'button',
						'content' => $this->lang->line('common_submit'),
						'class'=>'submit_button_pop'));
		?>
		</div>
		
	</fieldset>
</div>