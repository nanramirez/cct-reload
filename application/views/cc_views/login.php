<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Author" content="Adrián Ramirez | brontobytemx.com" />
<meta name="Subject" content="Apps Mexico" />
<meta name="GOOGLEBOT" content="INDEX, NO FOLLOW, ALL" />
<meta name="robots" content="index, no follow" />
<meta name="Generator" content="html" />
<meta name="Revisit" content="1 day" />
<meta name="Distribution" content="Global" />
<meta name="Robots" content="All" />
<meta property="og:url" content="<?php echo base_url();?>" />
<meta property="og:type" content="Login" />
<meta property="og:title" content="Inicio de Sesión Cash Control" />
<meta property="og:description" content="Escriba sus datos para accesar" />
<meta property="og:image" content="<?php echo base_url();?>images/cc_images/logo-cash/logo-cash-ctrol-login.png" />
<title>Cash Control Login |<?php echo $this->lang->line('common_login'); ?></title>
<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/cc_css/cash_style.css" />
<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/cc_css/bootstrap.css" />
<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/cc_css/cash_style_dispositivos.css" />
<link rel="shortcut icon" href="<?php echo base_url();?>images/cc_images/icon_cct/favicon_cact.ico">
<link href='https://fonts.googleapis.com/css?family=Roboto:400,500,300italic,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Raleway:400,300,500' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Cabin:400,600,500' rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300|Open+Sans:400,600" rel="stylesheet">
<link href='https://fonts.googleapis.com/css?family=Oxygen:400,300' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Cabin:400,600,500' rel='stylesheet' type='text/css'>
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
<meta name="theme-color" content="#003959" />
<script src="https://code.jquery.com/jquery.js"></script>
<script type="text/javascript">
$( document ).ready( readyFunction );
function readyFunction( jQuery ) 
{
    $("#username").focus();
    $("input[type='text']").on("click", function () 
    {
        $(this).select();
	});
	$("input[type='password']").on("click", function () 
	{
	    $(this).select();
	});
}
</script>
</head>
<body >
<div id="wrap_login">
<div id="cover_login">
<img id="logo_login" src="<?php echo base_url();?>images/cc_images/logo-cash/logo-cash-ctrol-login.png">
<?php echo form_open('login') ?>
<div id="container_login">

<?php if (validation_errors()!="")
{ 
?>
	<div id="box_welcome_login">
	<span id="welcome_login"><?php echo validation_errors(); ?></span>
	</div>
<?php 
} 
else 
{
?>
	<div id="box_error_login">
	<span id="error_login"></span>
	</div>
<?php 
}
?>

<div id="login_form">
<div class="box_login_label">
<label>
<?php echo $this->lang->line('common_user'); ?>:
</label>
</div>
<div class="box_login_input">
<?php echo form_input(
	array(
        'name'=>'username',
        'id'=>'username',
        'value'=>$_SERVER['HTTP_HOST'].'/demo/index.php/login' == 'brontobytemx.com/demo/index.php/login' ? 'demo' : '',
        'size'=>'20',
        'title'=>$this->lang->line('common_user')
        )
                    ); 
?>
</div>
<div class="box_login_label">
<label>
<?php  echo $this->lang->line('common_password'); ?>: </label>
</div>
<div class="box_login_input">
<?php echo form_password(
	array(
         'name'=>'password',
         'id'=>'password',
         'value'=>$_SERVER['HTTP_HOST'].'/demo/index.php/login' == 'brontobytemx.com/demo/index.php/login' ? 'secreto' : '',
         'size'=>'20',
         'title'=>$this->lang->line('common_password')
         )
                        ); 
?>
</div>
<div id="box_submit_login">
<?php echo form_submit('loginButton',$this->lang->line('common_login'),array('title'=>'inicia sesión')); ?>
</div>
</div>
</div>
<?php echo form_close(); ?>
</div>
</div>
</body>
</html>