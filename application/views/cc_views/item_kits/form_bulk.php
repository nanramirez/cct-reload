<?php
echo form_open_multipart('items/bulk_update/', array('id'=>'item_form','class'=>'forms_module'));
?>
<div id="item_basic_info" class="fieldset_form_popup">
<legend class="title_forms_popup"><?php echo $this->lang->line('module_item_kits_editon');
?></legend>
<input type="hidden" id="jsonerrors" value='<?php echo $json_form_errors; ?>'name="jsonerrors"/>	
<div id="cover_field_row_popup">
<?php if($config_info["user_fel"]!="")
	{ ?>
	<div class="box_field_row">
	<?php echo form_label($this->lang->line('module_item_ClaveProdServ').':', 'ClaveProdServ',array('class'=>'required','id'=>'ClaveProdServ_label')); ?>
		<div class='box_row'>
		<?php echo form_input(array(
			'name'=>'ClaveProdServ',
			'id'=>'ClaveProdServ',
			'value'=>'')
		);?>
		</div>
	</div>
	<div class="box_field_row">
	<?php echo form_label($this->lang->line('module_item_ClaveUnidad').':', 'ClaveUnidad',array('class'=>'required','id'=>'ClaveUnidad_label')); ?>
		<div class='box_row'>
		<?php echo form_input(array(
			'name'=>'ClaveUnidad',
			'id'=>'ClaveUnidad',
			'value'=>'')
		);?>
		</div>
	</div>
<?php } ?>
<div class="box_field_row">	
<?php echo form_label($this->lang->line('common_category').':', 'category',array('class'=>'required','id'=>'category_label')); ?>
	<div class='box_row'>
	<div id="ui-widget-category">
			<?php echo form_input(array(
				'name'=>'kit_category',
				'id'=>'category',
				'value'=>'')
			);?>
			</div>
	</div>
</div>
<div class="box_field_row">	
<?php echo form_label($this->lang->line('common_price').':', 'kit_price',array('class'=>'required','id'=>'kit_price_label')); ?>
	<div class='box_row'>
	<?php echo form_input(array(
		'name'=>'kit_price',
		'id'=>'kit_price',
		'val'=>'')
	);?>
	</div>
</div>
<div class="box_field_row">	
<?php echo form_label($this->lang->line('common_price_special').':', 'kit_price',array('class'=>'required','id'=>'kit_price_label')); ?>
<div class='box_row'>
<?php echo form_input(array(
'name'=>'other_price',
'id'=>'other_price',
'val'=>'')
);?>
</div>
</div>
<div class="box_field_row">
<?php echo form_label($this->lang->line('common_description').':', 'description',array('class'=>'required')); ?>
<div class='box_row'>
<?php echo form_textarea(array(
'name'=>'description',
'id'=>'description',
'value'=>'',
'rows'=>'5',
'cols'=>'17')
);?>
</div>
</div>
<div class='box_field_row bfr_checkbox'>
<?php echo form_label($this->lang->line('common_publish').':', 'publish',array('class'=>'required')); ?>
<div class='box_row'>
<?php echo form_dropdown('publicar', $publish_choices, '',array('id'=>'publicar'));?>
</div>
</div>
<div class='box_field_row bfr_checkbox'>
<?php echo form_label($this->lang->line('module_item_show_price').':', 'show_price',array('class'=>'required')); ?>
<div class='box_row'>
<?php echo form_dropdown('show_price', $show_price_choices, '',array('id'=>'show_price'));?>
</div>
</div>
<div class='box_field_row bfr_checkbox'>
<?php echo form_label($this->lang->line('module_item_destacado').':', 'destacar',array('class'=>'required')); ?>
<div class='box_row'>
<?php echo form_dropdown('destacar', $destacar_choices, '',array('id'=>'destacar'));?>
</div>
</div>
<div class='box_field_row bfr_checkbox'>
<?php echo form_label($this->lang->line('common_promo').':', 'promocionar',array('class'=>'required')); ?>
<div class='box_row'>
<?php echo form_dropdown('promocionar', $promocionar_choices, '',array('id'=>'promocionar'));?>
</div>
</div>
<div class='box_field_row bfr_checkbox'>
<?php echo form_label($this->lang->line('module_item_kits_as_service').':', 'service',array('class'=>'required')); ?>
<div class='box_row'>
<?php echo form_dropdown('service', $service_choices, '',array('id'=>'service'));?>
</div>
</div>
<div class="box_field_row"> 
<div class="quantity">
	<?php echo form_label($this->lang->line('module_item_qtyimg').':', 'img_qty',array('class'=>'required')); ?>
		<?php echo form_input(array(
			'type'=>'number',
			'name'=>'img_qty',
			'id'=>'img_qty',
			'value'=>'',
			'min'=>0,
			'max'=>99,
			'step'=>1
			)
		);?>
		</div>
		</div>
<?php
echo form_button(array(
			'name' => 'button',
			'type' => 'submit',
			'id' => 'button',
			'content' => $this->lang->line('common_submit'),
			'class'=>'submit_button_pop_2'));
?>
</div>
</div>
<?php 
echo form_close();
?>