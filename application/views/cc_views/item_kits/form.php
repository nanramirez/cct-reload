<?php
echo form_open_multipart('item_kits/save/'.$item_kit_info->item_kit_id,array('id'=>'item_kit_form','class'=>'forms_module'));
?>
<div id="item_kits_basic_info" class="fieldset_form_popup">
<legend class="title_forms_popup"><?php echo $title_form;
?></legend>
<input type="hidden" id="jsonerrors" value='<?php echo $json_form_errors; ?>'name="jsonerrors"/>
<input type="hidden" id="id_input" value="<?php echo $item_kit_info->item_kit_id; ?>" name="id_input"/>
<input type="hidden" id="id_input_kits" value="<?php echo url_title($item_kit_info->name,'underscore'); ?>" name="id_input_kits"/>
<input type="hidden" id="base_url" value='<?php echo base_url()?>'name="base_url"/>
<input type="hidden" id="loading_txt" value="<?php echo $loading_txt; ?>" name="loading_txt"/>
<div id="cover_field_row_popup">
		<div class="box_field_row">
		<?php echo form_label($this->lang->line('module_item_kit').':', 'name',array('class'=>'required','id'=>'kit_name_label')); ?>
			<div class='box_row'>
			<?php echo form_input(array(
				'name'=>'kit_name',
				'id'=>'kit_name',
				'value'=>$item_kit_info->name)
			);?>
			</div>
		</div>
		<div class="box_field_row">
		<?php echo form_label($this->lang->line('common_category').':', 'name',array('class'=>'required','id'=>'category_label')); ?>
			<div class='box_row'>
			<div id="ui-widget-category">
			<?php echo form_input(array(
				'name'=>'kit_category',
				'id'=>'category',
				'value'=>$item_kit_info->category)
			);?>
			</div>
			</div>
		</div>
		<div class="box_field_row">
		  <?php  echo form_label($this->lang->line('common_price').':', 'kit_price',array('class'=>'required','id'=>'kit_price_label'));?>  
			<div class='box_row'>
			<?php echo form_input(array(
				'name'=>'kit_price',
				'id'=>'kit_price',
				'value'=>$item_kit_info->kit_price)
				);?>
			</div>
		</div>
		<div class="box_field_row">
		  <?php  echo form_label($this->lang->line('common_price_special').':', 'other_price',array('class'=>'required','id'=>'kit_price_label'));?>  
			<div class='box_row'>
			<?php echo form_input(array(
				'name'=>'other_price',
				'id'=>'other_price',
				'value'=>$item_kit_info->other_price)
				);?>
			</div>
		</div>
		<div class="box_field_row">
		  <?php  echo form_label($this->lang->line('common_barcode').':', 'kit_number',array('class'=>'required'));?>  
			<div class='box_row'>
			<?php echo form_input(array(
				'name'=>'kit_number',
				'id'=>'kit_number',
				'value'=>$item_kit_info->kit_number)
				);?>
			</div>
		</div>
		<div class="box_field_row">
		<?php echo form_label($this->lang->line('common_description').':', 'description',array('class'=>'required')); ?>
			<div class='box_row'>
			<?php echo form_textarea(array(
				'name'=>'description',
				'id'=>'description',
				'value'=>$item_kit_info->description,
				'rows'=>'5',
				'cols'=>'17')
			);?>
			</div>
		</div>
		<?php if($config_info["user_cfdi"]!="")
	{ ?>
	<div class="box_field_row">
	<?php echo form_label($this->lang->line('module_item_ClaveProdServ').':', 'ClaveProdServ',array('class'=>'required','id'=>'ClaveProdServ_label')); ?>
		<div class='box_row'>
		<?php echo form_input(array(
			'name'=>'ClaveProdServ',
			'id'=>'ClaveProdServ',
			'value'=>$item_kit_cfdi_info->c_ClaveProdServ)
		);?>
		</div>
	</div>
	<div class="box_field_row">
	<?php echo form_label($this->lang->line('module_item_ClaveUnidad').':', 'ClaveUnidad',array('class'=>'required','id'=>'ClaveUnidad_label')); ?>
		<div class='box_row'>
		<?php echo form_input(array(
			'name'=>'ClaveUnidad',
			'id'=>'ClaveUnidad',
			'value'=>$item_kit_cfdi_info->c_ClaveUnidad)
		);?>
		</div>
	</div>
<?php } ?>
		<div class='box_field_row bfr_checkbox'>
		<?php echo form_label($this->lang->line('common_publish').':', 'publish',array('class'=>'required')); ?>
			<?php echo form_checkbox(array(
				'name'=>'publish',
				'id'=>'publish',
				'class'=> 'checkbox_form_n',
				'value'=>1,
				'checked'=>($item_kit_info->publicar)? 1 : 0)
			);?>
			<!--<div class='box_row'id="hidde">
			<?php //echo form_label($this->lang->line('common_contenido').':', 'language',array('class'=>'required')); ?>
				<?php //echo form_dropdown('language', array('0'=>$this->lang->line('common_select_language')),'');?>
			</div>-->
		</div>
		<div class='box_field_row bfr_checkbox'>
	<?php echo form_label($this->lang->line('module_item_show_price').':', 'show_price',array('class'=>'required')); ?>
		<?php 
		echo form_checkbox(array(
			'name'=>'show_price',
			'id'=>'show_price',
			'class'=> 'checkbox_form_n',
			'value'=>1,
			'checked'=>$item_kit_info->show_price)
		);?>
	</div>
		<div class='box_field_row bfr_checkbox'>
		<?php echo form_label($this->lang->line('module_item_destacado').':', 'destacar',array('class'=>'required')); ?>
		<?php echo form_checkbox(array(
		'name'=>'destacar',
		'id'=>'destacar',
		'class'=> 'checkbox_form_n',
		'value'=>1,
		'checked'=>($item_kit_info->destacar)? 1 : 0)
		);?>
		<!--<div class='box_row'id="hidde">
		<?php //echo form_label($this->lang->line('common_contenido').':', 'language',array('class'=>'required')); ?>
		<?php //echo form_dropdown('language', array('0'=>$this->lang->line('common_select_language')),'');?>
		</div>-->
		</div>
		<div class="box_field_row  bfr_checkbox">
	<?php echo form_label($this->lang->line('common_promo').':', 'promocionar',array('class'=>'required')); ?>
		<?php echo form_checkbox(array(
			'name'=>'promocionar',
			'class'=> 'checkbox_form_n',
			'id'=>'promocionar',
			'value'=>1,
			'checked'=>$item_kit_info->promocionar)
		);?>
	</div>
		<div class='box_field_row bfr_checkbox'>
			<?php echo form_label($this->lang->line('module_item_kits_as_service').':', 'as_service',array('class'=>'required')); ?>
			<?php echo form_checkbox(array(
				'name'=>'service',
				'id'=>'service',
				'class'=> 'checkbox_form_n',
				'value'=>1,
				'checked'=>$item_kit_info->servicio)
			);?>
		</div>
		<div class="box_field_row">
			<div class="quantity">
	<?php echo form_label($this->lang->line('module_item_qtyimg').':', 'img_qty',array('class'=>'required')); ?>
		<?php echo form_input(array(
			'type'=>'number',
			'name'=>'img_qty',
			'id'=>'img_qty',
			'value'=>$item_kit_info->img_qty==null ? 0:$item_kit_info->img_qty,
			'min'=>0,
			'max'=>99,
			'step'=>1
			)
		);?>
		</div>
		</div>
		<?php 
		if($images!=FALSE && $id!="menosuno")
		{
		foreach($images as $key=>$image)
		{
			if($key<=$item_kit_info->img_qty)
			{
				$img_info=getimagesize($image);
				$chars = array('.jpg','.png','.JPG','.JPEG','.jpeg','/',chr(92));
				$chars2 = array('/',chr(92));
				$img_name = explode('/'.$id.'/', $image);
				if(isset($img_name[1]))
				{
					$no_ext_name=str_replace($chars,"",$img_name[1]);
					$ext_name=str_replace($chars2,"",$img_name[1]);
					
				}
				else
				{
					$img_name = explode(chr(92).$id.chr(92), $image);
					$no_ext_name=str_replace($chars,"",$img_name[1]);
					$ext_name=str_replace($chars2,"",$img_name[1]);
				}
			//
?>
			<input type="hidden" id="ext_name_<?php echo $no_ext_name;?>" value='<?php echo $ext_name; ?>'name="ext_name"/>
			<input type="hidden" id="size_img_w<?php echo $no_ext_name;?>" value='512'name="ext_name"/>
			<input type="hidden" id="size_img_h<?php echo $no_ext_name;?>" value='512'name="ext_name"/>
			<input type="hidden" id="src_<?php echo $no_ext_name;?>" value='<?php echo base_url().'images/f_images/img_catalogo/productos/'.$id.'/'.$ext_name;?>'name="ext_name"/>
			<div class="box_field_row">
			<?php echo form_label($this->lang->line('common_img').": <b>".$no_ext_name."</b>", 'uploadfile_label',array('class'=>'label_img','id'=>'uploadfile_label_'.$no_ext_name)); ?>
				<div class="response" id="response_<?php echo $no_ext_name;?>" ></div>
					<ul class="image-list" id="image-list_<?php echo $no_ext_name;?>">
					<li id="first_li_<?php echo $no_ext_name;?>"><img id="content_thumbail_<?php echo $no_ext_name;?>" class="image-list" src="<?php echo base_url().'images/f_images/img-catalago/kits/'.$id.'/'.$ext_name;?>"/></li>
					</ul>
				</div>
			<div class="box_field_row">
			<?php echo form_label($img_info[0]."px x ".$img_info[1]."px", 'uploadfile_label',array('class'=>'wide','id'=>'uploadfile_label_'.$no_ext_name)); ?>
				<div class='box_row'>
				 <?php echo form_upload(array(
					'multiple'=>'multiple',
					'accept'=>'gif|jpg|png|jpeg',
					'class'=>'form-control',
					'name'=>$no_ext_name.'[]',
					'id'=>$no_ext_name,
					'size'=>'18')
				 );?>
			</div>
			</div>
			<div class="box_field_row">	
			<div class="drop" id="drop_<?php echo $no_ext_name;?>"><?php echo $this->lang->line('common_drag');?></div>
			<pre id="out"></pre>
			</div>
		<?php 
			}
		} 
		} ?>
		<legend class="sub_title_forms_popup"><?php echo $this->lang->line("module_item_kits_items_add"); ?>
		</legend>
		<div class="box_field_row">
		  <?php  echo form_label($this->lang->line('module_items_search').':', 'items_search',array('class'=>'required'));?>  
			<div id="look_input_pop" class='box_row'>
			<div id="ui-widget-items">
			<?php echo form_input(array(
				'name'=>'search_item',
				'id'=>'search_item')
				);?>
			</div>
			</div>
		</div>
	<div id="table_add_kit">
		<div id="cover_add_title_kit">
			<div class="title_add_kit" id="name_kit">
			<span><?php echo $this->lang->line('module_item');?></span>
			</div>
			<div class="title_add_kit">
			<span><?php echo $this->lang->line('common_unidades');?></span>
			</div>
			<div class="title_add_kit">
			<span><?php echo $this->lang->line('common_delete');?></span>
			</div>
		</div>
		<div id="cover_add_result_kit">
		<?php 
				foreach ($this->Item_kit_items->get_info($item_kit_info->item_kit_id) as $item_kit_item) {
				$info_item=$this->Item->get_info($item_kit_item['item_id']);
				?>
			<div class="file_add_result_kit" id="<?php echo 'result_name_kit_'.$item_kit_item['item_id']; ?>">
				<div class="detail_kit" id="result_name_kit">
				<span><?php echo $info_item->name; ?></span>
				</div>
				<div class="detail_kit">
				<span><input class="quantity" id="<?php echo $item_kit_item['item_id']; ?>" type='text' size='3' name="item_kit_item[<?php echo $item_kit_item['item_id']; ?>]" value='<?php echo $item_kit_item['quantity']; ?>'/></span>
				</div>
				<div class="detail_kit">
				<span><a class="delete" name="delete[]" id="<?php echo 'del_'.$item_kit_item['item_id']; ?>">X</a></span>
				</div>
			</div>
			<?php } ?>
		</div>
	</div>
	<?php
	echo form_button(array(
				'name' => 'button',
				'type' => 'submit',
				'id' => 'button',
				'content' => $this->lang->line('common_submit'),
				'class'=>'submit_button_pop'));
	?>
	</div>
</div>
<?php
echo form_close();
?>