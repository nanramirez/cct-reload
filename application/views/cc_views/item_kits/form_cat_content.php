<?php
echo form_open('',array('id'=>'item_form','class'=>'forms_module'));
?>
<div id="pagina_content_info" class="fieldset_form_popup">
<legend class="title_forms_popup"></legend>
<div id="cover_field_row_popup">
<span class="name_item_detail"><?php echo '<b>'.$item_kit_info->category.'</b> '.$idioma;?></span>
<input type="hidden" id="id_input" value='<?php echo $item_kit_info->item_kit_id; ?>'name="id_input"/>
<input type="hidden" id="loading_txt" value='<?php echo $loading_txt; ?>'name="loading_txt"/>

<?php
echo form_hidden('name_lang',url_title($item_kit_info->name, '_'));
// echo form_hidden('current_idiom',"asd );

 foreach($lang as $clave =>$campo) { 
 
$vals_lang=explode('_', $clave);
$format_lang=$vals_lang[1];
$field =explode('.', $campo);

?>
<div class="box_field_row">
<?php echo form_label($this->lang->line('common_category')." ".ucwords($format_lang), 'name',array('class'=>'wide')); ?>
<div class='box_row'>
 <?php 	echo form_textarea(array(
		'name'=>$clave,
		'id'=>$clave,
		'class'=>"content",
		'value'=>$campo
		));
		?>
	</div>
</div>

<?php } 
		

		if($images!=FALSE )
		{
		foreach($images as $key=>$image)
		{
			if($key<=$item_kit_info->img_qty)
			{
				$img_info=getimagesize($image);
				$chars = array('.jpg','.png','.JPG','.JPEG','.jpeg','/',chr(92));
				$chars2 = array('/',chr(92));
				$img_name = explode('/'.strtoupper(url_title($item_kit_info->category,'_')).'/', $image);
				if(isset($img_name[1]))
				{
					
					$no_ext_name=str_replace($chars,"",$img_name[1]);
					$ext_name=str_replace($chars2,"",$img_name[1]);
					
				}
				else
				{
					$img_name = explode(chr(92).strtoupper(url_title($item_kit_info->category,'_')).chr(92), $image);
					$no_ext_name=str_replace($chars,"",$img_name[1]);
					$ext_name=str_replace($chars2,"",$img_name[1]);
				}
			
			
?>
<input type="hidden" id="ext_name_<?php echo $no_ext_name;?>" value='<?php echo $ext_name; ?>'name="ext_name"/>
			<div class="box_field_row"> 
			<?php echo form_label($this->lang->line('common_img')." ".$this->lang->line('common_category').": <b>".$no_ext_name."</b>", 'uploadfile_label',array('class'=>'label_img','id'=>'uploadfile_label_'.$no_ext_name)); ?>
				<div class="response" id="response_<?php echo $no_ext_name;?>" ></div>
					<ul class="image-list" id="image-list_<?php echo $no_ext_name;?>">
					<li id="first_li_<?php echo $no_ext_name;?>"><img id="content_thumbail_<?php echo $no_ext_name;?>" class="image-list" src="<?php echo base_url().'images/f_images/img-catalago/'.strtoupper(url_title($item_kit_info->category,'_')).'/'.$ext_name;?>"/></li>
					</ul>
				</div>
			<div class="box_field_row">
			<?php echo form_label($img_info[0]."px x ".$img_info[1]."px", 'uploadfile_label',array('class'=>'wide','id'=>'uploadfile_label_'.$no_ext_name)); ?>
				<div class='box_row'>
				 <?php echo form_upload(array(
					'multiple'=>'multiple',
					'accept'=>'gif|jpg|png|jpeg',
					'class'=>'form-control',
					'name'=>$no_ext_name.'[]',
					'id'=>$no_ext_name,
					'size'=>'18')
				 );?>
				 
				
			</div>
			</div>
			<div class="box_field_row">	
			<div class="drop" id="drop_<?php echo $no_ext_name;?>"><?php echo $this->lang->line('common_drag');?></div>
			<pre id="out"></pre>
			</div>

		<?php 
			}
		} 
		}

echo form_button(array(
			'name' => 'button',
			'type' => 'submit',
			'id' => 'button',
			'content' => $this->lang->line('common_submit'),
			'class'=>'submit_cat_button'));
?>
</div>
</div>
<?php
echo form_close();
?>
