<?php $this->load->view("cc_views/partial/header"); ?>
<div id="content_area">
<div id="cover_interaction_bar">
	<div id="interaction_bar_top">
		<div id="title_interaction_bar" >
			<?php echo $this->lang->line('common_list_of').' '.$this->lang->line('module_'.$controller_name); ?>
		</div>
		<div id="title_interaction_bar" >
		<?php echo anchor($controller_name."/help/",
				"<img src='".base_url()."images/cc_images/icon_cct/manual.png'>",
				array('class'=>'ajax-popup-link','title'=>$this->lang->line("common_handbook")));
				?>
		</div>
		<div id="cover_buttons_interaction_bar">
				<?php echo anchor("$controller_name/view/",
			"<div class='buttons_interaction_bar' > <span>".$this->lang->line('common_new_M')."</span></div>",
			array('class'=>'ajax-popup-link','title'=>$this->lang->line($controller_name.'_new')));
			?>
		</div>
	</div>
	<div id="interaction_bar_bottom">
		<ul id="list_interaction_bar">
			<li  class="li_look_interaction_bar"  id="li_look_ib_three" >
				<?php  form_open("$controller_name/search",array('id'=>'search_form')); ?>
			<div class="ui-widget">
				<input type="text" name ="search" id="search" placeholder="<?php echo $this->lang->line('common_suggest_search');?>"/>
			</div>
			</li>
			<div id="cover_opt_three">
			<li  class="li_inte_bar" id="li_delete_interaction_bar" >
			<a id ="delete" ></a>
			</li>
			<li  class="li_inte_bar"  id="li_edit_interaction_bar" >
			<?php echo anchor("$controller_name/bulk_edit/",
			'<span></span>',
			array('id'=>'bulk_edit','title'=>$this->lang->line('module_items_editon')));?>
			</li>
			<li  id="li_barcode_interaction_bar" >
			<?php echo anchor("$controller_name/barcodes/",
			'<span></span>',
			array('id'=>'barcodes','title'=>$this->lang->line('common_barcode')));?>
			</li>
			</div>
		</ul>
	</div>
</div>
<div class="cover_more_options_look">
			<div id="titleTextImg">
				<span id="title_opc_look"><?php echo $this->lang->line('common_more_search_options'); ?></span>
			<a id="imageDivLink">
				<img src="
				<?php echo base_url().'images/cc_images/menubar/plus.png';?>" id="f_options"></a>
			</div>
			<div id="search_filter_section">
				<div class="ind_opt">
					<?php echo form_label($this->lang->line('module_item_kit').' '.':', 'name');?>
					<?php echo form_checkbox(array('name'=>'name','id'=>'name','value'=>'name','class'=>'sf')).' <span class="line-medium">|</span> ';?>
				</div>
				<div class="ind_opt">
					<?php echo form_label($this->lang->line('common_category').' '.':', 'category');?>
					<?php echo form_checkbox(array('name'=>'category','id'=>'category','value'=>'category','class'=>'sf')).' <span class="line-medium">|</span> ';?>
				</div>
				<div class="ind_opt">
					<?php echo form_label($this->lang->line('module_item_kit_id').' '.':', 'item_kit_id');?>
					<?php echo form_checkbox(array('name'=>'item_kit_id','id'=>'item_kit_id','value'=>'item_kit_id','class'=>'sf')).' <span class="line-medium">|</span> ';?>
				</div>
				<div class="ind_opt" id="ind_opt_item_kits">	
					<div id="li_select_lang_item_kits_interaction_bar" >
						<?php 
						$jse = array(
								'id'=>'idioma',
								'class'=>'idioma_changer'
						);
						echo form_dropdown('idioma',$idiomas, $idioma,$jse);
						?>
					</div>
					<input type="hidden" id="meses" value='<?php  array(); ?>'name="meses"/>
					<input type="hidden" id="estado" value='<?php  array(); ?>'name="estado"/>
				</div>
			</div>
	</div>
<div id="table_holder">
<?php echo $manage_table; ?>
</div>
<div class="box-pagination">
<?php echo $this->pagination->create_links();?>
</div>
</div>
<?php $this->load->view("cc_views/partial/footer"); ?>