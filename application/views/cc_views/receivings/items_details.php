<?php
echo form_open('pedidos/save/',array('id'=>'form_details'));
?>
<div id="" class="fieldset_form_popup_info">
<legend class="title_forms_popup">Detalles</legend>
<div id="cover_field_row_popup_info_start">
<div id="info_img_detail">
<img  src="<?php echo base_url();?>images/f_images/logo/logotipo.png"/>
</div>	
<div class="box_field_row_info" id="box_info_title">	
<span><b><?php echo $this->lang->line('module_supplier').'</b> '.$company;?></span>
</div>
</div>
<div id="cover_field_row_popup_info">
<div id="info_detail">
<div class="box_field_row_info">	<span><?php  echo $supplier ;?></span></div>
<div class="box_field_row_info">	<span><b><?php echo $this->lang->line("common_address").':</b> '.$supplier_info->address_1.' '.$supplier_info->city.' '.$supplier_info->country; ?></span></div>
<div class="box_field_row_info">	<span><?php echo "<b>Tel: </b>".$supplier_info->phone_number;?></span></div>
<div class="box_field_row_info">	<span><?php echo "<b>Email: </b>".$supplier_info->email;?></span></div>
</div>
<div id="info_detail_2">
<div class="cover_detail_ticket cdtright ">
<div class="box_field_row_info">
<span><b><?php echo $facturada;?></b></span>
</div>
<div class="box_field_row_info">
<span>
<b><?php echo $this->lang->line('module_employee').':</b> '.$employee; ?>
</span>
</div>
</div>
<div class="cover_detail_ticket">
<div class="box_field_row_info">
<span>
<?php echo '<b>'.$this->lang->line('common_date').':</b> '.$info->receiving_time; ?>
</span>
</div>
<div class="box_field_row_info">
<span><b><?php echo $this->lang->line('common_folio').":</b> ".$receiving_id;
?></span>
</div>
</div>
</div>
<div id="cover_field_row_popup_ticket">
<input type="hidden" id="id_input" value="<?php echo $receiving_id; ?>" name="id_input"/>
<div id="table_detail_ticket">
<div id="cover_title_detail_ticket">
<div id="cabecera_1" class="title_detail_ticket"><h3><?php echo $this->lang->line('common_quantity');?></h3></div>
<div id="cabecera_2" class="title_detail_ticket" ><h3><?php echo $this->lang->line('module_item');?></h3></div>
<div id="cabecera_3" class="title_detail_ticket" ><h3><?php echo $this->lang->line('common_cost_unity');?></h3></div>
<div id="cabecera_4" class="title_detail_ticket" ><h3><?php echo $this->lang->line('common_subtotal');?></h3></div>
</div>
<div id="cover_detail_ticket">
<?php 
foreach ($items as $item) { 
$item_info = $this->Item->get_info($item['item_id']);
?>
<div class="file_detail_ticket">
<div id="columna_1" class="detail_ticket">
	<div class="soloVisibleResponsivamente_ticket">
	<span><?php echo $this->lang->line('common_quantity');?></span>
	</div>
	<div class="content_tablet_ticket">
	<span class="span_r_d"><?php echo $item['quantity_purchased']; ?></span>
	</div>
</div>

<div id="columna_2" class="detail_ticket">
	<div class="soloVisibleResponsivamente_ticket">
	<span><?php echo $this->lang->line('module_items');?></span>
	</div>
	<div class="content_tablet_ticket">
	<span><?php echo $item_info->name; ?></span>
	</div>
	
</div>
<div id="columna_3" class="detail_ticket" >
	<div class="soloVisibleResponsivamente_ticket">
	<span ><?php echo $this->lang->line('common_price');?></span>
	</div>
	<div class="content_tablet_ticket">
	<span><?php echo to_currency($item['item_cost_price']); ?></span>
	</div>
	
</div>
<div id="columna_4" class="detail_ticket" >
	<div class="soloVisibleResponsivamente_ticket">
	<span ><?php echo $this->lang->line('common_subtotal');?></span>
	</div>
	<div class="content_tablet_ticket">
	<span><?php echo to_currency($item['item_cost_price']*$item['quantity_purchased']); ?></span>
	</div>
	
</div>
</div>
<?php } ?>
</div>
<div class="cover_total_detail_item_ticket">
<?php if($info->facturada==1)
{ ?>
<div class="subcover_tdit">
	<div class="num_tdit">
<?php
form_label($info->payment_type.'<br>', 'payment_type', array('class'=>'items_category'));
?>
	</div>
</div>
<div class="subcover_tdit">

<div  class="subtitle_tdit" id=""><?php echo $this->lang->line("common_subtotal");?>:</div>
<div class="num_tdit">
<?php
echo form_label(to_currency($total-$info->tax).' '.$info->coin.'<br>', 'subtotal_ticket', array('id'=>'subtotal_ticket'));
?>
</div>
</div>
<div class="subcover_tdit">
<div  class="subtitle_tdit" id=""><?php echo $config_info["default_tax_1_name"];?>:</div>	
<div class="num_tdit"><?php
echo form_label(to_currency($info->tax).' '.$info->coin.'<br>', 'tax_ticket', array('id'=>'tax_ticket'));?>

</div>	
</div> 	
<?php }?>
<div class="subcover_tdit">

<div  class="subtitle_tdit" id="txt_total"><?php echo $this->lang->line("common_total");?>:</div>
<div  class="num_tdit" id="txt_total_num">
<?php
echo form_label(to_currency($total).' '.$info->coin, 'total_ticket', array('id'=>'total_ticket'));?>
</div>
</div>
</div>

</div>
<div id="txt_total_letras"></div>
<input type="hidden" id="total_hidde" value="<?php echo $total; ?>" name="total_hidde"/>
<input type="hidden" id="coin" value="<?php echo $info->coin; ?>" name="coin"/>
<?php
echo form_input(array(
'name' => 'button',
'type' => 'button',
'id' => 'print_btn',
'value' => $this->lang->line('common_print')));
?>
</div>
</div>
</div><!--fieldset end-->
<div class="pdf_pedido"><a id="pdf" href="<?php echo base_url();?>formats/pedido.pdf"></a>
</div>

<?php
echo form_close();
?>