<?php $this->load->view("cc_views/partial/header"); ?>
<div id="content_area">
<div id="cover_interaction_bar">
	<div id="interaction_bar_top">
	<div id="title_interaction_bar" >
		<?php echo $this->lang->line('common_list_of').' '.$this->lang->line('module_'.$controller_name.'_titles'); ?>
	</div>
	<div id="title_interaction_bar" >
		<?php echo anchor($controller_name."/help/",
				"<img src='".base_url()."images/cc_images/icon_cct/manual.png'>",
				array('class'=>'ajax-popup-link','title'=>$this->lang->line("common_handbook")));
				?>
	</div>
	<div id="cover_buttons_interaction_bar">
			<?php echo anchor("$controller_name/register/",
			"<div class='buttons_interaction_bar'><span>".$this->lang->line('common_new_M')."</span></div>",
			array('class'=>'ajax-popup-receivings','title'=>$this->lang->line($controller_name.'_new')));
			?>
		</div>
	</div>
	<div id="interaction_bar_bottom">
		<ul id="list_interaction_bar">
			<li  class="li_look_interaction_bar" id="li_look_ib_one" >
			<?php echo form_open("$controller_name/search",array('id'=>'search_form')); ?>
				<input type="text" name ='search' id='search' placeholder="Escribe aquí tu busqueda"/><?php echo form_close();?>
			</li>
			<div id="cover_opt_one">
				<li class="li_inte_bar" id="li_delete_interaction_bar">
				<a id ="delete"> </a>
				</li>
			</div>		
		</ul>
	</div>
</div>
<div class="cover_more_options_look">
			<div id="titleTextImg">
				<span id="title_opc_look"><?php echo $this->lang->line('common_more_search_options'); ?></span>
				<a id="imageDivLink">
				<img src="
				<?php echo base_url().'images/cc_images/menubar/plus.png';?>" id="f_options"></a>
			</div>
	<div id="search_filter_section">
					<div class="ind_opt">
							<?php echo form_label($this->lang->line('module_customer').' '.':', 'customer');?>
						<?php echo form_checkbox(array('name'=>'customer','id'=>'customer','value'=>'customer','class'=>'sf')).' <span class="line-medium">| </span>';?>
					</div>
					<div class="ind_opt">
						<?php echo form_label($this->lang->line('common_folio').' '.':', 'folio');?>
						<?php echo form_checkbox(array('name'=>'folio','id'=>'folio','value'=>'folio','class'=>'sf')).' <span class="line-medium">| </span>';?>
					</div>
					<div class="ind_opt">
					<?php echo form_label($this->lang->line('common_date').' '.':', 'fecha');?>
							<?php echo form_checkbox(array('name'=>'fecha','id'=>'fecha','value'=>'fecha','class'=>'sf')).' <span class="line-medium">|</span> ';?>
					</div>
					<div class="ind_opt" id="ind_opt_standar_two">	
					<div id="li_select_standar_two_interaction_bar" >
						<?php 
						$jsm = array(
								'id'       => 'meses',
								'class'	   =>'filtro_changer'
						);
						echo form_dropdown('meses',$meses, $selected_month,$jsm); ?>
						<?php 
						$jse = array(
								'id'       => 'estado',
								'class'	   =>'filtro_changer'
						);
						echo form_dropdown('estado',$estado, $estado_selected,$jse);
						?>
					</div>
				</div>
			</div>
	</div>
<div id="table_holder">
<?php echo $manage_table; ?>
</div>
<div class="box-pagination">
<?php echo $this->pagination->create_links();?>
</div>
<div id="feedback_bar"></div>
</div>
<?php $this->load->view("cc_views/partial/footer"); ?>
</div>