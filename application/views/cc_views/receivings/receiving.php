<form>
	<div id="register_panel" class="fieldset_register_popup">
		<legend class="title_forms_popup title_forms_register_popup"><?php echo $this->lang->line('module_receivings_register'); ?></legend>
		<legend id="title_print"><?php echo $config_info['company']; ?></legend>
		<span id="address_print"> <?php echo $config_info['address']; ?> </span>
		<span id="tel_print">Tel:<?php echo $config_info['phone']; ?> </span>
		<input type="hidden" id="id_input" value="<?php echo $info_receiving->receiving_id; ?>" name="id_input"/> 
		<div id="cover_register_popup">
			<legend class="box_first_register_cajero"><span id="name_cajero"><?php echo $this->lang->line('module_employee').": ".$user_info->first_name." ".$user_info->last_name." | "."<b>".selected_hour().":".selected_minute()." hrs ".selected_day()."/".selected_month()."/".selected_year()."</b>"; ?></span></legend>
			<legend class="box_first_register">
			<?php
				if(isset($supplier)){	
					echo $this->lang->line('module_supplier').": "; ?>
					<div id="ui-widget-suppliers">
						<?php echo form_input(array('name'=>'search_suppliers','disabled'=>'disabled','id'=>'search_suppliers','size'=>'30','value'=>$supplier));?>
					</div>
				<?php
				}
				else{  
					echo $this->lang->line('module_supplier').": "; ?>
					<div id="ui-widget-suppliers">
						<?php echo form_input(array('name'=>'search_suppliers','id'=>'search_suppliers','size'=>'30'));?>
					</div>
				<?php }
				if(isset($supplier)){	
					echo '<span id="supplier_print">'.$supplier.'</span>';
				}?>
			</legend>
			<div id="cover_full_complete_register">
				<div id="cover_select_item_proccess">
					<div id="cover_register_middle" class="">
						<div id="ui-widget-items">
							<?php echo form_input(array('name'=>'search_item','id'=>'search_item','disabled'=>'disabled','class'=>'disabled'));?>
						</div>
					</div>
					<div id="cover_table_register_psr">
						<div class="cover_titles_register_psr">
							<?php
							$headers = array (
							$this->lang->line("common_quantity"),
							$this->lang->line("module_item"),
							$this->lang->line("common_price"),
							$this->lang->line("common_total"),
							$this->lang->line("common_quit")
							);
							foreach($headers as $header){ ?>
							<div class="titles_items_reg_rec" id="panel_header_<?php echo $header; ?>">
									<?php echo $header; ?>
							</div>
							<?php }?>
						</div>
						<?php echo $table_receiving; ?>
					</div>
				</div>
			</div>
			<div id="cover_complete_pay">
				<div id="options">
					<div id="box_checkbox_notify_facturar">
						<?php
						echo form_label($this->lang->line('common_deducible').':', 'facturar',array('class'=>'label_checkbox_form_n'));
						echo form_checkbox(array(
						'name'=>'facturar',
						'class'=>'checkbox_form_n',
						'id'=>'facturar',
						'value'=>1,
						'checked'=>$facturar
						)); ?>
					</div>
					<label class='label_field_sale'><?php echo $this->lang->line('common_modes_register'); ?></label>
					<div class='selects_opt'><?php 	echo form_dropdown('modos',$modos, '','id="modos" class ="box-inputs-rep"');?>	
						<div id="box_payment_procces">
							<?php 	echo form_dropdown('payment_options',$payment_options, '','id="payment_options" class ="box-inputs-rep"').form_input(array('name'=>'pay_recived','id'=>'pay_recived','disabled'=>'disabled','size'=>'30','value'=>"",'placeholder'=>$this->lang->line("common_amount_tendered")));?>		
						</div>
						<label class='label_field_sale'><?php echo '<a id="comment_display">'.$this->lang->line('common_comments').'</a>'; ?></label>
						<div id="comment_containner" style="display:none;">
							<?php echo form_textarea(array(
							'name'=>'comment',
							'id'=>'comment',
							'value'=>$comment,
							'rows'=>'5',
							'cols'=>'17'));?>
						</div>		
					</div>
				</div>
				<div class='box_final_procces_pay' for="item">
					<div id="recived_quantity" style="display:none;">
						<span class="span_text_quant_payment"><?php echo $this->lang->line('common_amount_tendered').':';?></span><output id="amount_tendered"></output>
					</div>
					<div id="cover_dashed_total">
						<div id="total">
							<span  class="span_text_payment"><?php echo $this->lang->line('common_total');?></span>
							<span class="by_pay_total"><?php echo to_currency($total); ?></span>
							<input type="hidden" id="hidde_total" value="<?php echo $total; ?>" name="hidde_total"/> 
						</div>
						<div id="change" style="display:none;" >
							<span class="span_text_payment"><?php echo $this->lang->line('common_amount_change');?></span><output id="amount_change"></output>
						</div>
						<div id="restante" style="display:none;" >
							<span  class="span_text_payment"><?php echo $this->lang->line('common_by_payment');?></span><output id="by_payment"></output>
						</div>
					</div>
				</div>
				<div class='cover_btn_register'>
					<a id='btn_register_receivings' title='<?php echo $this->lang->line('common_complete'); ?>'>
						<div id='btn_pause_register_sale'  class='btn_register_top'>
							<span><?php echo $this->lang->line('common_complete'); ?></span>
						</div>
					</a>
					<a id="cancel_button">
						<div id='btn_cancel_register'  class='btn_register_top'>
							<span><?php echo $this->lang->line('common_cancel')?></span>
						</div>
					</a>
				</div>
				<!--<div id="cover_register_top">
					<label class='label_field_sale'><?php echo $this->lang->line('common_modes_register'); ?> | <a id="comment_display"><?php echo $this->lang->line('common_add').' '.$this->lang->line('common_comments'); ?></a></label>
					<div id="comment_containner" style="display:none;">
					<?php echo form_textarea(array(
					'name'=>'comment',
					'id'=>'comment',
					'value'=>$comment,
					'rows'=>'5',
					'cols'=>'17')
					);?>
					</div>
					<div class='selects_opt'>
						<?php 	echo form_dropdown('modos',$modos, '','id="modos" class ="box-inputs-rep"').form_dropdown('recursos',$recursos, '','id="recursos" class ="box-inputs-rep"').form_dropdown('payment_options',$payment_options, '','id="payment_options" class ="box-inputs-rep"');?>
					</div>
					<div class='cover_btn_register_top'>
						<div id='btn_pause_register_top' class='btn_register_top'>
							<a id='btn_register_receivings'title='<?php echo $this->lang->line('common_done');?>'><span><?php echo $this->lang->line('common_done'); ?></span></a>
						</div>
					</div>
					<!--<a id="cancel_button">
					<div id='btn_cancel_register_top'  class='btn_register_top'><span><?php echo $this->lang->line('common_cancel')?></span></div></a>
					</div>
					</div>
					</div>
					<div id="cover_register_middle" class="">
					<label class='label_field_sale' for="item">
					<?php
					if($mode=='receive'){
						echo $this->lang->line('sales_find_or_scan_item');
					}
					else{
						echo $this->lang->line('sales_find_or_scan_item_or_receipt');
					}
					?>
					<!--<div id="mas"></div>
					<div class='box_total_procces'>
					<div id="total"><?php echo $this->lang->line('common_total').': '.to_currency($total);?>
					<input type="hidden" id="hidde_total" value="<?php echo $total; ?>" name="hidde_total"/> 
					</div>
					</div>
					</label>
				</div>-->
			</div>
		</div>
	</div>
</form>