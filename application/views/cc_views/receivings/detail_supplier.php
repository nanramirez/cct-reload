<?php
echo form_open('',array('id'=>'form_receiving'));
?>
<div  class="fieldset_form_popup_info">
<legend class="title_forms_popup_info"><?php echo $this->lang->line("module_supplier_info"); ?></legend>

<div id="cover_field_row_popup_info_start">
<div class="box_field_row_info" id="box_info_title">
	<?php echo form_label($supplier, 'supplier', array('class'=>'items_category')); ?>
</div>
</div>
<div id="cover_field_row_popup_info">

<div class="box_field_row_info ">	<?php echo form_label('<b>'.$this->lang->line('common_address').'</b> '.$dir, 'dir', array('class'=>'items_category')); ?></div>
<div class="box_field_row_info ">	<?php echo form_label('<b>'.$this->lang->line('common_city').': </b>  '.$city, 'city', array('class'=>'items_category')); ?></div>
<div class="box_field_row_info ">	<?php echo form_label('<b>'.$this->lang->line('common_zip').': </b> '.$zip, 'zip', array('class'=>'items_category')); ?></div>
<div class="box_field_row_info ">	<?php echo form_label('<b>'.$this->lang->line('common_country').':</b> '.$country, 'country', array('class'=>'items_category')); ?></div>
<div class="box_field_row_info ">	<?php echo form_label('<b>'.$this->lang->line('common_email').':</b> '.$email, 'email', array('class'=>'items_category')); ?></div>
<div class="box_field_row_info ">	<?php echo form_label('<b>'.$this->lang->line('common_phone_number').':</b> '.$tel, 'tel', array('class'=>'items_category')); ?></div>
<!--<div class="box_field_row_info ">	<?php //echo form_label('<b>'.$this->lang->line('common_total').':</b> '.$total, 'total', array('class'=>'items_category')); ?></div>-->
<div id="drop" style="display:none"></div>
</div>
</div><!--fieldset end-->

<?php
echo form_close();
?>

