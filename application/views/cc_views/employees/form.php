<form class="forms_module" id="employee_form">
	<div id="employee_basic_info" class="fieldset_form_popup">
		<legend class="title_forms_popup"><?php echo $title_form;?></legend>
		<input type="hidden" id="jsonerrors" value='<?php echo $json_form_errors; ?>'name="jsonerrors"/>
		<input type="hidden" id="id_input" value="<?php if($id!="menosuno") echo $id; ?>" name="id_input"/>
		<input type="hidden" id="loading_txt" value="<?php echo $loading_txt; ?>" name="loading_txt"/>
		<div id="cover_field_row_popup">
			<?php $this->load->view("cc_views/people/form_basic_info"); ?>
			<legend class="sub_title_forms_popup"><?php echo $this->lang->line("common_users_info"); ?></legend>
			<div class="box_field_row">
				<?php echo form_label($this->lang->line('common_user').':', 'username',array('class'=>'required','id'=>'username_label')); ?>
				<div class='box_row'>
					<?php echo form_input(array(
					'name'=>'username',
					'id'=>'username',
					'value'=>$person_info->username,
					'title'=>$this->lang->line('common_user_title')
					));?>
				</div>
			</div>
			<div class="box_field_row">	
				<?php echo form_label($this->lang->line('common_password').':', 'password', array('class'=>'required','id'=>'password_label')); ?>
				<div class='box_row'>
					<?php echo form_password(array(
					'name'=>'password',
					'id'=>'password',
					'value'=>'',
					'title'=>$this->lang->line('common_password_title')
					));?>
				</div>
			</div>
			<div class="box_field_row">
				<?php echo form_label($this->lang->line('common_confirm_password').':', 'repeat_password',array('class'=>'required','id'=>'repeat_password_label')); ?>
				<div class='box_row'>
					<?php echo form_password(array(
					'name'=>'repeat_password',
					'id'=>'repeat_password',
					'value'=>'',
					'title'=>$this->lang->line('common_confirm_password_title')
					));?>
				</div>
			</div>
			<legend class="sub_title_forms_popup">
				<?php echo $this->lang->line("common_users_allow"); ?>
			</legend>
			<p class="descrip_pop"><?php echo $this->lang->line("employees_permission_desc"); ?></p>
			<div class="box_field_row">	
				<div class='box_row'>
					<ul id="permission_list_pop">
						<?php
						foreach($all_modules->result() as $module){ 
							echo form_label($this->lang->line('module_'.$module->module_id).':', 'account_number'); ?>
						<li>
							<?php echo form_checkbox(array(
								'name'=>'permissions[]',
								'id'=>'permissions',
								'title'=>$this->lang->line('module_'.$module->module_id)
							),$module->module_id,$this->Employee->has_permission($module->module_id,$person_info->person_id)); ?>
							<span class="small_txt"><?php echo $this->lang->line('module_'.$module->module_id.'_desc');?></span>
						</li>
						<?php
						}
						?>
					</ul>
				</div>
			</div>
			<?php
			echo form_button(array(
				'name' => 'button',
				'type' => 'submit',
				'id' => 'button',
				'content' => $this->lang->line('common_submit'),
				'class'=>'submit_button_pop',
				'title'=>$this->lang->line('common_submit')
			));
			?>
		</div>
	</div>
</form>