<!DOCTYPE html>
<html lang="<?php echo $config_info['language'];?>">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="Author" content="Adrián Ramirez | brontobytemx.com" />
		<meta name="Subject" content="Apps Mexico" />
		<!--<meta name="GOOGLEBOT" content="INDEX, FOLLOW, ALL" />-->
		<title>Cash Control | <?php echo $this->lang->line('module_'.$controller_name);  ?></title>
		<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/cc_css/cash_style.css" />
		<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/cc_css/bootstrap.css" />
		<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/cc_css/cash_style_dispositivos.css" />
		<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/cc_css/cash_style_print.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/cc_css/magnific-popup.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/cc_css/jquery-ui.css">
		<link href='https://fonts.googleapis.com/css?family=Roboto:400,500,300italic,700
		|Raleway:400,300,500|Lato|Cabin:400,600,500|Open+Sans+Condensed:300|Open+Sans:400,600|Oxygen:400,300|Loved+by+the+King|Patrick+Hand+SC|Advent+Pro|Alegreya+Sans+SC|Basic|Farsan|Julius+Sans+One|Marcellus+SC|Marck+Script|Tauri|Voltaire|Cabin+Condensed|Roboto+Condensed' rel='stylesheet' type='text/css'>
		<script src="<?php echo base_url();?>js/cc_js/jquery-3.3.1.min.js"></script>
		<script src="<?php echo base_url();?>js/cc_js/jquery-ui.min.js"></script>
		<script src="<?php echo base_url();?>js/cc_js/jquery.magnific-popup.min.js"></script>
		<script src="<?php echo base_url();?>js/cc_js/jquery.cookie-1.4.1.min.js"></script>
		<script src="<?php echo base_url();?>js/cc_js/jquery.jkey.js"></script>
		<script src="<?php echo base_url();?>js/cc_js/manage_tables.js"></script>
		<script src="<?php echo base_url();?>js/cc_js/jquery-barcode.js"></script>
		<script src="<?php echo base_url();?>js/cc_js/validar_forms_modules.js"></script>
		<script src="<?php echo base_url();?>js/cc_js/wwwzip.js"></script>
		<script src="<?php echo base_url();?>js/cc_js/ajax_forms.js"></script>
		<script src="<?php echo base_url();?>js/cc_js/bootstrap.js"></script>
		<script src="<?php echo base_url();?>js/cc_js/xlsx.js"></script>
		<script src="<?php echo base_url();?>js/cc_js/receivings.js"></script>
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
		<meta name="theme-color" content="#003959" />
		<link rel="shortcut icon" href="<?php echo base_url();?>images/cc_images/icon_cct/favicon_cact.ico">
	</head>
	<body>
		<header id="header_menu">
			<div class="accordion" id="accordion">
				<div class="accordion-group">
					<div class="accordion-heading">
						<a  class="accordion-toggle collapsed" id="c-tog-menu" data-toggle="collapse" data-parent="#accordion" href="#ans2">
							<div id="ico_menu"></div>
							<div id="cover_logo"><img id="logo-menu" src="<?php echo base_url();?>images/cc_images/logo-cash/logo-cash-ctrol.png"></div>
						</a>
						<div id="box_config_header">
							<div id="box_user">
								<div class="accordion" id="accordion_user">
									<div  id="acco-group" class="accordion-group">
										<div class="accordion-heading">
											<a class="accordion-toggle "  id="c-tog-user"  data-toggle="collapse" data-parent="#accordion_user" href="#ans4">			
												<img class="" src="<?php echo base_url();?>images/cc_images/icon_cct/user-img-ico.png">
											</a>
										</div>
										<div id="ans4" class="accordion-body collapse">
											<div class="accordion-inner">
												<div class="triangle_notif"></div>
												<ul id="list_user">
													<li><?php if(isset($version))echo '<a>'.$version.'</a>';?></li>
													<li><?php echo anchor("home/backup",'Respaldar'); ?></li>
													<li id="btn-logout"><?php echo anchor("home/logout",$this->lang->line("common_logout")); ?></li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div id="box_name_user">
								<span>
									<?php echo $this->lang->line('common_welcome')." $user_info->first_name $user_info->last_name! | "; ?>
								</span>
							</div>
							<div id="box_notification">
								<div class="accordion" id="accordion_notification">
									<div  class="accordion-group">
										<div class="accordion-heading">
											<a class="accordion-toggle collapsed"  id="c-tog-notifi"  data-toggle="collapse" data-parent="#accordion_notification" href="#ans5">			
												<img class="" src="<?php echo base_url();?>images/cc_images/icon_cct/notification-ico.png">
												<?php if($notificacion_qty > 0)echo '<span id="box_notifi_number">'.$notificacion_qty.'</span>';?>
											</a>
										</div>
										<?php if($notificacion_qty > 0)
										{
										?>
										<div id="ans5" class="accordion-body collapse">
											<div class="accordion-inner">
												<div class="triangle_notif triangle_notif_b"></div>
												<ul id="list_notification">
													<?php echo $notificaciones; ?>
												</ul>
											</div>
										</div>
										<?php }?>
									</div>
								</div>
							</div>
						</div>
						<div id="menu_date">
							
								<?php
							
								echo "<b>".selected_hour().":".selected_minute()." hrs ".selected_day()."/".selected_month()."/".selected_year()."</b>";
								/*echo date('F d, Y h:i a') */
								?>
							
						</div>
					</div>
					<div id="ans2" class="accordion-body collapse">
						<div class="accordion-inner">
							<div id="cover_navigation_modules_header">
								<?php
								foreach($allowed_modules->result() as $module)
								{
								?>
								<div class="box_modules_header">
									<a href="<?php echo site_url("$module->module_id");?>">
										<img src="<?php echo base_url().'images/cc_images/menubar/'.$module->module_id.'.png';?>" border="0" alt="Menubar Image" />
									</a>
									<a class="name_menu_modules_header" href="<?php echo site_url("$module->module_id");?>"><?php echo $this->lang->line("module_".$module->module_id) ?></a>
								</div>
								<?php
								}
								?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</header>
		<!-- bootstrop  accordion-->
		<!-- bootstrop-->
		<script>
		$( document ).ready( readyFunction );
		function readyFunction( jQuery ) 
		{
			var filtro ='<?php if (isset($filtro))echo $filtro; ?>';
			var module ='<?php echo $controller_name; ?>';
			var base_url="<?php echo base_url();?>";
			search_module(base_url,module);
			show_hide_search_filter(base_url,filtro,module); 
			enable_select_all(module,base_url);
			form_ajax(module,base_url);
			info_forms(module,base_url);
			show_hidde_responsive_manage();
			if(module==="items"){
				content_forms(module,base_url);
				items_forms(module,base_url);
				excel(base_url,module);
				language_filter(module,base_url);
			}
			else if(module==="recursos"){
				recursos_operation(module,base_url);
			}
			else if(module==="gastos"){
				set_filtro_manage(module,base_url);
			}
			else if(module==="receivings"){
				register_receivings(module,base_url);
				set_filtro_manage(module,base_url);
			}
			 
			$("#content_area_wrapper").on("click",function()
			{
				if($("#c-tog-menu").attr("class")=="accordion-toggle"){
					$("#c-tog-menu").trigger('click');
				}
				if($("#c-tog-notifi").attr("class")=="accordion-toggle"){
					$("#c-tog-notifi").trigger('click');
				}
			});
			$(".accordion-body").on("click",function(){
				if($("#c-tog-menu").attr("class")=="accordion-toggle"){
					$("#c-tog-menu").trigger('click');
				}
				if($("#c-tog-notifi").attr("class")=="accordion-toggle"){
					$("#c-tog-notifi").trigger('click');
				}
			});
		}
		</script>
		<div id="content_area_wrapper">