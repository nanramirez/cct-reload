<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Author" content="Adrián Ramirez | brontobytemx.com" />
<meta name="Subject" content="Apps Mexico" />
<meta name="GOOGLEBOT" content="INDEX, FOLLOW, ALL" />
<title>Cash Control | <?php echo $this->lang->line('module_'.$controller_name);  ?></title>
<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/cc_css/cash_style.css" />
<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/cc_css/bootstrap.css" />
<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/cc_css/cash_style_dispositivos.css" />
<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/cc_css/cash_style_print.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/cc_css/magnific-popup.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/cc_css/jquery-ui.css">
<link href='https://fonts.googleapis.com/css?family=Roboto:400,500,300italic,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Raleway:400,300,500' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Cabin:400,600,500' rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300|Open+Sans:400,600" rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Oxygen:400,300' rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/css?family=Loved+by+the+King|Patrick+Hand+SC" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Advent+Pro|Alegreya+Sans+SC|Basic|Farsan|Julius+Sans+One|Marcellus+SC|Marck+Script|Tauri|Voltaire" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Cabin+Condensed|Roboto+Condensed" rel="stylesheet">
<script src="http://code.jquery.com/jquery.js"></script>

<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
<meta name="theme-color" content="#003959" />
<link rel="shortcut icon" href="<?php echo base_url();?>images/cc_images/icon_cct/favicon_cact.ico">
</head>
<body>
<header id="cover_full_top_home">
<div id="box_logo_home">
<a  class="" id="c-tog-menu">
<img id="logo-home" src="<?php echo base_url();?>images/cc_images/logo-cash/logo-cash-ctrol.png">
</a>
</div>
<div id="box_config_header">
<div id="box_user">
<div class="accordion" id="accordion_user">
<div  id="acco-group" class="accordion-group">
<div class="accordion-heading">
<a class="accordion-toggle"  id="c-tog-user"  data-toggle="collapse" data-parent="#accordion_user" href="#ans4">
<img class="" src="<?php echo base_url();?>images/cc_images/icon_cct/user-img-ico.png">
</a>
</div>
<div id="ans4" class="accordion-body collapse">
<div class="accordion-inner">
<div class="triangle_notif"></div>
<ul id="list_user">
<li><?php if(isset($version))echo '<a>'.$version.'</a>';?></li>
<li>
<?php echo anchor("home/backup",$this->lang->line("common_backup")); ?></li>
<li id="btn-logout">
<?php echo anchor("home/logout",$this->lang->line("common_logout")); ?></li>
</ul>
</div>
</div>
</div>
</div>
</div>
<div id="box_name_user">
<span id="bnu_home">
<?php echo $this->lang->line('common_welcome')." $user_info->first_name $user_info->last_name! | "; ?>
</span>
</div>
<div id="box_notification">
<div class="accordion" id="accordion_notification">
<div  class="accordion-group">
<div class="accordion-heading">
<a class="accordion-toggle"  id="c-tog-notifi"  data-toggle="collapse" data-parent="#accordion_notification" href="#ans5">
<img class="" src="<?php echo base_url();?>images/cc_images/icon_cct/notification-ico.png">
<?php if($notificacion_qty > 0)echo '<span id="box_notifi_number">'.$notificacion_qty.'</span>';?>
</a>
</div>
<?php if($notificacion_qty > 0)
{
?>
	<div id="ans5" class="accordion-body collapse">
	<div class="accordion-inner">
	<div class="triangle_notif triangle_notif_b"></div>
	<ul id="list_notification">
	<?php echo $notificaciones; ?>
	</ul>
	</div>
	</div>
<?php 
}
?>
</div>
</div>
</div>
</div>
</header>
  <!-- bootstrop  accordion-->
 <script src="<?php echo base_url();?>js/cc_js/bootstrap.js">
 </script>
  <!-- bootstrop-->
<script>
$( document ).ready( readyFunction );
function readyFunction( jQuery ) {
var base_url="<?php echo base_url();?>";
console.log(base_url);
first_config(base_url);
}
/*
	var filtro ='<?php if (isset($filtro))echo $filtro; ?>';
	var module ='<?php echo $controller_name; ?>';
	config_ajax(module);
	search_module(base_url,module);
	show_hide_search_filter(base_url,filtro,module); 
	enable_select_all(module,base_url);
	form_ajax(module,base_url);
	items_forms(module);
	items_pedido(module,base_url);
	send_msj(module,base_url);
	register_sales(module,base_url);
	register_receivings(module,base_url);
	excel(base_url);
	info_forms(module);
	get_coin(base_url);
	//console.log($("#c-tog-menu").attr("class"));
	$("#content_area_wrapper").on("click",function()
	{
		//console.log($("#c-tog-menu").attr("class"))
		if($("#c-tog-menu").attr("class")=="accordion-toggle")
		{
			$("#c-tog-menu").trigger('click');
		}  
	});
	$(".accordion-body").on("click",function()
	{
		if($("#c-tog-menu").attr("class")=="accordion-toggle")
		{
		$("#c-tog-menu").trigger('click');
        }
	});
}
$(function() {
$( "#button_menu_display" ).click(function() {
  $( ".icono_menu_" ).addClass( "newClass", 1000, callback );
});
function callback() {
  setTimeout(function() {
	$( ".icono_menu_" ).removeClass( "newClass" );
  }, 1500 );
}
});*/
</script>
 <!-- cambia clase por otra
 <script>
 /*
  $(function() {
    $( "#button_menu_display" ).click(function(){
      $( ".newClass" ).switchClass( "newClass", "anotherNewClass", 1000 );
      $( ".anotherNewClass" ).switchClass( "anotherNewClass", "newClass", 1000 );
    });
  });*/
  </script>-->
<div id="content_area_wrapper">