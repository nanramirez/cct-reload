<?php $this->load->view("cc_views/partial/header_home"); ?>
<div id="cover_modules_home">
<div id="subcover_modules_home">
<div id="cover_title_module">
<span id="title_module_home"><?php echo $this->lang->line("module_home_welcome");?></span>
<?php if($config_info["nueva_config"]==1){?>
</div>
<div id="fieldset_config_template">
<!-- ----------------------------------------------------
--------------------PAGINA # 1------------------------- -->
<div id="paso1">
<input type="hidden" id="jsonerrors" value='<?php echo $json_errors_paso_uno; ?>'name="jsonerrors"/>	
<div class="cover_title_config_template">
<legend class="title_forms_popup_template">Datos Generales</legend>
<span class="subtitle_forms_popup_template">Ingrese sus datos para configurar su sitio web en la "Página principal"</span>
</div>
<ul id="error_message_box"></ul>
<div id="cover_field_row_config_template_left">
<div class="box_field_row ">
<?php echo form_label($this->lang->line('common_company').':', 'company',array('class'=>'wide required')); ?>
<div class='box_row'>
<?php echo form_input(
	array(
		'name'=>'company',
		'id'=>'company',
		'value'=>$config_info["company"],
		'title'=>$this->lang->line('common_company')
	    )         
	               );
?>
</div>
</div>
<div class="box_field_row">
<?php echo form_label($this->lang->line('module_config_logo').' '.$this->lang->line('common_img').':', 'logo',array('class'=>'required')); ?>
<div id="box_row_uploadfile" class='box_row'>
<?php echo form_upload(
	array(
		'multiple'=>'multiple',
		'accept'=>'gif|jpg|png',
		'class'=>'form-control',
		'name'=>'logo[]',
		'id'=>'logo',
		'size'=>'18',
		'title'=>$this->lang->line('module_config_logo')
	    )
					);
?>
</div>
</div>
<div class="box_field_row ">
<?php echo form_label($this->lang->line('module_config_slogan').':', 'fax',array('class'=>'wide')); ?>
<div class='box_row'>
<?php echo form_input(
	array(
		'name'=>'slogan',
		'id'=>'slogan',
		'value'=>$config_info["slogan"],
		'title'=>$this->lang->line('module_config_slogan')
	    )
                    );
?>
</div>
</div>
<div class="box_field_row">
<label>Imagenes para el banner</label>
<span class="descrip_ch">Éstas imagenes se mostraran en el banner principal</span>
<div id="box_row_uploadfile" class='box_row'>
<?php echo form_upload(
	array(
		'multiple'=>'multiple',
		'accept'=>'gif|jpg|png',
		'class'=>'form-control',
		'name'=>'banner[]',
		'id'=>'banner',
		'size'=>'18',
		'title'=>''
	     )
				 );
?>

</div>
<div class="box_field_row">
<div id="drop"><?php echo $this->lang->line('common_drag');?></div>
<pre id="out"></pre>
</div>
</div>
</div>
<div id="cover_field_row_config_template_rigth">
<!---CAMPOS NO PROGRAMADOS--->
<div class="box_field_row ">
<label>Descripción sobre su empresa (Quienes son)</label>
<div class='box_row'>
<?php echo form_textarea(
	array(
		'name'=>'description_empresa',
		'id'=>'description_empresa',
		'value'=>$config_info["company_description"],
		'rows'=>4,
		'cols'=>17,
		'title'=>''
	    )
                     );
?>
</div>
</div>
<!---CAMPOS NO PROGRAMADOS--->
<div class="box_field_row ">
<label>Orígenes/historia/trayectoria </label>
<div class='box_row'>
<?php echo form_textarea(
	array(
		'name'=>'history',
		'id'=>'history',
		'value'=>$config_info["history"],
		'rows'=>4,
		'cols'=>17,
		'title'=>''
	    )
                       );
?>
</div>
</div>
<!---CAMPOS NO PROGRAMADOS
<div class="box_field_row ">
<label>Productos Destacados</label>
<span class="descrip_ch">Agrega 4 productos destacados, estos apareceran en la pagina principal.</span>
<div class='box_row'>
<div id="cover_btn_add_items_template">
<?php /* echo anchor("$controller_name/view/","<div class='buttons_interaction_bar' > <span>".$this->lang->line('common_new_M')."</span></div>",array('class'=>'ajax-popup-link','title'=>$this->lang->line($controller_name.'_new')));*/
?>
</div>
</div>
</div>--->
<!---CAMPOS NO PROGRAMADOS--->
<div class="box_field_row ">
<label>Cuantas seeciones aparte de su catalogo de productos tendrá su página</label>
<span class="descrip_ch">Generalmente las paginas suelen tener 3 Secciones: Principal,Nosotros,Contacto. Pero puede agregar secciones extras si usted necesita más</span>
<div class='box_row'>
<div id="cover_btn_add_items_template">
<?php echo form_dropdown('numero_secciones',$numero_secciones , "" );?>
</div>
</div>
</div>
<div class="box_field_row ">
<a class="btn_next_config_template" id="btn_next_config_template" >SIGUIENTE</a>
</div>
</div>
</div>
<!-- ----------------------------------------------------
--------------------PAGINA # 2------------------------- -->	
<div id="paso2">
<input type="hidden" id="jsonerrors" value='<?php echo $json_errors_paso_uno; ?>'name="jsonerrors"/>
<div class="cover_title_config_template">
<legend class="title_forms_popup_template">Datos de Contacto y de operación</legend>
<span class="subtitle_forms_popup_template">Ingrese su información de contacto, horarios de operacion"</span>
</div>
<ul id="error_message_box"></ul>
<div id="cover_field_row_config_template_left">
<div class="box_field_row ">
<?php echo form_label($this->lang->line('common_email').':', 'email',array('class'=>'wide required')); ?>
<div class='box_row'>
<?php echo form_input(
	array(
		'name'=>'email',
		'id'=>'email',
		'value'=>$config_info["email"],
		'title'=>''
	    )
                    );
?>
</div>
</div>
<div class="box_field_row ">
<?php echo form_label($this->lang->line('common_address').':', 'address',array('class'=>'wide required')); ?>
<div class='box_row'>
<?php echo form_input(
	array(
		'name'=>'address',
		'id'=>'address',
		'value'=>$config_info["address"],
		'title'=>''
		)
					);
?>
</div>
</div>
<div class="box_field_row ">
<?php echo form_label($this->lang->line('common_phone_number').':', 'company',array('class'=>'wide required')); ?>
<div class='box_row'>
<?php echo form_input(
	array(
		'name'=>'phone',
		'id'=>'phone',
		'value'=>$config_info["phone"],
		'title'=>''
		)
					);
?>
</div>
</div>
<div class="box_field_row ">
<?php echo form_label($this->lang->line('common_phone_number2').':', 'company',array('class'=>'wide required')); ?>
<div class='box_row'>
<?php echo form_input(
	array(
		'name'=>'phone2',
		'id'=>'phone2',
		'value'=>$config_info["phone2"],
		'title'=>''
		)
					);
?>
</div>
</div>
</div>
<div id="cover_field_row_config_template_rigth">
<!---CAMPOS NO PROGRAMADOS--->
<div class="box_field_row ">
<label>Descripción sobre su empresa (Quienes son)</label>
<div class='box_row'>
<?php echo form_textarea(
	array(
		'name'=>'description_empresa',
		'id'=>'description_empresa',
		'value'=>$config_info["company_description"],
		'rows'=>4,
		'cols'=>17,
		'title'=>''
		)
					);
?>
</div>
</div>
<!---CAMPOS NO PROGRAMADOS--->
<div class="box_field_row ">
<label>Orígenes/historia/trayectoria </label>
<div class='box_row'>
<?php echo form_textarea(
	array(
		'name'=>'history',
		'id'=>'history',
		'value'=>$config_info["history"],
		'rows'=>4,
		'cols'=>17,
		'title'=>''
		)
					);
?>
</div>
</div>
<!---CAMPOS NO PROGRAMADOS
<div class="box_field_row ">
<label>Productos Destacados</label>
<span class="descrip_ch">Agrega 4 productos destacados, estos apareceran en la pagina principal.</span>
<div class='box_row'>
<div id="cover_btn_add_items_template">
<?php /* echo anchor("$controller_name/view/","<div class='buttons_interaction_bar' > <span>".$this->lang->line('common_new_M')."</span></div>",array('class'=>'ajax-popup-link','title'=>$this->lang->line($controller_name.'_new')));*/
?>
</div>
</div>
</div>--->
<!---CAMPOS NO PROGRAMADOS--->
<div class="box_field_row ">
<label>Cuantas seeciones aparte de su catalogo de productos tendrá su página</label>
<span class="descrip_ch">Generalmente las paginas suelen tener 3 Secciones: Principal,Nosotros,Contacto. Pero puede agregar secciones extras si usted necesita más</span>
<div class='box_row'>
<div id="cover_btn_add_items_template">
<?php echo form_dropdown('numero_secciones',$numero_secciones , "" );?>
</div>
</div>
</div>
<div class="box_field_row ">
<a class="btn_next_config_template" id="btn_next_config_template" >SIGUIENTE</a>
</div>
</div>
</div>
<!-------------------------------------------------
--------------------PAGINA # 3--------------------------
----------------------------------------------------------------->
<div id="paso3">
<div class="cover_title_config_template">
<legend class="title_forms_popup_template">Información de secciones extras</legend>
<span class="subtitle_forms_popup_template">Ingrese sus datos para configurar su sitio web en la "Sección de Servicios o Actividades "</span>
</div>
<div id="cover_field_row_config_template_left">
<div class="cover_servicios_template cover_servicios_template_b">
<div class="box_field_row ">
<label>Contenido #1</label>
<span class="descrip_ch">Aqui podras describir el tipo de servicio , actividades ó articulos de interes de la  empresa.</span>
<div class='box_row' id='title_serv_template'>
<?php echo form_input(
	array(
		'name'=>'company',
		'id'=>'company',
		'value'=>'titulos',
		'title'=>''
		)
					);
?>
</div>
<div class='box_row'>
<?php echo form_textarea(
	array(
		'name'=>'address',
		'id'=>'address',
		'rows'=>4,
		'cols'=>17,
		'value'=>'Descripción',
		'title'=>''
		)
					);
?>
</div>
</div>
<div class="box_field_row">
<label>Imagen Contenido #1</label>
<div id="box_row_uploadfile" class='box_row'>
 <?php echo form_upload(
 	array(
		'multiple'=>'multiple',
		'accept'=>'gif|jpg|png',
		'class'=>'form-control',
		'name'=>'uploadfile[]',
		'id'=>'uploadfile',
		'size'=>'18',
		'title'=>''
		)
					 );
?>
					
</div>
</div>
</div>
<!---CAMPOS NO PROGRAMADOS--->
<div class="cover_servicios_template">
<div class="box_field_row ">
<label>Contenido #3</label>
<span class="descrip_ch">Aqui podras describir el tipo de servicio , actividades ó articulos de interes de la  empresa.</span>
<div class='box_row' id='title_serv_template'>
<?php echo form_input(
	array(
		'name'=>'company',
		'id'=>'company',
		'value'=>'titulos',
		'title'=>''
		)
					);
?>
</div>
<div class='box_row'>
<?php echo form_textarea(
	array(
		'name'=>'address',
		'id'=>'address',
		'rows'=>4,
		'cols'=>17,
		'value'=>'Descripción',
		'title'=>''
		)
					);
?>
</div>
</div>
<div class="box_field_row">
<label>Imagen Contenido #3</label>
<div id="box_row_uploadfile" class='box_row'>
 <?php echo form_upload(
 	array(
		'multiple'=>'multiple',
		'accept'=>'gif|jpg|png',
		'class'=>'form-control',
		'name'=>'uploadfile[]',
		'id'=>'uploadfile',
		'size'=>'18',
		'title'=>''
		)
					);
?>
</div>
</div>
</div>
</div>
<div id="cover_field_row_config_template_rigth">
<div class="cover_servicios_template cover_servicios_template_b">
<div class="box_field_row ">
<label>Contenido #2</label>
<span class="descrip_ch">Aqui podras describir el tipo de servicio , actividades ó articulos de interes de la  empresa.</span>
<div class='box_row' id='title_serv_template'>
<?php echo form_input(
	array(
		'name'=>'company',
		'id'=>'company',
		'value'=>'titulos',
		'title'=>''
		)
					);
?>
</div>
<div class='box_row'>
<?php echo form_textarea(
	array(
		'name'=>'address',
		'id'=>'address',
		'rows'=>4,
		'cols'=>17,
		'value'=>'Descripción',
		'title'=>''
		)
					);
?>
</div>
</div>
<div class="box_field_row">
<label>Imagen Contenido #2</label>
<div id="box_row_uploadfile" class='box_row'>
 <?php echo form_upload(
 	array(
		'multiple'=>'multiple',
		'accept'=>'gif|jpg|png',
		'class'=>'form-control',
		'name'=>'uploadfile[]',
		'id'=>'uploadfile',
		'size'=>'18',
		'title'=>''
		)
					);
?>
</div>
</div>
</div>
<div class="cover_servicios_template">
<div class="box_field_row ">
<label>Contenido #4</label>
<span class="descrip_ch">Aqui podras describir el tipo de servicio , actividades ó articulos de interes de la  empresa.</span>
<div class='box_row' id='title_serv_template'>
<?php echo form_input(
	array(
		'name'=>'company',
		'id'=>'company',
		'value'=>'titulos',
		'title'=>''
		)
					);
?>
</div>
<div class='box_row'>
<?php echo form_textarea(
	array(
		'name'=>'address',
		'id'=>'address',
		'rows'=>4,
		'cols'=>17,
		'value'=>'Descripción',
		'title'=>''
		)
					);
?>
</div>
</div>
<div class="box_field_row">
<label>Imagen Contenido #4</label>
<div id="box_row_uploadfile" class='box_row'>
 <?php echo form_upload(
 	array(
		'multiple'=>'multiple',
		'accept'=>'gif|jpg|png',
		'class'=>'form-control',
		'name'=>'uploadfile[]',
		'id'=>'uploadfile',
		'size'=>'18',
		'title'=>''
		)
					);
?>
</div>
</div>
</div>
</div>
</div>
<!------------------------------------PAGINA#4------------------------------------------------------------------------------------------------->
<div id="paso4">
<div class="cover_title_config_template">
<legend class="title_forms_popup_template">Datos de contacto y redes sociales</legend>
<span class="subtitle_forms_popup_template">Ingrese sus datos para configurar su sitio web en la "Sección de Contacto"</span>
</div>
<div id="cover_field_row_config_template_left">
<!---CAMPOS NO PROGRAMADOS--->
<div class="box_field_row ">
<?php echo form_label($this->lang->line('common_address').':', 'address',array('class'=>'wide required')); ?>
<div class='box_row'>
<?php echo form_textarea(
	array(
		'name'=>'address',
		'id'=>'address',
		'rows'=>4,
		'cols'=>17,
		'value'=>$config_info["address"],
		'title'=>''
		)
					);
?>
</div>
</div>
<div class="box_field_row ">	
<?php echo form_label($this->lang->line('common_phone_number').':', 'phone',array('class'=>'wide required')); ?>
<div class='box_row'>
<?php echo form_input(
	array(
		'name'=>'phone',
		'id'=>'phone',
		'value'=>$config_info["phone"],
		'title'=>''
		)
					);
?>
</div>
</div>
<div class="box_field_row ">	
<?php echo form_label($this->lang->line('common_phone_number2').':', 'phone2',array('class'=>'wide required')); ?>
<div class='box_row'>
<?php echo form_input(
	array(
		'name'=>'phone2',
		'id'=>'phone2',
		'value'=>$config_info["phone2"],
		'title'=>''
		)
					);
?>
</div>
</div>
<div class="box_field_row ">	
<?php echo form_label($this->lang->line('common_email').':', 'email',array('class'=>'wide')); ?>
<div class='box_row'>
<?php echo form_input(
	array(
		'name'=>'email',
		'id'=>'email',
		'value'=>$config_info["email"],
		'title'=>''
		)
					);
?>
</div>
</div>

<!---CAMPOS NO PROGRAMADOS--->
<div class="box_field_row ">	
<?php echo form_label($this->lang->line('common_email').':', 'email',array('class'=>'wide')); ?>
<div class='box_row'>
<?php echo form_input(
	array(
		'name'=>'email',
		'id'=>'email',
		'value'=>$config_info["email"],
		'title'=>''
		)
					);
?>
</div>
</div>
		</div>
		
		<div id="cover_field_row_config_template_rigth">
		

<div class="box_field_row ">	
<?php echo form_label($this->lang->line('common_phone_schedule').':', 'schedule',array('class'=>'wide required')); ?>
<div class='box_row'>
<?php echo form_input(
	array(
		'name'=>'schedule',
		'id'=>'schedule',
		'value'=>$config_info["schedule"],
		'title'=>''
		)
					);
?>
</div>
</div>
<!---CAMPOS NO PROGRAMADOS--->
<div class="box_field_row ">	
<label>Direcciones de las redes sociales.</label>
<span class="descrip_ch">Anexa las direcciones de las redes que manejas para vincularlas con el sitio</span>
<div class='box_row'>
<?php echo form_textarea(
	array(
		'name'=>'address',
		'id'=>'address',
		'rows'=>4,
		'cols'=>17,
		'title'=>''
		)
					);
?>
</div>
</div>

<div class="box_field_row ">
<label>Vínculo de Google maps de la empresa.</label>
<span class="descrip_ch">Comparte la url del mapa de tu dirección</span>
<div class='box_row'>
<?php echo form_textarea(
	array(
		'name'=>'address',
		'id'=>'address',
		'rows'=>4,
		'cols'=>17,
		'title'=>''
		)
						);
?>
</div>
</div>
</div>
</div>
<!-------------------------------------------------------
----------------PAGINA # 5-----------------------------------
----------------------------------------------------------------->
<div id="paso5">
<div class="cover_title_config_template">
<legend class="title_forms_popup_template">Agregue sus primeros cuatro productos</legend>
<span class="subtitle_forms_popup_template">Tienen que ser sus cuatro producto más populares y vendidos o los que usted quiera destacar en su página principal, estos se pueden modificar posteriormente</span>
</div>
	<div id="cover_field_row_config_template_left">
<div class="box_field_row">
<?php echo form_label($this->lang->line('module_item').':', 'name',array('class'=>'required','id'=>'items_name_label')); ?>
<div class='box_row'>
<?php echo form_input(
	array(
		'name'=>'name',
		'id'=>'name',
		'value'=>"",
		'title'=>''
		)
					);
?>
</div>
</div>
	<div class="box_field_row">
<?php echo form_label($this->lang->line('module_item_brand').':', 'brand',array('class'=>'required' ,'id'=>'brand_label')); ?>
<div class='box_row'>
<?php echo form_input(
	array(
		'name'=>'brand',
		'id'=>'brand',
		'value'=>"",
		'title'=>''
		)
					);
?>
</div>
	</div>
	<div class="box_field_row">
	<?php echo form_label($this->lang->line('module_item_modelo').':', 'modelo',array('class'=>'required','id'=>'modelo_label')); ?>
<div class='box_row'>
<?php echo form_input(
	array(
		'name'=>'modelo',
		'id'=>'modelo',
		'value'=>"",
		'title'=>''
		)
					);
?>
</div>
	</div>
	<div class="box_field_row">
	<?php echo form_label($this->lang->line('common_category').':', 'category_label',array('class'=>'required','id'=>'category_label')); ?>
<div class='box_row'>
<div class="ui-widget">
<?php echo form_input(
	array(
		'name'=>'category',
		'id'=>'category',
		'value'=>"",
		'title'=>''
		)
					);
?>
</div>
</div>
	</div>
	
	<div class="box_field_row">
	<?php echo form_label($this->lang->line('common_cost').' '.$config_info['coin'].':', 'cost_price',array('class'=>'required','id'=>'cost_price_label')); ?>
<div class='box_row'>
<?php echo form_input(
	array(
		'name'=>'cost_price',
		'id'=>'cost_price',
		'value'=>0,
		'title'=>''
		)
					);
?>
</div>
	</div>
	<div class="box_field_row">
	<?php echo form_label($this->lang->line('common_price').' '.$config_info['coin'].':', 'unit_price',array('class'=>'required','id'=>'unit_price_label')); ?>
<div class='box_row'>
<?php echo form_input(
	array(
		'name'=>'unit_price',
		'id'=>'unit_price',
		'value'=>0,
		'title'=>''
		)
					);
?>
</div>
	</div>
	<div class="box_field_row">
	<?php echo form_label($this->lang->line('common_price_special').' '.$config_info['coin'].':', 'other_price',array('class'=>'required' ,'id'=>'other_price_label')); ?>
<div class='box_row'>
<?php echo form_input(
	array(
		'name'=>'other_price',
		'id'=>'other_price',
		'value'=>0,
		'title'=>''
		)
					);
?>
</div>
	</div>
	<div class="box_field_row">
	<?php echo form_label($this->lang->line('module_item_unit_stock').':', 'quantity',array('class'=>'required','id'=>'quantity_label')); ?>
<div class='box_row'>
<?php if(empty($item_info->item_id))
	{
	echo form_input(
	array(
		'name'=>'quantity',
		'id'=>'quantity',
		'value'=>0,
		'title'=>''
		)
					);
	}
	else
	{
	echo form_input(
	array(
		'name'=>'quantity',
		'id'=>'quantity',
		'disabled'=>'disabled',
		'value'=>0,
		'title'=>''
		)
				);
	}
?>
</div>
</div>
<div class="box_field_row">
<?php echo form_label($this->lang->line('module_item_stock_minimum').':', 'reorder_level',array('class'=>'required','id'=>'reorder_level_label')); ?>
<div class='box_row'>
<?php echo form_input(
	array(
		'name'=>'reorder_level',
		'id'=>'reorder_level',
		'value'=>0,
		'title'=>''
		)
					);
?>
</div>
</div>
<div class="box_field_row">
<?php echo form_label($this->lang->line('common_description').':', 'description',array('class'=>'required')); ?>
<div class='box_row'>
<?php echo form_textarea(
	array(
		'name'=>'description',
		'id'=>'description',
		'value'=>"",
		'rows'=>'5',
		'cols'=>'17',
		'title'=>''
		)
					);
?>
</div>
</div>
<div class="box_field_row">
<?php echo form_label($this->lang->line('common_upload').' '.$this->lang->line('common_img').':', 'uploadfile',array('class'=>'required')); ?>
<div id="box_row_uploadfile" class='box_row'>
 <?php echo form_upload(
 	array(
		'multiple'=>'multiple',
		'accept'=>'gif|jpg|png',
		'class'=>'form-control',
		'name'=>'uploadfile[]',
		'id'=>'uploadfile',
		'size'=>'18',
		'title'=>''
		)
					);
?>
</div>
</div>
</div>
<div id="cover_field_row_config_template_rigth">
<div class="box_field_row">
<?php echo form_label($this->lang->line('module_item').':', 'name',array('class'=>'required','id'=>'items_name_label')); ?>
<div class='box_row'>
<?php echo form_input(
	array(
		'name'=>'name',
		'id'=>'name',
		'value'=>"",
		'title'=>''
		)
				);
?>
</div>
</div>
<div class="box_field_row">
<?php echo form_label($this->lang->line('module_item_brand').':', 'brand',array('class'=>'required' ,'id'=>'brand_label')); ?>
<div class='box_row'>
<?php echo form_input(
	array(
		'name'=>'brand',
		'id'=>'brand',
		'value'=>"",
		'title'=>''
		)
				);
?>
</div>
</div>
<div class="box_field_row">
<?php echo form_label($this->lang->line('module_item_modelo').':', 'modelo',array('class'=>'required','id'=>'modelo_label')); ?>
<div class='box_row'>
<?php echo form_input(
	array(
		'name'=>'modelo',
		'id'=>'modelo',
		'value'=>"",
		'title'=>''
		)
					);
?>
</div>
</div>
<div class="box_field_row">
<?php echo form_label($this->lang->line('common_category').':', 'category_label',array('class'=>'required','id'=>'category_label')); ?>
<div class='box_row'>
<div class="ui-widget">
<?php echo form_input(
	array(
		'name'=>'category',
		'id'=>'category',
		'value'=>"",
		'title'=>''
		)
					);
?>
</div>
</div>
</div>
<div class="box_field_row">
<?php echo form_label($this->lang->line('common_cost').' '.$config_info['coin'].':', 'cost_price',array('class'=>'required','id'=>'cost_price_label')); ?>
<div class='box_row'>
<?php echo form_input(
	array(
		'name'=>'cost_price',
		'id'=>'cost_price',
		'value'=>0,
		'title'=>''
		)
				);
?>
</div>
</div>
<div class="box_field_row">
<?php echo form_label($this->lang->line('common_price').' '.$config_info['coin'].':', 'unit_price',array('class'=>'required','id'=>'unit_price_label')); ?>
<div class='box_row'>
<?php echo form_input(
	array(
		'name'=>'unit_price',
		'id'=>'unit_price',
		'value'=>0,
		'title'=>''
		)
				);
?>
</div>
</div>
<div class="box_field_row">
<?php echo form_label($this->lang->line('common_price_special').' '.$config_info['coin'].':', 'other_price',array('class'=>'required' ,'id'=>'other_price_label')); ?>
<div class='box_row'>
<?php echo form_input(
	array(
		'name'=>'other_price',
		'id'=>'other_price',
		'value'=>0,
		'title'=>''
		)
				);
?>
</div>
</div>
<div class="box_field_row">
<?php echo form_label($this->lang->line('module_item_unit_stock').':', 'quantity',array('class'=>'required','id'=>'quantity_label')); ?>
<div class='box_row'>
<?php if(empty($item_info->item_id))
	{
		echo form_input(
			array(
				'name'=>'quantity',
				'id'=>'quantity',
				'value'=>0,
				'title'=>''
				)
					);
	}
	else
	{
		echo form_input(
			array(
			'name'=>'quantity',
			'id'=>'quantity',
			'disabled'=>'disabled',
			'value'=>0,
			'title'=>''
			)
					);
	}
?>
</div>
</div>
<div class="box_field_row">
<?php echo form_label($this->lang->line('module_item_stock_minimum').':', 'reorder_level',array('class'=>'required','id'=>'reorder_level_label')); ?>
<div class='box_row'>
<?php echo form_input(
	array(
		'name'=>'reorder_level',
		'id'=>'reorder_level',
		'value'=>0,
		'title'=>''
		)
					);
?>
</div>
</div>
<div class="box_field_row">
<?php echo form_label($this->lang->line('common_description').':', 'description',array('class'=>'required')); ?>
<div class='box_row'>
<?php echo form_textarea(
	array(
		'name'=>'description',
		'id'=>'description',
		'value'=>"",
		'rows'=>'5',
		'cols'=>'17',
		'title'=>''
		)
					);
?>
</div>
</div>
<div class="box_field_row">
<?php echo form_label($this->lang->line('common_upload').' '.$this->lang->line('common_img').':', 'uploadfile',array('class'=>'required')); ?>
<div id="box_row_uploadfile" class='box_row'>
<?php echo form_upload(
	array(
		'multiple'=>'multiple',
		'accept'=>'gif|jpg|png',
		'class'=>'form-control',
		'name'=>'uploadfile[]',
		'id'=>'uploadfile',
		'size'=>'18',
		'title'=>''
		)
		 			);
?>

</div>
</div>
</div>
</div>
<!---aqui abajo se cierra el fieldset config template--->
</div>
<?php } 
else 
	{
		?>
<span id="subtitle_module"><?php echo $this->lang->line("module_home_select");?></span>
<?php
		foreach($allowed_modules->result() as $module)
		{
?>
			<div class="box_modules_home">
			<a class="img-module-home" href="<?php echo site_url("$module->module_id");?>">
			<img src="<?php echo base_url().'images/cc_images/menubar/big-icon/'.$module->module_id.'.png';?>" border="0" alt="" />
			</a>
			<a class="modules_name"  href="<?php echo site_url("$module->module_id");?>"><?php echo $this->lang->line("module_".$module->module_id) ?></a>
			<p class="descrip_module_home"><?php echo $this->lang->line('module_'.$module->module_id.'_desc');?></p>
			</div>
<?php
		}
	}
?>
	</div>
</div>
<?php $this->load->view("cc_views/partial/footer"); ?>