<?php
class Person extends CI_Model
{
	/*Determina si la persona existe mediante id*/
	function exists($person_id){
		$this->db->from('people');
		$this->db->where('people.person_id',$person_id);
		$query = $this->db->get();
		return ($query->num_rows()==1);
	}
	/*Determina si el correo existe en la bd*/
	function exists_mail($email){
		$this->db->from('people');
		$this->db->where('people.email',$email);
		$query = $this->db->get();
		return ($query->num_rows()==1);
	}
	/*Obtiene todos los datos de personas */
	function get_all($filtro,$limit=10000, $offset=0){
		$this->db->from('people');
		$this->db->order_by("last_name", "asc");
		$this->db->limit($limit);
		$this->db->offset($offset);
		return $this->db->get();
	}
	/*Devuelve la cantidad de personas registradas*/
	function count_all(){
		$this->db->from('people');
		$this->db->where('deleted',0);
		return $this->db->count_all_results();
	}
	/*Obtener los datos de una persona y devolverlos como un array*/
	function get_info($person_id){
		$query = $this->db->get_where('people', array('person_id' => $person_id), 1);
		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			/*crea objeto con propiedades vacías*/
			$fields = $this->db->list_fields('people');
			$person_obj = new stdClass;
			foreach ($fields as $field)
			{
				$person_obj->$field='';
			}
			return $person_obj;
		}
	}
	/*Obtiene datos de personas con los ids especifico*/
	function get_multiple_info($person_ids){
		$this->db->from('people');
		$this->db->where_in('person_id',$person_ids);
		$this->db->order_by("last_name", "asc");
		return $this->db->get();
	}
	/*Inserta o actualiza a una persona*/
	function save(&$person_data,$person_id=false){
		if (!$person_id or !$this->exists($person_id))
		{
			if ($this->db->insert('people',$person_data))
			{
				$person_data['person_id']=$this->db->insert_id();
				return true;
			}
				return false;
		}
		$this->db->where('person_id', $person_id);
		return $this->db->update('people',$person_data);
	}
	/*Elimina una persona mediante id*/
	function delete($person_id){
		return true;
	}
	/*Elimina una lista de personas (en realidad solo devuelve verdadero)jajajaja*/
	function delete_list(&$person_ids){
		return true;
 	}
}
?>