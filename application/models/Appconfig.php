<?php
class Appconfig extends CI_Model 
{
	/*verificar si existe app_config con key*/
	function exists($key){
		$this->db->from('app_config');
		$this->db->where('app_config.key',$key);
		$query = $this->db->get();
		return ($query->num_rows()==1);
	}
	/*Recupera todos los datos de app_config */
	function get_all(){
		$this->db->from('app_config');
		$this->db->order_by("key","asc");
		return $this->db->get();
	}
	/*Recupera datos de app_config y pasa los datos a $config_array con índice de key y su valor */
	function get_info(){
		$this->db->from('app_config');
		$this->db->order_by("key","asc");
		$query =$this->db->get();
		$config_array=array();
		foreach ($query->result_array() as $row)
		{
		$config_array[$row['key']]=$row['value'];
		}
		return $config_array;
	}
	/*Recupera datos de app_config mediante key y con limit 1, si no hay resultados devuelve "nada" */
	function get($key){
		$query = $this->db->get_where('app_config',array('key'=>$key),1);
		if($query->num_rows()==1)
		{
			return $query->row()->value;
		}
		return "";
	}
	/*Recibe una key y un valor, se compara key de app_config y se actualiza los datos*/
	function save($key,$value){
		/*Se crea array para el update(actualización de datos) */
		$config_data=
		array(
		   'key'=>$key,
		   'value'=>$value
		);
		$this->db->where('app_config.key',$key);
		$this->db->update('app_config',$config_data);
	}
	/*Recibe un array, donde key es la tabla y $value es el array de valores a guardar*/
	function batch_save(&$batch_save_data){
		$success=true;
		/*Ejecutar estas consultas como una transacción. queremos estar seguros de que hemos hecho todo o nada*/
		$this->db->trans_start();
		foreach($batch_save_data as $key=>$value)
		{
			$this->save($key,$value);
		}
		$this->db->trans_complete();
		return $success;
	}
	/*Elimina los datos de app_config, mediante la key*/
	function delete($key){
		return $this->db->delete('app_config',array('key'=>$key));
	}
	/*Elimina todos los datos de app_config*/
	function delete_all(){
		return $this->db->empty_table('app_config');
	}
}
?>