<?php
class Notificacion extends CI_Model
{
  /*Crea una nueva notificacion*/
  function make_new(&$notificacion_data){
	if($this->db->insert('notificaciones',$notificacion_data))
  {
		$notificacion_data['notificacion_id']=$this->db->insert_id();
		return true;
	}
	return false;
  }
  /*Cambia la notificacion a visto*/
  function change_to_seen($notificacion_id){
	  $this->db->where('notificacion_id', $notificacion_id);
		return $this->db->update('notificaciones',array('visto' => 1));
  }
  /*Obtiene información sobre una notificacion en particular */
  function get_info($notificacion_id){
		$this->db->from('notificaciones');
		$this->db->where('notificacion_id',$recurso_id);
		$query = $this->db->get();
		if($query->num_rows()==1){
			return $query->row();
		}
		else{
			$item_obj=new stdClass();
			$fields = $this->db->list_fields('notificaciones');
			foreach ($fields as $field){
				$item_obj->$field='';
			}
			return $item_obj;
		}
  }
  /*Obtiene la noificación mas reciente*/
  function get_newest(){
	  
  }
  /*Borra las notificaiones mas viejas*/
  function delete_oldest(){
	  
  }
  /*Cabia la prioridad de una notificacion*/
  function change_priority($prioridad){

  }
  /*Devuelve todas los notificaciones*/
  function get_all($limit=10000, $offset=0){
	$this->db->from('notificaciones');
	$this->db->where('visto',0);
	$this->db->order_by("time", "asc");
	$this->db->limit($limit);
	$this->db->offset($offset);
	return $this->db->get();
  }
}
?>