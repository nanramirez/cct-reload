<?php
class Supplier extends Person
{
	/*Verifica la exisencia de algun registro por el id*/
	function exists($person_id){
		$this->db->from('suppliers');
		$this->db->join('people', 'people.person_id = suppliers.person_id');
		$this->db->where('suppliers.person_id',$person_id);
		$query = $this->db->get();
		return ($query->num_rows()==1);
	}
	/* Devuelve todos los proveedores */
	function get_all($filtro,$limit=10000, $offset=0){
		if($filtro=="person_id" )
		{
			$filtro="suppliers.".$filtro;
		}
		elseif($filtro=="rfc" )
		{$filtro="last_name";}
		$this->db->from('suppliers');
		$this->db->join('people','suppliers.person_id=people.person_id');
		$this->db->where('deleted',0);
		$this->db->order_by($filtro, "asc");
		$this->db->limit($limit);
		$this->db->offset($offset);
		return $this->db->get();
	}
	/*Regresa el numero de registros de la tabla*/
	function count_all(){
		$this->db->from('suppliers');
		$this->db->where('deleted',0);
		return $this->db->count_all_results();
	}
	/*Obtiene información sobre un proveedor en particular */
	function get_info($supplier_id){
		$this->db->from('suppliers');
		$this->db->join('people', 'people.person_id = suppliers.person_id');
		$this->db->where('suppliers.person_id',$supplier_id);
		$query = $this->db->get();
		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			/* Obtenga el objeto principal base vacío, ya que $supplier_id NO es un proveedor */
			$person_obj=parent::get_info(-1);
			/* Obtenga todos los campos de la tabla de proveedores */
			$fields = $this->db->list_fields('suppliers');
			/* Añada esos campos al objeto padre base, tenemos un objeto vacío completo */
			foreach ($fields as $field)
			{
				$person_obj->$field='';
			}
			return $person_obj;
		}
	}
	/*Obtiene información multiple de varios proveedores */
	function get_multiple_info($suppliers_ids){
		$this->db->from('suppliers');
		$this->db->join('people', 'people.person_id = suppliers.person_id');
		$this->db->where_in('suppliers.person_id',$suppliers_ids);
		$this->db->order_by("last_name", "asc");
		return $this->db->get();
	}
	/*Guarda o actualiza la información de un proveedor*/
	function save_supplier(&$person_data, &$supplier_data,$supplier_id=null){
		$success=false;
		/* Ejecutar estas consultas como una transacción, queremos asegurarnos de que hacemos todo o nada */
		$this->db->trans_start();
		if(parent::save($person_data,$supplier_id))
		{
			if (!isset($supplier_id) or !$this->exists($supplier_id))
			{
				$supplier_data['person_id'] = $person_data['person_id'];
				$success = $this->db->insert('suppliers',$supplier_data);
			}
			else
			{
				$supplier_data["person_id"] =$supplier_id;
				$this->db->where('person_id', $supplier_id);
				$success = $this->db->update('suppliers',$supplier_data);
			}
		}
		$this->db->trans_complete();
		return $success;
	}
	/*Cambia el estado deleted=1 de un proveedor*/
	function delete($supplier_id){
		$this->db->where('person_id', $supplier_id);
		return $this->db->update('suppliers', array('deleted' => 1));
	}
	/*Cambia el estado deleted=1 de un lista de proveedores*/
	function delete_list(&$suppliers_to_delete){
		$this->db->trans_start();
		foreach($suppliers_to_delete as $delete_id)
		{
			$this->db->where_in('person_id',$delete_id);
			$this->db->update('suppliers', array('deleted' => 1));
		}
		return $this->db->trans_complete();
 	}
	/*Obtiene la sugerencia de busquedas por filtro*/
	function get_search_suggestions($search,$limit=25,$filtro, $offset=0){
		$suggestions = array();
		if($filtro=="company")
		{
			$this->db->from("suppliers");
			$this->db->join("people","suppliers.person_id=people.person_id");
			$this->db->where("deleted", 0);
			$this->db->like("company",$search);
			$this->db->order_by("company", "asc");
			$consulta = $this->db->get();
			foreach($consulta->result() as $row)
			{
			$suggestions[]=$row->company;
			}
		}
		elseif($filtro=="email")
		{
			$this->db->from("suppliers");
			$this->db->join("people","suppliers.person_id=people.person_id");
			$this->db->where("deleted", 0);
			$this->db->like("email",$search);
			$this->db->order_by("email", "asc");
			$consulta = $this->db->get();
			foreach($consulta->result() as $row)
			{
			$suggestions[]=$row->email;
			}
		}
		elseif($filtro=="person_id")
		{
			$this->db->from("suppliers");
			$this->db->join("people","suppliers.person_id=people.person_id");
			$this->db->where("deleted", 0);
			$this->db->like("suppliers.person_id",$search);
			$this->db->order_by("suppliers.person_id", "asc");
			$consulta = $this->db->get();
			foreach($consulta->result() as $row)
			{
			$suggestions[]=$row->person_id;
			}
		}
		elseif($filtro=="last_name")
		{
			$this->db->from("suppliers");
			$this->db->join("people","suppliers.person_id=people.person_id");
			$this->db->where("(first_name LIKE '%".$this->db->escape_like_str($search)."%' or 
			last_name LIKE '%".$this->db->escape_like_str($search)."%' or 
			CONCAT(`first_name`,' ',`last_name`) LIKE '%".$this->db->escape_like_str($search)."%') and deleted=0");
			$this->db->order_by("last_name", "asc");
			$consulta = $this->db->get();
			foreach($consulta->result() as $row)
			{
			$suggestions[]=$row->first_name.' '.$row->last_name;
			}
		}
		elseif($filtro=="rfc")
		{
			$this->db->from("suppliers");
			$this->db->join("fiscales","suppliers.person_id=fiscales.person_id");
			$this->db->where("deleted", 0);
			$this->db->like($filtro,$search);
			$consulta = $this->db->get();
			foreach($consulta->result() as $row)
			{
			$suggestions[]=$row->rfc;
			}
		}
		return $suggestions;
	}
	/*Obtiene la sugerencia de busquedas del panel de entradas*/
	function get_search_suggestions_receivings($search,$limit=25){
		$this->db->from("suppliers");
		$this->db->join("people","suppliers.person_id=people.person_id");
		$this->db->where("(company LIKE '%".$this->db->escape_like_str($search)."%' or
		first_name LIKE '%".$this->db->escape_like_str($search)."%' or 
		last_name LIKE '%".$this->db->escape_like_str($search)."%' or 
		CONCAT(`first_name`,' ',`last_name`) LIKE '%".$this->db->escape_like_str($search)."%') and deleted=0");
		$this->db->order_by("last_name", "asc");
		$consulta = $this->db->get();
		foreach($consulta->result() as $row)
		{
		$suggestions[]=$row->person_id.'|'.$row->first_name.' '.$row->last_name.'('.$row->company.')';
		}
		/*$this->db->from("suppliers");
		$this->db->join("people","suppliers.person_id=people.person_id");
		$this->db->where("deleted", 0);
		$this->db->like("email",$search);
		$this->db->order_by("email", "asc");
		$consulta = $this->db->get();
		foreach($consulta->result() as $row)
		{
		$suggestions[]=$row->person_id.'|'.$row->email;
		}*/
		if(count($suggestions) > $limit)
		{
		$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;
	}
	/*Obtiene la sugerencia de busquedas*/
	function get_suppliers_search_suggestions($search,$limit=25){
		$suggestions = array();
		$this->db->from('suppliers');
		$this->db->join('people','suppliers.person_id=people.person_id');
		$this->db->where('deleted', 0);
		$this->db->like("company",$search);
		$this->db->order_by("company", "asc");
		$by_company_name = $this->db->get();
		foreach($by_company_name->result() as $row)
		{
			$suggestions[]=$row->company;
		}
		$this->db->from('suppliers');
		$this->db->join('people','suppliers.person_id=people.person_id');
		$this->db->where("(first_name LIKE '%".$this->db->escape_like_str($search)."%' or 
		last_name LIKE '%".$this->db->escape_like_str($search)."%' or 
		CONCAT(`first_name`,' ',`last_name`) LIKE '%".$this->db->escape_like_str($search)."%') and deleted=0");
		$this->db->order_by("last_name", "asc");
		$by_name = $this->db->get();
		foreach($by_name->result() as $row)
		{
			$suggestions[]=$row->first_name.' '.$row->last_name;
		}
		/* Solo devuelve sugerencias de $limit */
		if(count($suggestions) > $limit)
		{
			$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;
	}
	/*Busqueda general*/
	function search($search){
		$this->db->from('suppliers');
		$this->db->join('people','suppliers.person_id=people.person_id');
		$this->db->where("(first_name LIKE '%".$this->db->escape_like_str($search)."%' or 
		last_name LIKE '%".$this->db->escape_like_str($search)."%' or 
		company LIKE '%".$this->db->escape_like_str($search)."%' or 
		email LIKE '%".$this->db->escape_like_str($search)."%' or 
		phone_number LIKE '%".$this->db->escape_like_str($search)."%' or 
		account_number LIKE '%".$this->db->escape_like_str($search)."%' or 
		CONCAT(`first_name`,' ',`last_name`) LIKE '%".$this->db->escape_like_str($search)."%') and deleted=0");
		$this->db->order_by("last_name", "asc");
		return $this->db->get();
	}
	/*Busqueda especifica por filtro*/
	function specific_search($search,$filtro,$offset=0){
		if ($filtro=="last_name")
		{
			
			$this->db->from("suppliers");
			$this->db->join("people","suppliers.person_id=people.person_id");
			$this->db->where("(first_name LIKE '%".$this->db->escape_like_str($search)."%' ESCAPE '!' or 
			last_name LIKE '%".$this->db->escape_like_str($search)."%' ESCAPE '!' or 
			CONCAT(`first_name`,' ',`last_name`) LIKE '%".$this->db->escape_like_str($search)."%' ESCAPE '!') and deleted=0");
			$this->db->order_by("last_name", "asc");
			return $this->db->get();
		}
		elseif($filtro=="person_id")
		{
			$this->db->from("suppliers");
			$this->db->join("people","suppliers.person_id=people.person_id");
			$this->db->where("deleted", 0);
			$this->db->like("suppliers.person_id",$search);
			$this->db->order_by("suppliers.person_id", "asc");
			return 	$this->db->get();
	
		}
		/*esta opcion complementa a la de compañia */
		/*elseif($filtro=="email")
		{
			$this->db->from("suppliers");
			$this->db->join("people","suppliers.person_id=people.person_id");
			$this->db->where("(".$filtro." LIKE '%".$this->db->escape_like_str($search)."%') and deleted=0");
			$this->db->order_by("".$filtro."", "asc");
			return $this->db->get();
		}*/
		elseif($filtro=="rfc")
		{
			$this->db->from("suppliers");
			$this->db->join("fiscales","suppliers.person_id=fiscales.person_id");
			$this->db->join("people","suppliers.person_id=people.person_id");
			$this->db->where("(".$filtro." LIKE '%".$this->db->escape_like_str($search)."%' ESCAPE '!') and deleted=0");
			$this->db->order_by("".$filtro."", "asc");
			return $this->db->get();
		}
		/*asi podemos tener el filtro de correo y el de compañia*/
		else
		{
			$this->db->from("suppliers");
			$this->db->join("people","suppliers.person_id=people.person_id");
			$this->db->where("(".$filtro." LIKE '%".$this->db->escape_like_str($search)."%' ESCAPE '!') and deleted=0");
			$this->db->order_by("".$filtro."", "asc");
			return $this->db->get();
		}
	}
}
?>