<?php
class Item_taxes extends CI_Model
{
	/*Obtiene información de impuestos para un artículo en particular*/
	function get_info($item_id){
		$this->db->from('items_taxes');
		$this->db->where('item_id',$item_id);
		/* devolver una variedad de impuestos para un artículo */
		return $this->db->get()->result_array();
	}
	/*Inserta o actualiza los impuestos de un artículo*/
	function save(&$items_taxes_data, $item_id){
		/* Ejecutar estas consultas como una transacción, queremos asegurarnos de que hacemos todo o nada */
		$this->db->trans_start();
		$this->delete($item_id);
		foreach ($items_taxes_data as $row)
		{
			$row['item_id'] = $item_id;
			$this->db->insert('items_taxes',$row);
		}
		$this->db->trans_complete();
		return true;
	}
	/*Guarda los impuestos de un item*/
	function save_multiple(&$items_taxes_data, $item_ids){
		foreach($item_ids as $item_id){
			$this->save($items_taxes_data, $item_id);
		}
	}
	/* Elimina impuestos dado un artículo*/
	function delete($item_id){
		return $this->db->delete('items_taxes', array('item_id' => $item_id)); 
	}
}
?>