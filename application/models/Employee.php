<?php
class Employee extends Person
{
	/*Determina si una $person_id es un empleado*/
	function exists($person_id){
		$this->db->from('employees');
		$this->db->join("people","people.person_id = employees.person_id");
		$this->db->where('employees.person_id',$person_id);
		$query = $this->db->get();
		return ($query->num_rows()==1);
	}
	/*Recupera todos los empleados con una unión de persona (person_id), con un limit y offset (mostrara cantidad dependiendo de las variables) */
	function get_all_wf($limit=10000, $offset=0){
		$this->db->from('employees');
		$this->db->join('people','employees.person_id=people.person_id');
		$this->db->order_by("employees.person_id", "asc");
		$this->db->limit($limit);
		$this->db->offset($offset);
		return $this->db->get();
	}
	/*Recupera todos los datos de empleado y persona (unión por person_id), ordenado por person_id o last_name. aplicando limit y offset para mostrar una cantidad controlable */
	function get_all($filtro,$limit=10000, $offset=0){
		if($filtro=="person_id" )
		{
			$filtro="employees.".$filtro;
		}
		elseif($filtro=="rfc" )
		{$filtro="last_name";}
		$this->db->from('employees');
		$this->db->where('deleted',0);
		$this->db->where('employees.person_id!=',101);
		$this->db->join('people','employees.person_id=people.person_id');
		$this->db->order_by($filtro, "asc");
		$this->db->limit($limit);
		$this->db->offset($offset);
		return $this->db->get();
	}
	/*Recupera la cantidad de empleados resultado numérico*/
	function count_all(){
		$this->db->from('employees');
		$this->db->where('deleted',0);
		return $this->db->count_all_results();
	}
	/*Obtiene información sobre empleado mediante búsqueda de id(employee_id)*/
	function get_info($employee_id){
		$this->db->from('employees');
		$this->db->join('people', 'people.person_id = employees.person_id');
		$this->db->where('employees.person_id',$employee_id);
		$query = $this->db->get();
		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			/*$persona_obj recupera un objeto vació ya que no hay empleado con $employee_id*/
			$person_obj=parent::get_info(-1);
			/*Obtener todos los campos de la tabla employees*/
			$fields = $this->db->list_fields('employees');
			/*añadimos campos de $fields al objeto de $person_obj el cual esta vació*/
			foreach ($fields as $field)
			{
				$person_obj->$field='';
			}
			return $person_obj;
		}
	}
	/*Obtenemos información sobre múltiples empleados mediante ids*/
	function get_multiple_info($employee_ids){
		$this->db->from('employees');
		$this->db->join('people', 'people.person_id = employees.person_id');
		$this->db->where_in('employees.person_id',$employee_ids);
		$this->db->order_by("last_name", "asc");
		return $this->db->get();
	}
	/*insertar o actualizar un empleado*/
	function save_employee(&$person_data, &$employee_data,&$permission_data,$employee_id=false){
		$success=false;
		/*Ejecutar las siguientes consultas como una transacción, queremos asegurarnos de que se hace todo o nada*/
		$this->db->trans_start();
		if(parent::save($person_data,$employee_id))
		{
			if (!isset($employee_id) or !$this->exists($employee_id))
			{
				$employee_id = $person_data["person_id"];
				$employee_data["person_id"]=$employee_id;
				$success = $this->db->insert("employees",$employee_data);
			}
			else
			{
				$this->db->where("person_id", $employee_id);
				$success = $this->db->update("employees",$employee_data);
			}
			/*Hemos insertado o actualizado un nuevo empleado, ahora estableceremos permisos*/
			if($success)
			{
				/*Primero borraremos todos los permisos que el empleado tenga actualmente*/
				$success=$this->db->delete("permissions", array("person_id" => $employee_id));
				/*Ahora insertamos los nuevos permisos*/
				if($success)
				{
					foreach($permission_data as $allowed_module)
					{
						$success = $this->db->insert("permissions",
						array(
						"module_id"=>$allowed_module,
						"person_id"=>$employee_id));
					}
				}
			}
		}
		$this->db->trans_complete();
		/*if ($this->db->trans_status() === FALSE)
		{
			$success=false;
		}*/
		return $success;
	}
	/*Eliminar un empleado*/
	function delete($employee_id){
		$success=false;
		/*No permite eliminar el empleado si el empleado es el mismo de la sesión*/
		if($employee_id==$this->get_logged_in_employee_info()->person_id)
			return false;
		/*Ejecutar las siguientes consultas como una transacción, queremos asegurarnos de que se hace todo o nada*/
		$this->db->trans_start();
		/*Eliminar permisos*/
		if($this->db->delete('permissions', array('person_id' => $employee_id)))
		{
			$this->db->where('person_id', $employee_id);
			$success = $this->db->update('employees', array('deleted' => 1));
		}
		$this->db->trans_complete();
		return $success;
	}
	/*Eliminar lista de empleados*/
	function delete_list(&$employee_ids){
		/*No permite eliminar el empleado si el empleado es el mismo de la session*/
		if(in_array($this->get_logged_in_employee_info()->person_id,$employee_ids))
		return false;
		/*Ejecutar las siguientes consultas como una transacción, queremos asegurarnos de que se hace todo o nada*/
		$this->db->trans_start();
		/*Elimina el permiso y cambia deleted a 1*/
		foreach($employee_ids as $delete_id)
		{
			if ($this->db->delete('permissions',$employee_ids))
			{
				$this->db->where_in('person_id',$employee_ids);
				$this->db->update('employees', array('deleted' => 1));
			}
		}
		return $this->db->trans_complete();
 	}
	/*obtener búsquedas sugeridas para encontrar empleados*/
	function get_search_suggestions($search,$limit=25,$filtro){
		$suggestions = array();
		/*búsqueda por nombre*/
		if($filtro=="last_name")
		{
			$this->db->from("employees");
			$this->db->join("people","employees.person_id=people.person_id");
			$this->db->where("(first_name LIKE '%".$this->db->escape_like_str($search)."%' or 
			last_name LIKE '%".$this->db->escape_like_str($search)."%' or 
			CONCAT(`first_name`,' ',`last_name`) LIKE '%".$this->db->escape_like_str($search)."%') and deleted=0");
			$this->db->where('employees.person_id!=',101);
			$this->db->order_by("last_name", "asc");
			$consulta = $this->db->get();
			foreach($consulta->result() as $row)
			{
			$suggestions[]=$row->first_name.' '.$row->last_name;
			}
		}
		/*búsqueda por correo*/
		elseif($filtro=="email")
		{
			$this->db->from("employees");
			$this->db->join("people","employees.person_id=people.person_id");
			$this->db->where("deleted", 0);
			$this->db->where('employees.person_id!=',101);
			$this->db->like("email",$search);
			$this->db->order_by("email", "asc");
			$consulta = $this->db->get();
			foreach($consulta->result() as $row)
			{
			$suggestions[]=$row->email;
			}
		}
		/*búsqueda por id*/
		elseif($filtro=="person_id")
		{
			$this->db->from("employees");
			$this->db->join("people","employees.person_id=people.person_id");
			$this->db->where("deleted", 0);
			$this->db->where('employees.person_id!=',101);
			$this->db->like("employees.person_id",$search);
			$this->db->order_by("employees.person_id", "asc");
			$consulta = $this->db->get();
			foreach($consulta->result() as $row)
			{
			$suggestions[]=$row->person_id;
			}
		}
		
		return $suggestions;
	}
	/*Realiza una búsqueda de los empleados*/
	function search($search){
		$this->db->from('employees');
		$this->db->join('people','employees.person_id=people.person_id');
		$this->db->where("(first_name LIKE '%".$this->db->escape_like_str($search)."%' ESCAPE '!' or 
		last_name LIKE '%".$this->db->escape_like_str($search)."%' ESCAPE '!' or 
		email LIKE '%".$this->db->escape_like_str($search)."%' ESCAPE '!' or 
		phone_number LIKE '%".$this->db->escape_like_str($search)."%' ESCAPE '!' or 
		username LIKE '%".$this->db->escape_like_str($search)."%' ESCAPE '!' or 
		CONCAT(`first_name`,' ',`last_name`) LIKE '%".$this->db->escape_like_str($search)."%' ESCAPE '!') and deleted=0");
		$this->db->order_by("last_name", "asc");
		return $this->db->get();
	}
	/*realiza una búsqueda especifica*/
	function specific_search($search,$filtro){
		/*búsqueda por nombre*/
		if ($filtro=="last_name")
		{
			$this->db->from("employees");
			$this->db->join("people","employees.person_id=people.person_id");
			$this->db->where('employees.person_id!=',101);
			$this->db->where("(first_name LIKE '%".$this->db->escape_like_str($search)."%' ESCAPE '!' or 
			last_name LIKE '%".$this->db->escape_like_str($search)."%' ESCAPE '!' or 
			CONCAT(`first_name`,' ',`last_name`) LIKE '%".$this->db->escape_like_str($search)."%' ESCAPE '!') and deleted=0");
			$this->db->order_by("last_name", "asc");
			return $this->db->get();
			
		}
        /*búsqueda por rfc*/
		elseif ($filtro=="rfc")
		{
			$this->db->from("employees");
			$this->db->join("fiscales","employees.person_id=fiscales.person_id");
			$this->db->join("people","employees.person_id=people.person_id");
			$this->db->where("(rfc LIKE '%".$this->db->escape_like_str($search)."%' ESCAPE '!') and deleted=0");
			$this->db->order_by("last_name", "asc");
			return $this->db->get();
			
		}
		/*búsqueda por filtro*/
		elseif($filtro!="person_id" && $filtro!="last_name" )
		{
			$this->db->from("employees");
			/* se cambio esta linea por que si rfc no esta relacionado con persona no aparece correo */
			/*$this->db->join("fiscales","employees.person_id=fiscales.person_id");^*/
			$this->db->join("people","employees.person_id=people.person_id");
			$this->db->where('employees.person_id!=',101);
			$this->db->where("(".$filtro." LIKE '%".$this->db->escape_like_str($search)."%' ESCAPE '!') and deleted=0");
			$this->db->order_by("".$filtro."", "asc");
			return $this->db->get();
		}
		/*búsqueda por id */
		else
		{
			$this->db->from("employees");
			$this->db->join("people","employees.person_id=people.person_id");
			$this->db->where("deleted", 0);
			$this->db->where('employees.person_id!=',101);
			$this->db->like("employees.person_id",$search);
			$this->db->order_by("employees.person_id", "asc");
			return 	$this->db->get();
		}
	}
	/*cuando se intenta iniciar sesión con el empleado y establecer la sesión. devuelve un resultado booleano.*/
	function login($username, $password){
		$query = $this->db->get_where('employees', array('username' => $username,'password'=>md5($password), 'deleted'=>0), 1);
		if ($query->num_rows() ==1)
		{
			$row=$query->row();
			$this->session->set_userdata('person_id', $row->person_id);
			return true;
		}
		return false;
	}
	/*Desconecta al usuario y destruye todos los datos de sesión, al final redirigiré a inicia sesión*/
	function logout(){
		$this->session->sess_destroy();
		redirect('login');
	}
	/*determina si un empleado esta conectado*/
	function is_logged_in(){
		return $this->session->userdata('person_id')!=false;
	}
	/*Obtiene información sobre el empleado actualmente conectado*/
	function get_logged_in_employee_info(){
		if($this->is_logged_in())
		{
			return $this->get_info($this->session->userdata('person_id'));
		}
		return false;
	}
	/*Determinar si el empleado especificado tiene acceso al modulo*/
	function has_permission($module_id,$person_id){
		/*Si modulo_id es diferente a null, permite el acceso*/
		if($module_id==null)
		{
			return true;
		}
		$query = $this->db->get_where('permissions', array('person_id' => $person_id,'module_id'=>$module_id), 1);
		return $query->num_rows() == 1;
		return false;
	}
}
?>