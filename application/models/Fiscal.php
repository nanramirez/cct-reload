<?php
class Fiscal extends CI_Model
{
	/*Verifica la exisencia de algun registro por el id*/
	function exists($rfc){
		$this->db->from('fiscales');
		$this->db->where('rfc',$rfc);
		$query = $this->db->get();
		return ($query->num_rows()==1);
	}
	/*Obtiene información fiscal de una persona en particular*/
	function get_info($person_id){
		$this->db->from('fiscales');
		$this->db->where('person_id',$person_id);
		$query = $this->db->get();
		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			$person_obj=new stdClass();
			/* Obtener todos los campos de la tabla de empleados */
			$fields = $this->db->list_fields('fiscales');
			/* Añada esos campos al objeto padre base, tenemos un objeto vacío completo */
			foreach ($fields as $field)
			{
				$person_obj->$field='';
			}
			return $person_obj;
		}
	}
	/*Verifica la exisencia de alguna persona por el id*/
	function exists_person($person_id){
		$this->db->from('fiscales');
		$this->db->join('people', 'people.person_id = fiscales.person_id');
		$this->db->where('fiscales.person_id',$person_id);
		$query = $this->db->get();
		return ($query->num_rows()==1);
	}
	/*Guarda lo datos fiscales de una persona*/
	function save_datos_fiscales(&$fiscal_data,$person_id){
		$success=false;
		$this->db->trans_start();
		if(!$this->exists($fiscal_data["rfc"]) and !$this->exists_person($person_id))
		{
			$success = $this->db->insert("fiscales",$fiscal_data);
		}
		else
		{
			$this->db->where("person_id", $person_id);
			$success = $this->db->update("fiscales",$fiscal_data);
		}
		$this->db->trans_complete();
		return $success;
	}
}
?>