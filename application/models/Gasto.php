<?php
class Gasto extends CI_Model
{
	/*Verifica si el gasto existe*/
	function exists($gasto_id){
		$this->db->from('gastos');
		$this->db->where('gasto_id',$gasto_id);
		$query = $this->db->get();
		return ($query->num_rows()==1);
	}
	/*Obtiene todos los gastos */
	function get_all($limit=10000, $offset=0, $mes=0, $estado="Pendiente"){
		$this->db->from('gastos');
		$num_dias=date('t',$mes);
		if($mes==date('n'))
		{
			$this->db->where('fecha BETWEEN "'. date('Y-'.date('n').'-01 00:00:00'). '" and "'. date('Y-'.date('n').'-'.date('t',date('n')).' 00:00:00)"'));
		}
		elseif($mes>date('n'))
		{
			$this->db->where('fecha BETWEEN "'. date('Y-'.$mes.'-01 00:00:00'). '" and "'. date('Y-'.$mes.'-'.$num_dias.' 00:00:00)"'));
		}
		elseif($mes<date('n') && $mes!=0)
		{
			$this->db->where('fecha BETWEEN "'. date('Y-'.$mes.'-01 00:00:00'). '" and "'. date('Y-'.$mes.'-'.$num_dias.' 00:00:00)"'));
		}
		$this->db->where('estado', $estado);
		$this->db->order_by("fecha", "asc");
		$this->db->limit($limit);
		$this->db->offset($offset);
		return $this->db->get();
	}
	/*Cuenta la cantidad de gastos*/
	function count_all($mes=0){
		if($mes>0)
		{
			$this->db->where('fecha BETWEEN "'. date('Y-'.$mes.'-01 00:00:00'). '" and "'. date('Y-'.++$mes.'-01 00:00:00)"'));
		}
		$this->db->from('gastos');
		return $this->db->count_all_results();
	}
	/*Obtiene la informacion especifica de un gasto*/
	function get_info($gasto_id){
		$this->db->from('gastos');
		$this->db->where('gasto_id',$gasto_id);
		$query = $this->db->get();
		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			/* Obtiene el objeto principal base vacío, ya que $item_id NO es un elemento */
			$item_obj=new stdClass();
			/* Obtener todos los campos de la tabla de elemento */
			$fields = $this->db->list_fields('gastos');
			foreach ($fields as $field)
			{
				$item_obj->$field='';
			}
			return $item_obj;
		}
	}
	/*Guarda un gasto*/
	function save(&$gastos_data,$gasto_id=false){
		if(!$gasto_id or !$this->exists($gasto_id))
		{
			if($this->db->insert('gastos',$gastos_data))
			{
				$gastos_data['gasto_id']=$this->db->insert_id();
				return true;
			}
			return false;
		}
		$this->db->where('gasto_id', $gasto_id);
		return $this->db->update('gastos',$gastos_data);
	}
	/*Cambia el estado deleted=1 de un gasto hecho*/
	function cancelar($gasto_id){
		$this->db->where('gasto_id', $gasto_id);
		return $this->db->update('gastos', array('estado' => 1)); 
	}
	/*Busqueda*/
	function search($search){
		$this->db->from('gastos');
		$this->db->where("fecha LIKE '%".$this->db->escape_like_str($search)."%' ESCAPE '!' or 
		estado LIKE '%".$this->db->escape_like_str($search)."%' ESCAPE '!'");
		$this->db->order_by("fecha", "DESC");
		return $this->db->get();
	}
	/*Obtiene la sugerencia de la categoria*/
	function get_category_suggestions($search,$limit=25){
		$suggestions = array();
		$this->db->distinct();
		$this->db->select('category');
		$this->db->from('gastos');
		$this->db->like('category', $search);
		$this->db->order_by("category", "asc");
		$by_category = $this->db->get();
		foreach($by_category->result() as $row)
		{
			$suggestions[]=$row->category;
		}
		if(count($suggestions) > $limit)
		{
		$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;
	}
	/*Obtiene las sugerencias de busqueda por filtro*/
	function get_search_suggestions($search,$filtro,$estado){
		$suggestions = array();
		if($filtro=="concepto")
		{
			$this->db->from('gastos');
			$this->db->like('concepto', $search);
			$this->db->where('estado',$estado);
			$this->db->order_by("concepto", "asc");
			$by_name = $this->db->get();
			foreach($by_name->result() as $row)
			{
				$suggestions[]=$row->concepto;
			}
		}
		if($filtro=="category")
		{
			$this->db->distinct();
			$this->db->select('category');
			$this->db->from('gastos');
			$this->db->like('category', $search);
			$this->db->where('estado',$estado);
			$this->db->order_by("category", "asc");
			$by_name = $this->db->get();
			foreach($by_name->result() as $row)
			{
				$suggestions[]=$row->category;
			}
		}
		elseif($filtro=="fecha")
		{
			$this->db->from('gastos');
			$this->db->like('fecha', $search);
			$this->db->where('estado',$estado);
			$this->db->order_by("fecha", "asc");
			$by_name = $this->db->get();
			foreach($by_name->result() as $row)
			{
				$fecha=explode(" ", $row->fecha);
				$suggestions[]=$fecha[0];
			}
		}
		return $suggestions;
	}
	/*Busqueda especifica por filtro*/
	function specific_search($search,$filtro,$estado){
		$this->db->from('gastos');
		$this->db->like($filtro, $search);
		$this->db->where('estado',$estado);
		/*$this->db->where("(".$filtro." LIKE '%".$this->db->escape_like_str($search)."%' ESCAPE '!') and estado=".$estado);*/
		$this->db->order_by($filtro, "asc");
		return $this->db->get();
	}
	/*Obtiene el monto total de todos los gastos pendientes de la misma moneda en un mes especifico*/
	function undone_mes($mes,$coin){
		$this->db->where('estado', 'Pendiente');
		if($mes>0)
		{
			$this->db->where('fecha BETWEEN "'. date('Y-'.$mes.'-01 00:00:00'). '" and "'. date('Y-'.++$mes.'-01 00:00:00)"'));
		}
		$query = $this->db->select_sum('cantidad', 'totales');
		$this->db->where('coin', $coin);
		$query = $this->db->get('gastos');
		$result = $query->result();
		if(isset($result[0]->totales)){
			return $result[0]->totales;
		}else
		{
			return 0;
		}
	}
	/*Obtiene el monto total de todos los gastos hechos de la misma moneda en un mes especifico*/
	function done_mes($mes,$coin){
		$this->db->where('estado', 'Hecho');
		if($mes>0)
		{
			$this->db->where('fecha BETWEEN "'. date('Y-'.$mes.'-01 00:00:00'). '" and "'. date('Y-'.++$mes.'-01 00:00:00)"'));
		}
		$query = $this->db->select_sum('cantidad', 'totales');
		$this->db->where('coin', $coin);
		$query = $this->db->get('gastos');
		$result = $query->result();
		if(isset($result[0]->totales)){
			return $result[0]->totales;
		}else
		{
			return 0;
		}
	}
	/*Obtiene el monto total de todos los gastos pendientes de la misma moneda*/
	function undone_total($coin){
		$this->db->where('estado', 'Pendiente');
		$query = $this->db->select_sum('cantidad', 'totales');
		$this->db->where('coin', $coin);
		$query = $this->db->get('gastos');
		$result = $query->result();
		if(isset($result[0]->totales)){
			return $result[0]->totales;
		}else
		{
			return 0;
		}
	}
	/*Obtiene el monto total de todos los gastos hechos de la misma moneda*/
	function done_total($coin){
		$this->db->where('estado', 'Hecho');
		$query = $this->db->select_sum('cantidad', 'totales');
		$this->db->where('coin', $coin);
		$query = $this->db->get('gastos');
		$result = $query->result();
		if(isset($result[0]->totales)){
			return $result[0]->totales;
		}
		else{
			return 0;
		}
	}
	/*Borra el registro(s) del gasto(s) en la tabla solo si es un gasto(s) pendiente*/
	function delete_list(&$gastos_to_delete){
		$this->db->trans_start();
		foreach($gastos_to_delete as $delete_id){
			$this->db->where_in('gasto_id', $delete_id);
			$this->db->where_in('estado',0);
			$this->db->delete('gastos');
		}
		return $this->db->trans_complete();
 	}
	/*Genera un gasto nuevo recurrente automatico por el resto del año en curso*/
	function generar_gasto_recurrente($suscrito,$corte,$monto,$concepto){
		$config_info=$this->Appconfig->get_info();
		$show_coin=$config_info["coin"];
		if($config_info["xchange_show"]=="1")$show_coin=$config_info["coin2"];
		$succes=false;
		$jours=array();	/*$suscriptor_data=$this->get_info_by_customer($customer_id,$concepto,"Pendiente");*/
			/*$this->renew_list($customer_id,$this->lang->line("module_gasto_recurrente"));*/
			if($suscrito=="mensual")
			{	
				/* $this->update_list($customer_id,$this->lang->line("module_gasto").' '.'mensual'.' '.$mes); */
				$fecha_corte=explode("/", $corte);
				$meses_restantes=12-intval($fecha_corte[1]);
				if($fecha_corte[1]==date('m'))$meses_restantes=12-date('n');
				for($i=0;$i<=$meses_restantes;$i++)
				{
					$mes=date("F",strtotime(date('Y').'-'.(13-($i+1)).'-'.$fecha_corte[0]));
					if($config_info["language"]=="ES")
					{
					switch ($mes) {
					case "January":
					$mes="Enero";
					break;
					case "February":
					$mes="Febrero";
					break;
					case "March":
					$mes="Marzo";
					break;
					case "April":
					$mes="Abril";
					break;
					case "May":
					$mes="Mayo";
					break;
					case "June":
					$mes="Junio";
					break;
					case "July":
					$mes="Julio";
					break;
					case "August":
					$mes="Agosto";
					break;
					case "September":
					$mes="Septiembre";
					break;
					case "October":
					$mes="Octubre";
					break;
					case "November":
					$mes="Noviembre";
					break;
					case "December":
					$mes="Diciembre";
					break;
					default:
					/* echo "Your favorite color is neither red, blue, nor green!"; */
					}
					}
					$gastos_data = array(
					'fecha'=>date('Y').'-'.(13-($i+1)).'-'.$fecha_corte[0].' 00:00:00',
					'concepto'=>$concepto.'-'.$suscrito.' '.$mes,
					'coin'=>$show_coin,
					'estado'=>"Pendiente",
					'cantidad'=>$monto,
					'category'=>$this->lang->line("module_gasto_recurrente"),
					'recurso_id'=>1,
					"suscrito"=>$suscrito,
					"receiving_id"=>-1
					);
					if($this->save($gastos_data))
					{
					$succes=true;
					}
				}
			}
			elseif($suscrito=="semanal")
			{
			/*$this->update_list($customer_id,$this->lang->line("module_gasto").' '.'semanal'); */
			$fecha_corte=explode("/", $corte);
			$dias_restantes=date('z',strtotime(date('Y').'-'.$fecha_corte[1].'-01'));
			if($fecha_corte[1]==date('m'))$dias_restantes=date('z');
			for($i=$dias_restantes;$i<=365;$i++)
			{
			$dias_restantes-=$i;
			$fecha = DateTime::createFromFormat('z', $i);
			$week_day = DateTime::createFromFormat('Y-m-d',date('Y').'-'.$fecha_corte[1].'-'.$fecha_corte[0]);
			/*$fecha->format('w'); */

			if($fecha->format('w')==$week_day->format('w'))
			{
			$gastos_data = array(
			'fecha'=>$fecha->format('Y-m-d H:i:s'),
			'concepto'=>$concepto.'-'.$suscrito.' #'.date('W',strtotime($fecha->format('Y-m-d'))),
			'coin'=>$show_coin,
			'estado'=>"Pendiente",
			'cantidad'=>$monto,
			'category'=>$this->lang->line("module_gasto_recurrente"),
			'recurso_id'=>1,
			"suscrito"=>$suscrito
			);
			if($this->save($gastos_data))
			{
			$succes=true;
			}
			}
			}

			}
			elseif($suscrito=="quincenal")
			{
			/* $this->update_list($customer_id,$this->lang->line("module_gasto").' '.'quincenal'); */
			$fecha_corte=explode("/", $corte);
			$meses_restantes=intval($fecha_corte[1]);
			if(intval($fecha_corte[1])==date('n'))$meses_restantes=date('n');
			for($i=$meses_restantes;$i<=12;$i++)
			{
			$corte2=0;
			$mes=date("F",strtotime(date('Y').'-'.(13-($i+1)).'-'.$fecha_corte[0]));
			if($config_info["language"]=="ES")
			{
			switch ($mes) {
			case "January":
			$mes="Enero";
			break;
			case "February":
			$mes="Febrero";
			break;
			case "March":
			$mes="Marzo";
			break;
			case "April":
			$mes="Abril";
			break;
			case "May":
			$mes="Mayo";
			break;
			case "June":
			$mes="Junio";
			break;
			case "July":
			$mes="Julio";
			break;
			case "August":
			$mes="Agosto";
			break;
			case "September":
			$mes="Septiembre";
			break;
			case "October":
			$mes="Octubre";
			break;
			case "November":
			$mes="Noviembre";
			break;
			case "December":
			$mes="Diciembre";
			break;
			default:
			/* echo "Your favorite color is neither red, blue, nor green!"; */
			}
			}
			if($fecha_corte[0]<15 && $fecha_corte[0]+15<=25)
			{	
			$corte2=$fecha_corte[0]+15;
			}
			elseif($fecha_corte[0]<15 && $fecha_corte[0]+15>=25)
			{
			$fecha_corte[0]=10;
			$corte2=25;
			}
			elseif($fecha_corte[0]>15)
			{
			$corte2=$fecha_corte[0]-15;
			}
			elseif($fecha_corte[0]==15)
			{
			$fecha_corte[0]=1;
			$corte2=16;
			}
			$gastos_data = array(
			'fecha'=>date('Y').'-'.$i.'-'.$fecha_corte[0].' 00:00:00',
			'concepto'=>$concepto.'-'.$suscrito.' '.$mes,
			'coin'=>$show_coin,
			'estado'=>"Pendiente",
			'cantidad'=>$monto,
			'category'=>$this->lang->line("module_gasto_recurrente"),
			'recurso_id'=>1,
			"suscrito"=>$suscrito
			);
			if($this->save($gastos_data))
			{
			$succes=true;
			}
			$gastos_data2 = array(
			'fecha'=>date('Y').'-'.$i.'-'.$corte2.' 00:00:00',
			'concepto'=>$concepto.'-'.$suscrito.' '.$mes,
			'coin'=>$show_coin,
			'estado'=>"Pendiente",
			'cantidad'=>$monto,
			'category'=>$this->lang->line("module_gasto_recurrente"),
			'recurso_id'=>1,
			"suscrito"=>$suscrito
			);
			if($this->save($gastos_data2))
			{
			$succes=true;
			}
			}
			}
			elseif($suscrito=="bimestral")
			{
			/* $this->update_list($customer_id,$this->lang->line("module_gasto").' '.'bimestral'); */
			$fecha_corte=explode("/", $corte);
			$meses_restantes=intval($fecha_corte[1]);
			if($fecha_corte[1]==date('m'))$meses_restantes=date('n');
			$bimestral=intval($fecha_corte[1]);
			for($i=$meses_restantes;$i<=13;$i++)
			{
			$mes=date("F",strtotime(date('Y').'-'.$bimestral.'-'.$fecha_corte[0]));
			if($config_info["language"]=="ES")
			{
			switch ($mes) {
			case "January":
			$mes="Enero";
			break;
			case "February":
			$mes="Febrero";
			break;
			case "March":
			$mes="Marzo";
			break;
			case "April":
			$mes="Abril";
			break;
			case "May":
			$mes="Mayo";
			break;
			case "June":
			$mes="Junio";
			break;
			case "July":
			$mes="Julio";
			break;
			case "August":
			$mes="Agosto";
			break;
			case "September":
			$mes="Septiembre";
			break;
			case "October":
			$mes="Octubre";
			break;
			case "November":
			$mes="Noviembre";
			break;
			case "December":
			$mes="Diciembre";
			break;
			default:
			/* echo "Your favorite color is neither red, blue, nor green!"; */
			}
			}
			$gastos_data = array(
			'fecha'=>date('Y').'-'.$bimestral.'-'.$fecha_corte[0].' 00:00:00',
			'concepto'=>$concepto.'-'.$suscrito.' '.$mes,
			'coin'=>$show_coin,
			'estado'=>"Pendiente",
			'cantidad'=>$monto,
			'category'=>$this->lang->line("module_gasto_recurrente"),
			'recurso_id'=>1,
			"suscrito"=>$suscrito
			);
			if($bimestral<=12)
			{	
			if($this->save($gastos_data))
			{
			$succes=true;
			}
			}
			$bimestral+=2;
			}
			}
			elseif($suscrito=="trimestral")
			{
			/* $this->update_list($customer_id,$this->lang->line("module_gasto").' '.'trimestral'); */
			$fecha_corte=explode("/", $corte);
			$meses_restantes=intval($fecha_corte[1]);
			if($fecha_corte[1]==date('m'))$meses_restantes=date('n');
			$trimestral=intval($fecha_corte[1]);
			for($i=$meses_restantes;$i<=13;$i++)
			{
			$mes=date("F",strtotime(date('Y').'-'.$trimestral.'-'.$fecha_corte[0]));
			if($config_info["language"]=="ES")
			{
			switch ($mes) {
			case "January":
			$mes="Enero";
			break;
			case "February":
			$mes="Febrero";
			break;
			case "March":
			$mes="Marzo";
			break;
			case "April":
			$mes="Abril";
			break;
			case "May":
			$mes="Mayo";
			break;
			case "June":
			$mes="Junio";
			break;
			case "July":
			$mes="Julio";
			break;
			case "August":
			$mes="Agosto";
			break;
			case "September":
			$mes="Septiembre";
			break;
			case "October":
			$mes="Octubre";
			break;
			case "November":
			$mes="Noviembre";
			break;
			case "December":
			$mes="Diciembre";
			break;
			default:
			/* echo "Your favorite color is neither red, blue, nor green!"; */
			}
			}
			$gastos_data = array(
			'fecha'=>date('Y').'-'.$trimestral.'-'.$fecha_corte[0].' 00:00:00',
			'concepto'=>$concepto.'-'.$suscrito.' '.$mes,
			'coin'=>$show_coin,
			'estado'=>"Pendiente",
			'cantidad'=>$monto,
			'category'=>$this->lang->line("module_gasto_recurrente"),
			'recurso_id'=>1,
			"suscrito"=>$suscrito
			);
			if($trimestral<=12)
			{	
			if($this->save($gastos_data))
			{
			$succes=true;
			}
			}
			$trimestral+=3;
			}
			}
			elseif($suscrito=="semestral")
			{
			/*$this->update_list($customer_id,$this->lang->line("module_gasto").' '.'semestral');*/
			$fecha_corte=explode("/", $corte);
			$meses_restantes=intval($fecha_corte[1]);
			if($fecha_corte[1]==date('m'))$meses_restantes=date('n');
			$semestral=intval($fecha_corte[1]);
			for($i=$meses_restantes;$i<=13;$i++)
			{
			$mes=date("F",strtotime(date('Y').'-'.$semestral.'-'.$fecha_corte[0]));
			if($config_info["language"]=="ES")
			{
			switch ($mes) {
			case "January":
			$mes="Enero";
			break;
			case "February":
			$mes="Febrero";
			break;
			case "March":
			$mes="Marzo";
			break;
			case "April":
			$mes="Abril";
			break;
			case "May":
			$mes="Mayo";
			break;
			case "June":
			$mes="Junio";
			break;
			case "July":
			$mes="Julio";
			break;
			case "August":
			$mes="Agosto";
			break;
			case "September":
			$mes="Septiembre";
			break;
			case "October":
			$mes="Octubre";
			break;
			case "November":
			$mes="Noviembre";
			break;
			case "December":
			$mes="Diciembre";
			break;
			default:
			/*echo "Your favorite color is neither red, blue, nor green!";*/
			}
			}
			$gastos_data = array(
			'fecha'=>date('Y').'-'.$semestral.'-'.$fecha_corte[0].' 00:00:00',
			'concepto'=>$concepto.'-'.$suscrito.' '.$mes,
			'coin'=>$show_coin,
			'estado'=>"Pendiente",
			'cantidad'=>$monto,
			'category'=>$this->lang->line("module_gasto_recurrente"),
			'recurso_id'=>1,
			"suscrito"=>$suscrito
			);
			if($semestral<=12)
			{
			if($this->save($gastos_data))
			{
			$succes=true;
			}
			}
			$semestral+=6;
			}
			}
			elseif($suscrito=="anual")
			{
			/*$this->update_list($customer_id,$this->lang->line("module_gasto").' '.'anual');*/
			$fecha_corte=explode("/", $corte);
			$gastos_data = array(
			'fecha'=>date('Y').'-'.$fecha_corte[1].'-'.$fecha_corte[0].' 00:00:00',
			'concepto'=>$concepto.'-'.$suscrito.' '.date('Y'),
			'coin'=>$show_coin,
			'estado'=>"Pendiente",
			'cantidad'=>$monto,
			'category'=>$this->lang->line("module_gasto_recurrente"),
			'recurso_id'=>1,
			"suscrito"=>$suscrito
			);
			if($this->save($gastos_data))
			{
			$succes=true;
			}
			}
				/**/
		/*}*/
		return $succes;
	}
	/*Obtiene todos los gastos pendientes*/
	function renew_list($gasto_id){
			$this->db->where_in('gasto_id',$gasto_id);
			$this->db->where('estado','Pendiente');
			$query=$this->db->delete('gastos');
			return $query;
 	}
	/*Obtiene la informacion de todos los gastos recurrentes entre un rango de fecha*/
	function get_info_gastos_recurrentes($concepto,$category){
		$this->db->from('gastos');
		$this->db->where('fecha BETWEEN "'. date('Y-01-01 00:00:00'). '" and "'. date('Y-12-31 00:00:00)"'));
		$this->db->where('concepto',$concepto);
		$this->db->where('category',$category);
		$this->db->order_by("fecha", "asc");
		return $this->db->get();
	}
	/*Obtiene la informacion de todos los gastos recurrentes vencidos*/
	function get_info_gastos_recurrentes_anteriores($concepto,$category){
		$this->db->from('gastos');
		$this->db->where('fecha BETWEEN "'. date(''.(date('Y')-1).'-01-01 00:00:00'). '" and "'. date(''.(date('Y')-1).'-12-31 00:00:00)"'));
		$this->db->where('concepto',$concepto);
		$this->db->where('category',$category);
		$this->db->order_by("fecha", "asc");
		return $this->db->get();
	}
	/*Obtiene la informacion de todos los gastos recurrentes*/
	function get_all_recurrentes($suscripcion){
		$this->db->from('gastos');
		$this->db->where('suscrito', $suscripcion);
		$this->db->where('category', $this->lang->line("module_gasto_recurrente"));
		return $this->db->get();
	}
	/*Renueva los gastos recurrentes en el nuevo año*/
	function generar_gastos_new_year($suscripcion){
		/*$customer_id=null;*/
		/*$cobros_actualizados=false;*/
		$recurrentes=$this->Gasto->get_all_recurrentes($suscripcion);
		$config_info=$this->Appconfig->get_info();
		$show_coin=$config_info["coin"];

		if($config_info["xchange_show"]=="1")$show_coin=$config_info["coin2"];
		foreach($recurrentes->result() as $recurrente)
 		{	
			if($suscripcion=="semanal")
			{
				$info_gasto=$this->get_info_gastos_recurrentes($recurrente->concepto,$recurrente->category);
				if($info_gasto->num_rows()==1)
				{	
					
					break;
				}
				if($info_gasto->num_rows()==0)
				{
					$info_gasto_anteriores=$this->get_info_gastos_recurrentes_anteriores($recurrente->concepto,$recurrente->category);
					$date=date_create($info_gasto_anteriores->result()[0]->fecha);
					$week_day =date_format($date, 'w');

					for($i=0;$i<=365;$i++)
					{
						$fecha = DateTime::createFromFormat('z', $i);
						
						if($fecha->format('w')==$week_day)
						{
							$concepto=explode("-",$recurrente->concepto);
							$gastos_data = array(
							'fecha'=>$fecha->format('Y-m-d H:i:s'),
							'concepto'=>$concepto[0].'-'.$recurrente->suscrito.' #'.date('W',strtotime($fecha->format('Y-m-d'))),
							'coin'=>$recurrente->coin,
							'estado'=>"Pendiente",
							'cantidad'=>$recurrente->cantidad,
							'category'=>$recurrente->category,
							'recurso_id'=>1,
							"suscrito"=>$recurrente->suscrito
							);
							if($this->save($gastos_data))
							{
							$succes=true;
							}
						}
					}
					break;
				}
			}
			elseif($suscripcion=="quincenal")
			{
				$info_gasto=$this->get_info_gastos_recurrentes($recurrente->concepto,$recurrente->category);

				if($info_gasto->num_rows()==1)
				{
					
					break;
				}
				elseif($info_gasto->num_rows()==0)
				{
					$info_gasto_anteriores=$this->get_info_gastos_recurrentes_anteriores($recurrente->concepto,$recurrente->category);
					$date=date_create($info_gasto_anteriores->result()[0]->fecha);
					$week_day =date_format($date, 'd');
					$num_mes =date_format($date, 'm');
					for($i=1;$i<=12;$i++)
					{
					$fecha=date_create((date('Y')).'-'.$i.'-'.$week_day);
						$fecha=date_format($fecha, 'Y-m-d H:i:s');
						$corte=$fecha;
						$mes=date("F",strtotime($fecha));
						if($config_info["language"]=="ES")
						{
							switch ($mes) {
							case "January":
							$mes="Enero";
							break;
							case "February":
							$mes="Febrero";
							break;
							case "March":
							$mes="Marzo";
							break;
							case "April":
							$mes="Abril";
							break;
							case "May":
							$mes="Mayo";
							break;
							case "June":
							$mes="Junio";
							break;
							case "July":
							$mes="Julio";
							break;
							case "August":
							$mes="Agosto";
							break;
							case "September":
							$mes="Septiembre";
							break;
							case "October":
							$mes="Octubre";
							break;
							case "November":
							$mes="Noviembre";
							break;
							case "December":
							$mes="Diciembre";
							break;
							default:
							/*echo "Your favorite color is neither red, blue, nor green!";*/
							}
						}
						if($week_day<15 && $week_day+15<=25)
						{	
							$corte2=$week_day+15;
						}
						elseif($week_day<15 && $week_day+15>=25)
						{
							$week_day=10;
							$corte2=25;
						}
						elseif($week_day>15)
						{
							$corte2=$week_day-15;
						}
						elseif($week_day==15)
						{
							$week_day=1;
							$corte2=16;
						}
							$concepto=explode("-",$recurrente->concepto);
							$gastos_data = array(
							'fecha'=>$corte,
							'concepto'=>$concepto[0].'-'.$recurrente->suscrito.' '.$mes,
							'coin'=>$recurrente->coin,
							'estado'=>"Pendiente",
							'cantidad'=>$recurrente->cantidad,
							'category'=>$recurrente->category,
							'recurso_id'=>1,
							"suscrito"=>$recurrente->suscrito
							);
							if($this->save($gastos_data))
							{
							$succes=true;
							}
							$gastos_data2 = array(
							'fecha'=>date('Y').'-'.$i.'-'.$corte2.' 00:00:00',
							'concepto'=>$concepto[0].'-'.$recurrente->suscrito.' '.$mes,
							'coin'=>$recurrente->coin,
							'estado'=>"Pendiente",
							'cantidad'=>$recurrente->cantidad,
							'category'=>$recurrente->category,
							'recurso_id'=>1,
							"suscrito"=>$recurrente->suscrito
							);
							if($this->save($gastos_data2))
							{
							$succes=true;
							}
					}
					break;
				}
			}
			elseif($suscripcion=="mensual")
			{
				$info_gasto=$this->get_info_gastos_recurrentes($recurrente->concepto,$recurrente->category);
				if($info_gasto->num_rows()==1)
				{
				}
				elseif($info_gasto->num_rows()==0)
				{
					$info_gasto_anteriores=$this->get_info_gastos_recurrentes_anteriores($recurrente->concepto,$recurrente->category);
					$date=date_create($info_gasto_anteriores->result()[0]->fecha);
					$week_day =date_format($date, 'd');
					$num_mes =date_format($date, 'm');
					for($i=1;$i<=12;$i++)
					{
						$mes=date("F",strtotime(date('Y').'-'.$i.'-'.$week_day));
						if($config_info["language"]=="ES")
						{
							switch ($mes) {
							case "January":
							$mes="Enero";
							break;
							case "February":
							$mes="Febrero";
							break;
							case "March":
							$mes="Marzo";
							break;
							case "April":
							$mes="Abril";
							break;
							case "May":
							$mes="Mayo";
							break;
							case "June":
							$mes="Junio";
							break;
							case "July":
							$mes="Julio";
							break;
							case "August":
							$mes="Agosto";
							break;
							case "September":
							$mes="Septiembre";
							break;
							case "October":
							$mes="Octubre";
							break;
							case "November":
							$mes="Noviembre";
							break;
							case "December":
							$mes="Diciembre";
							break;
							default:
							/*echo "Your favorite color is neither red, blue, nor green!";*/
							}
						}
						$fecha=date_create((date('Y')).'-'.$i.'-'.$week_day);
						$fecha=date_format($fecha, 'Y-m-d H:i:s');
						$corte=$fecha;
						$concepto=explode("-",$recurrente->concepto);
							$gastos_data = array(
							'fecha'=>$corte,
							'concepto'=>$concepto[0].'-'.$recurrente->suscrito.' '.$mes,
							'coin'=>$recurrente->coin,
							'estado'=>"Pendiente",
							'cantidad'=>$recurrente->cantidad,
							'category'=>$recurrente->category,
							'recurso_id'=>1,
							"suscrito"=>$recurrente->suscrito
							);
							if($this->save($gastos_data))
							{
							$succes=true;
							}
					}
					break;
				}
			}
			elseif($suscripcion=="bimestral")
			{
				$info_gasto=$this->get_info_gastos_recurrentes($recurrente->concepto,$recurrente->category);
				if($info_gasto->num_rows()==1)
				{
					break;
				}
				elseif($info_gasto->num_rows()==0)
				{
					$info_gasto_anteriores=$this->get_info_gastos_recurrentes_anteriores($recurrente->concepto,$recurrente->category);
					$date=date_create($info_gasto_anteriores->result()[0]->fecha);
					$week_day =date_format($date, 'd');
					$num_mes =date_format($date, 'm');
					if((intval($num_mes)%2)==0)
					{
						$bimestral=2;
					}	
					else
					{
						$bimestral=1;
					}
					for($i=1;$i<=12;$i++)
					{
						$mes=date("F",strtotime(date('Y').'-'.$bimestral.'-'.$week_day));
						if($config_info["language"]=="ES")
						{
							switch ($mes) {
							case "January":
							$mes="Enero";
							break;
							case "February":
							$mes="Febrero";
							break;
							case "March":
							$mes="Marzo";
							break;
							case "April":
							$mes="Abril";
							break;
							case "May":
							$mes="Mayo";
							break;
							case "June":
							$mes="Junio";
							break;
							case "July":
							$mes="Julio";
							break;
							case "August":
							$mes="Agosto";
							break;
							case "September":
							$mes="Septiembre";
							break;
							case "October":
							$mes="Octubre";
							break;
							case "November":
							$mes="Noviembre";
							break;
							case "December":
							$mes="Diciembre";
							break;
							default:
							/* echo "Your favorite color is neither red, blue, nor green!";*/
							}
						}
						$concepto=explode("-",$recurrente->concepto);
						$gastos_data = array(
						'fecha'=>date('Y').'-'.$bimestral.'-'.$week_day.' 00:00:00',
						'concepto'=>$concepto[0].'-'.$recurrente->suscrito.' '.$mes,
						'coin'=>$recurrente->coin,
						'estado'=>"Pendiente",
						'cantidad'=>$recurrente->cantidad,
						'category'=>$this->lang->line("module_gasto_recurrente"),
						'recurso_id'=>1,
						'receiving_id'=>-1,
						'suscrito'=>$recurrente->suscrito
						);
						if($bimestral<=12)
						{	
							if($this->save($gastos_data))
							{
								$succes=true;
							}
						}
						$bimestral+=2;
					}
				}
			}
			elseif($suscripcion=="trimestral")
			{
				$info_gasto=$this->get_info_gastos_recurrentes($recurrente->concepto,$recurrente->category);
				if($info_gasto->num_rows()==1)
				{
					break;
				}
				elseif($info_gasto->num_rows()==0)
				{
					$info_gasto_anteriores=$this->get_info_gastos_recurrentes_anteriores($recurrente->concepto,$recurrente->category);
					$date=date_create($info_gasto_anteriores->result()[0]->fecha);
					$week_day =date_format($date, 'd');
					$num_mes =date_format($date, 'm');
					if((intval($num_mes)+12)%3==0)
					{
						$trimestral=3;
					}	
					elseif((intval($num_mes)+12)%3==2)
					{
						$trimestral=2;
					}
					elseif((intval($num_mes)+12)%3==1)
					{
						$trimestral=1;
					}
					for($i=1;$i<=12;$i++)
					{
					$mes=date("F",strtotime(date('Y').'-'.$trimestral.'-'.$week_day));
						if($config_info["language"]=="ES")
						{
							switch ($mes) {
							case "January":
							$mes="Enero";
							break;
							case "February":
							$mes="Febrero";
							break;
							case "March":
							$mes="Marzo";
							break;
							case "April":
							$mes="Abril";
							break;
							case "May":
							$mes="Mayo";
							break;
							case "June":
							$mes="Junio";
							break;
							case "July":
							$mes="Julio";
							break;
							case "August":
							$mes="Agosto";
							break;
							case "September":
							$mes="Septiembre";
							break;
							case "October":
							$mes="Octubre";
							break;
							case "November":
							$mes="Noviembre";
							break;
							case "December":
							$mes="Diciembre";
							break;
							default:
							/*echo "Your favorite color is neither red, blue, nor green!";*/
							}
						}
						$concepto=explode("-",$recurrente->concepto);
						$gastos_data = array(
						'fecha'=>date('Y').'-'.$trimestral.'-'.$week_day.' 00:00:00',
						'concepto'=>$concepto[0].'-'.$recurrente->suscrito.' '.$mes,
						'coin'=>$recurrente->coin,
						'estado'=>"Pendiente",
						'cantidad'=>$recurrente->cantidad,
						'category'=>$this->lang->line("module_gasto_recurrente"),
						'recurso_id'=>1,
						'receiving_id'=>-1,
						'suscrito'=>$recurrente->suscrito
						);
						if($trimestral<=12)
						{	
							if($this->save($gastos_data))
							{
								$succes=true;
							}
						}
						$trimestral+=3;
					}
				}
			}
			elseif($suscripcion=="semestral")
			{
				$info_gasto=$this->get_info_gastos_recurrentes($recurrente->concepto,$recurrente->category);
				if($info_gasto->num_rows()==1)
				{
					break;
				}
				elseif($info_gasto->num_rows()==0)
				{
					$info_gasto_anteriores=$this->get_info_gastos_recurrentes_anteriores($recurrente->concepto,$recurrente->category);
					$date=date_create($info_gasto_anteriores->result()[0]->fecha);
					$week_day =date_format($date, 'd');
					$num_mes =date_format($date, 'm');
					if((intval($num_mes)+12)%6==0)
					{
						$semestral=6;
					}	
					elseif((intval($num_mes)+12)%6==5)
					{
						$semestral=5;
					}
					elseif((intval($num_mes)+12)%6==4)
					{
						$semestral=4;
					}
					elseif((intval($num_mes)+12)%6==3)
					{
						$semestral=3;
					}
					elseif((intval($num_mes)+12)%6==2)
					{
						$semestral=2;
					}
					elseif((intval($num_mes)+12)%6==1)
					{
						$semestral=1;
					}
					for($i=1;$i<=12;$i++)
					{
						$mes=date("F",strtotime(date('Y').'-'.$semestral.'-'.$week_day));
						if($config_info["language"]=="ES")
						{
							switch ($mes) {
							case "January":
							$mes="Enero";
							break;
							case "February":
							$mes="Febrero";
							break;
							case "March":
							$mes="Marzo";
							break;
							case "April":
							$mes="Abril";
							break;
							case "May":
							$mes="Mayo";
							break;
							case "June":
							$mes="Junio";
							break;
							case "July":
							$mes="Julio";
							break;
							case "August":
							$mes="Agosto";
							break;
							case "September":
							$mes="Septiembre";
							break;
							case "October":
							$mes="Octubre";
							break;
							case "November":
							$mes="Noviembre";
							break;
							case "December":
							$mes="Diciembre";
							break;
							default:
							/*echo "Your favorite color is neither red, blue, nor green!";*/
							}
						}
						$concepto=explode("-",$recurrente->concepto);
						$gastos_data = array(
						'fecha'=>date('Y').'-'.$semestral.'-'.$week_day.' 00:00:00',
						'concepto'=>$concepto[0].'-'.$recurrente->suscrito.' '.$mes,
						'coin'=>$recurrente->coin,
						'estado'=>"Pendiente",
						'cantidad'=>$recurrente->cantidad,
						'category'=>$this->lang->line("module_gasto_recurrente"),
						'recurso_id'=>1,
						'receiving_id'=>-1,
						'suscrito'=>$recurrente->suscrito
						);
						if($semestral<=12)
						{	
							if($this->save($gastos_data))
							{
								$succes=true;
							}
						}
						$semestral+=6;
					}
					break;
				}
			}
			elseif($suscripcion=="anual")
			{
				$concepto=explode("-",$recurrente->concepto);
				$info_gasto=$this->get_info_gastos_recurrentes($concepto[0].'-'.$recurrente->suscrito.' '.date('Y'),$recurrente->category);

				if($info_gasto->num_rows()==1)
				{
					break;
				}
				elseif($info_gasto->num_rows()==0)
				{
					$info_gasto_anteriores=$this->get_info_gastos_recurrentes_anteriores($recurrente->concepto,$recurrente->category);

					$date=date_create($info_gasto_anteriores->result()[0]->fecha);
					$week_day =date_format($date, 'd');
					$num_mes =date_format($date, 'm');
						$gastos_data = array(
						'fecha'=>date('Y').'-'.$num_mes.'-'.$week_day.' 00:00:00',
						'concepto'=>$concepto[0].'-'.$recurrente->suscrito.' '.date('Y'),
						'coin'=>$recurrente->coin,
						'estado'=>"Pendiente",
						'cantidad'=>$recurrente->cantidad,
						'category'=>$this->lang->line("module_gasto_recurrente"),
						'recurso_id'=>1,
						'receiving_id'=>-1,
						'suscrito'=>$recurrente->suscrito
						);
							if($this->save($gastos_data))
							{
								$succes=true;
							}
						break;
				}
			}
		}
	}
	/*Cancela un gasto recurrente*/
	function cancelar_gasto_recurrente($gasto_id){
		$config_info=$this->Appconfig->get_info();
		$gasto_info=$this->get_info($gasto_id);
		$suscrito=$gasto_info->suscrito;
		$concepto=explode("-",$gasto_info->concepto);
		if($gasto_info->suscrito=="mensual" || $gasto_info->suscrito=="bimestral"||$gasto_info->suscrito=="trimestral"|| $gasto_info->suscrito=="semestral"||$gasto_info->suscrito=="quincenal")
		{
			for($i=1;$i<=12;$i++)
			{
				$mes=date("F",strtotime(date('Y').'-'.$i.'-01'));
				if($config_info["language"]=="ES")
				{
					switch ($mes) {
					case "January":
					$mes="Enero";
					break;
					case "February":
					$mes="Febrero";
					break;
					case "March":
					$mes="Marzo";
					break;
					case "April":
					$mes="Abril";
					break;
					case "May":
					$mes="Mayo";
					break;
					case "June":
					$mes="Junio";
					break;
					case "July":
					$mes="Julio";
					break;
					case "August":
					$mes="Agosto";
					break;
					case "September":
					$mes="Septiembre";
					break;
					case "October":
					$mes="Octubre";
					break;
					case "November":
					$mes="Noviembre";
					break;
					case "December":
					$mes="Diciembre";
					break;
					default:
					/*echo "Your favorite color is neither red, blue, nor green!";*/
					}
				}
				$this->db->trans_start();
				$this->db->from('gastos');
				$this->db->where('fecha <=',$gasto_info->fecha);
				$this->db->where('concepto',$concepto[0].'-'.$suscrito.' '.$mes);
				$this->db->update('gastos',array('suscrito' => 0));
				$this->db->trans_complete();
				$this->db->trans_start();
				$this->db->from('gastos');
				$this->db->where('fecha >',$gasto_info->fecha);
				$this->db->where('concepto',$concepto[0].'-'.$suscrito.' '.$mes);
				$this->db->delete('gastos');
				$this->db->trans_complete();
			}
			$this->db->trans_start();
				$this->db->from('gastos');
				$this->db->where('fecha ',$gasto_info->fecha);
				$this->db->where('concepto',$gasto_info->concepto);
				$this->db->update('gastos',array('suscrito' => 0));
				$this->db->trans_complete();
		}
		elseif($gasto_info->suscrito=="semanal")
		{
			for($i=0;$i<=53;$i++)
			{
				$week=$i;
				if($i==1)$week='01';
				if($i==2)$week='02';
				if($i==3)$week='03';
				if($i==4)$week='04';
				if($i==5)$week='05';
				if($i==6)$week='06';
				if($i==7)$week='07';
				if($i==8)$week='08';
				if($i==9)$week='09';
				$this->db->trans_start();
				$this->db->from('gastos');
				$this->db->where('fecha <=',$gasto_info->fecha);
				$this->db->where('concepto',$concepto[0].'-'.$suscrito.' #'.$week);
				$this->db->update('gastos',array('suscrito' => 0));
				$this->db->trans_complete();
				$this->db->trans_start();
				$this->db->from('gastos');
				$this->db->where('fecha >',$gasto_info->fecha);
				$this->db->where('concepto',$concepto[0].'-'.$suscrito.' #'.$week);
				$this->db->delete('gastos');
				$this->db->trans_complete();
			}
			$this->db->trans_start();
				$this->db->from('gastos');
				$this->db->where('fecha ',$gasto_info->fecha);
				$this->db->where('concepto',$gasto_info->concepto);
				$this->db->update('gastos',array('suscrito' => 0));
				$this->db->trans_complete();
		}
		elseif($gasto_info->suscrito=="anual")
		{
			$this->db->trans_start();
			$this->db->select_min('fecha');
			$this->db->from('gastos');
			$this->db->where('suscrito',$gasto_info->suscrito);
			$query=$this->db->get();
			$oldest_date=$query->result();
			$this->db->trans_complete();
			$years=explode("-",$oldest_date[0]->fecha);
			$year_start=intval($years[0]);
			$years2=explode("-",$gasto_info->fecha);
			$year_end=intval($years2[0]);
			for($i=$year_start;$i<$year_end;$i++)
			{
				$this->db->trans_start();
				$this->db->from('gastos');
				$this->db->where('concepto',$concepto[0].'-'.$suscrito.' '.$i);
				$this->db->update('gastos',array('suscrito' => 0));
				$this->db->trans_complete();
			}
				$this->db->trans_start();
				$this->db->from('gastos');
				$this->db->where('fecha ',$gasto_info->fecha);
				$this->db->where('concepto',$gasto_info->concepto);
				$this->db->update('gastos',array('suscrito' => 0));
				$this->db->trans_complete();
		}
		return $oldest_date; 
	}
	/*Obtiene todas las categorias de los gastos*/
	function get_categories(){
		$this->db->select('category');
		$this->db->from('gastos');
		$this->db->distinct();
		$this->db->order_by("category", "asc");
		return $this->db->get();
	}
	/*Cambia el estado deducible de un gasto*/
	function deducible($id_gasto, $respuesta=1){
		$success=false;
		/*Ejecutar las siguientes consultas como una transacción, queremos asegurarnos de que se hace todo o nada*/
		$this->db->trans_start();
			$this->db->where('gasto_id', $id_gasto);
			$success = $this->db->update('gastos', array('deducible' => $respuesta));
		$this->db->trans_complete();
		return $success;
	}
}
?>