<?php 
class Item extends CI_Model
{
	/*Verifica la exisencia de algun registro por el id*/
	function exists($item_id){
		$this->db->from('items');
		$this->db->where('item_id',$item_id);
		$query = $this->db->get();
		return ($query->num_rows()==1);
	}
	/*Verifica si existe cfdi*/
	function exists_cfdi_field($item_id){
		$this->db->from('items_cfdi');
		$this->db->where('item_id',$item_id);
		$query = $this->db->get();
		return ($query->num_rows()==1);
	}
	/*Verifica si el codigo de barras no esta en uso por algun item*/
	function barcode_exists($barcode,$item_id){
		$this->db->from('items');
		$this->db->where("item_id !=",$item_id);
		$this->db->where('item_number',$barcode);
		$query = $this->db->get();
		return ($query->num_rows()==1);
	}
	/*consulta todo los items con filtros con limite*/
	function get_all($filtro,$limit=10000, $offset=0){
		$this->db->from('items');
		$this->db->where('deleted',0);
		$this->db->order_by($filtro, "asc");
		$this->db->limit($limit);
		$this->db->offset($offset);
		return $this->db->get();
	}
	/*consulta todos los items con filtros pero sin limite*/
	function get_all_no_limit($filtro){
		$this->db->from('items');
		$this->db->where('deleted',0);
		$this->db->order_by($filtro, "asc");
		return $this->db->get();
	}
	/* cuenta todos los items incluyendo los eleminados */
	function count_alls(){
		$this->db->from('items');
		return $this->db->count_all_results();
	}
	/*cuenta todos los items no eleminados*/
	function count_all(){
		$this->db->from('items');
		$this->db->where('deleted',0);
		return $this->db->count_all_results();
	}
	/*cuenta todos los items de la categoria*/
	function count_all_cat($categoria){
		$this->db->from('items');
		$this->db->where('deleted',0);
		$this->db->where('publicar',1);
		$this->db->where('category',$categoria);
		return $this->db->count_all_results();
	}
	/* cuenta todas las categorias publicas*/
	function count_all_cat_public(){
		$this->db->from('items');
		$this->db->where('deleted',0);
		$this->db->where('publicar',1);
		return $this->db->count_all_results();
	}
	/*Obtiene solo los items con numero de serie, de bajo inventario o con descripcion alterna*/
	function get_all_filtered($low_inventory=0,$is_serialized=0,$no_description){
		$this->db->from('items');
		if ($low_inventory !=0 ){
			$this->db->where('quantity <=','reorder_level', false);
		}
		if ($is_serialized !=0 ){
			$this->db->where('is_serialized',1);
		}
		if ($no_description!=0 ){
			$this->db->where('description','');
		}
		$this->db->where('deleted',0);
		$this->db->order_by("name", "asc");
		return $this->db->get();
	}
	/*Obtiene la informacion de un item especifico por id*/
	function get_info($item_id){
		$this->db->from('items');
		$this->db->where('item_id',$item_id);
		$query = $this->db->get();
		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			/* Obtiene el objeto principal base vacío, ya que $item_id NO es un elemento */
			$item_obj=new stdClass();
			/* Obtener todos los campos de la tabla de elementos */
			$fields = $this->db->list_fields('items');
			foreach ($fields as $field)
			{
				$item_obj->$field='';
			}
			return $item_obj;
		}
	}
	/*Obtiene la informacion del codigo de producto y de servicio de un item especifico por su id*/
	function get_info_cfdi($item_id){
		$this->db->from('items_cfdi');
		$this->db->where('item_id',$item_id);
		$query = $this->db->get();
		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			/* Obtiene el objeto principal base vacío, ya que $item_id NO es un elemento */
			$item_obj=new stdClass();
			/* Obtener todos los campos de la tabla de elementos */
			$fields = $this->db->list_fields('items_cfdi');
			foreach ($fields as $field)
			{
				$item_obj->$field='';
			}
			return $item_obj;
		}
	}
	/*Consulta el item por item_number*/
	function get_item_id($item_number){
		$this->db->from('items');
		$this->db->where('item_number',$item_number);
		$query = $this->db->get();
		if($query->num_rows()==1)
		{
			return $query->row()->item_id;
		}
		return false;
	}
	/*Obtiene la informacion multiple de algunos items por sus ids*/
	function get_multiple_info($filtro,$item_ids){
		$this->db->from('items');
		
		$this->db->where_in('item_id',$item_ids);
		
		$this->db->where('deleted',0);
		$this->db->order_by($filtro, "asc");
		return $this->db->get();
	}
/*******************fin***************************/
/****Inserciones y Actualizaciones de datos****/
	/*Guarda un nuevo o edita los datos de un item */
	function save(&$item_data,$item_id=false){
		$success=false;
		$this->db->trans_start();
		if (!$item_id or !$this->exists($item_id))
		{
			$success =$this->db->insert('items',$item_data);
			$item_data['item_id']=$this->db->insert_id();
		}
		else
		{
			$this->db->where('item_id', $item_id);
			$success =$this->db->update('items',$item_data);
		}
		$this->db->trans_complete();
		return $success;
	}
	/*Edita la informacion de ciertas columnas en varios items por sus ids*/
	function update_multiple($item_data,$item_ids){
		
		foreach($item_ids as $item_id)
		{
			$this->save_items_content($item_id);
			
		}
		$this->db->where_in('item_id',$item_ids);
		return $this->db->update('items',$item_data);
	}
	/*Edita la informacion del codigo de producto y de servicio en varios items por sus ids*/
	function update_multiple_cfdi_data($cfdi_fields,$item_ids){
		foreach($item_ids as $item_id)
		{
			
			$this->save_cfdi_fields($cfdi_fields,$item_id);
		}
		/*$this->db->where_in('item_id',$item_ids);
		return $this->db->update('items_cfdi',$cfdi_fields);*/
	}
	/**/
	function update_cost_price($cost_price,$item_id){
		$this->db->set('cost_price',$cost_price);
		$this->db->where('item_id', $item_id);
		return $this->db->update('items');
	}
	/*Cambia el estado deleted=1 de un item*/
	function delete($item_id){
		$this->db->where('item_id', $item_id);
		return $this->db->update('items', array('deleted' => 1));
	}
	/*Inserta un batch de items en la bd*/
	function save_list(&$items_to_save){
		/*siempre devuelve falso */
		/*if(!$this->db->insert_batch('items', $items_to_save)){return true;}else{return false;}*/
		 if($num_insertions=$this->db->insert_batch('items', $items_to_save)){
			 
			 
			 $data["num_insertions"]=$num_insertions;
			 $data["first_id"]=$this->db->insert_id();
			 return $data;
		}else{return false;}
	}
	/*Cambia el estado deleted=1 de multiples items por id*/
	function delete_list(&$items_to_delete){
		$this->db->trans_start();
		foreach($items_to_delete as $delete_id)
		{
			$this->db->where_in('item_id',$delete_id);
			$this->db->update('items', array('deleted' => 1));
		}
		return $this->db->trans_complete();
 	}
	/*Guarda la informacion del codigo de producto y de servicio de un item especifico por su id*/
	function save_cfdi_fields(&$cfdi_fields,$item_id){
		$success=false;
		$this->db->trans_start();
		if (!$this->exists_cfdi_field($item_id))
		{
			$cfdi_fields["item_id"]=$item_id;
			$success =$this->db->insert('items_cfdi',$cfdi_fields);
		}
		else
		{
			$this->db->where('item_id', $item_id);
			$success =$this->db->update('items_cfdi',$cfdi_fields);
		}
		$this->db->trans_complete();
		return $success;
	}
/************************Busquedas*****************************/
	/*Obtiene las sugerencias de busqueda segun su filtro*/
	function get_search_suggestions($search,$limit=10,$filtro, $offset=0){
		if($filtro=="brand" || $filtro=="category" )
		{
			$this->db->select($filtro);
			$this->db->from('items');
			$this->db->where('deleted',0);
			$this->db->distinct();
			$this->db->like($filtro, $search);
			$this->db->order_by($filtro, "asc");
			$this->db->limit($limit,$offset);
			$by_category = $this->db->get();
			foreach($by_category->result() as $row)
			{
				$suggestions[]=$row->$filtro;
			}
		}
		else
		{
			$suggestions = array();
			$this->db->from('items');
			$this->db->like($filtro, $search);
			$this->db->where('deleted',0);
			$this->db->order_by($filtro, "asc");
			$this->db->limit($limit,$offset);
			$by_name = $this->db->get();
			foreach($by_name->result() as $row)
			{
			$suggestions[]=$row->$filtro;
			}
		}
		/*if($filtro=="name")
		{
			$this->db->from('items');
			$this->db->like('name', $search);
			$this->db->where('deleted',0);
			$this->db->order_by("name", "asc");
			$this->db->offset($offset);
			$by_name = $this->db->get();
			foreach($by_name->result() as $row)
			{
				$suggestions[]=$row->name;
			}
		}
		elseif($filtro=="modelo")
		{
			$this->db->from('items');
			$this->db->like('modelo', $search);
			$this->db->where('deleted',0);
			$this->db->order_by("modelo", "asc");
			$this->db->offset($offset);
			$by_name = $this->db->get();
			foreach($by_name->result() as $row)
			{
				$suggestions[]=$row->modelo;
			}
		}
		elseif($filtro=="brand")
		{
			$this->db->select('brand');
			$this->db->from('items');
			$this->db->where('deleted',0);
			$this->db->distinct();
			$this->db->like('brand', $search);
			$this->db->order_by("brand", "asc");
			$by_category = $this->db->get();
			foreach($by_category->result() as $row)
			{
				$suggestions[]=$row->brand;
			}
		}
		elseif($filtro=="category")
		{
			$this->db->select('category');
			$this->db->from('items');
			$this->db->where('deleted',0);
			$this->db->distinct();
			$this->db->like('category', $search);
			$this->db->order_by("category", "asc");
			$this->db->offset($offset);
			$by_category = $this->db->get();
			foreach($by_category->result() as $row)
			{
				$suggestions[]=$row->category;
			}
		}
		elseif($filtro=="item_number")
		{
			$this->db->select('item_number');
			$this->db->from('items');
			$this->db->where('deleted',0);
			$this->db->distinct();
			$this->db->like('item_number', $search);
			$this->db->order_by("item_number", "asc");
			$this->db->offset($offset);
			$by_category = $this->db->get();
			foreach($by_category->result() as $row)
			{
				$suggestions[]=$row->item_number;
			}
		}
		elseif($filtro=="item_id")
		{
			$this->db->select('item_id');
			$this->db->from('items');
			$this->db->where('deleted',0);
			$this->db->distinct();
			$this->db->like('item_id', $search);
			$this->db->order_by("item_id", "asc");
			$this->db->offset($offset);
			$by_category = $this->db->get();
			foreach($by_category->result() as $row)
			{
				$suggestions[]=$row->item_id;
			}
		}*/
		return $suggestions;
	}
	/*Obtiene las sugerencias de busqueda por nombre o por codigo de barras*/
	function get_item_search_suggestions($search,$limit=25, $offset=0){
		$suggestions = array();
		$this->db->from('items');
		$this->db->where('deleted',0);
		$this->db->like('name', $search);
		$this->db->order_by("name", "asc");
		$this->db->offset($offset);
		$by_name = $this->db->get();
		foreach($by_name->result() as $row){
			$suggestions[]=$row->item_id.'|'.$row->name;
		}
		$this->db->from('items');
		$this->db->where('deleted',0);
		$this->db->like('item_number', $search);
		$this->db->order_by("item_number", "asc");
		$this->db->offset($offset);
		$by_item_number = $this->db->get();
		foreach($by_item_number->result() as $row){
			$suggestions[]=$row->item_id.'|'.$row->item_number;
		}
		if(count($suggestions) > $limit){
			$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;
	}
	/*Obtiene las sugerencias de busqueda de las categorias existentes*/
	function get_category_suggestions($search,$limit=25){
		$suggestions = array();
		$this->db->distinct();
		$this->db->select('category');
		$this->db->from('items');
		$this->db->like('category', $search);
		$this->db->where('deleted', 0);
		$this->db->order_by("category", "asc");
		$by_category = $this->db->get();
		foreach($by_category->result() as $row)
		{
			$suggestions[]=$row->category;
		}
		if(count($suggestions) > $limit)
		{
		$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;
	}
	/*Obtiene las sugerencias de busqueda de las marcas existentes*/
	function get_brand_suggestions($search,$limit=25){
		$suggestions = array();
		$this->db->distinct();
		$this->db->select('brand');
		$this->db->from('items');
		$this->db->like('brand', $search);
		$this->db->where('deleted', 0);
		$this->db->order_by("brand", "asc");
		$by_category = $this->db->get();
		foreach($by_category->result() as $row)
		{
			$suggestions[]=$row->brand;
		}
		if(count($suggestions) > $limit)
		{
		$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;
	}
	/*Busqueda*/
	function search($search){
		$this->db->from('items');
		$this->db->where("(name LIKE '%".$this->db->escape_like_str($search)."%' or 
		item_number LIKE '%".$this->db->escape_like_str($search)."%' or 
		category LIKE '%".$this->db->escape_like_str($search)."%') and deleted=0");
		$this->db->order_by("name", "asc");
		return $this->db->get();
	}
	/*Busqueda especifica por filtro*/
	function specific_search($search,$filtro,$offset=0,$limit=20){
		$this->db->from('items');
		$this->db->where("(".$filtro." LIKE '%".$this->db->escape_like_str($search)."%' ESCAPE '!') and deleted=0");
		$this->db->order_by($filtro, "asc");
		$this->db->limit($limit,$offset);
		/*$this->db->offset($offset);*/
		return $this->db->get();
	}
	/*Obtiene todas las categorias de items deleted=0*/
	function get_categories(){
		$this->db->select('category');
		$this->db->from('items');
		$this->db->where('deleted',0);
		$this->db->distinct();
		$this->db->order_by("category", "asc");
		return $this->db->get();
	}
	/*Obtiene todas las categorias por marca*/
	function get_categories_reports($brand){
		$this->db->select('category');
		$this->db->from('items');
		$this->db->where('deleted',0);
		$this->db->where('quantity >=',0);
		if($brand!="all"){
			$this->db->where('brand',$brand);
		}
		$this->db->distinct();
		$this->db->order_by("category", "asc");
		return $this->db->get();
	}
	/*Obtiene todas las marcas existentes*/
	function get_brands(){
		$this->db->select('brand');
		$this->db->from('items');
		$this->db->where('deleted',0);
		$this->db->distinct();
		$this->db->order_by("brand", "asc");
		return $this->db->get();
	}
	/*Obtiene todas las marcas por categoria*/
	function get_brands_reports($category){
		$this->db->select('brand');
		$this->db->from('items');
		$this->db->where('deleted',0);
		$this->db->where('quantity >=',0);
		if($category!="all"){
			$this->db->where('category',$category);
		}
		$this->db->distinct();
		$this->db->order_by("brand", "asc");
		return $this->db->get();
	}
	/*Obtiene todos los proveedores delos items*/
	function get_suppliers(){
		$this->db->from('items');
		$this->db->join('suppliers','items.supplier_id=suppliers.person_id');
		$this->db->join('people','suppliers.person_id=people.person_id');
		return $this->db->get();
	}
	/*Obtiene todos los items publicados de cierta categoria*/
	function get_itemsCAT($categoria,$limit=10000, $offset=0){
		$this->db->from('items');
		$this->db->where('category',$categoria);
		$this->db->where('deleted',0);
		$this->db->where('publicar',1);
		$this->db->order_by("name", "asc");
		$this->db->limit($limit);
		$this->db->offset($offset);
		return $this->db->get();
	}
	/*Obtiene todas las categorias de items publicados*/
	function get_categories_public(){
		$this->db->select('category');
		$this->db->from('items');
		$this->db->where('deleted',0);
		$this->db->where('publicar',1);
		$this->db->distinct();
		$this->db->order_by("category", "asc");
		return $this->db->get();
	}
	/*Obtiene todos los items de cierta categoria*/
	function get_all_public($categoria,$limit=10000, $offset=0){
		$this->db->from('items');
		$this->db->where('deleted',0);
		$this->db->order_by($categoria, "asc");
		$this->db->limit($limit);
		$this->db->offset($offset);
		return $this->db->get();
	}
	/* Obtiene los productos de promocion */
	function get_items_promo($limit=10000, $offset=0){
		$this->db->from('items');
		$this->db->where('deleted',0);
		$this->db->where('publicar',1);
		$this->db->where('promocionar',1);
		$this->db->order_by("name", "asc");
		$this->db->limit($limit);
		$this->db->offset($offset);
		return $this->db->get();
	}
	/*Guarda la info del contenido en los idiomas disponibles en la bd*/
	function save_items_content($item_id){
		$languages =get_dir_file_info(APPPATH.'language/');
		
		foreach($languages as $language)
		{
			if($language["name"]!="index.html")
			{
			$elementos=$this->lang_lib->get_items_content_file($language['name'],$item_id);
			
			foreach($elementos as $key=>$elemento)
			{
				
				$name_element=explode('_',$key);
				
				if($name_element[1]==$item_id)
				{
					$content_data=array(
					'name_element'=>$name_element[2],
					'cont_element'=>$elemento,
					'lang_element'=>$language['name'],
					'item_id'=>$item_id
					);
					if(!$this->exists_content($item_id,$name_element[2],$language['name']))
					{
						$this->db->trans_start();
						$this->db->insert('items_contenido',$content_data);
						$this->db->trans_complete();
						
						
					}
					else
					{
					
						
						$this->db->trans_start();
						$this->db->set('cont_element', $elemento);
						$this->db->where('item_id', $item_id);
						$this->db->where('name_element', $name_element[2]);
						$this->db->where('lang_element', $language['name']);
						$this->db->update('items_contenido');
						$this->db->trans_complete();
					
					
						/**/
					}
				}
	
			}
			}
		}
	}
	/*Verifica si existen registros de contenido de cierto item*/
	function exists_content($item_id,$name_element,$lang){
		$this->db->from('items_contenido');
		$this->db->where('item_id',$item_id);
		$this->db->where('name_element',$name_element);
		$this->db->where('lang_element',$lang);
		$query = $this->db->get();

		return ($query->num_rows()==1);
	}
	/*Obtiene todas las rutas de las meta_url de los items*/
	function get_items_urls_routes($item_id){
		$this->db->distinct();
		$this->db->from('items_contenido');
		$this->db->where('item_id',$item_id);
		$this->db->where('name_element','MetaUrl');
		$query = $this->db->get();
		$urls=array();
		foreach($query->result() as $url)
		{
			$urls[intval($url->item_id)][$url->lang_element]=$url->cont_element;
		}
		return $urls;
		
		
	}
	/*Obtiene un rango para saber los items importados*/
	function get_items_import($num){
		$this->db->from('items');
		$this->db->where('deleted',0);
		$this->db->order_by("item_id", "desc");
		$this->db->limit($num);
		return $this->db->get();

	}
	/*Obtiene el ultimo registro de los items importados*/
	function get_last_row(){
		$this->db->select('item_id');
		$this->db->from('items');
		$this->db->limit(1);
		$this->db->order_by('item_id', "desc");
		return $this->db->get()->result_array()[0];

	}
	/*Obtiene la cantidad de items importados ESTA FUNCION NO DEBE DE IR EN EL MODELO*/
	function get_list_items_import_num(){
		return $this->session->userdata('items_excel');
	}
}
?>