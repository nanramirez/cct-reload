<?php
class Receiving extends CI_Model
{
	/*Obtiene información sobre una entrada en particular */
	function get_info($receiving_id){
		$this->db->from('receivings');
		$this->db->where('receiving_id',$receiving_id);
		$query = $this->db->get();
		if($query->num_rows()==1){
			return $query->row();
		}
		else{
			/* Obtiene el objeto principal base vacío, ya que $item_id NO es un elemento */
			$item_obj=new stdClass();
			/* Obtener todos los campos de la tabla de elementos */
			$fields = $this->db->list_fields('receivings');
			foreach ($fields as $field){
				$item_obj->$field='';
			}
			return $item_obj;
		}
		/*$this->db->from('receivings');
		$this->db->where('receiving_id',$receiving_id);
		return $this->db->get();*/
	}
	/*Regresa un array con información sobre los items de una entrada en particular */
	function get_items_info($receiving_id){
		$this->db->from('receivings_items');
		$this->db->where('receiving_id',$receiving_id);
		/* devolver una serie de elementos del kit de elementos para un artículo */
		return $this->db->get()->result_array();
	}
	/*Devuelve todas los entradas*/
	function get_all($limit=10000, $offset=0, $mes, $estado){
		$this->db->from('receivings');
		$this->db->where('modo ', $estado);
		$this->db->join('suppliers','receivings.supplier_id=suppliers.person_id');
		if($mes>0)
		{
			$this->db->where('receiving_time BETWEEN "'. date('Y-'.$mes.'-01 00:00:00'). '" and "'. date('Y-'.++$mes.'-01 00:00:00)"'));
		}
		$this->db->order_by("receiving_time", "desc");
		$this->db->limit($limit);
		$this->db->offset($offset);
		return $this->db->get();
	}
	/*Regresa el numero de registros de la tabla*/
	function count_all(){
		$this->db->from('receivings');
		return $this->db->count_all_results();
	}
	/*Verifica la exisencia de algun registro por el id*/
	function exists($receiving_id){
		$this->db->from('receivings');
		$this->db->where('receiving_id',$receiving_id);
		$query = $this->db->get();
		return ($query->num_rows()==1);
	}
	/*Guarda o actualiza la información de una entrada*/
	function save ($items,&$receivings_data ,$receiving_id=false){
		if(count($items)==0)return -1;
		$estado=$receivings_data["modo"];
		if($estado=="Receive")$estado=$this->lang->line("module_receiving_id");
		/* Ejecutar estas consultas como una transacción, queremos asegurarnos de que hacemos todo o nada */
		$this->db->trans_start();
		$this->db->insert('receivings',$receivings_data);
		$receivings_data["receiving_id"] = $this->db->insert_id();
			foreach($items as $line=>$item)
			{
				$cur_item_info = $this->Item->get_info($item['item_id']);
				$receivings_items_data = array
				(
					'receiving_id'=>$receivings_data["receiving_id"],
					'item_id'=>$item['item_id'],
					'line'=>$item['line'],
					'description'=>$item['description'],
					'serialnumber'=>$item['serialnumber'],
					'quantity_purchased'=>$item['quantity'],
					'discount_percent'=>$item['discount'],
					'item_cost_price' => $item['price'],
					'item_unit_price'=>$cur_item_info->unit_price
				);
				$this->db->insert('receivings_items',$receivings_items_data);
				$qty_recv ="";
				if($receivings_data["modo"]!="Pendiente")
				{
					if($item['price'] > $cur_item_info->cost_price)
					{
						$this->Item->update_cost_price($item['price'],$item['item_id']);
					}
					/* Actualizar stock de cantidad */
					if($receivings_data["modo"]=="Receive")
					{	$item_data = array('quantity'=>$cur_item_info->quantity + $item['quantity']);
						$qty_recv ="+".$item['quantity'];
					}
					elseif($receivings_data["modo"]=="Return")
					{
						$item_data = array('quantity'=>$cur_item_info->quantity - $item['quantity']);
						$qty_recv ="-".$item['quantity'];
					}
					$this->Item->save($item_data,$item['item_id']);
					$recv_remarks =$estado.'# '.$receivings_data["receiving_id"];
					$inv_data = array
					(
						'trans_date'=>date('Y-m-d H:i:s'),
						'trans_items'=>$item['item_id'],
						'trans_user'=>$receivings_data["employee_id"],
						'trans_comment'=>$recv_remarks,
						'trans_inventory'=>$qty_recv,
						'trans_type'=>'entrada'
					);
					$this->Inventory->insert($inv_data);
				}
				/* $supplier = $this->Supplier->get_info($supplier_id); */
			}
		return $this->db->trans_complete();
	}
	/*Regresa un objeto con información sobre los items de una entrada en particular */
	function get_receiving_items($receiving_id){
		$this->db->from('receivings_items');
		$this->db->where('receiving_id',$receiving_id);
		return $this->db->get();
	}
	/*Obtiene el proveedor de cierta entrada*/
	function get_supplier($receiving_id){
		$this->db->from('receivings');
		$this->db->where('receiving_id',$receiving_id);
		return $this->Supplier->get_info($this->db->get()->row()->supplier_id);
	}
	/*Creamos una tabla temporal que nos permite hacer consultas fáciles de informes / recepción */
	public function create_receivings_items_temp_table(){
		$this->db->query("CREATE TEMPORARY TABLE ".$this->db->dbprefix('receivings_items_temp')."
		(SELECT date(receiving_time) as receiving_date, ".$this->db->dbprefix('receivings_items').".receiving_id, comment,payment_type, employee_id, 
		".$this->db->dbprefix('items').".item_id, ".$this->db->dbprefix('receivings').".supplier_id, quantity_purchased, item_cost_price, item_unit_price,
		discount_percent,
		ROUND((item_cost_price*quantity_purchased-item_cost_price*quantity_purchased*discount_percent/100)*(SUM(percent)/100),2) as tax,	(item_cost_price*quantity_purchased-item_cost_price*quantity_purchased*discount_percent/100) as subtotal,
		".$this->db->dbprefix('receivings_items').".line as line, serialnumber, ".$this->db->dbprefix('receivings_items').".description as description,
		ROUND((item_cost_price*quantity_purchased-item_cost_price*quantity_purchased*discount_percent/100),2) as total,
		(item_unit_price*quantity_purchased)-(item_cost_price*quantity_purchased) as profit
		FROM ".$this->db->dbprefix('receivings_items')."
		INNER JOIN ".$this->db->dbprefix('receivings')." ON  ".$this->db->dbprefix('receivings_items').'.receiving_id='.$this->db->dbprefix('receivings').'.receiving_id'."
		INNER JOIN ".$this->db->dbprefix('items')." ON  ".$this->db->dbprefix('receivings_items').'.item_id='.$this->db->dbprefix('items').'.item_id'."
		LEFT OUTER JOIN ".$this->db->dbprefix('receivings_items_taxes')." ON  "
		.$this->db->dbprefix('receivings_items').'.receiving_id='.$this->db->dbprefix('receivings_items_taxes').'.receiving_id'." and "
		.$this->db->dbprefix('receivings_items').'.item_id='.$this->db->dbprefix('receivings_items_taxes').'.item_id'." and "
		.$this->db->dbprefix('receivings_items').'.line='.$this->db->dbprefix('receivings_items_taxes').'.line'."
		GROUP BY receiving_id, item_id, line)");
	}
	/*Obtiene la sugerencia de busquedas por filtro*/
	function get_search_suggestions($search,$limit=25){
		$suggestions = array();
		$this->db->from('receivings');
		$this->db->like('receiving_time', $search);
		$this->db->order_by("receiving_time", "asc");
		$by_name = $this->db->get();
		foreach($by_name->result() as $row)
		{
			$suggestions[]=$row->receiving_time;
		}
		$this->db->from('suppliers');
		$this->db->join('people','suppliers.person_id=people.person_id');
		$this->db->like("company",$search);
		$this->db->order_by("company", "asc");
		$by_company_name = $this->db->get();
		foreach($by_company_name->result() as $row)
		{
			$suggestions[]=$row->company;
		}
		/* solo devuelve sugerencias de $limite */
		if(count($suggestions > $limit))
		{
			$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;
	}
	/*Busqueda*/
	function search($search){
		$this->db->from('receivings');
		$this->db->join('suppliers','receivings.supplier_id=suppliers.person_id');
		$this->db->where("receiving_time LIKE '%".$this->db->escape_like_str($search)."%' ESCAPE '!' or 
		company LIKE '%".$this->db->escape_like_str($search)."%' ESCAPE '!'");
		$this->db->order_by("receiving_id", "asc");
		return $this->db->get();
	}
	/*Borra una lista de entradas pendientes*/
	function delete_list(&$entradas_to_delete){
		$this->db->trans_start();
		foreach($entradas_to_delete as $delete_id)
		{
			$this->db->delete('receivings_items', array('receiving_id' => $delete_id));
			$this->db->where_in('receiving_id', $delete_id);
			$this->db->where_in('modo', 'Pendiente');
			$this->db->delete('receivings');
		}
		return $this->db->trans_complete();
	}
}
?>