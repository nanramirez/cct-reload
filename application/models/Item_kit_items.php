<?php
class Item_kit_items extends CI_Model
{
	/*Obtiene la informacion particular de los productos contenidos en un kit*/
	function get_info($item_kit_id){
		$this->db->from('item_kit_items');
		$this->db->where('item_kit_id',$item_kit_id);
		/* devolver una serie de elementos del kit de elementos para un artículo */
		$query=$this->db->get();
		return $query->result_array();
	}
	/*Guarda los items de un kit en la BD */
	function save(&$item_kit_items, $item_kit_id){
		$success=false;
		$this->db->trans_start();
		$this->db->from('item_kit_items');
		$this->db->where('item_kit_id',$item_kit_id);
		$query = $this->db->get();
		/* Insercion Si solo hay un tipo de item */
		if($query->num_rows()<1 && count($item_kit_items)==1)
		{
			$this->db->insert('item_kit_items',
						array(
						'item_kit_id'=>$item_kit_id,
						'item_id'=>key($item_kit_items),
						'quantity'=>current($item_kit_items)));
		}
		/* Insercion si ya habia un item y se agrega uno mas */
		if($query->num_rows()==1 && count($item_kit_items)==1)
		{
			$this->db->delete('item_kit_items', array('item_kit_id' => $item_kit_id));
			$this->db->insert('item_kit_items',
						array(
						'item_kit_id'=>$item_kit_id,
						'item_id'=>key($item_kit_items),
						'quantity'=>current($item_kit_items)));
		}
		/* Insercion si ya habia un item y se agrega mas de uno */
		if(count($item_kit_items)>1)
		{
			$this->db->delete('item_kit_items', array('item_kit_id' => $item_kit_id));
			$this->db->insert('item_kit_items',
						array(
						'item_kit_id'=>$item_kit_id,
						'item_id'=>key($item_kit_items),
						'quantity'=>current($item_kit_items)));
			foreach($item_kit_items as $itemq)
					{
						if(key($item_kit_items)!=null)
						{
						$success = $this->db->insert('item_kit_items',
						array(
						'item_kit_id'=>$item_kit_id,
						'item_id'=>key($item_kit_items),
						'quantity'=>current($item_kit_items)));
						}
					}
		}
		/* Quita un item cuando solo hay dos */
		if(count($item_kit_items)==1)
		{
			$this->db->delete('item_kit_items', array('item_kit_id' => $item_kit_id));
			$this->db->insert('item_kit_items',
						array(
						'item_kit_id'=>$item_kit_id,
						'item_id'=>key($item_kit_items),
						'quantity'=>current($item_kit_items)));
		}
		/* Borra todo si no hay items que guardar */
		if(count($item_kit_items)<1)
		{
			$this->db->delete('item_kit_items', array('item_kit_id' => $item_kit_id));
		}
		$this->db->trans_complete();
	}
	/*Borra los items de un kit*/
	function delete($item_kit_id){
		return $this->db->delete('item_kit_items', array('item_kit_id' => $item_kit_id)); 
	}
}
?>