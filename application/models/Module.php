<?php
class Module extends CI_Model 
{
	/*Se inicia el constructor*/
    function __construct(){
        parent::__construct();
    }
	/*Se obtiene el nombre del modulo mediante la búsqueda id y un limit*/
	function get_module_name($module_id){
		$query = $this->db->get_where('modules', array('module_id' => $module_id), 1);
		if ($query->num_rows() ==1)
		{
			$row = $query->row();
			return $this->lang->line($row->name_lang_key);
		}
		return $this->lang->line('error_unknown');
	}
	/*obtiene la descripción del modulo mediante la búsqueda id*/
	function get_module_desc($module_id){
		$query = $this->db->get_where('modules', array('module_id' => $module_id), 1);
		if ($query->num_rows() ==1)
		{
			$row = $query->row();
			return $this->lang->line($row->desc_lang_key);
		}
		return $this->lang->line('error_unknown');
	}
	/*Obtiene todos los datos de todos los módulos*/
	function get_all_modules(){
		$this->db->from('modules');
		$this->db->order_by("sort", "asc");
		return $this->db->get();
	}
	/*Obtiene los datos relacionados con módulos relacionados con una persona mediante id*/
	function get_allowed_modules($person_id){
		$this->db->from('modules');
		$this->db->join('permissions','permissions.module_id=modules.module_id');
		$this->db->where("permissions.person_id",$person_id);
		$this->db->order_by("sort", "asc");
		return $this->db->get();
	}
}
?>