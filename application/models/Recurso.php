<?php
class Recurso extends CI_Model
{
	/*Devuelve todos los Recursos*/
	function get_all($limit=10000, $offset=0){
		$this->db->from('recursos');
		$this->db->order_by("recurso_id", "desc");
		$this->db->limit($limit);
		$this->db->offset($offset);
		return $this->db->get();
	}
	/*Devuelve todas las operaciones de los Recursos*/
	function get_all_operations($limit=10000, $offset=0,$mes=0,$recurso_id){
		$this->db->from('recursos_operaciones');
		$num_dias=date('t',$mes);
		if($mes==date('n'))
		{
			$this->db->where('fecha BETWEEN "'. date('Y-'.date('n').'-01 00:00:00'). '" and "'. date('Y-'.date('n').'-'.date('t',date('n')).' 00:00:00)"'));
		}
		elseif($mes>date('n'))
		{
			$this->db->where('fecha BETWEEN "'. date('Y-'.$mes.'-01 00:00:00'). '" and "'. date('Y-'.$mes.'-'.$num_dias.' 00:00:00)"'));
		}
		elseif($mes<date('n') && $mes!=0)
		{
			$this->db->where('fecha BETWEEN "'. date('Y-'.$mes.'-01 00:00:00'). '" and "'. date('Y-'.$mes.'-'.$num_dias.' 00:00:00)"'));
		}
		$this->db->where('recurso_id',$recurso_id);
		$this->db->order_by("operation_time", "desc");
		$this->db->limit($limit);
		$this->db->offset($offset);
		return $this->db->get();
	}
	/*Verifica la exisencia de algun registro por el id*/
	function exists($recurso_id){
		$this->db->from('recursos');
		$this->db->where('recurso_id',$recurso_id);
		$query = $this->db->get();
		return ($query->num_rows()==1);
	}
	/*Regresa el numero de registros de la tabla*/
	function count_all(){
		$this->db->from('recursos');
		return $this->db->count_all_results();
	}
	/*Obtiene información sobre un recurso en particular */
	function get_info($recurso_id){
		$this->db->from('recursos');
		$this->db->where('recurso_id',$recurso_id);
		$query = $this->db->get();
		if($query->num_rows()==1){
			return $query->row();
		}
		else{
			/* Get empty base parent object, as $item_id is NOT an item */
			$item_obj=new stdClass();
			/* Get all the fields from items table */
			$fields = $this->db->list_fields('recursos');
			foreach ($fields as $field){
				$item_obj->$field='';
			}
			return $item_obj;
		}
	}
	/*Obtiene las formas de pago de un recurso */
	function get_info_formas_pago($recurso_id){
		$this->db->from('formas_pago');
		$this->db->where('recurso_id',$recurso_id);
		$query=$this->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->result_array();
		}
		else
		{
			/*Get empty base parent object, as $item_id is NOT an item*/
			$item_obj=new stdClass();
			/* Get all the fields from items table */
			$fields = $this->db->list_fields('formas_pago');
			foreach ($fields as $field)
			{
				$item_obj->$field='';
			}
			return $item_obj;
		}
	}
	/*Obtiene todas las formas de pago de los recursos*/
	function get_all_formas_pago(){
		$this->db->from('formas_pago');
		$this->db->order_by("nombre", "asc");
		return $this->db->get()->result_array();
	}
	/*Obtiene el nombre del pago por el id */
	function get_info_nombre_metodo($formas_pago_id){
		$this->db->from('formas_pago');	
		$this->db->where_in('formas_pago_id',$formas_pago_id);
		foreach($this->db->get()->result_array() as $method)
		{
			return $method["nombre"];
		}
	}
	/*Obtiene el recurso segun el id*/
	function get_info_recurso_metodo($formas_pago_id){
		$this->db->from('formas_pago');	
		$this->db->where_in('formas_pago_id',$formas_pago_id);
		foreach($this->db->get()->result_array() as $method)
		{
			return $method["recurso_id"];
		}
	}
	/*Obtiene el id del recurso por el nombre del metodo de pago*/
	function get_info_recurso_id($nombre){
		$this->db->from('formas_pago');	
		$this->db->where_in('nombre',$nombre);
		foreach($this->db->get()->result_array() as $method)
		{
			return $method["recurso_id"];
		}
	}
	/*Obtiene todas el tipo de monedas*/
	function get_info_coins(){
		$this->db->distinct();
		$this->db->select('coin');
		$this->db->select('nombre');
		$this->db->from('recursos');
		$query=$this->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->result_array();
		}
		else
		{
			/*Get empty base parent object, as $item_id is NOT an item*/
			$item_obj=new stdClass();
			/*Get all the fields from items table*/
			$fields = $this->db->list_fields('coin');
			foreach ($fields as $field)
			{
				$item_obj->$field='';
			}
			return $item_obj;
		}
	}
	/*Cambia el estado deleted=1 de un lista de recursos*/
	function delete_list(&$recursos_to_delete){
		$this->db->trans_start();
		foreach($recursos_to_delete as $delete_id)
		{
			$this->db->where_in('recurso_id',$delete_id);
			$this->db->delete('recursos');
		}
		return $this->db->trans_complete();	
 	}
	/*Guarda o actualiza la información de un recurso*/
	function save(&$recurso_data,$recurso_id=false){

		
		if (!$recurso_id or !$this->exists($recurso_id))
		{
			if($this->db->insert('recursos',$recurso_data))
			{
				$recurso_data['recurso_id']=$this->db->insert_id();
				return true;
			}
			return false;
		}
		$this->db->where('recurso_id', $recurso_id);
		return $this->db->update('recursos',$recurso_data);
	}
	/*Guarda una nueva forma de pago*/
	function save_formas_pago(&$formas_pago,$recurso_id=false){
			if($this->db->insert('formas_pago',array("nombre"=>$formas_pago,"recurso_id"=>$recurso_id)))
			{
				return true;
			}
			return false;
	}
	/*Busqueda*/
	function search($search){
		$this->db->from('recursos');
		$this->db->where("nombre LIKE '%".$this->db->escape_like_str($search)."%' ESCAPE '!'");
		$this->db->order_by("nombre", "asc");
		return $this->db->get();
	}
	/*Resta a un recurso*/
	function rest_recurso($cantidad,$recurso_id,$coin){
		$recurso=$this->get_info($recurso_id);
		$config_info=$this->Appconfig->get_info();
		if($recurso->coin == $coin)
		{
		$datos = array('recurso'=>$recurso->recurso - $cantidad);
		$recurso->recurso-=$cantidad;
		}
		elseif($recurso->coin != $coin && $recurso->coin != $config_info['coin'])
		{
			$datos = array('recurso'=>$recurso->recurso - ($cantidad/$config_info['xchange_rate']));
			$recurso->recurso-=$cantidad;
		}
		elseif($recurso->coin != $coin && $recurso->coin == $config_info['coin'] )
		{
			$datos = array('recurso'=>$recurso->recurso - ($cantidad*$config_info['xchange_rate']));
			$recurso->recurso-=$cantidad;
		}
		$this->db->where_in('recurso_id',$recurso_id);
		return $this->db->update('recursos',$datos);
	}
	/*Suma a un recurso*/
	function sum_recurso($cantidad,$recurso_id,$coin){
		$recurso=$this->get_info($recurso_id);
		$config_info=$this->Appconfig->get_info();
	if($recurso->coin == $coin)
		{
			$datos = array('recurso'=>$recurso->recurso + $cantidad);
			$recurso->recurso-=$cantidad;
		}
		elseif($recurso->coin != $coin && $recurso->coin != $config_info['coin'])
		{
			$datos = array('recurso'=>$recurso->recurso + ($cantidad/$config_info['xchange_rate']));
			$recurso->recurso-=$cantidad;
		}
		elseif($recurso->coin != $coin && $recurso->coin == $config_info['coin'] )
		{
			$datos = array('recurso'=>$recurso->recurso + ($cantidad*$config_info['xchange_rate']));
			$recurso->recurso-=$cantidad;
		}
		$this->db->where_in('recurso_id',$recurso_id);
		return $this->db->update('recursos',$datos);
	}
	/*Obtiene la sugerencia de busquedas por filtro*/
	function get_search_suggestions($search,$limit=25,$filtro){
		$suggestions = array();
		if($filtro=="nombre")
		{
		$this->db->from("recursos");
		$this->db->like("nombre",$search);
		$this->db->order_by("nombre", "asc");
		$consulta = $this->db->get();
		foreach($consulta->result() as $row)
		{
		$suggestions[]=$row->nombre;
		}
		}
		return $suggestions;
	}
	/*Busqueda especifica por filtro*/
	function specific_search($search,$filtro){
		if ($filtro=="nombre")
		{
			$this->db->from("recursos");
			$this->db->where("deleted", 0);
			$this->db->like("nombre",$search);
			$this->db->order_by("nombre", "asc");
			return 	$this->db->get();
		}
	}
	/*Borra una forma de pago*/
	function delete_forma_pago($formas_pago_id){
		return $this->db->delete('formas_pago', array('formas_pago_id' => $formas_pago_id)); 
	}
	/*Guarda los datos de una nueva operacion en algun recurso*/
	function save_operation(&$operation_data){
		if($this->db->insert('recursos_operaciones',$operation_data))
		{
			$operation_data['operation_id']=$this->db->insert_id();
			return true;
		}
		else{
			return false;
		}
	}
}
?>