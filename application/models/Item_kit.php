<?php
class Item_kit extends CI_Model
{       
    /*Verifica la exisencia de algun registro por el nombre de un kit*/
	function exists($item_kit_name){
		$this->db->from('item_kits');
		$this->db->where('deleted',0);
		$this->db->where('name',$item_kit_name);
		$query = $this->db->get();
		return ($query->num_rows()==1);
	}
	/*Verifica si el codigo de barras no esta en uso por algun kit*/
	function barcode_exists($barcode,$item_kit_id){
		$this->db->from('item_kits');
		$this->db->where("item_kit_id !=",$item_kit_id);
		$this->db->where('kit_number',$barcode);
		$query = $this->db->get();
		return ($query->num_rows()==1);
	}
	/*Verifica la exisencia de algun registro por el id*/
	function exists_by_id($item_kit_id){
		$this->db->from('item_kits');
		$this->db->where('item_kit_id',$item_kit_id);
		$this->db->where('deleted',0);
		$query = $this->db->get();
		return ($query->num_rows()==1);
	}
	/*Verifica si existe cfdi*/
	function exists_cfdi_field($item_kit_id){
		$this->db->from('item_kits_cfdi');
		$this->db->where('item_kit_id',$item_kit_id);
		$query = $this->db->get();
		return ($query->num_rows()==1);
	}
	/*Verifica la vigencia de los kits de una categoria especifica*/
	function count_all_cat($categorie){
		$hoy= date("Y").'-'.date("m").'-'.date("d").' 00:00:00';
		$this->db->from('item_kits');
		$this->db->where('deleted',0);
		$this->db->where('category',$categorie);
		$this->db->where('expiracion >=',$hoy);
		$this->db->where('publicar',1);
		$this->db->order_by("category", "asc");
		return $this->db->get();
	}
	/*Devuelve todas las categorias de kits con vigencia*/
	function get_categories(){
		$hoy= date("Y").'-'.date("m").'-'.date("d").' 00:00:00';
		$this->db->select('category');
		$this->db->from('item_kits');
		$this->db->where('deleted',0);
		$this->db->where('expiracion >=',$hoy);
		$this->db->where('publicar',1);
		$this->db->distinct();
		$this->db->order_by("category", "asc");
		$by_category=$this->db->get();
		foreach($by_category->result() as $row)
		{
			$cats[]=$row->category;
		}
		if(isset($cats))
		{
			return $cats;
		}
	}
	/*Devuelve todos los kits*/
	function get_all($filtro,$limit=10000, $offset=0){
		$this->db->from('item_kits');
		$this->db->where('deleted',0);
		$this->db->order_by($filtro, "asc");
		$this->db->limit($limit);
		$this->db->offset($offset);
		return $this->db->get();
	}
	/*Regresa todos los kits usados como servicios*/
	function get_all_services(){
		$this->db->from('item_kits');
		$this->db->where('deleted',0);
		$this->db->where('servicio',1);
		$this->db->where('publicar',0);
		$this->db->order_by('servicio', "asc");
		return $this->db->get();
	}
	/*Regresa el numero de registros de la tabla*/
	function count_all(){
		$this->db->from('item_kits');
		$this->db->where('deleted',0);
		return $this->db->count_all_results();
	}
	/*Obtiene la informacion de un kit especifico por id*/
	function get_info($item_kit_id){
		$this->db->from('item_kits');
		$this->db->where('item_kit_id',$item_kit_id);
		$this->db->where('deleted',0);
		$query = $this->db->get();
		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			/*Get empty base parent object, as $item_kit_id is NOT an item kit*/
			$item_obj=new stdClass();
			/*Get all the fields from items table*/
			$fields = $this->db->list_fields('item_kits');
			foreach ($fields as $field)
			{
				$item_obj->$field='';
			}
			return $item_obj;
		}
	}
	/*Obtiene la informacion de un kit especifico por su nombre*/
	function get_info_by_name($item_kit_name){
		$this->db->from('item_kits');
		$this->db->where('name',$item_kit_name);
		$this->db->where('deleted',0);
		$query = $this->db->get();
		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			/*Get empty base parent object, as $item_kit_id is NOT an item kit*/
			$item_obj=new stdClass();
			/*Get all the fields from items table*/
			$fields = $this->db->list_fields('item_kits');
			foreach ($fields as $field)
			{
				$item_obj->$field='';
			}
			return $item_obj;
		}
	}
	/*Obtiene la informacion multiple de algunos kits por sus ids*/
	function get_multiple_info($item_kit_ids){
		$this->db->from('item_kits');
		$this->db->where_in('deleted',0);
		$this->db->where_in('item_kit_id',$item_kit_ids);
		$this->db->order_by("name", "asc");
		return $this->db->get();
	}
	/*Obtiene la informacion del codigo de producto y de servicio de un kit especifico por su id*/
	function get_info_cfdi($item_kit_id){
		$this->db->from('item_kits_cfdi');
		$this->db->where('item_kit_id',$item_kit_id);
		$query = $this->db->get();
		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			$item_obj=new stdClass();
			/*Get all the fields from items table*/
			$fields = $this->db->list_fields('item_kits_cfdi');
			foreach ($fields as $field)
			{
				$item_obj->$field='';
			}
			return $item_obj;
		}
	}
	/*Guarda la informacion del codigo de producto y de servicio de un kit especifico por su id*/
	function save_cfdi_fields(&$cfdi_fields,$item_kit_id){
		$success=false;
		$this->db->trans_start();
		if (!$this->exists_cfdi_field($item_kit_id))
		{
			$cfdi_fields["item_kit_id"]=$item_kit_id;
			$success =$this->db->insert('item_kits_cfdi',$cfdi_fields);
		}
		else
		{
			$this->db->where('item_kit_id', $item_kit_id);
			$success =$this->db->update('item_kits_cfdi',$cfdi_fields);
		}
		$this->db->trans_complete();
		return $success;
	}
	/*Guarda un nuevo o edita los datos de un kit */
	function save(&$item_kit_data,$item_kit_id=false){
		if (!$item_kit_id or !$this->exists_by_id($item_kit_id))
		{
			if($this->db->insert('item_kits',$item_kit_data))
			{
				$item_kit_data['item_kit_id']=$item_kit_id=$this->db->insert_id();
				return true;
			}
			return false;
		}
		$this->db->where('item_kit_id', $item_kit_id);
		return $this->db->update('item_kits',$item_kit_data);
	}
	/*Cambia el estado deleted=1 de un kit*/
	function delete($item_kit_id){
		return $this->db->delete('item_kits', array('item_kit_id' => $id));
	}
	/*Cambia el estado deleted=1 de multiples kits por id*/
	function delete_list(&$item_kits_to_delete){
		$this->db->trans_start();
		foreach($item_kits_to_delete as $delete_id)
		{
			$this->db->where_in('item_kit_id',$delete_id);
			$this->db->update('item_kits', array('deleted' => 1));
		}
		return $this->db->trans_complete();
 	}
	/*Obtiene las sugerencias de busqueda segun su filtro*/
	function get_search_suggestions($search,$limit=10,$filtro,$offset=0){
		$suggestions = array();
		if($filtro=="name")
		{
			$this->db->from('item_kits');
			$this->db->like($filtro, $search);
			$this->db->where('deleted',0);
			$this->db->order_by($filtro, "asc");
			$this->db->limit($limit,$offset);
			$by_name = $this->db->get();
			foreach($by_name->result() as $row)
			{
				$suggestions[]=$row->name;
			}
		}
		elseif($filtro=="category")
		{
			$this->db->select($filtro);
			$this->db->from('item_kits');
			$this->db->where('deleted',0);
			$this->db->distinct();
			$this->db->like($filtro, $search);
			$this->db->order_by($filtro, "asc");
			$this->db->limit($limit,$offset);
			$by_name = $this->db->get();
			foreach($by_name->result() as $row)
			{
				$suggestions[]=$row->category;
			}
		}
		elseif($filtro=="item_kit_id")
		{
			$this->db->from('item_kits');
			$this->db->like($filtro, $search);
			$this->db->where('deleted',0);
			$this->db->order_by($filtro, "asc");
			$this->db->limit($limit,$offset);
			$by_category = $this->db->get();
			foreach($by_category->result() as $row)
			{
				$suggestions[]=$row->item_kit_id;
			}
		}
		$this->db->from('item_kits');
		return $suggestions;
	}
	/*Obtiene las sugerencias de busqueda de las categorias existentes*/
	function get_category_suggestions($search,$limit=25){
		$suggestions = array();
		$this->db->distinct();
		$this->db->select('category');
		$this->db->from('item_kits');
		$this->db->like('category', $search);
		$this->db->where('deleted', 0);
		$this->db->order_by("category", "asc");
		$by_category = $this->db->get();
		foreach($by_category->result() as $row)
		{
			$suggestions[]=$row->category;
		}
		if(count($suggestions) > $limit)
		{
		$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;
	}
	/*Obtiene las sugerencias de busquedas de kits usados como servicios*/
	function get_item_kit_search_suggestions($search, $limit=25){
		$suggestions = array();
		$this->db->from('item_kits');
		$this->db->like('name', $search);
		$this->db->where('deleted',0);
		$this->db->where('servicio',0);
		$this->db->order_by("name", "asc");
		$by_name = $this->db->get();
		foreach($by_name->result() as $row)
		{
			$suggestions[]=''.$row->name.'|'.$row->name;
		}
		/* only return $limit suggestions */
		if(count($suggestions) > $limit)
		{
			$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;
	}
	/*Busqueda*/
	function search($search){
		$this->db->from('item_kits');
		$this->db->where("name LIKE '%".$this->db->escape_like_str($search)."%' ESCAPE '!' or 
		description LIKE '%".$this->db->escape_like_str($search)."%'");
		$this->db->order_by("name", "asc");
		return $this->db->get();
	}
	/*Busqueda especifica por filtro*/
	function specific_search($search,$filtro){
		$this->db->from('item_kits');
		$this->db->where("(".$filtro." LIKE '%".$this->db->escape_like_str($search)."%' ESCAPE '!') and deleted=0");
		$this->db->order_by($filtro, "asc");
		return $this->db->get();
	}
	/*Edita la informacion de ciertas columnas en varios kits por sus ids*/
	function update_multiple($kit_data,$kits_to_update){
		$this->db->where_in('item_kit_id',$kits_to_update);
		return $this->db->update('item_kits',$kit_data);
	}
	/*Edita la informacion del codigo de producto y de servicio en varios items por sus ids*/
	function update_multiple_cfdi_data($cfdi_fields,$item_kit_ids){
		foreach($item_kit_ids as $item_kit_id)
		{
			
			$this->save_cfdi_fields($cfdi_fields,$item_kit_id);
		}
	}
	/*Guarda la info del contenido en los idiomas disponibles en la bd*/
	function save_kits_content($kit_id){
		$languages =get_dir_file_info(APPPATH.'language/');
		foreach($languages as $language)
		{
			if($language["name"]!="index.html")
			{
				$elementos=$this->lang_lib->get_kits_content_file($language['name'],$kit_id);
				
				foreach($elementos as $key=>$elemento)
				{
					
					$name_element=explode('_',$key);
					
					if($name_element[1]==$kit_id)
					{
						$content_data=array(
						'name_element'=>$name_element[2],
						'cont_element'=>$elemento,
						'lang_element'=>$language['name'],
						'kit_id'=>$kit_id
						);
						if(!$this->exists_content($kit_id,$name_element[2],$language['name']))
						{
							$this->db->trans_start();
							$this->db->insert('kits_contenido',$content_data);
							$this->db->trans_complete();
						}
						else
						{
							
							$this->db->trans_start();
							$this->db->set('cont_element', $elemento);
							$this->db->where('kit_id', $kit_id);
							$this->db->where('name_element', $name_element[2]);
							$this->db->where('lang_element', $language['name']);
							$this->db->update('kits_contenido');
							$this->db->trans_complete();
							/**/
						}
					}
				}
			}	
		}
	}
	/*Verifica si existen registros de contenido de cierto kit*/
	function exists_content($kit_id,$name_element,$lang){
		$this->db->from('kits_contenido');
		$this->db->where('kit_id',$kit_id);
		$this->db->where('name_element',$name_element);
		$this->db->where('lang_element',$lang);
		$query = $this->db->get();
		return ($query->num_rows()==1);
	}
	/*Obtiene todas las rutas de las meta_url de los kits*/
	function get_kits_urls_routes($kit_id){
		$this->db->distinct();
		$this->db->from('kits_contenido');
		$this->db->where('kit_id',$kit_id);
		$this->db->where('name_element','MetaUrl');
		$query = $this->db->get();
		$urls=array();
		foreach($query->result() as $url)
		{
			$urls[intval($url->kit_id)][$url->lang_element]=$url->cont_element;
		}
		return $urls;
	}
}
?>