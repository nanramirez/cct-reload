<?php
require_once ("Secure_area.php");
class Home extends Secure_area
{   /*Es una extensión del controller login pero solo accesible de manera segura*/
	function __construct(){
		parent::__construct();
	}
	/*Carga el view de home*/
	function index(){
		$data['notificacion_qty']=$this->Notificacion->get_all()->num_rows();
		$data['notificaciones']=notification_header($this->Notificacion->get_all());
		$data['version']=get_cc_version();
		$config_info=$this->Appconfig->get_info();
		$data['config_info']=0;
		$data['controller_name']=strtolower(get_class());
		$this->load->view("cc_views/home",$data);
	}
	/*Cierra la sesión de usuario actual*/
	function logout(){
		$this->Employee->logout();
	}
	/*Respalda la BD del sistema*/
	function backup(){
		$this->load->dbutil();
		$año=date('Y');
		$mes=date('F');
		$dia=date('j');
		if(!file_exists('backups/'.$año)){
			mkdir('backups/'.$año,0777);
		}
		if(!file_exists('backups/'.$año.'/'.$mes)){
			mkdir('backups/'.$año.'/'.$mes,0777);
		}
		if(!file_exists('backups/'.$año.'/'.$mes.'/'.$dia)){
			mkdir('backups/'.$año.'/'.$mes.'/'.$dia,0777);
		}
		$prefs = array(
			'tables'      => array(),          /* Aquí agregamos todas las tablas*/
			'ignore'      => array(),          /*Se puede excluir alguna por lo pronto ninguna*/
			'format'      => 'txt',             /* gzip, zip, txt*/
			'filename'    => 'respaldo_cct.sql',    /* Nombre del archivo*/
			'add_drop'    => TRUE,              /*Crea una copia de la estructura */
			'add_insert'  => TRUE,              /* Crea una sentencia de insertar con los datos de las tablas para futuras restauraciones*/
			'newline'     => "\n"               /* Caracteres de salto de linea*/);
		$backup =& $this->dbutil->backup($prefs);
		$this->load->helper('file');
		write_file('backups/'.$año.'/'.$mes.'/'.$dia.'/respaldo_cct.sql', $backup);
		$this->load->helper('download');
		force_download('respaldo_cct.sql', $backup);
	}
}
?>