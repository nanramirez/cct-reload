<?php
require_once ("Secure_area.php");
class Recursos extends Secure_area 
{
	function __construct(){
		parent::__construct('recursos');
		$data['controller_name']=strtolower(get_class());
		$this->load->model('Recurso');
	}
	/*Carga el manage view del modulo*/
	function index(){
		$data['controller_name']=strtolower(get_class());
		$data['notificacion_qty']=$this->Notificacion->get_all()->num_rows();
		$data['notificaciones']=notification_header($this->Notificacion->get_all());
		$data['version']=get_cc_version();
		$language=$this->lang_lib->get_idioma();
		$this->lang_lib->switch_to($language);
		$data['selected_month']=$this->config_lib->get_mes_show();
		if(!isset($data['selected_month']))$this->config_lib->set_mes_show(0);
		$filtro_uno=$this->config_lib->get_filtro_recursos();
		if(empty($filtro_uno)){
			$this->config_lib->set_filtro_recursos("fecha");
		}
		$data['filtro']=$this->config_lib->get_filtro_recursos();
		$data['config_info']=$this->Appconfig->get_info();
		$config['base_url'] = base_url()."index.php/".$data['controller_name'].'/page/';
		$config['total_rows'] = $this->Recurso->count_all();
		$config['per_page'] = '20'; 
		$this->pagination->initialize($config);
		$data['manage_table']=get_recursos_manage_table($this->Recurso->get_all($config['per_page'], $this->input->get('per_page')),$this);
		$this->load->view('cc_views/recursos/manage',$data);
	}
	/*Carga el form para editar o guardar un nuevo recurso*/
	function view($recurso_id=-1){
		$data['config_info']=$this->Appconfig->get_info();
		$language=$this->lang_lib->get_idioma();
		$this->lang_lib->switch_to($language);
		$data['json_form_errors']=json_encode(array(
			"error_nombre_cuenta"=>$this->lang->line('module_recursos_required_nombre_cuenta'),
			"error_cantidad"=>$this->lang->line('common_numeric_required'),
			"error_coin"=>$this->lang->line('module_recursos_coin_msj_error'),
			"error_formas_pago"=>$this->lang->line('common_field_required_payment_name')));
		$data['info']=$this->Recurso->get_info($recurso_id);
		$data['title_form']=$data['info']->nombre;
		$data['select_method']=-1;
		$data['payment_methods']=array(
			-1=>$this->lang->line('common_select_pay_method'),
			$this->lang->line('common_cash')=>$this->lang->line('common_cash'),
			$this->lang->line('common_deposit')=>$this->lang->line('common_deposit'),
			$this->lang->line('common_transfer')=>$this->lang->line('common_transfer'),
			$this->lang->line('common_card')=>$this->lang->line('common_card'),
			$this->lang->line('common_paypal')=>$this->lang->line('common_paypal'),
			$this->lang->line('common_moneygram')=>$this->lang->line('common_moneygram'),
			$this->lang->line('common_btc')=>$this->lang->line('common_btc'),
			$this->lang->line('common_cheque')=>$this->lang->line('common_cheque'),
			-2=>$this->lang->line('common_add_another_pay_method'));
		$data['payment_method']=$this->lang->line('common_add_pay_method');
		if($recurso_id==-1){
			$data['payment_method']=$this->lang->line('common_payments_methods');
			$data['title_form']=$this->lang->line('common_new').'  '.$this->lang->line('module_recursos_nombre_cuenta');
		}
		if(empty($data)){
			$data= array('nombre','recurso');
		 }
		foreach($this->Recurso->get_info_formas_pago($recurso_id) as $key=> $metodo){
			$formas_pago[$key]=$metodo;
		}
		$data['recurso_id']=$recurso_id;
		$data['metodos']=$formas_pago;
		$this->load->view("cc_views/recursos/form",$data);
	}
	/*Actualiza o guarda un nuevo recurso en la BD*/
	function save($recurso_id=-1){
		$language=$this->lang_lib->get_idioma();
		$this->lang_lib->switch_to($language);
		$recurso_data = array(
		"nombre"=>$this->input->post("nombre_cuenta"),
		"coin"=>$this->input->post("coin"),
		"recurso"=>$this->input->post("cantidad"));
		$formas_pago=$this->input->post("formas_pago");
		if($this->Recurso->save($recurso_data,$recurso_id)){
			if($recurso_id==-1)
			{
				echo json_encode(array('success'=>'true','message'=>$this->lang->line('module_recursos_success_msj'),'recurso_id'=>$recurso_data['recurso_id']));
				$recurso_id = $recurso_data['recurso_id'];
			}
			else /*artículo anterior*/
			{
				echo json_encode(array('success'=>'true','message'=>$this->lang->line('module_recursos_update_msj'),'recurso_id'=>$recurso_id));
			}
			if($formas_pago!="")$this->Recurso->save_formas_pago($formas_pago,$recurso_id);
		}
		else{
			/*fracaso*/
			echo json_encode(array('error'=>'false','message'=>$this->lang->line('common_form_error')));
		}
	}
	/*Sugerencia de busqueda*/
	function suggest(){
		$filtro="nombre";
		$valores=$this->input->post("q");
		$suggestions = $this->Recurso->get_search_suggestions($valores,'',$filtro);
		echo json_encode($suggestions);
	}
	/*Busca el Recursos mediante un filtro específico de busqueda*/
	function search(){
		$search=trim($this->input->post("search"));
		$filtro="nombre";
		$data_rows=get_recursos_manage_table_data_rows($this->Recurso->specific_search($search,$filtro),$this,true);
		echo json_encode(array("data_rows"=>$data_rows));
	}
	/*Obtiene una fila actualizada del manage table*/
	function get_row(){
		$language=$this->lang_lib->get_idioma();
		$this->lang_lib->switch_to($language);
		$recurso_id = $this->input->post("row_id");
		$data_row=get_recursos_data_row($this->Recurso->get_info($recurso_id),$this);
		echo $data_row;
	}
	/*Borra recursos en la BD*/
	function delete(){
		$language=$this->lang_lib->get_idioma();
		$this->lang_lib->switch_to($language);
		$recursos_to_delete=$this->input->post("ids")!=false ? $this->input->post("ids"):array();
		if($this->Recurso->delete_list($recursos_to_delete)){
			echo json_encode(array("success"=>"true","message"=>$this->lang->line("common_delete_succes")." ".
			count($recursos_to_delete)." ".$this->lang->line("module_recursos_nombre_cuenta")."(s)"));
		}
		else{
			echo json_encode(array('success'=>"false",'message'=>$this->lang->line('common_form_error')));
		}
	}
	/*Borra un metodo de pago de algun recurso*/
	function delete_forma_pago($formas_pago_id){
			$response=$this->Recurso->delete_forma_pago($formas_pago_id);
			echo json_encode(array('success'=>$response));
	}
	/*Regresa un objeto via ajax con la información de un recurso*/
	function get_recurso_info(){
		$recurso_info=$this->Recurso->get_info($this->input->post("recurso_id"));
		echo json_encode($recurso_info);
	}
	/*Carga el form para registrar una nueva operación*/
	function new_operation($recurso_id){
		$data['config_info']=$this->Appconfig->get_info();
		$language=$this->lang_lib->get_idioma();
		$this->lang_lib->switch_to($language);
		$data['json_form_errors']=json_encode(array(
		"error_concepto"=>$this->lang->line('common_concepto_required'),
		"error_select_recurso"=>$this->lang->line('module_recursos_operation_select_error'),
		"error_numeric"=>$this->lang->line('common_numeric_required_cero'),
		"error_numeric_positive"=>$this->lang->line('common_numeric_positive_required'),
		"error_less_resource"=>$this->lang->line('module_recursos_operation_less_resource_error'),
		"error_cantidad_monto"=>$this->lang->line('common_numeric_required_higher')
		));
		$data['concept_transfer_from']=$this->lang->line('module_recursos_operation_transfer_from');
		$data['concept_transfer_to']=$this->lang->line('module_recursos_operation_transfer_to');
		$recursos=$this->Recurso->get_all();
		foreach($recursos->result_array() as $recurso)
		{
			if($recurso["recurso_id"]!=$recurso_id)$options[$recurso["recurso_id"]]=$recurso["nombre"];
		}
		$options[-1]=$this->lang->line('module_recursos_operation_select');
		$data['recursos']=$options;
		$data['recurso_info']=$this->Recurso->get_info($recurso_id);
		$this->load->view("cc_views/recursos/form_operations",$data);
	}
	/*Guarda la operación manual y transferencia via ajax*/
	function save_operation($recurso_id){
		$language=$this->lang_lib->get_idioma();
		$this->lang_lib->switch_to($language);
		$operation_data = array(
			"recurso_id"=>$recurso_id,
			"monto"=>floatval($this->input->post("monto")),
			"coin"=>$this->input->post("coin"),
			"concepto"=>$this->input->post("concepto"),
			"saldo_anterior"=>floatval($this->input->post("saldo_anterior")),
			"saldo_restante"=>floatval($this->input->post("saldo_restante")));
		if($this->Recurso->save_operation($operation_data)){
				$this->Recurso->sum_recurso($operation_data["monto"],$recurso_id,$operation_data['coin']);
				echo json_encode(array('success'=>'true','message'=>$this->lang->line('module_recursos_success_msj'),'operation_id'=>$operation_data['operation_id'],'recurso_id'=>$operation_data['recurso_id']));
				$operation_id = $operation_data['operation_id'];
		}
		else{
			/*fracaso*/
			echo json_encode(array('error'=>'false','message'=>$this->lang->line('common_form_error')));
		}

	}
	/*Manual*/
	function help(){
		$language=$this->lang_lib->get_idioma();
		$this->lang_lib->switch_to($language);
		$this->lang->load('manual', $language);
		$data['controller_name']=strtolower(get_class());;
		$data['contenido']=get_table_manual($data['controller_name']);
		$this->load->view("cc_views/".$data['controller_name']."/manual",$data);
	}
}