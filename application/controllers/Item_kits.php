<?php
require_once ("Secure_area.php");
class Item_kits extends Secure_area
{   
	function __construct(){
		parent::__construct('item_kits');
		$this->load->library('kits_lib');
		$this->load->model('Item_kit');
		$this->load->model('Item_kit_items');
	}
	/*Carga el manage view del modulo*/
	function index(){
		$data['notificacion_qty']=$this->Notificacion->get_all()->num_rows();
		$data['notificaciones']=notification_header($this->Notificacion->get_all());
		$data['version']=get_cc_version();
		$data['controller_name']=strtolower(get_class());
		$language=$this->lang_lib->get_idioma();
		$this->lang_lib->switch_to($language);
		$config['total_rows'] = $this->Item_kit->count_all();
		$config['per_page'] = 20;
		$data['config_info']=$this->Appconfig->get_info();
		$config['base_url'] = base_url()."index.php/".$data['controller_name'].'/page/';
		$config['first_link'] = base_url().$data['controller_name'];
		if($config['total_rows']>$config['per_page'])$this->pagination->initialize($config);
		$filtro_uno=$this->config_lib->get_filtro_item_kits();
		if(empty($filtro_uno)){
			$this->config_lib->set_filtro_item_kits("item_kit_id");
		}
		$data['filtro']=$this->config_lib->get_filtro_item_kits();
		$data['manage_table']=get_item_kits_manage_table( $this->Item_kit->get_all($data['filtro'],$config['per_page'], $this->input->get('per_page')),$this);
		$idiomas=array();
		$languages =get_dir_file_info(APPPATH.'language/');
		foreach($languages as $language){
			if($language["name"]!="index.html")$idiomas[$language["name"]]=$language["name"];
		}
		$data["idiomas"]=$idiomas;
		$data['idioma']=$this->lang_lib->get_content_idioma();
		$this->load->view('cc_views/item_kits/manage',$data);
	} 
	/*Busca el Items_kit mediante un filtro específico de busqueda*/
	function search(){
		$search=trim($this->input->post("search"));
		$filtro=$this->config_lib->get_filtro_item_kits();
		$data_rows=get_item_kits_manage_table_data_rows($this->Item_kit->specific_search($search,$filtro),$this,true);
		echo json_encode(array("data_rows"=>$data_rows));
	}
	/*Sugiere las busquedas en el manage table*/
	function suggest(){
		$filtro=$this->config_lib->get_filtro_item_kits();
		$valores=$this->input->post("q");
		$suggestions = $this->Item_kit->get_search_suggestions($valores,'',$filtro);
		/*echo json_encode(array($filtro=>$valores));*/
		echo json_encode($suggestions);
	}
	/*Sugiere las busquedas de las categorias existentes en el form*/
	function suggest_category(){
		$suggestions = array();
		$suggestions = $this->Item_kit->get_category_suggestions($this->input->post("q"));
		echo json_encode($suggestions);
	}
	/*Obtiene una fila para insertar en la tabla*/
	function get_row(){
		$item_kit_id = $this->input->post("row_id");
		$data_row=get_item_kit_data_row($this->Item_kit->get_info($item_kit_id),$this);
		echo $data_row;
	}
	/*Carga el form para editar o guardar un nuevo Kit*/
	function view($item_kit_id=-1){
		$language=$this->lang_lib->get_idioma();
		$this->lang_lib->switch_to($language);
		$this->kits_lib->delete_items_in_kit();
		$data['json_form_errors']=json_encode(array(
			"error_kit_name"=>$this->lang->line('module_item_kit_required'),
			"error_category"=>$this->lang->line('common_category_required'),
			"error_kit_price"=>$this->lang->line('common_price_required')));
		$data['controller_name']=strtolower(get_class());
		$item_kit_info=$this->Item_kit->get_info($item_kit_id);
		$data['item_kit_info']=$item_kit_info;
		$data['title_form']=$item_kit_info->name;
		foreach($this->Item_kit_items->get_info($item_kit_id) as $item_kit_item){
			$this->kits_lib->add_item_to_kit($item_kit_item['item_id'],$item_kit_item['quantity']);
		}
		$data['loading_txt']=$this->lang->line('common_uploadproccess');
		$data['id']=$item_kit_id;
		if($item_kit_id==-1){
			$data['id']="menosuno";
			$data['title_form']=$this->lang->line('common_new').'  '.$this->lang->line('module_item_kit');
		}
		$img_folder='./images/f_images/img-catalago/kits/'.$data['id'].'/';
		$data['images']=get_filenames($img_folder, TRUE);
		$data['item_kit_cfdi_info']=$this->Item_kit->get_info_cfdi($item_kit_id);
		$data['config_info']=$this->Appconfig->get_info();
		$this->load->view("cc_views/item_kits/form",$data);
	}
	/*Guarda un nuevo kit o lo actualiza en la BD*/
	function save($item_kit_id=-1){
		$language=$this->lang_lib->get_idioma();
		$this->lang_lib->switch_to($language);
		$chars = array(",", "(", ")");
		$item_kit_data = array(
		"name"=>trim($this->input->post("kit_name")),
		"description"=>trim($this->input->post("description")),
        "kit_number"=>$this->input->post("kit_number")=="" ? null:$this->input->post("kit_number"),
		'category'=>trim(str_replace($chars, "", $this->input->post('kit_category'))),
		"kit_price"=>$this->input->post("kit_price"),
		"other_price"=>$this->input->post("other_price"),
		"publicar"=>$this->input->post("publish"),
		'show_price'=>$this->input->post('show_price'),
		'destacar'=>$this->input->post('destacar'),
		'promocionar'=>$this->input->post('promocionar'),
		"servicio"=>$this->input->post("servicio"),
		"img_qty"=>$this->input->post('img_qty')
		);
		$cfdi_fields=array(
		'c_ClaveProdServ'=>trim($this->input->post('c_ClaveProdServ')),
		'c_ClaveUnidad'=>trim($this->input->post('c_ClaveUnidad'))
		);
		$barcode_exists=false;
		if($item_kit_data["kit_number"]!=null)$barcode_exists=$this->Item_kit->barcode_exists($item_kit_data["kit_number"],$item_kit_id);
		$srcfile='./images/f_images/img_catalogo/kits/menosuno/main.jpg';
		$cat_folder='./images/f_images/img_catalogo/'.strtoupper(url_title($item_kit_data['category'],'underscore'));
		if(file_exists($srcfile)&&!file_exists($cat_folder)){
			mkdir($cat_folder,0777,true);
			copy($srcfile, $cat_folder."/main.jpg");
			$languages =get_dir_file_info(APPPATH.'language/');
			foreach($languages as $language){
				if($language["name"]!="index.html")$this->lang_lib->content_cat_file_new($language['name'],$item_kit_data['category']);
			}
		}
		$item_kit_items=$this->kits_lib->get_items_in_kit();
		foreach($item_kit_items as $key=>$valor){
			$insert_items_kit_data[$item_kit_items[$key]['item_id']]=$item_kit_items[$key]['quantity'];
		}
		if($item_kit_data["kit_number"]!=null && $barcode_exists==false)$barcode_exists=$this->Item->barcode_exists($item_kit_data["kit_number"],-1);
		if($barcode_exists==false){	
			if($this->Item_kit->save($item_kit_data,$item_kit_id)){
				/*Nuevo kit de artículos*/
				if($item_kit_id==-1){
					$kitfolder='./images/f_images/img_catalogo/kits/'.$item_kit_id.'/';
					if (file_exists($srcfile)&&!file_exists($kitfolder)){
						mkdir($kitfolder, 0777, true);
						copy($srcfile, $kitfolder."/main.jpg");
					}
					else{
						copy($srcfile, $kitfolder."/main.jpg");
					}
					$languages =get_dir_file_info(APPPATH.'language/');
					foreach($languages as $language){
						if($language["name"]!="index.html")$this->lang_lib->kits_file_new($language['name'],$item_kit_id);
					}
					$this->Item_kit_items->save($insert_items_kit_data, $item_kit_id);
					echo json_encode(array("success"=>"true","message"=>$this->lang->line("module_item_kits_success_msj")." ".
					$item_kit_data['name'],"item_kit_id"=>$item_kit_data['item_kit_id']));
				}
				else{
					/*previous item*/
					$this->Item_kit_items->save($insert_items_kit_data, $item_kit_id);
					echo json_encode(array("success"=>"true","message"=>$this->lang->line('module_item_kits_update_msj')." ".
					$item_kit_data['name'],"item_kit_id"=>$item_kit_id));
				}
				/*$item_kit_info = $this->Item_kit->get_info($item_kit_id);*/
				for($i=1;$i<=$item_kit_data["img_qty"];$i++){
					$dstfile='./images/f_images/img-catalago/kits/'.$item_kit_id.'/sec_'.$i.'.jpg';
					if (!file_exists($dstfile)){
						copy($srcfile, $dstfile);
					}
				}
				$this->Item_kit->save_cfdi_fields($cfdi_fields,$item_kit_id);
			}
			else{
				/*failure*/
				echo json_encode(array("success"=>"false","message"=>$this->lang->line('common_form_error')." ".
				$item_kit_data['name'],"item_kit_id"=>-1));
			}
		}
		else{
			echo json_encode(array('success'=>'false','message'=>$this->lang->line('module_item_codebar_error'),'item_kit_id'=>-1));
		}
		$this->Item_kit->save_kits_content($item_kit_id);
		$this->load->library("routes_lib");
		$this->load->library("Home_lib");
		$this->routes_lib->make_routes_catalogo();
		$this->home_lib->make_sitemap();
		$this->home_lib->make_robotstxt();
	}
	/*Borra kits en la BD*/
	function delete(){
		$item_kits_to_delete=$this->input->post('ids');
		if($this->Item_kit->delete_list($item_kits_to_delete)){
			echo json_encode(array("success"=>"true","message"=>$this->lang->line("common_delete_succes").' '.
			count($item_kits_to_delete).' '.$this->lang->line("module_item")."(s)"));
		}
		else{
			echo json_encode(array("success"=>"false","message"=>$this->lang->line("common_form_error")));
		}
		$this->load->library("routes_lib");
		$this->load->library("Home_lib");
		$this->routes_lib->make_routes_front();
		$this->home_lib->make_sitemap();
		$this->home_lib->make_robotstxt();
	}
	/*Regresa el json para cargar el codigo de barras en el view*/
	function generate_barcodes($id_to_barcode){
		$language=$this->lang_lib->get_idioma();
		$this->lang_lib->switch_to($language);
		if($to_barcode=$this->Item_kit->get_info($id_to_barcode)){
			echo json_encode($to_barcode);
		}
		else{
			echo json_encode(array("success"=>"false","message"=>$this->lang->line("common_form_error")));
		}
	}
	/*Carga el view para mostrar los codigos de barras*/
	function barcodes(){
		$language=$this->lang_lib->get_idioma();
		$this->lang_lib->switch_to($language);
		$data['sheet']=get_codebar_sheet($this->config_lib->get_barcode_config());
		$data['config']=$this->config_lib->get_barcode_config();
		$this->load->view("cc_views/barcode_sheet",$data);
	}
	/*Guarda con la ayuda de Config_lib en una cockie de sesión el filtrado de busquedas del modulo */
	function set_filter_search(){
		$filter=$this->input->post("filter");
		$this->config_lib->set_filtro_item_kits($filter);
		echo json_encode( array('success'=>$this->config_lib->get_filtro_item_kits()));
	}
	/*Guarda los items del kit en una cockie*/
	function items_in_kit($item_id){
		$this->kits_lib->add_item_to_kit($item_id);
		$items=$this->kits_lib->get_items_in_kit();
		echo json_encode($items);
	}
	/*Borra los items del kit de la cockie*/
	function delete_item($line){
		$this->kits_lib->delete_item($line);
		echo json_encode((array("success"=>"true")));
	}
	/*Edita los items del kit en la cockie*/
	function edit_item($item_id){
		$data= array();
		$quantity = $this->input->post("quantity");
		$this->kits_lib->edit_item($item_id,$quantity);
	}
	/*Carga el form para editar un bunche de kits*/
	function bulk_edit(){
		$language=$this->lang_lib->get_idioma();
		$this->lang_lib->switch_to($language);
		$data = array();
		$data['json_form_errors']=json_encode(array(
			"error_items_form_bulk"=>$this->lang->line('module_item_form_bulk'),
			"error_cost_price"=>$this->lang->line('common_cost_required'),
			"error_price"=>$this->lang->line('common_price_required'),
			"error_other_price"=>$this->lang->line('common_other_price_required'),
			"error_numeric"=>$this->lang->line('common_numeric_required')));
		$data['config_info']=$this->Appconfig->get_info();
		$suppliers = array('' => $this->lang->line('items_none'));
		$data['item_kit_info']=$this->Item_kit->get_info(-1);
		$filtro=$this->config_lib->get_filtro_suppliers();
		if(empty($filtro)){
			$this->config_lib->set_filtro_suppliers("last_name");
			$filtro=$this->config_lib->get_filtro_suppliers();
		}
		foreach($this->Supplier->get_all($filtro)->result_array() as $row){
			$suppliers[$row['person_id']] = $row['first_name'] .' '. $row['last_name'];
		}
		$data['suppliers'] = $suppliers;
		$data['publish_choices'] = array(
			''=>$this->lang->line('module_item_no_action'), 
			1 =>$this->lang->line('module_item_action_all'),
			0 =>$this->lang->line('module_item_action_quite_all'));
		$data['show_price_choices'] = array(
			''=>$this->lang->line('module_item_no_action'), 
			1 =>$this->lang->line('module_item_action_all'),
			0 =>$this->lang->line('module_item_action_quite_all'));
		$data['destacar_choices'] = array(
			''=>$this->lang->line('module_item_no_action'), 
			1 =>$this->lang->line('module_item_action_all'),
			0 =>$this->lang->line('module_item_action_quite_all'));
		$data['service_choices'] = array(
			''=>$this->lang->line('module_item_no_action'), 
			1 =>$this->lang->line('module_item_action_all'),
			0 =>$this->lang->line('module_item_action_quite_all'));	
		$data['promocionar_choices'] = array(
			''=>$this->lang->line('module_item_no_action'), 
			1 =>$this->lang->line('module_item_action_all'),
			0 =>$this->lang->line('module_item_action_quite_all'));	
		$this->load->view("cc_views/item_kits/form_bulk", $data);
	}
	/*Edita algunas cosas en un bunche de kits*/
	function bulk_update(){
		$kits_to_update=$this->input->post("kit_ids")!=false ? $this->input->post("kit_ids"):array();
		$kit_data = array();
		$cfdi_fields=array();
		$language=$this->lang_lib->get_idioma();
		$this->lang_lib->switch_to($language);
		/*Los datos del artículo estarían vacíos si se actualiza la información fiscal*/
		foreach($_POST as $key=>$value){
			if($key != 'c_ClaveUnidad' and $key != 'c_ClaveProdServ' and $value!='' and !(in_array($key, array('kit_ids')))){
				$kit_data["$key"]=$value;
			}
			elseif($key == 'c_ClaveUnidad' or $key == 'c_ClaveProdServ'){
				if($value!='')$cfdi_fields["$key"]=$value;
			}
			if($key=='img_qty' and $value!=''){
				$img_qty=$value;
			}
		}
		if(isset($img_qty)){
			$srcfile='./images/f_images/img-catalago/kits/menosuno/main.jpg';
			foreach($kits_to_update as $item_kit_id){
				for($i=1;$i<=$img_qty;$i++){
					$dstfile='./images/f_images/img-catalago/kits/'.$item_kit_id.'/sec_'.$i.'.jpg';
					if (!file_exists($dstfile)){
						copy($srcfile, $dstfile);
					}
				}
			}
		}
		if(count($cfdi_fields)>0){
			$this->Item_kit->update_multiple_cfdi_data($cfdi_fields,$kits_to_update);
		}
		if(empty($kit_data) ||$this->Item_kit->update_multiple($kit_data,$kits_to_update)){
			echo json_encode(array("success"=>"true","message"=>$this->lang->line("module_items_editon_success")));
		}
		else{
			echo json_encode(array("success"=>"false","message"=>$this->lang->line("module_items_editon_fail")));
		}
		$this->load->library("routes_lib");
		$this->load->library("Home_lib");
		$this->routes_lib->make_routes_front();
		$this->home_lib->make_sitemap();
		$this->home_lib->make_robotstxt();
	}
	/*Sube la imagen del item*/
	function upload_image($item_kit_id,$ext_name){
		$ext_name_array = explode("---", $ext_name);
		$no_ext_name=$ext_name_array[0];
		$ext_name = str_replace("---", ".", $ext_name);
		$image_folder='./images/f_images/img-catalago/kits/'.$item_kit_id.'/';
		if (!file_exists($image_folder)){
				mkdir($image_folder,0777,true);
				chmod($image_folder,0777);
		}
		$img_info=getimagesize($image_folder.$ext_name);
		/*comprobamos que sea una petición ajax*/
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
			/*obtenemos el archivo a subir*/
			$files = $_FILES[$no_ext_name]['name'];
			$i = 0;
			/*comprobamos si el archivo ha subido*/
			foreach($files as $file){
				if (move_uploaded_file($_FILES[$no_ext_name]['tmp_name'][$i],$image_folder.$ext_name)){
					echo "EL archivo ".$no_ext_name." ha subido correctamente<br>";
					$config['image_library'] = 'gd2';
					$config['source_image'] = $image_folder.$ext_name;
					$config['create_thumb'] = FALSE;
					$config['maintain_ratio'] = TRUE;
					$config['width']         = $img_info[0];
					$config['height']       = $img_info[1];
					$this->load->library('image_lib', $config);
					if ( ! $this->image_lib->resize()){
					echo $this->image_lib->display_errors($this->lang->line("common_img_resizefail"));
					}
					$i++;
				}
			}
		}
		else{
			echo $this->lang->line("common_uploadfail");  
		}
	}
	/*Sube la imagen de la categoria*/
	function upload_cat_image($item_kit_id,$ext_name){
		$ext_name_array = explode("---", $ext_name);
		$no_ext_name=$ext_name_array[0];
		$ext_name = str_replace("---", ".", $ext_name);
		$item_kit_info= $this->Item_kit->get_info($item_kit_id);
		$image_folder='./images/f_images/img-catalago/'.strtoupper(url_title($item_kit_info->category,'_')).'/';
		if (!file_exists($image_folder)){
			mkdir($image_folder,0777,true);
			chmod($image_folder,0777);
		}
		$img_info=getimagesize($image_folder.$ext_name);
		/*comprobamos que sea una petición ajax*/
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			/*obtenemos el archivo a subir*/
			$files = $_FILES[$no_ext_name]['name'];
			$i = 0;
			/*comprobamos si el archivo ha subido*/
			foreach($files as $file){
				if (move_uploaded_file($_FILES[$no_ext_name]['tmp_name'][$i],$image_folder.$ext_name)){
					echo "EL archivo ".$no_ext_name." ha subido correctamente<br>";
					$config['image_library'] = 'gd2';
					$config['source_image'] = $image_folder.$ext_name;
					$config['create_thumb'] = FALSE;
					$config['maintain_ratio'] = TRUE;
					$config['width']         = $img_info[0];
					$config['height']       = $img_info[1];
					$this->load->library('image_lib', $config);
					if ( ! $this->image_lib->resize()){
						echo $this->image_lib->display_errors($this->lang->line("common_img_resizefail"));
					}
					$i++;
				}
			}
		}
		else{
			echo $this->lang->line("common_uploadfail");  
		}
	}
	/*Carga lo mismo que el index pero con cierto offset para paginar la tabla del modulo*/
	function page($offset=0){
		if($offset < 0)$offset=0;
		$data['notificacion_qty']=$this->Notificacion->get_all()->num_rows();
		$data['notificaciones']=notification_header($this->Notificacion->get_all());
		$language=$this->lang_lib->get_idioma();
		$this->lang_lib->switch_to($language);
		$data['controller_name']=strtolower(get_class());
		$config['first_link'] = $this->lang->line("common_first_link");
		$config['last_link'] = $this->lang->line("common_last_link");;
		$config['total_rows'] = $this->Item_kit->count_all();
		$config['per_page'] = '20';
		$config['base_url'] = base_url().'/index.php/'.$data['controller_name'].'/page/';
		$this->pagination->initialize($config);
		$filtro_uno=$this->config_lib->get_filtro_item_kits();
		$barcode_config=$this->config_lib->get_barcode_config();
		if(empty($barcode_config)){
			$this->config_lib->set_barcode_config("24");
		}
		if(empty($filtro_uno)){
			$this->config_lib->set_filtro_item_kits("name");
		}
		$data['filtro']=$this->config_lib->get_filtro_item_kits();
		$manage_table=get_item_kits_manage_table($this->Item_kit->get_all($data['filtro'],$config['per_page'], $offset),$this);
		$data['filtro']=$this->config_lib->get_filtro_item_kits();
		$data['config_info']=$this->Appconfig->get_info();
		$data['manage_table']=$manage_table;
		$idiomas=array();
		$languages =get_dir_file_info(APPPATH.'language/');
		foreach($languages as $language){
			if($language["name"]!="index.html")$idiomas[$language["name"]]=$language["name"];
		}
		$data["idiomas"]=$idiomas;
		$data['idioma']=$this->lang_lib->get_content_idioma();
		$this->load->view('cc_views/item_kits/manage',$data);
	}
	/*Carga el formulario del contenido de los kits*/
	function form_content($item_kit_id){
		$idioma=$this->lang_lib->get_content_idioma();
		$data['idioma']=$idioma;
		$data['loading_txt']=$this->lang->line('common_uploadproccess');
		$data['lang']=$this->lang_lib->get_kits_content_file($idioma,$item_kit_id);
		$data['controller_name']=strtolower(get_class());
		$data['item_kit_info']=$this->Item_kit->get_info($item_kit_id);
		$this->load->view("cc_views/item_kits/form_content",$data);
	}
	/*Carga el formulario del contenido de la categorias*/
	function form_cat_content($item_kit_id){
		$idioma=$this->lang_lib->get_content_idioma();
		$data['idioma']=$idioma;
		$data['loading_txt']=$this->lang->line('common_uploadproccess');
		$data['item_kit_info']=$this->Item_kit->get_info($item_kit_id);
		$data['lang']=$this->lang_lib->get_cats_content_file($idioma,url_title($data['item_kit_info']->category));
		$data['controller_name']=strtolower(get_class());
		$img_folder='./images/f_images/img-catalago/'.strtoupper(url_title($data['item_kit_info']->category,'_')).'/';
		$data['images']=get_filenames($img_folder, TRUE);
		$this->load->view("cc_views/item_kits/form_cat_content",$data);
	}
	/*Guarda los cambios del form_content de los kits*/
	function save_edit_content($item_kit_id){
		$fields=$this->input->post(NULL, FALSE);
		$idioma=$this->lang_lib->get_content_idioma();
		$result=$this->lang_lib->save_item_kits_edit_content($idioma,$item_kit_id,$fields);
		$item_kit_info=$this->Item_kit->get_info($item_kit_id);
		echo json_encode(array('success'=>'true','message'=>$this->lang->line('module_items_success_msj'),'id'=>$item_kit_id));
	}
	/* Guarda los cambios del form_content de las categorias*/
	function save_edit_cat_content($item_kit_id){
		$fields=$this->input->post(NULL, FALSE);
		$idioma=$this->lang_lib->get_content_idioma();
		$result=$this->lang_lib->save_cats_edit_content($idioma,$item_kit_id,$fields);
		$item_kit_info=$this->Item_kit->get_info($item_kit_id);
		echo json_encode(array('success'=>'true','message'=>$this->lang->line('module_items_success_msj'),'id'=>$item_kit_id));
	}
	/*Guarda el filtro para mostrar idioma de los form de contenido*/
	function filtro_idioma(){
		$this->lang_lib->set_content_idioma($this->input->post("idioma"));
		$idioma=$this->lang_lib->get_content_idioma();
		echo json_encode(array('idioma'=>$idioma,'post'=>$this->input->post("idioma")));
	}
	/*Manual*/
	function help(){
		$language=$this->lang_lib->get_idioma();
		$this->lang_lib->switch_to($language);
		$this->lang->load('manual', $language);
		$data['controller_name']=strtolower(get_class());;
		$data['contenido']=get_table_manual($data['controller_name']);
		$this->load->view("cc_views/".$data['controller_name']."/manual",$data);
	}
}
?>