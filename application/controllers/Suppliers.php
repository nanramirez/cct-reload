<?php
require_once ("Secure_area.php");
class Suppliers extends Secure_area
{
	function __construct(){
		parent::__construct('suppliers');
		$this->load->model('Fiscal');
	}
	/*Carga el manage view del modulo*/
	function index(){
		$data['notificacion_qty']=$this->Notificacion->get_all()->num_rows();
		$data['notificaciones']=notification_header($this->Notificacion->get_all());
		$data['version']=get_cc_version();
		$data['controller_name']=strtolower(get_class());
		$config['total_rows'] = $this->Supplier->count_all();
		$config['per_page'] = 20;
		$config['base_url'] = base_url()."index.php/".$data['controller_name'].'/page/';
		$config['first_link'] = base_url().$data['controller_name'];
		if($config['total_rows']>$config['per_page'])$this->pagination->initialize($config);
		$data['config_info']=$this->Appconfig->get_info();
		$filtro_uno=$this->config_lib->get_filtro_suppliers();
		if(empty($filtro_uno)){
			$this->config_lib->set_filtro_suppliers("company");
		}
		$data['filtro']=$this->config_lib->get_filtro_suppliers();
		$data['manage_table']=get_suppliers_manage_table($this->Supplier->get_all($data['filtro'],$config['per_page'], $this->input->get('per_page')),$this);
		$this->load->view('cc_views/suppliers/manage',$data);
	}
	/*Busca el Supplier mediante un filtro específico de busqueda*/
	function search(){
		$search=trim($this->input->post("search"));
		$filtro=$this->config_lib->get_filtro_suppliers();
		$data_rows=get_suppliers_manage_table_data_rows($this->Supplier->specific_search($search,$filtro),$this,true);
		echo json_encode(array("data_rows"=>$data_rows));
	}
	/*Sugerencias de busquedas*/
	function suggest($s=null){
		if(isset($s)){
			$suggestions = $this->Supplier->get_search_suggestions_receivings($this->input->post("q"));
		}
		else{
			$filtro=$this->config_lib->get_filtro_suppliers();
			$valores=$this->input->post("q");
			$suggestions = $this->Supplier->get_search_suggestions($valores,'',$filtro);
		}
		echo json_encode($suggestions);
	}
	/*Carga el form para editar o guardar un nuevo proveedor*/
	function view($supplier_id=-1){
		$data['json_form_errors']=json_encode(array(
			"error_first_name"=>$this->lang->line('common_first_name_required'),
			"error_last_name"=>$this->lang->line('common_last_name_required'),
			"error_email_exists"=>$this->lang->line('common_email_exists'),
			"error_email"=>$this->lang->line('common_email_required')));
		$data['json_fiscal_errors']=json_encode(array(
			"error_rfc"=>$this->lang->line('common_rfc_required'),
			"error_field"=>$this->lang->line('common_field_required'),
			"error_zip"=>$this->lang->line('common_zip_required')
		));
		$data['controller_name']=strtolower(get_class());
		$person_info=$this->Supplier->get_info($supplier_id);
		$data['person_info']=$person_info;
		$data['title_form']=$person_info->first_name." ".$person_info->last_name;
		$data['fiscal_info']=$this->Fiscal->get_info($supplier_id);
		$data['loading_txt']=$this->lang->line('common_uploadproccess');
		$data['id']=$supplier_id;
		if($supplier_id==-1){
			$data['id']="menosuno";
			$data['title_form']=$this->lang->line('common_new').'  '.$this->lang->line('module_supplier');
		}
		$img_folder='./images/cc_images/people/'.$data['id'].'/';
		$data['images']=get_filenames($img_folder, TRUE);
		$data['taxable']=null;
		if($data['fiscal_info']->rfc!="")$data['taxable']=1;
		$data['controller_name']=strtolower(get_class());
		$this->load->view("cc_views/suppliers/form",$data);
	}
	/*Carga el form con la info de algun proveedor*/
	function view_info($supplier_id){
		$img_folder='./images/cc_images/people/'.$supplier_id."/";
		$data['images']=get_filenames($img_folder, TRUE);
		$data['controller_name']=strtolower(get_class());
		$data['person_info']=$this->Supplier->get_info($supplier_id);
		$this->load->view("cc_views/suppliers/details",$data);
	}
	/*Carga el form con la info fiscal de algun proveedor*/
	function view_info_rfc($supplier_id){
		$data['controller_name']=strtolower(get_class());
		$data['person_info']=$this->Fiscal->get_info($supplier_id);
		$this->load->view("cc_views/people/rfc_info",$data);
	}
	/*Actualiza o guarda un nuevo proveedor en la BD*/
	function save($supplier_id=-1){
		$person_data = array(
			'first_name'=>$this->input->post('first_name'),
			'last_name'=>$this->input->post('last_name'),
			'email'=>$this->input->post('email'),
			'phone_number'=>$this->input->post('phone_number'),
			'address_1'=>$this->input->post('address_1'),
			'address_2'=>$this->input->post('address_2'),
			'city'=>$this->input->post('city'),
			'state'=>$this->input->post('state'),
			'zip'=>$this->input->post('zip'),
			'country'=>$this->input->post('country'),
			'comments'=>$this->input->post('comments'));
		$fiscal_data = array(
			"rfc"=>$this->input->post("rfc"),
			"razon"=>$this->input->post("razon_social"),
			"ce"=>$this->input->post("ce"),
			"noExterior"=>$this->input->post("no_exterior"),
			"noInterior"=>$this->input->post("no_interior"),
			"colonia"=>$this->input->post("colonia"),
			"localidad"=>$this->input->post("localidad"),
			"municipio"=>$this->input->post("municipio"),
			"estado"=>$this->input->post("estado"),
			"pais"=>$this->input->post("pais"),
			"codigoPostal"=>$this->input->post("cp"));
		$supplier_data=array(
			'company'=>$this->input->post('company'),
			'accounts_number'=>$this->input->post('accounts_number')=='' ? null:$this->input->post('accounts_number'));
		$taxable=0;
		if($this->input->post("rfc")!="")$taxable=1;
		if($this->Supplier->save_supplier($person_data,$supplier_data,$supplier_id)){
				if($taxable==1){
					$fiscal_data["person_id"]=$supplier_data['person_id'];
					$this->Fiscal->save_datos_fiscales($fiscal_data,$supplier_data['person_id']);
				}
			/* Nuevo proveedor */
				if($supplier_id==-1){
				$image_folder='./images/cc_images/people/'.$supplier_data['person_id']."/";
				$srcfile='./images/cc_images/people/menosuno/person.jpg';
				if (!file_exists($image_folder)&&file_exists($srcfile)){
				mkdir($image_folder,0777,true);
				chmod($image_folder,0777);
				copy($srcfile, $image_folder."/person.jpg");
				}
				/**/
				echo json_encode(array('success'=>'true','message'=>$this->lang->line('module_supplier_success_msj').' '.
				$supplier_data['company'],'person_id'=>$supplier_data['person_id']));
			}
			 /* proveedor anterior */
			else{
				echo json_encode(array('success'=>'true','message'=>$this->lang->line('module_supplier_update_msj').' '.
				$supplier_data['company'],'person_id'=>$supplier_id));
			}
		}
		/* Error */
		else{
			echo json_encode(array('success'=>'false','message'=>$this->lang->line('common_form_error').' '.
			$supplier_data['company'],'person_id'=>-1));
		}
	}
	/*Borra proveedores en la BD^*/
	function delete(){
		$suppliers_to_delete=$this->input->post("ids")!=false ? $this->input->post("ids"):array();
		if($this->Supplier->delete_list($suppliers_to_delete)){
			echo json_encode(array("success"=>"true","message"=>$this->lang->line("common_delete_succes")." ".
			count($suppliers_to_delete)." ".$this->lang->line("module_supplier")."(s)"));
		}
		else{
			echo json_encode(array('success'=>"false",'message'=>$this->lang->line('common_form_error')));
		}
	}
	/*Obtiene una fila actualizada del manage table*/
	function get_row(){
		$person_id = $this->input->post("row_id");
		$data_row=get_supplier_data_row($this->Supplier->get_info($person_id),$this);
		echo $data_row;
	}
	/*Guarda con la ayuda de Config_lib en una cockie de sesión el filtrado de busquedas del modulo */
	function set_filter_search(){
		$filter=$this->input->post("filter");
		$this->config_lib->set_filtro_suppliers($filter);
		echo json_encode( array('success'=>$this->config_lib->get_filtro_suppliers()));
	}
	/*Sube la imagen del proveedor*/
	function upload_image($id,$ext_name){	
		$ext_name_array = explode("---", $ext_name);
		$no_ext_name=$ext_name_array[0];
		$ext_name = str_replace("---", ".", $ext_name);
		$image_folder='./images/cc_images/people/'.$id.'/';
		if (!file_exists($image_folder)){
					mkdir($image_folder,0777,true);
					chmod($image_folder,0777);
		}
		$img_info=getimagesize($image_folder.$ext_name);
	/* comprobamos que sea una petición ajax */
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
		/* obtenemos el archivo a subir */
		$files = $_FILES[$no_ext_name]['name'];
		$i = 0;
		/* comprobamos si el archivo ha subido */
		foreach($files as $file){
			if(move_uploaded_file($_FILES[$no_ext_name]['tmp_name'][$i],$image_folder.$ext_name)){
				echo "EL archivo ".$no_ext_name." ha subido correctamente<br>";
				$config['image_library'] = 'gd2';
				$config['source_image'] = $image_folder.$ext_name;
				$config['create_thumb'] = FALSE;
				$config['maintain_ratio'] = TRUE;
				$config['width']         = $img_info[0];
				$config['height']       = $img_info[1];
				$this->load->library('image_lib', $config);
				if ( ! $this->image_lib->resize()){
				echo $this->image_lib->display_errors($this->lang->line("common_img_resizefail"));
				}
				$i++;
			}
		}
		}else{
			echo $this->lang->line("common_uploadfail");  
		}
	}
	/*Carga lo mismo que el index pero con cierto offset para paginar la tabla del modulo*/
	function page($offset=0){
		if($offset < 0)$offset=0;
		$data['notificacion_qty']=$this->Notificacion->get_all()->num_rows();
		$data['notificaciones']=notification_header($this->Notificacion->get_all());
		$language=$this->lang_lib->get_idioma();
		$this->lang_lib->switch_to($language);
		$data['controller_name']=strtolower(get_class());
		$config['first_link'] = $this->lang->line("common_first_link");
		$config['last_link'] = $this->lang->line("common_last_link");;
		$config['total_rows'] = $this->Supplier->count_all();
		$config['per_page'] = '20';
		$config['base_url'] = base_url().'/index.php/'.$data['controller_name'].'/page/';
		$this->pagination->initialize($config);
		$filtro_uno=$this->config_lib->get_filtro_suppliers();
		if(empty($filtro_uno))
		{
			$this->config_lib->set_filtro_suppliers("name");
		}
		$data['filtro']=$this->config_lib->get_filtro_suppliers();
		$manage_table=get_suppliers_manage_table($this->Supplier->get_all($data['filtro'],$config['per_page'],$offset),$this);
		$data['filtro']=$this->config_lib->get_filtro_suppliers();
		$data['config_info']=$this->Appconfig->get_info();
		$data['manage_table']=$manage_table;
		$this->load->view('cc_views/suppliers/manage',$data);
	}
	/*Valida si el correo existe en la bd*/
	function validar_mail(){
		echo json_encode(array("response"=>$this->Person->exists_mail($this->input->post("email"))));
	}
	/*Manual*/
	function help(){
		$language=$this->lang_lib->get_idioma();
		$this->lang_lib->switch_to($language);
		$this->lang->load('manual', $language);
		$data['controller_name']=strtolower(get_class());;
		$data['contenido']=get_table_manual($data['controller_name']);
		$this->load->view("cc_views/".$data['controller_name']."/manual",$data);
	}
}
?>