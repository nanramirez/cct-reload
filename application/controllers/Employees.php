<?php
require_once ("Secure_area.php");
class Employees extends Secure_area
{
	function __construct(){
		parent::__construct('employees');
	}
	/*Carga el manage view del modulo*/
	function index(){
		
		$data['notificacion_qty']=$this->Notificacion->get_all()->num_rows();
		$data['notificaciones']=notification_header($this->Notificacion->get_all());
		$data['version']=get_cc_version();
		$data['controller_name']=strtolower(get_class());
		$config['total_rows'] = $this->Employee->count_all();
		$config['per_page'] = 20;
		$config['base_url'] = base_url()."index.php/".$data['controller_name'].'/page/';
		$config['first_link'] = base_url().$data['controller_name'];
		if($config['total_rows']>$config['per_page'])$this->pagination->initialize($config);
		$data['config_info']=$this->Appconfig->get_info();
		$filtro_uno=$this->config_lib->get_filtro_employees();
		if(empty($filtro_uno)){
			$this->config_lib->set_filtro_employees("last_name");
		}
		$data['filtro']=$this->config_lib->get_filtro_employees();
		$data['manage_table']=get_employees_manage_table($this->Employee->get_all($data['filtro'],$config['per_page'], $this->input->get('per_page')),$this);
		$this->load->view("cc_views/".$data['controller_name']."/manage",$data);
	}
	/*Busca al empleado mediante un filtro específico de busqueda*/
	function search(){
		$search=trim($this->input->post("search"));
		$filtro=$this->config_lib->get_filtro_employees();
		$data_rows=get_employees_manage_table_data_rows($this->Employee->specific_search($search,$filtro),$this,true);
		$this->Employee->specific_search($search,$filtro);
		echo json_encode(array("data_rows"=>$data_rows));
	}
	/*Sugiere las busquedas en el manage table*/
	function suggest(){
		$filtro=$this->config_lib->get_filtro_employees();
		$valores=$this->input->post("q");
		$suggestions = $this->Employee->get_search_suggestions($valores,'',$filtro);
		/*echo json_encode(array($filtro=>$valores));*/
		echo json_encode($suggestions);
		
	}
	/*Carga el form para editar o guardar un nuevo Empleado*/
	function view($employee_id=-1){
		$data['json_form_errors']=json_encode(array(
		"error_first_name"=>$this->lang->line('common_first_name_required'),
		"error_last_name"=>$this->lang->line('common_last_name_required'),
		"error_email"=>$this->lang->line('common_email_required'),
		"error_email_exists"=>$this->lang->line('common_email_exists'),
		"error_user"=>$this->lang->line('common_user_required'),
		"error_pass"=>$this->lang->line('common_pass_required'),
		"error_pass_rpt"=>$this->lang->line('common_pass_rpt_required')
		));
		$data['person_info']=$this->Employee->get_info($employee_id);
		$data['title_form']=$data['person_info']->first_name." ".$data['person_info']->last_name;
		$data['loading_txt']=$this->lang->line('common_uploadproccess');
		$data['id']=$employee_id;
		if($employee_id==-1){
			$data['id']="menosuno";
			$data['title_form']=$this->lang->line('common_new').'  '.$this->lang->line('module_employee');
		}
		$img_folder='./images/cc_images/people/'.$data['id'].'/';
		$data['images']=get_filenames($img_folder, TRUE);
		$data['controller_name']=strtolower(get_class());
		
		$data['all_modules']=$this->Module->get_all_modules();
		$this->load->view("cc_views/employees/form",$data);
	}
	/*Carga el form con la info de algun Empleado*/
	function view_info($employee_id){
		$img_folder='./images/cc_images/people/'.$employee_id."/";
		$data['images']=get_filenames($img_folder, TRUE);
		$data['controller_name']=strtolower(get_class());
		$data['person_info']=$this->Employee->get_info($employee_id);
		$data['all_modules']=$this->Module->get_all_modules();
		$this->load->view("cc_views/employees/details",$data);
		
	}
	/*Guarda un nuevo Empleado o lo actualiza en la BD */
	function save($employee_id=-1){
		$person_data = array(
			"first_name"=>$this->input->post("first_name"),
			"last_name"=>$this->input->post("last_name"),
			"email"=>$this->input->post("email"),
			"phone_number"=>$this->input->post("phone_number"),
			"address_1"=>$this->input->post("address_1"),
			"address_2"=>$this->input->post("address_2"),
			"city"=>$this->input->post("city"),
			"state"=>$this->input->post("state"),
			"zip"=>$this->input->post("zip"),
			"country"=>$this->input->post("country"),
			"comments"=>$this->input->post("comments"));
		$permission_data = $this->input->post("permissions")!=null ? $this->input->post("permissions"):array();
		/*La contraseña ha sido cambiada o la primera contraseña establecida*/
		if($this->input->post("password")!=null){
			$employee_data=array(
			"username"=>$this->input->post("username"),
			"password"=>md5($this->input->post("password"))
			);
		}
		else {
			/* Contraseña no modificada */
			$employee_data=array("username"=>$this->input->post("username"));
		}
		if ($employee_id == 1){
			/* Error*/
			echo json_encode(array('success'=>'false','message'=>$this->lang->line('module_employees_main_user_warning').' '.
			$person_data['first_name'].' '.$person_data['last_name'],'person_id'=>-1));
		}
		elseif($this->Employee->save_employee($person_data,$employee_data,$permission_data,$employee_id)){
			/* Nuevo empleado */
			if($employee_id==-1){
				$srcfile='./images/cc_images/people/menosuno/person.jpg';
				$dstfile='./images/cc_images/people/'.$person_data['person_id'].'/person.jpg';
				if (file_exists($srcfile))
				{
				mkdir(dirname($dstfile), 0777, true);
				copy($srcfile, $dstfile);
				}
				$employee_id=$person_data['person_id'];
				echo json_encode(array('success'=>'true','message'=>$this->lang->line('module_employee_success_msj').' '.
				$person_data['first_name'].' '.$person_data['last_name'],'person_id'=>$employee_id));
			}
			else{
				/* empleado anterior */
				echo json_encode(array('success'=>'true','message'=>$this->lang->line('module_employee_update_msj').' '.
				$person_data['first_name'].' '.$person_data['last_name'],'person_id'=>$employee_id));
			}
		}
		else{
			/* Error */
			echo json_encode(array('success'=>'false','message'=>$this->lang->line('common_form_error').' '.
			$person_data['first_name'].' '.$person_data['last_name'],'person_id'=>-1));
		}
	
	}
	/*Borra Empleados en la BD*/
	function delete(){
		$employees_to_delete=$this->input->post('ids');
		if ($_SERVER['HTTP_HOST'] == base_url() && in_array(1,$employees_to_delete)){
			/* Error */
			echo json_encode(array('success'=>'false','message'=>$this->lang->line('module_employees_main_user_warning')));
		}
		elseif($this->Employee->delete_list($employees_to_delete)){
			echo json_encode(array('success'=>'true','message'=>$this->lang->line('common_delete_succes').' '.
			count($employees_to_delete).' '.$this->lang->line('module_employee')));
		}
		else{
			echo json_encode(array('success'=>'false','message'=>$this->lang->line('common_form_error')));
		}
	}
	/*Obtiene una fila actualizada del manage table*/
	function get_row(){
		$person_id = $this->input->post("row_id");
		$data_row=get_employee_data_row($this->Employee->get_info($person_id),$this);
		echo $data_row;
	}
	/* Guarda con la ayuda de Config_lib en una cockie de sesión el filtrado de busquedas del modulo */
	function set_filter_search(){			
		$filter=$this->input->post("filter");
		$this->config_lib->set_filtro_employees($filter);
		echo json_encode( array('success'=>$this->config_lib->get_filtro_employees()));
	}
	/*Sube la imagen del form*/
	function upload_image($id,$ext_name){	
		$ext_name_array = explode("---", $ext_name);
		$no_ext_name=$ext_name_array[0];
		$ext_name = str_replace("---", ".", $ext_name);
		$image_folder='./images/cc_images/people/'.$id.'/';
		if (!file_exists($image_folder)){
					mkdir($image_folder,0777,true);
					chmod($image_folder,0777);
		}
		$img_info=getimagesize($image_folder.$ext_name);
		/*comprobamos que sea una petición ajax*/
			if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
				/*obtenemos el archivo a subir*/
				$files = $_FILES[$no_ext_name]['name'];
				$i = 0;
				/*comprobamos si el archivo ha subido */
				foreach($files as $file){
					if (move_uploaded_file($_FILES[$no_ext_name]['tmp_name'][$i],$image_folder.$ext_name)){
						echo "EL archivo ".$no_ext_name." ha subido correctamente<br>";
						$config['image_library'] = 'gd2';
						$config['source_image'] = $image_folder.$ext_name;
						$config['create_thumb'] = FALSE;
						$config['maintain_ratio'] = TRUE;
						$config['width']         = $img_info[0];
						$config['height']       = $img_info[1];
						$this->load->library('image_lib', $config);
						if ( ! $this->image_lib->resize())
						{
							echo $this->image_lib->display_errors($this->lang->line("common_img_resizefail"));
						}
						$i++;
						
					}
				
				}
			}else{
				echo $this->lang->line("common_uploadfail");
			}
	}
	/*Carga lo mismo que el index pero con cierto offset para paginar la tabla del modulo*/
	function page($offset=0){
		if($offset < 0)$offset=0;
		$data['notificacion_qty']=$this->Notificacion->get_all()->num_rows();
		$data['notificaciones']=notification_header($this->Notificacion->get_all());
		$language=$this->lang_lib->get_idioma();
		$this->lang_lib->switch_to($language);
		$data['controller_name']=strtolower(get_class());
		$config['first_link'] = $this->lang->line("common_first_link");
		$config['last_link'] = $this->lang->line("common_last_link");;
		$config['total_rows'] = $this->Employee->count_all();
		$config['per_page'] = '20';
		$config['base_url'] = base_url().'/index.php/'.$data['controller_name'].'/page/';
		$this->pagination->initialize($config);
		$filtro_uno=$this->config_lib->get_filtro_employees();
		if(empty($filtro_uno))
		{
			$this->config_lib->set_filtro_employees("name");
		}
		$data['filtro']=$this->config_lib->get_filtro_employees();
		$manage_table=get_employees_manage_table($this->Employee->get_all($data['filtro'],$config['per_page'],$offset),$this);
		$data['filtro']=$this->config_lib->get_filtro_employees();
		$data['config_info']=$this->Appconfig->get_info();
		$data['manage_table']=$manage_table;
		$this->load->view("cc_views/".$data['controller_name']."/manage",$data);
	}
	/*Valida si el correo existe en la bd*/
	function validar_mail(){
		echo json_encode(array("response"=>$this->Person->exists_mail($this->input->post("email"))));
	}
	/*Manual*/
	function help(){
		$language=$this->lang_lib->get_idioma();
		$this->lang_lib->switch_to($language);
		$this->lang->load('manual', $language);
		$data['controller_name']=strtolower(get_class());;
		$data['contenido']=get_table_manual($data['controller_name']);
		$this->load->view("cc_views/".$data['controller_name']."/manual",$data);
	}
}
?>