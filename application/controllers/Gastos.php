<?php
require_once ("Secure_area.php");
class Gastos extends Secure_area
{   
	function __construct(){
		parent::__construct('gastos');
		$this->load->model('Gasto');
		$this->load->model('Recurso');
		$this->load->library('gastoscobros_lib');
	}
	/*Carga el manage view del modulo*/
	function index(){
		$data['notificacion_qty']=$this->Notificacion->get_all()->num_rows();
		$data['notificaciones']=notification_header($this->Notificacion->get_all());
		$data['version']=get_cc_version();
		$data['controller_name']=strtolower(get_class());
		$config_info=$this->Appconfig->get_info();
		$data['config_info']=$config_info;
		$language=$this->lang_lib->get_idioma();
		$this->lang_lib->switch_to($language);
		$data['selected_month']=$this->config_lib->get_mes_show();
		$filtro_uno=$this->config_lib->get_filtro_gastos();
		if(empty($filtro_uno)){
			$this->config_lib->set_filtro_gastos("concepto");
		}
		$data['filtro']=$this->config_lib->get_filtro_gastos();
		$config['total_rows'] = $this->Gasto->count_all($data['selected_month']);
		$config['per_page'] = 10000;
		if($data['selected_month']=='0')$config['per_page'] = 20;
		$config['base_url'] = base_url()."index.php/".$data['controller_name'].'/page/';
		$config['first_link'] = base_url().$data['controller_name'];
		if($config['total_rows']>$config['per_page'])$this->pagination->initialize($config);
		/*///////////////////*/
		$total_undone_mes_coin = $this->Gasto->undone_mes(date('n'),$config_info["coin"]);
		$total_undone_mes_coin2 = $this->Gasto->undone_mes(date('n'),$config_info["coin2"]);
		$data['total_undone_mes_coin'] =$total_undone_mes_coin;
		$data['total_undone_mes_coin2'] =$total_undone_mes_coin2;
		$data['total_done_mes_coin'] =$this->Gasto->done_mes(date('n'),$config_info["coin"]);
		$data['total_done_mes_coin2'] =$this->Gasto->done_mes(date('n'),$config_info["coin2"]);
		$data['total_done_coin'] =$this->Gasto->done_total($config_info["coin"]);
		$data['total_done_coin2'] =$this->Gasto->done_total($config_info["coin2"]);
		$data['total_undone_coin'] =$this->Gasto->undone_total($config_info["coin"]);
		$data['total_undone_coin2'] =$this->Gasto->undone_total($config_info["coin2"]);
		if(empty($total_undone_mes_coin)){
			$data['total_undone_mes_coin'] =0;
		}
		if(empty($total_undone_mes_coin2)){
			$data['total_undone_mes_coin2'] =0;
		}
		$data['estado']=array(
			'Pendiente'  => $this->lang->line("common_pending"),
			'Hecho'    =>$this->lang->line("common_done"));
		if(empty($total_done)){
			$data['total_done'] =0;
		}
		$data['meses']=get_full_months();
		$data['estado_selected']=$this->config_lib->get_estado_gastos();/*Aqui va el filtro*/
		$data['manage_table']=get_gastos_manage_table($this->Gasto->get_all($config['per_page'], $this->input->get('per_page'),$data['selected_month'],$data['estado_selected']),$data['estado_selected']);
		/* if(empty($cuentas)){
			$cuentas['-1'] ='No existen';
		}
		$data['cuentas']=$cuentas;*/
		if(date('n')=="12")$this->renovar_gastos_recurrentes();
		$this->load->view('cc_views/gastos/manage',$data);
	}
	/*Guarda con la ayuda de Config_lib en una cockie de sesión el filtrado de busquedas del modulo*/
	function filtro(){
		if($this->input->post("meses")!= null)$this->config_lib->set_mes_show($this->input->post("meses"));
		if($this->input->post("estado")!= null)$this->config_lib->set_estado_gastos($this->input->post("estado"));
		$state=$this->config_lib->get_estado_gastos();
		$data['manage_table']=get_gastos_manage_table($this->Gasto->get_all(20, $this->input->get('per_page'),$this->input->post("meses"),$state),$state);
		echo json_encode(array('success'=>'true','manage_table'=>$data['manage_table'],'estado'=>$this->input->post("estado")));
	}
	/*Carga el form para editar o guardar un nuevo gasto*/
	function view($gasto_id=-1){
		$info_gastos=$this->Gasto->get_info($gasto_id);
		$data['info']=$info_gastos;
		$data['config_info']=$this->Appconfig->get_info();
		$language=$this->lang_lib->get_idioma();
		$this->lang_lib->switch_to($language);
		$data['gasto_recurrente']=$this->lang->line('module_gasto_recurrente');
		$data['json_form_errors']=json_encode(array(
			"error_cantidad"=>$this->lang->line('common_numeric_required'),
			"error_date"=>$this->lang->line('common_numeric_date'),
			"error_concepto"=>$this->lang->line('common_concepto_required'),
			"error_category"=>$this->lang->line('common_category_required'),));
		$data['controller_name']=strtolower(get_class());
		$estado=array(
                  'Pendiente'  => $this->lang->line("common_pending"),
                  'Hecho'    =>$this->lang->line("common_done"));
		$payment_options=$this->Recurso->get_all_formas_pago();
		foreach($payment_options as $payment){
			$options[$payment["formas_pago_id"]]=$payment["nombre"];
		}
		$data['id']=$gasto_id;
		$data['report_date_range_simple'] = get_simple_date_ranges();
		$data['months'] = get_months();
		$data['days'] = get_days();
		$data['years'] = get_years();
		$data['hours'] = get_hours();
		$data['minutes'] = get_minutes();
		if($gasto_id==-1){
			$data['id']="menosuno";
			$data['selected_month']=selected_month();
			$data['selected_day']=selected_day();
			$data['selected_year']=selected_year();
			$data['selected_hour']=selected_hour();
			$data['selected_minute']=selected_minute();
			$supplier_id=$this->gastoscobros_lib->get_supplier();
			if($supplier_id!=-1){
				$info=$this->Supplier->get_info($supplier_id);
				$data['supplier']=$info->first_name." ".$info->last_name;
			}
			$data['title']=$this->lang->line('common_new_M').'  '.$this->lang->line('module_gasto');
		}
		else{
			$fecha=explode(' ', $data['info']->fecha);
			$dias=explode('-', $fecha[0]);
			$horas=explode(':', $fecha[1]);
			$data['selected_month']=$dias[1];
			$data['selected_day']=$dias[2];
			$data['selected_year']= $dias[0];
			$data['selected_hour']=$horas[0];
			$data['selected_minute']=$horas[1];
			//$receiving=$this->Receiving->get_info($info_gastos->receiving_id);
			//$this->gastoscobros_lib->set_supplier($receiving->supplier_id);
			//$info=$this->Supplier->get_info($receiving->supplier_id);
			//$data['supplier']=$info->first_name." ".$info->last_name;
			$data['supplier']="";
			foreach(get_days() as $dia){
				$dias[$dia] =$dia;
			}
			if($info_gastos->suscrito!=0){
				unset($dias[29]);
				unset($dias[30]);
				unset($dias[31]);
			}
			if($info_gastos->suscrito=="semanal"){
				unset($dias);
				$dias=array(
				1=>"Lunes",
				2=>"Martes",
				3=>"Miércoles",
				4=>"Jueves",
				5=>"Viernes",
				6=>"Sábado",
				0=>"Domingo",
				);
			}
			if($info_gastos->suscrito=="quincenal"){
				unset($dias[26]);
				unset($dias[27]);
				unset($dias[28]);
			}
			$data['title']=$this->lang->line('module_gasto')."#".$gasto_id;
		}
		$img_folder='./images/cc_images/gastos/'.$data['id'].'/';
		$data['images']=get_filenames($img_folder, TRUE);
		$data['modo']=$options;
		$data['estado']=$estado;
		$data['suscritos']=array(
		0  => $this->lang->line("common_no_suscrito"),
		'semanal'=>$this->lang->line("common_semanal"),
		'quincenal'=>$this->lang->line("common_quincenal"),
		'mensual'=>$this->lang->line("common_mensualidades"),
		'bimestral'=>$this->lang->line("common_bimestral"),
		'trimestral'=>$this->lang->line("common_trimestral"),
		'semestral'=>$this->lang->line("common_semestral"),
		'anual'=>$this->lang->line("common_anual"));
		$this->load->view("cc_views/gastos/form",$data);
	}
	/*Muestra los detalles de algun gasto*/
	function details($gasto_id=-1){
		$data['config_info']=$this->Appconfig->get_info();
		$language=$this->lang_lib->get_idioma();
		$data['info_gasto']=$this->Gasto->get_info($gasto_id);
		$data['id']=$gasto_id;
		if($gasto_id==-1)$data['id']="menosuno";
		$img_folder='./images/cc_images/gastos/'.$gasto_id.'/';
		$data['images']=get_filenames($img_folder, TRUE);
		$data['gasto_id']=$gasto_id;
		$this->load->view("cc_views/gastos/details",$data);
		/**/
	}
	/*Guarda un nuevo gasto o lo actualiza en la BD */
	function save($gasto_id=-1){
		$config_info=$this->Appconfig->get_info();
		$data['config_info']=$config_info;
		$language=$this->lang_lib->get_idioma();
		$info=$this->Gasto->get_info($gasto_id);
		$recurso_id=$this->Recurso->get_info_recurso_metodo($this->input->post('modo'));
		$recurso_info=$this->Recurso->get_info($recurso_id);
		$gastos_data = array(
		"fecha"=>$this->input->post("year")."-".$this->input->post("month")."-".$this->input->post("day")." ".$this->input->post("hour").":".$this->input->post("minute").":00",
		"concepto"=>$this->input->post("concepto").' '.$this->input->post("search_suppliers"),
		"category"=>$this->input->post("category"),
		'coin'=>$recurso_info->coin,
		"estado"=>$this->input->post("estado"),
		"cantidad"=>$this->input->post("cantidad"),
		"suscrito"=>$this->input->post('suscrito_box'),
		'recurso_id'=>$recurso_id
		);
		$corte=$this->input->post("day").'/'.$this->input->post("month");
		$estado_filtro=$this->config_lib->get_estado_gastos();
		if($gastos_data['cantidad']<$info->cantidad && $gastos_data['estado']=="Hecho" && $gasto_id!=-1)
		{
			$gasto_pendiente_data = array(
			"fecha"=>$this->input->post("year")."-".$this->input->post("month")."-".($this->input->post('day')+1)." ".$this->input->post("hour").":".$this->input->post("minute").":00",
			"concepto"=>$info->concepto,
			"category"=>$info->category,
			'coin'=>$info->coin,
			"estado"=>"Pendiente",
			"cantidad"=>$info->cantidad-$this->input->post("cantidad"),
			'recurso_id'=>$recurso_id,
			"suscrito"=>0
			);
			if($this->Gasto->save($gasto_pendiente_data)){
				$notificacion_data=array(
				'time' => date('Y-m-d H:i:s'),
				'notificacion'=>$this->lang->line("module_gasto_parcial")." ".$info->concepto,
				'modulo'=>strtolower(get_class()),
				'prioridad'=>"medium"
				);
				$this->Notificacion->make_new($notificacion_data);
			}
		}
		if($gasto_id!=-1 && $gastos_data['estado']=="Pendiente")$gastos_data['cantidad']=$info->cantidad;
		/*var_dump($gastos_data);*/
		/*var_dump(($gastos_data['suscrito']==0));*/
		/*var_dump(($gastos_data['suscrito']!='0'));*/
		if($gastos_data['suscrito']!='0' && $gastos_data['estado']=="Pendiente" && $gasto_id==-1)
		{
			$resp=$this->Gasto->generar_gasto_recurrente($gastos_data["suscrito"],$corte,$gastos_data["cantidad"],$gastos_data["concepto"]);
			$notificacion_data=array(
			'time' => date('Y-m-d H:i:s'),
			'notificacion'=>$this->lang->line("module_gasto_recurrente_new")." ".$gastos_data["concepto"],
			'modulo'=>strtolower(get_class()),
			'prioridad'=>"medium"
			);
			$this->Notificacion->make_new($notificacion_data);
			$data['total_undone_mes_coin'] =$this->Gasto->undone_mes(date('n'),$config_info["coin"]);
			$data['total_undone_mes_coin2'] =$this->Gasto->undone_mes(date('n'),$config_info["coin2"]);
			$data['total_done_mes_coin'] =$this->Gasto->done_mes(date('n'),$config_info["coin"]);
			$data['total_done_mes_coin2'] =$this->Gasto->done_mes(date('n'),$config_info["coin2"]);
			$data['total_done_coin'] =$this->Gasto->done_total($config_info["coin"]);
			$data['total_done_coin2'] =$this->Gasto->done_total($config_info["coin2"]);
			$data['total_undone_coin'] =$this->Gasto->undone_total($config_info["coin"]);
			$data['total_undone_coin2'] =$this->Gasto->undone_total($config_info["coin2"]);
			echo json_encode(array('success'=>'true','message'=>$this->lang->line('module_gastos_recurrente_success_msj').' '.
				$gastos_data['suscrito'],'gasto_id'=>'-1','total_undone_mes_coin'=>to_currency($data['total_undone_mes_coin']).' '.$config_info["coin"],'total_undone_mes_coin2'=>to_currency($data['total_undone_mes_coin2']).' '.$config_info["coin2"],'total_done_mes_coin'=>to_currency($data['total_done_mes_coin']).' '.$config_info["coin"],'total_done_mes_coin2'=>to_currency($data['total_done_mes_coin2']).' '.$config_info["coin2"],'total_undone_coin'=>to_currency($data['total_undone_coin']).' '.$config_info["coin"],'total_undone_coin2'=>to_currency($data['total_undone_coin2']).' '.$config_info["coin2"],'total_done_coin'=>to_currency($data['total_done_coin']).' '.$config_info["coin"],'total_done_coin2'=>to_currency($data['total_done_coin2']).' '.$config_info["coin2"],'estado'=>$gastos_data['estado'],'estado_filtro'=>$estado_filtro));
		}
		else
		{
			$resp=$this->Gasto->save($gastos_data,$gasto_id);
			$data['total_undone_mes_coin'] =$this->Gasto->undone_mes(date('n'),$config_info["coin"]);
			$data['total_undone_mes_coin2'] =$this->Gasto->undone_mes(date('n'),$config_info["coin2"]);
			$data['total_done_mes_coin'] =$this->Gasto->done_mes(date('n'),$config_info["coin"]);
			$data['total_done_mes_coin2'] =$this->Gasto->done_mes(date('n'),$config_info["coin2"]);
			$data['total_done_coin'] =$this->Gasto->done_total($config_info["coin"]);
			$data['total_done_coin2'] =$this->Gasto->done_total($config_info["coin2"]);
			$data['total_undone_coin'] =$this->Gasto->undone_total($config_info["coin"]);
			$data['total_undone_coin2'] =$this->Gasto->undone_total($config_info["coin2"]);
			if($gastos_data['estado']=='Hecho'){
				$operation_data = array(
				"recurso_id"=>$gastos_data['recurso_id'],
				"monto"=>$gastos_data['cantidad'],
				"coin"=>$recurso_info->coin,
				"concepto"=>"#".$gastos_data['gasto_id']."-".$gastos_data['concepto'],
				"saldo_anterior"=>$recurso_info->recurso,
				"saldo_restante"=>($recurso_info->recurso)-$gastos_data['cantidad']
				);
				if($this->Recurso->save_operation($operation_data)){
					$this->Recurso->rest_recurso($gastos_data['cantidad'],$gastos_data['recurso_id'],$recurso_info->coin);
					$notificacion_data=array(
					'time' => date('Y-m-d H:i:s'),
					'notificacion'=>$this->lang->line("module_gasto_hecho")." ".$gastos_data["concepto"],
					'modulo'=>strtolower(get_class()),
					'prioridad'=>"medium"
					);
					$this->Notificacion->make_new($notificacion_data);
				}
			}
			else{
				$notificacion_data=array(
				'time' => date('Y-m-d H:i:s'),
				'notificacion'=>$this->lang->line("module_gasto_pendiente_new")." ".$gastos_data["concepto"],
				'modulo'=>strtolower(get_class()),
				'prioridad'=>"medium"
				);
				$this->Notificacion->make_new($notificacion_data);
			}
			if($gasto_id==-1 && $resp==true)
			{  
				$image_folder='./images/cc_images/gastos/'.$gastos_data['gasto_id'].'/';
				$srcfile='./images/cc_images/gastos/menosuno/main.jpg';
				if (file_exists($srcfile)&&!file_exists($image_folder))
				{
					mkdir($image_folder,0777,true);
					chmod($image_folder,0777);
					copy($srcfile, $image_folder."/main.jpg");
				}
				echo json_encode(array('success'=>'true','message'=>$this->lang->line('module_gastos_success_msj').' '.
				$gastos_data['concepto'],'gasto_id'=>$gastos_data['gasto_id'],'total_undone_mes_coin'=>to_currency($data['total_undone_mes_coin']).' '.$config_info["coin"],'total_undone_mes_coin2'=>to_currency($data['total_undone_mes_coin2']).' '.$config_info["coin2"],'total_done_mes_coin'=>to_currency($data['total_done_mes_coin']).' '.$config_info["coin"],'total_done_mes_coin2'=>to_currency($data['total_done_mes_coin2']).' '.$config_info["coin2"],'total_undone_coin'=>to_currency($data['total_undone_coin']).' '.$config_info["coin"],'total_undone_coin2'=>to_currency($data['total_undone_coin2']).' '.$config_info["coin2"],'total_done_coin'=>to_currency($data['total_done_coin']).' '.$config_info["coin"],'total_done_coin2'=>to_currency($data['total_done_coin2']).' '.$config_info["coin2"],'estado'=>$gastos_data['estado'],'estado_filtro'=>$estado_filtro));
			}
			elseif($gasto_id!=-1 && $resp==true) /*artículo anterior */
			{
				echo json_encode(array('success'=>'true','message'=>$this->lang->line('module_gastos_update_msj'),'gasto_id'=>$gasto_id,
				'total_undone_mes_coin'=>to_currency($data['total_undone_mes_coin']).' '.$config_info["coin"],'total_undone_mes_coin2'=>to_currency($data['total_undone_mes_coin2']).' '.$config_info["coin2"],'total_done_mes_coin'=>to_currency($data['total_done_mes_coin']).' '.$config_info["coin"],'total_done_mes_coin2'=>to_currency($data['total_done_mes_coin2']).' '.$config_info["coin2"],'total_undone_coin'=>to_currency($data['total_undone_coin']).' '.$config_info["coin"],'total_undone_coin2'=>to_currency($data['total_undone_coin2']).' '.$config_info["coin2"],'total_done_coin'=>to_currency($data['total_done_coin']).' '.$config_info["coin"],'total_done_coin2'=>to_currency($data['total_done_coin2']).' '.$config_info["coin2"],'estado'=>$gastos_data['estado'],'estado_filtro'=>$estado_filtro));
			}
			elseif($resp==false)/* fracaso */
			{
			echo json_encode(array('success'=>'true','message'=>$this->lang->line('common_form_error')));
			}
		}
		/**/
	}
	/*Borra gastos en la BD */
	function delete(){
		$data['config_info']=$this->Appconfig->get_info();
		$language=$this->lang_lib->get_idioma();
		$gastos_to_delete=$this->input->post("ids")!=false ? $this->input->post("ids"):array();
		if($this->Gasto->delete_list($gastos_to_delete))
		{
				$data['total_undone_mes_coin'] =$this->Gasto->undone_mes(date('n'),$config_info["coin"]);
			$data['total_undone_mes_coin2'] =$this->Gasto->undone_mes(date('n'),$config_info["coin2"]);
			$data['total_done_mes_coin'] =$this->Gasto->done_mes(date('n'),$config_info["coin"]);
			$data['total_done_mes_coin2'] =$this->Gasto->done_mes(date('n'),$config_info["coin2"]);
			$data['total_done_coin'] =$this->Gasto->done_total($config_info["coin"]);
			$data['total_done_coin2'] =$this->Gasto->done_total($config_info["coin2"]);
			$data['total_undone_coin'] =$this->Gasto->undone_total($config_info["coin"]);
			$data['total_undone_coin2'] =$this->Gasto->undone_total($config_info["coin2"]);
			echo json_encode(array("success"=>"true","message"=>$this->lang->line("common_delete_succes")." ".
			count($gastos_to_delete)." ".$this->lang->line("module_gasto")."(s)",'total_undone_mes_coin2'=>to_currency($data['total_undone_mes_coin2']).' '.$config_info["coin2"],'total_done_mes_coin'=>to_currency($data['total_done_mes_coin']).' '.$config_info["coin"],'total_done_mes_coin2'=>to_currency($data['total_done_mes_coin2']).' '.$config_info["coin2"],'total_undone_coin'=>to_currency($data['total_undone_coin']).' '.$config_info["coin"],'total_undone_coin2'=>to_currency($data['total_undone_coin2']).' '.$config_info["coin2"],'total_done_coin'=>to_currency($data['total_done_coin']).' '.$config_info["coin"],'total_done_coin2'=>to_currency($data['total_done_coin2']).' '.$config_info["coin2"]));
		}
		else
		{
			echo json_encode(array('success'=>"false",'message'=>$this->lang->line('common_form_error')));
		}
	}
	/*Obtiene una fila para insertar en la tabla*/
	function get_row(){
		$data['config_info']=$this->Appconfig->get_info();
		$language=$this->lang_lib->get_idioma();
		$estado=$this->config_lib->get_estado_gastos();
		$gasto_id = $this->input->post("row_id");
		$data_row=get_gastos_data_row($this->Gasto->get_info($gasto_id),$estado);
		echo $data_row;
	}
	/*Busca un gasto mediante un filtro específico de busqueda*/
	function search(){
		$search=trim($this->input->post("search"));
		$estado=$this->config_lib->get_estado_gastos();
		$filtro=$this->config_lib->get_filtro_gastos();
		$data_rows=get_gastos_manage_table_data_rows($this->Gasto->specific_search($search,$filtro,$estado),$estado,true);
		echo json_encode(array("data_rows"=>$data_rows));
	}
	/*Sugiere las busquedas en el manage table*/
	function suggest(){
		$estado=$this->config_lib->get_estado_gastos();
		$filtro=$this->config_lib->get_filtro_gastos();
		$valores=$this->input->post("q");
		$suggestions = $this->Gasto->get_search_suggestions($valores,$filtro,$estado);
		/*echo json_encode(array($filtro=>$valores));*/
		echo json_encode($suggestions);
		/**/
	}
	/*Establece el filtro para las busquedas*/
	function set_filter_search(){
		$filter=$this->input->post("filter");
		$this->config_lib->set_filtro_gastos($filter);
		echo json_encode( array('success'=>$this->config_lib->get_filtro_gastos()));
	}
	/*Carga lo mismo que el index pero con cierto offset para paginar la tabla del modulo*/
	function page($offset=0){
		if($offset < 0)$offset=0;
		$data['notificacion_qty']=$this->Notificacion->get_all()->num_rows();
		$data['notificaciones']=notification_header($this->Notificacion->get_all());
		$data['version']=get_cc_version();
		$data['controller_name']=strtolower(get_class());
		$config_info=$this->Appconfig->get_info();
		$data['config_info']=$config_info;
		$language=$this->lang_lib->get_idioma();
		$this->lang_lib->switch_to($language);
		$data['selected_month']=$this->config_lib->get_mes_show();
		$filtro_uno=$this->config_lib->get_filtro_gastos();
		if(empty($filtro_uno))
		{
			$this->config_lib->set_filtro_gastos("concepto");
		}
		$data['filtro']=$this->config_lib->get_filtro_gastos();
		$config['total_rows'] = $this->Gasto->count_all($data['selected_month']);
		$config['per_page'] = 10000;
		if($data['selected_month']=='0')$config['per_page'] = 20;
		$config['base_url'] = base_url()."index.php/".$data['controller_name'].'/page/';
		$config['first_link'] = base_url().$data['controller_name'];
		if($config['total_rows']>$config['per_page'])$this->pagination->initialize($config);
		/*///////////////////*/
		$total_undone_mes_coin = $this->Gasto->undone_mes(date('n'),$config_info["coin"]);
		$total_undone_mes_coin2 = $this->Gasto->undone_mes(date('n'),$config_info["coin2"]);
		$data['total_undone_mes_coin'] =$total_undone_mes_coin;
		$data['total_undone_mes_coin2'] =$total_undone_mes_coin2;
		$data['total_done_mes_coin'] =$this->Gasto->done_mes(date('n'),$config_info["coin"]);
		$data['total_done_mes_coin2'] =$this->Gasto->done_mes(date('n'),$config_info["coin2"]);
		$data['total_done_coin'] =$this->Gasto->done_total($config_info["coin"]);
		$data['total_done_coin2'] =$this->Gasto->done_total($config_info["coin2"]);
		$data['total_undone_coin'] =$this->Gasto->undone_total($config_info["coin"]);
		$data['total_undone_coin2'] =$this->Gasto->undone_total($config_info["coin2"]);
		if(empty($total_undone_mes_coin))
		{
			$data['total_undone_mes_coin'] =0;
		}
		if(empty($total_undone_mes_coin2))
		{
			$data['total_undone_mes_coin2'] =0;
		}
		/*//*/
		$data['estado']=array(
                  'Pendiente'  => 'Pendientes',
                  'Hecho'    =>'Hechos');
		if(empty($total_done))
		{
			$data['total_done'] =0;
		}
		$meses=array(
				  '0'  => 'Todos',
                  '01'  => 'Enero',
                  '02'  => 'Febrero',
				  '03'  => 'Marzo',
                  '04'  => 'Abril',
				  '05'  => 'Mayo',
                  '06'  => 'Junio',
				  '07'  => 'julio',
                  '08'  => 'Agosto',
				  '09'  => 'Septiembre',
                  '10'  => 'Octubre',
				  '11'  => 'Noviembre',
                  '12'  => 'Diciembre'
				  );
		$data['meses'] =$meses;
		$data['estado_selected']=$this->config_lib->get_estado_gastos();/*Aqui va el filtro*/
		/*Muestra cuentas*/
		/*foreach($this->Recurso->get_all()->result_array() as $row)
		{
		$recursos[$row['recurso_id']] =$row['nombre'];
		}
		foreach($this->Recurso->get_all()->result_array() as $row)
		{
		$cuentas[$row['recurso_id']] ='  '.$row['nombre'].'  '.$row['recurso'];
		}*/
		$data['manage_table']=get_gastos_manage_table($this->Gasto->get_all($config['per_page'], $this->input->get('per_page'),$data['selected_month'],$data['estado_selected']),$data['estado_selected']);
		 if(empty($cuentas))
				  {
					  $cuentas['-1'] ='No existen';
				  }
		$data['cuentas']=$cuentas;
		$this->load->view('cc_views/gastos/manage',$data);
	}
	/*Sube la imagen del form*/
	function upload_image($id=-1){
		if($id==-1)$id="menosuno";
		$image_folder='./images/cc_images/gastos/'.$id;
		if (!file_exists($image_folder))
			{
				mkdir($image_folder,0777,true);
				chmod($image_folder,0777);
			}
		/*comprobamos que sea una petición ajax*/
			if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') 
			{
				/*obtenemos el archivo a subir*/
				$files = $_FILES['uploadfile']['name'];
				$i = 0;
				/*comprobamos si el archivo ha subido*/
				foreach($files as $file)
				{
					if (move_uploaded_file($_FILES['uploadfile']['tmp_name'][$i],"images/cc_images/gastos/".$id."/1.jpg"))
					{
						echo "EL archivo ".$_FILES['uploadfile']['name'][$i]." ha subido correctamente<br>";
						$i++;
					}
				}
			}
			else
			{
				throw new Exception("Error Processing Request", 1);
			}
	}
	/*Sugiere la categoria del gasto en el input category del form*/
	function suggest_category(){
		$suggestions = array();
		$suggestions = $this->Gasto->get_category_suggestions($this->input->post("q"));
		echo json_encode($suggestions);
	}
	/*Renueva los gastos recurrentes al iniciar un nuevo año*/
	function renovar_gastos_recurrentes(){
		$this->Gasto->generar_gastos_new_year('semanal');
		$this->Gasto->generar_gastos_new_year('quincenal');
		$this->Gasto->generar_gastos_new_year('mensual');
		$this->Gasto->generar_gastos_new_year('bimestral');
		$this->Gasto->generar_gastos_new_year('trimestral');
		$this->Gasto->generar_gastos_new_year('semestral');
		$this->Gasto->generar_gastos_new_year('anual');
	}
	/*Quita la recurrencia de un gasto*/
	function cancelar_gastos_recurrentes(){
		$resp=false;
		$gasto_id=$this->input->post("gasto_id");
		$resp=$this->Gasto->cancelar_gasto_recurrente($gasto_id);
		echo json_encode(array('success'=>$resp));
	}
	/*Cambia el estado deducible de un gasto via ajax*/
	function deducibles($gasto_id){
		$data['config_info']=$this->Appconfig->get_info();
		$language=$this->lang_lib->get_idioma();
		$respuesta=$this->input->post('deducible');
		$resp=$this->Gasto->deducible($gasto_id,$respuesta);
		echo json_encode(array('success'=>$resp, 'msj'=>''));
	}
	/*Manual*/
	function help(){
		$language=$this->lang_lib->get_idioma();
		$this->lang_lib->switch_to($language);
		$this->lang->load('manual', $language);
		$data['controller_name']=strtolower(get_class());;
		$data['contenido']=get_table_manual($data['controller_name']);
		$this->load->view("cc_views/".$data['controller_name']."/manual",$data);
	}
}	