<?php
class Login extends CI_Controller 
{
	function __construct(){
		parent::__construct();
	}
	/*Verifica la session si no es valida carga el view para hacer login.*/
	function index(){
		$language=$this->lang_lib->get_idioma();
		$this->lang_lib->switch_to($language);
		if($this->Employee->is_logged_in()){
			redirect('home');
		}
		else{
			$this->form_validation->set_rules('username', 'lang:login_undername', 'callback_login_check');
    	    $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
			if($this->form_validation->run() == FALSE){
				$this->load->view('cc_views/login');
			}
			else{
				redirect('home');
			}
		}
	}
	/*Checa en la bd si las credenciales son validas.*/
	function login_check($username){
		$language=$this->lang_lib->get_idioma();
		$this->lang_lib->switch_to($language);
		$password = $this->input->post("password");
		if(!$this->Employee->login($username,$password)){
			$this->form_validation->set_message('login_check', $this->lang->line('common_invalid_username_and_password'));
			return false;
		}
		return true;
	}
}
?>