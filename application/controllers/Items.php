<?php
require_once ("Secure_area.php");
class Items extends Secure_area
{
	function __construct(){
		parent::__construct('items');
	}
	/*Carga el manage view del modulo*/
	function index(){
		$data['notificacion_qty']=$this->Notificacion->get_all()->num_rows();
		$data['notificaciones']=notification_header($this->Notificacion->get_all());
		$data['version']=get_cc_version();
		$data['controller_name']=strtolower(get_class());
		$language=$this->lang_lib->get_idioma();
		$this->lang_lib->switch_to($language);
		$config['total_rows'] = $this->Item->count_all();
		$config['per_page'] = 20;
		$config['base_url'] = base_url()."index.php/".$data['controller_name'].'/page/';
		$config['first_link'] = base_url().$data['controller_name'];
		if($config['total_rows']>$config['per_page'])$this->pagination->initialize($config);
		$filtro_uno=$this->config_lib->get_filtro_items();
		$barcode_config=$this->config_lib->get_barcode_config();
		if(empty($barcode_config)){
			$this->config_lib->set_barcode_config("24");
		}
		if(empty($filtro_uno)){
			$this->config_lib->set_filtro_items("name");
		}
		$data['filtro']=$this->config_lib->get_filtro_items();
		$data['config_info']=$this->Appconfig->get_info();
		$data['manage_table']=get_items_manage_table($this->Item->get_all($data['filtro'],$config['per_page'], $this->input->get('per_page')),$this);
		$idiomas=array();
		$languages =get_dir_file_info(APPPATH.'language/');
		foreach($languages as $language){
			if($language["name"]!="index.html")$idiomas[$language["name"]]=$language["name"];
		}
		$data["idiomas"]=$idiomas;
		$data['idioma']=$this->lang_lib->get_content_idioma();
		$this->load->view('cc_views/items/manage',$data);
	}
	/*Regresa un objeto via ajax con la información de un item*/
	function find_item_info($item_id){
		echo json_encode($this->Item->get_info($item_id));
	}
	/*Busca el Item mediante un filtro específico de busqueda*/
	function search(){
		$offset=$this->uri->segment(3, 0);
		$search=trim($this->input->post("search"));
		$filtro=$this->config_lib->get_filtro_items();
		$data_rows=get_items_manage_table_data_rows($this->Item->specific_search($search,$filtro,$offset),$this,true);
		echo json_encode(array("data_rows"=>$data_rows));
	}
	/*Sugiere las busquedas en el manage table*/
	function suggest(){
		$filtro=$this->config_lib->get_filtro_items();
		$valores=$this->input->post("q");
		$offset=$this->uri->segment(3, 0);
		$suggestions = $this->Item->get_search_suggestions($valores,10,$filtro,$offset);
		echo json_encode($suggestions);
	}
	/*Sugiere una busqueda ilimitada de items filtrada por nombre */
	function suggest_forms(){
		$filtro="name";
		$valores=$this->input->post("q");
		$offset=$this->uri->segment(3, 0);
		$suggestions = $this->Item->get_search_suggestions($valores,'',$filtro,$offset);
		echo json_encode($suggestions);
	}
	/*Hace una sugerencia mesclada de items y kits */
	function item_search(){
		$suggestions = $this->Item->get_item_search_suggestions($this->input->post('q'),$this->input->post('limit'));
		$suggestions = array_merge($suggestions, $this->Item_kit->get_item_kit_search_suggestions($this->input->post('q'),$this->input->post('limit')));
		echo json_encode($suggestions);
	}
	/*Obtiene una sugerencia busqueda exclusivamente items*/
	function items_only_search(){
		$suggestions = $this->Item->get_item_search_suggestions($this->input->post('q'),$this->input->post('limit'));
		echo json_encode($suggestions);
	}
	/*Sugiere las busquedas de las categorias existentes en el form*/
	function suggest_category(){
		$suggestions = array();
		$suggestions = $this->Item->get_category_suggestions($this->input->post("q"));
		echo json_encode($suggestions);
	}
	/*Sugiere las busquedas de las marcas existentes en el form*/
	function suggest_brand(){
		$suggestions = array();
		$suggestions = $this->Item->get_brand_suggestions($this->input->post("q"));
		echo json_encode($suggestions);
	}
	/*Obtiene una fila actualizada del manage table*/
	function get_row(){
		$item_id = $this->input->post("row_id");
		$data_row=get_item_data_row($this->Item->get_info($item_id),$this);
		echo $data_row;
	}
	/*Carga el form para editar o guardar un nuevo Producto*/
	function view($item_id=-1){
		$language=$this->lang_lib->get_idioma();
		$this->lang_lib->switch_to($language);
		$data['json_form_errors']=json_encode(array(
		"error_items_name"=>$this->lang->line('module_item_required'),
		"error_brand"=>$this->lang->line('module_item_brand_required'),
		"error_modelo"=>$this->lang->line('module_item_modelo_required'),
		"error_category"=>$this->lang->line('common_category_required'),
		"error_cost_price"=>$this->lang->line('common_cost_required'),
		"error_price"=>$this->lang->line('common_price_required'),
		"error_other_price"=>$this->lang->line('common_other_price_required'),
		"error_quantity"=>$this->lang->line('module_item_stock_required'),
		"error_reorder_level"=>$this->lang->line('module_item_reorder_level_required'),
		"error_numeric"=>$this->lang->line('common_numeric_required')
		));
		$data['loading_txt']=$this->lang->line('common_uploadproccess');
		$data['config_info']=$this->Appconfig->get_info();
		$data['controller_name']=strtolower(get_class());
		$item_info=$this->Item->get_info($item_id);
		$data['item_cfdi_info']=$this->Item->get_info_cfdi($item_id);
		$data['item_info']=$item_info;
		$data['item_tax_info']=$this->Item_taxes->get_info($item_id);
		$data['title_form']=$item_info->name;
		$suppliers = array();
		$filtro=$this->config_lib->get_filtro_suppliers();
		if(empty($filtro)){
			$this->config_lib->set_filtro_suppliers("last_name");
			$filtro=$this->config_lib->get_filtro_suppliers();
		}
		foreach($this->Supplier->get_all($filtro)->result_array() as $row){
			$suppliers[$row['person_id']] = $row['company'] .' ('.$row['first_name'] .' '. $row['last_name'].')';
		}
		$data['id']=$item_id;
		if($item_id==-1){
			$data['id']="menosuno";
			$data['title_form']=$this->lang->line('common_new').'  '.$this->lang->line('module_item');
		}
		$img_folder='./images/f_images/img_catalogo/productos/'.$data['id'].'/';
		$data['images']=get_filenames($img_folder, TRUE);
		$data['suppliers']=$suppliers;
		$data['selected_supplier'] = $item_info->supplier_id;
		$data['default_tax_1_rate']=($item_id==-1) ? $this->Appconfig->get('default_tax_1_rate') : '';
		$data['default_tax_2_rate']=($item_id==-1) ? $this->Appconfig->get('default_tax_2_rate') : '';
		$this->load->view("cc_views/items/form",$data);
	}
	/* Carga el form para editar el inventario */
	function inventory($item_id){
		$language=$this->lang_lib->get_idioma();
		$this->lang_lib->switch_to($language);
		$data['json_msjs']=json_encode(array(
			"error_inventory_stock"=>$this->lang->line("common_numeric_required_cero"),
			"error_inventory_razon"=>$this->lang->line("common_inventory_razon")));
		$data['item_info']=$this->Item->get_info($item_id);
		$this->load->view("cc_views/items/inventory",$data);
	}
	/*Carga el form de detalles de los items*/
	function count_details($item_id){
		$language=$this->lang_lib->get_idioma();
		$this->lang_lib->switch_to($language);
		$data['item_tax_info']=$this->Item_taxes->get_info($item_id);
		$data['default_tax_1_rate']=($item_id==-1) ? $this->Appconfig->get('default_tax_1_rate') : '';
		$data['default_tax_2_rate']=($item_id==-1) ? $this->Appconfig->get('default_tax_2_rate') : '';
		$data['item_info']=$this->Item->get_info($item_id);
		$img_folder='./images/f_images/img_catalogo/productos/'.$item_id.'/';
		$data['images']=get_filenames($img_folder, TRUE);
		$supplier_info=$this->Supplier->get_info($this->Item->get_info($item_id)->supplier_id);
		$data["supplier"]=$supplier_info->first_name." ".$supplier_info->last_name." (".$supplier_info->company.")";
		$this->load->view("cc_views/items/count_details",$data);
	} /*------------------------------------------- Ramel*/
	/*Regresa el json para cargar el codigo de barras en el view*/
	function generate_barcodes($id_to_barcode){
		$language=$this->lang_lib->get_idioma();
		$this->lang_lib->switch_to($language);
		if($to_barcode=$this->Item->get_info($id_to_barcode)){
			echo json_encode($to_barcode);
		}
		else{
			echo json_encode(array("success"=>"false","message"=>$this->lang->line("common_form_error")));
		}
	}
	/*Carga el view para mostrar los codigos de barras*/
	function barcodes(){
		$language=$this->lang_lib->get_idioma();
		$this->lang_lib->switch_to($language);
		$data['sheet']=get_codebar_sheet($this->config_lib->get_barcode_config());
		$data['config']=$this->config_lib->get_barcode_config();
		$this->load->view("cc_views/barcode_sheet",$data);
	}
	/*Carga el form para editar un bunche de items*/
	function bulk_edit(){
		$language=$this->lang_lib->get_idioma();
		$this->lang_lib->switch_to($language);
		$data = array();
		$data['json_form_errors']=json_encode(array(
			"error_items_form_bulk"=>$this->lang->line('module_item_form_bulk'),
			"error_cost_price"=>$this->lang->line('common_cost_required'),
			"error_price"=>$this->lang->line('common_price_required'),
			"error_other_price"=>$this->lang->line('common_other_price_required'),
			"error_numeric"=>$this->lang->line('common_numeric_required')));
		$data['config_info']=$this->Appconfig->get_info();
		$suppliers = array('' => $this->lang->line('items_none'));
		$filtro=$this->config_lib->get_filtro_suppliers();
		if(empty($filtro)){
			$this->config_lib->set_filtro_suppliers("last_name");
			$filtro=$this->config_lib->get_filtro_suppliers();
		}
		foreach($this->Supplier->get_all($filtro)->result_array() as $row){
			$suppliers[$row['person_id']] = $row['company'] .' ('.$row['first_name'] .' '. $row['last_name'].')';
		}
		$data['suppliers'] = $suppliers;
		$data['publish_choices'] = array(
			''=>$this->lang->line('module_item_no_action'), 
			1 =>$this->lang->line('module_item_action_all'),
			0 =>$this->lang->line('module_item_action_quite_all'));
		$data['destacar_choices'] = array(
			''=>$this->lang->line('module_item_no_action'), 
			1 =>$this->lang->line('module_item_action_all'),
			0 =>$this->lang->line('module_item_action_quite_all'));
		$data['show_price_choices'] = array(
			''=>$this->lang->line('module_item_no_action'), 
			1 =>$this->lang->line('module_item_action_all'),
			0 =>$this->lang->line('module_item_action_quite_all'));
		$data['show_stock_choices'] = array(
			''=>$this->lang->line('module_item_no_action'), 
			1 =>$this->lang->line('module_item_action_all'),
			0 =>$this->lang->line('module_item_action_quite_all'));	
		$data['promocionar_choices'] = array(
			''=>$this->lang->line('module_item_no_action'), 
			1 =>$this->lang->line('module_item_action_all'),
			0 =>$this->lang->line('module_item_action_quite_all'));	
		$data['allow_alt_desciption_choices'] = array(
			''=>$this->lang->line('module_item_no_action'), 
			1 =>$this->lang->line('module_item_action_all'),
			0 =>$this->lang->line('module_item_action_quite_all'));
		$data['serialization_choices'] = array(
			''=>$this->lang->line('module_item_no_action'), 
			1 =>$this->lang->line('module_item_action_all'),
			0 =>$this->lang->line('module_item_action_quite_all'));
		$this->load->view("cc_views/items/form_bulk", $data);
	}
	/*Actualiza o guarda un nuevo Producto en la BD*/
	function save($item_id=-1){
		$language=$this->lang_lib->get_idioma();
		$this->lang_lib->switch_to($language);
		$chars = array(",", "(", ")");
		$item_data = array(
			'name'=>trim($this->input->post('name')),
			'category'=>trim(str_replace($chars, "", $this->input->post('category'))),
			'supplier_id'=>$this->input->post('supplier'),
			'item_number'=>$this->input->post('item_number')=='' ? null:$this->input->post('item_number'),
			'description'=>trim($this->input->post('description')),
			'cost_price'=>$this->input->post('cost_price'),
			'unit_price'=>$this->input->post('unit_price'),
			'other_price'=>$this->input->post('other_price'),
			'quantity'=>$this->input->post('quantity'),
			'reorder_level'=>$this->input->post('reorder_level'),
			'location'=>trim($this->input->post('location')),
			'allow_alt_description'=>$this->input->post('allow_alt_description'),
			'is_serialized'=>$this->input->post('is_serialized'),
			'publicar'=>$this->input->post('publish'),
			'destacar'=>$this->input->post('destacar'),
			'show_price'=>$this->input->post('show_price'),
			'show_stock'=>$this->input->post('show_stock'),
			'promocionar'=>$this->input->post('promocionar'),
			'brand'=>trim($this->input->post('brand')),
			'modelo'=>trim($this->input->post('modelo')),
			'img_qty'=>$this->input->post('img_qty')
		);
		$cfdi_fields=array(
			'c_ClaveProdServ'=>trim($this->input->post('c_ClaveProdServ')),
			'c_ClaveUnidad'=>trim($this->input->post('c_ClaveUnidad'))
		);
		$employee_id=$this->Employee->get_logged_in_employee_info()->person_id;
		$barcode_exists=false;
		if($item_data["item_number"]!=null)$barcode_exists=$this->Item->barcode_exists($item_data["item_number"],$item_id);
		$srcfile='./images/f_images/img_catalogo/productos/menosuno/main.jpg';
		$cat_folder='./images/f_images/img_catalogo/'.strtoupper(url_title($item_data['category'],'underscore'));
		if(file_exists($srcfile)&&!file_exists($cat_folder)){
			mkdir($cat_folder,0777,true);
			copy($srcfile, $cat_folder."/main.jpg");
			$languages =get_dir_file_info(APPPATH.'language/');
			foreach($languages as $language){
				if($language["name"]!="index.html")$this->lang_lib->content_cat_file_new($language['name'],$item_data['category']);
			}
		}
		if($item_data["item_number"]!=null && $barcode_exists==false)$barcode_exists=$this->Item->barcode_exists($item_data["item_number"],-1);
		if($barcode_exists==false){	
			if($this->Item->save($item_data,$item_id)){
				/* Nuevo artículo */
				if($item_id==-1){
					$item_id = $item_data['item_id'];
					$languages =get_dir_file_info(APPPATH.'language/');
					foreach($languages as $language){
						if($language["name"]!="index.html")$this->lang_lib->items_file_new($language['name'],$item_id);
					}
					echo json_encode(array('success'=>'true','message'=>$this->lang->line("module_items_success_msj").' '.$item_data['name'],'item_id'=>$item_data['item_id']));
				}
				else{
					echo json_encode(array('success'=>'true','message'=>$this->lang->line("module_items_update_msj").' '.$item_data['name'],'item_id'=>$item_id));
				}
				$itemfolder='./images/f_images/img_catalogo/productos/'.$item_id.'/';
				if (file_exists($srcfile)&&!file_exists($itemfolder)){
					mkdir($itemfolder, 0777, true);
					copy($srcfile, $itemfolder."/main.jpg");
				}
				else{
					copy($srcfile, $itemfolder."/main.jpg");
				}
				$cur_item_info = $this->Item->get_info($item_id);
				for($i=1;$i<=$item_data["img_qty"];$i++){
					$dstfile='./images/f_images/img_catalogo/productos/'.$item_id.'/sec_'.$i.'.jpg';
					if (file_exists($srcfile)&&!file_exists($dstfile)){
						copy($srcfile, $dstfile);
					}
				}
				$this->Item->save_cfdi_fields($cfdi_fields,$item_id);
				$items_taxes_data = array();
				$tax_names = $this->input->post('tax_names');
				$tax_percents = $this->input->post('tax_percents');
				for($k=0;$k<count($tax_percents);$k++){
					if (is_numeric($tax_percents[$k])){
						$items_taxes_data[] = array('name'=>$tax_names[$k], 'percent'=>$tax_percents[$k] );
					}
				}
				$this->Item_taxes->save($items_taxes_data, $item_id);
			}
			else{
				/* Fracaso */
				echo json_encode(array('success'=>'false','message'=>$this->lang->line('common_form_error').' '.
				$item_data['name'],'item_id'=>-1));
			}
		}
		else{
			echo json_encode(array('success'=>'false','message'=>$this->lang->line('module_item_codebar_error'),'item_id'=>-1));
		}
		$this->Item->save_items_content($item_id);
		/*$this->load->library("routes_lib");
		$this->load->library("Home_lib");
		$this->routes_lib->make_routes_catalogo();*/
		/*$this->home_lib->make_sitemap();
		$this->home_lib->make_robotstxt();*/
		/**/
	}
	/*Actualiza el stock y registra la razon en la BD*/
	function save_inventory($item_id=-1){	
		$language=$this->lang_lib->get_idioma();
		$this->lang_lib->switch_to($language);
		$employee_id=$this->Employee->get_logged_in_employee_info()->person_id;
		$cur_item_info = $this->Item->get_info($item_id);
		if($this->input->post('newquantity')>0){
			$trans_type="entrada";
		}
		else{
			$trans_type="salida";
		}
		$inv_data = array
		(
			'trans_date'=>date('Y-m-d H:i:s'),
			'trans_items'=>$item_id,
			'trans_user'=>$employee_id,
			'trans_comment'=>$this->input->post('trans_comment'),
			'trans_inventory'=>$this->input->post('newquantity'),
			'trans_type'=>$trans_type
		);
		$this->Inventory->insert($inv_data);
		/* Actualizar stock de cantidad */
		$item_data = array(
		"quantity"=>$cur_item_info->quantity + $this->input->post("newquantity")
		);
		if($this->Item->save($item_data,$item_id)){
			$notificacion_data=array(
			'time' => date('Y-m-d H:i:s'),
			'notificacion'=>$this->lang->line("module_items_inventory_noty")." ".$cur_item_info->name,
			'modulo'=>strtolower(get_class()),
			'prioridad'=>"medium"
			);
			$this->Notificacion->make_new($notificacion_data);
			echo json_encode(array("success"=>"true","message"=>$this->lang->line('module_items_update_msj')." ".
			$cur_item_info->name,"item_id"=>$item_id));
		}
		else{
			/* fracaso */
			echo json_encode(array("success"=>"false","message"=>$this->lang->line("common_form_error")." ".
			$cur_item_info->name,"item_id"=>-1));
		}
	}
	/*Edita algunas cosas en un bunche de items*/
	function bulk_update(){
		$items_to_update=$this->input->post("item_ids")!=false ? $this->input->post("item_ids"):array();
		$item_data = array();
		$cfdi_fields=array();
		$language=$this->lang_lib->get_idioma();
		$this->lang_lib->switch_to($language);
		foreach($_POST as $key=>$value)
		{
			/* Este campo es nulo, así que trátelo de manera diferente */
			if ($key == 'supplier_id')
			{
				$item_data["$key"]=$value == '' ? null : $value;
			}
			elseif($key != 'c_ClaveUnidad' and $key != 'c_ClaveProdServ' and $value!='' and !(in_array($key, array('item_ids', 'tax_names', 'tax_percents'))))
			{
				$item_data["$key"]=$value;
			}
			elseif($key == 'c_ClaveUnidad' or $key == 'c_ClaveProdServ')
			{
				if($value!='')$cfdi_fields["$key"]=$value;
			}
			if($key=='img_qty' and $value!='')
			{
				$img_qty=$value;
			}
		}
		if(isset($img_qty))
		{
			$srcfile='./images/f_images/img_catalogo/productos/menosuno/main.jpg';
			foreach($items_to_update as $item_id)
			{
				for($i=1;$i<=$img_qty;$i++)
				{
					$dstfile='./images/f_images/img_catalogo/productos/'.$item_id.'/sec_'.$i.'.jpg';
					if (file_exists($srcfile)&&!file_exists($dstfile))
					{
						copy($srcfile, $dstfile);
					}
				}
			}
		}
		if(count($cfdi_fields)>0)
		{
			$this->Item->update_multiple_cfdi_data($cfdi_fields,$items_to_update);
		}
		/* Los datos del artículo podrían estar vacíos si se actualiza la información fiscal */
		if(empty($item_data) || $this->Item->update_multiple($item_data,$items_to_update))
		{
			$items_taxes_data = array();
			$tax_names = $this->input->post('tax_names');
			$tax_percents = $this->input->post('tax_percents');
			for($k=0;$k<count($tax_percents);$k++)
			{
				if (is_numeric($tax_percents[$k]))
				{
					$items_taxes_data[] = array('name'=>$tax_names[$k], 'percent'=>$tax_percents[$k] );
				}
			}
			$this->Item_taxes->save_multiple($items_taxes_data, $items_to_update);
			echo json_encode(array("success"=>"true","message"=>$this->lang->line("module_items_editon_success")));
		}
		else
		{
			echo json_encode(array("success"=>"false","message"=>$this->lang->line("module_items_editon_fail")));
		}
		/*$this->load->library("routes_lib");
		$this->load->library("Home_lib");
		$this->routes_lib->make_routes_front();
		$this->home_lib->make_sitemap();
		$this->home_lib->make_robotstxt();*/
	}
	/*Borra Productos en la BD*/
	function delete(){
		$items_to_delete=$this->input->post("ids")!=false ? $this->input->post("ids"):array();
		$language=$this->lang_lib->get_idioma();
		$this->lang_lib->switch_to($language);
		if($this->Item->delete_list($items_to_delete)){
			echo json_encode(array("success"=>"true","message"=>$this->lang->line("common_delete_succes").' '.
			count($items_to_delete).' '.$this->lang->line("module_item")."(s)"));
		}
		else{
			echo json_encode(array("success"=>"false","message"=>$this->lang->line("common_form_error")));
		}
		/*$this->load->library("routes_lib");
		$this->load->library("Home_lib");
		$this->routes_lib->make_routes_front();
		$this->home_lib->make_sitemap();
		$this->home_lib->make_robotstxt();*/
	}
	/*Importa items desde un excel*/
	function excel_import(){
		$json = $this->input->post("xlsxsheet");
		$data=json_decode($json, TRUE); 
		$items_to_save=array();
		/*contar registros*/
		$srcfile='./images/f_images/img_catalogo/productos/menosuno/main.jpg';
		foreach($data as $key=>$item){
			if(isset($data[$key]["name"])&& $data[$key]["name"]=="")unset($data[$key]);
			if(isset($data[$key]["item_id"]))unset($data[$key]["item_id"]);
			if(isset($data[$key]["name"])&&isset($data[$key]["category"])&&isset($data[$key]["cost_price"])){
				$items_to_save[$key]=array("name"=>$data[$key]["name"],
				"modelo"=>$data[$key]["modelo"],"brand"=>$data[$key]["brand"],"category"=>$data[$key]["category"],"cost_price"=>$data[$key]["cost_price"],"unit_price"=>$data[$key]["unit_price"],"other_price"=>$data[$key]["other_price"],"quantity"=>$data[$key]["quantity"],"reorder_level"=>$data[$key]["reorder_level"],"location"=>$data[$key]["location"],
				"description"=>$data[$key]["description"]
				);
				$cat_folder='./images/f_images/img_catalogo/'.strtoupper(url_title($data[$key]['category'],"underscore")).'/';
				if(file_exists($srcfile)&&!file_exists($cat_folder)){
						mkdir($cat_folder,0777,true);
						copy($srcfile, $cat_folder."/main.jpg");
						$languages =get_dir_file_info(APPPATH.'language/');
						foreach($languages as $language){
							if($language["name"]!="index.html")$this->lang_lib->content_cat_file_new($language['name'],$data[$key]['category']);
						}
				}
			}
		}
		$result_insertion=$this->Item->save_list($items_to_save);
		/* para saber la cantidad y recuperarla en el get */
			$row=$this->Item->get_last_row();
			for($i=$result_insertion["first_id"];$i<=$row["item_id"];$i++){
			$itemfolder='./images/f_images/img_catalogo/productos/'.$i.'/';
			if (file_exists($srcfile)&&!file_exists($itemfolder)){
				mkdir($itemfolder, 0777,true);
				copy($srcfile, $itemfolder."/main.jpg");
			}
			else{
				copy($srcfile, $itemfolder."/main.jpg");
			}
			$languages =get_dir_file_info(APPPATH.'language/');
			foreach($languages as $language){
				if($language["name"]!="index.html")$this->lang_lib->items_file_new($language['name'],$i);
			}
		}
		$this->session->set_userdata('items_excel',$result_insertion["num_insertions"]);
		$msj="";
		if($result_insertion!=false){
			$msj=$this->lang->line('module_items_success_multi_msj');
		}
		else{
			$msj=$this->lang->line('module_items_fail_msj');
		}
		echo json_encode(array("success"=>$result_insertion["num_insertions"],"message"=>$msj));
	}
	/*Exporta determinado numero de items a excel*/
	function excel_export($all_selected){
		/*phpinfo();*/
		$items_selected=$this->input->post("ids")!=false ? $this->input->post("ids"):array();
		$this->load->library('excel_lib');
		$this->load->helper('excel_helper');
		$filtro=$this->config_lib->get_filtro_items();
		/* $items_ids= array(); */
		if(count($items_selected)==0){
			$selected_items=$this->Item->get_all_no_limit($filtro);
			items_to_excel($selected_items,$this);
			echo json_encode(array("success"=>"Todos"));
		}
		else{
			/*foreach($items_selected as $id)
			{
				$items_ids['item_id']=$id;
			}
			$expl=implode(" ",$items_selected);
			$expl2=implode(" ",array_keys($items_selected));*/
			$selected_items=$this->Item->get_multiple_info($filtro,$items_selected);
			items_to_excel($selected_items,$this);
			echo json_encode(array("success"=>'algunos'));
		}
		$fichero='./application/helpers/excel_helper.xlsx';
		$nuevo_fichero='./formats/excel_helper.xlsx';
		copy($fichero, $nuevo_fichero);
		/**/
	}
	/*Carga el form de acciones con excel*/
	function excel(){
		$this->load->view("cc_views/items/excel", null);
	}
	/*Guarda con la ayuda de Config_lib en una cockie de sesión el filtrado de busquedas del modulo */
	function set_filter_search(){
		$filter=$this->input->post("filter");
		$this->config_lib->set_filtro_items($filter);
		echo json_encode( array('success'=>$this->config_lib->get_filtro_items()));
	}
	/*Sube la imagen del item*/
	function upload_image($item_id,$ext_name){
		$ext_name_array = explode("---", $ext_name);
		$no_ext_name=$ext_name_array[0];
		$ext_name = str_replace("---", ".", $ext_name);
		$image_folder='./images/f_images/img_catalogo/productos/'.$item_id.'/';
		if (!file_exists($image_folder))
			{
					mkdir($image_folder,0777,true);
					chmod($image_folder,0777);
			}
			$img_info=getimagesize($image_folder.$ext_name);
		/* comprobamos que sea una petición ajax */
			if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') 
			{
			/* obtenemos el archivo a subir */
			$files = $_FILES[$no_ext_name]['name'];
			$i = 0;
			/* comprobamos si el archivo ha subido */
			foreach($files as $file)
			{
				/* /"images/f_images/paginas/".$id."/img_banner_1.jpg" */
			if (move_uploaded_file($_FILES[$no_ext_name]['tmp_name'][$i],$image_folder.$ext_name))
			{
				echo "EL archivo ".$no_ext_name." ha subido correctamente<br>";
				$config['image_library'] = 'gd2';
				$config['source_image'] = $image_folder.$ext_name;
				$config['create_thumb'] = FALSE;
				$config['maintain_ratio'] = TRUE;
				$config['width']         = $img_info[0];
				$config['height']       = $img_info[1];
				$this->load->library('image_lib', $config);
				if ( ! $this->image_lib->resize())
				{
				echo $this->image_lib->display_errors($this->lang->line("common_img_resizefail"));
				}
				$i++;
			}
			}
			}else{
				echo $this->lang->line("common_uploadfail");  
			}
	}
	/*Sube la imagen de la categoria*/
	function upload_cat_image($item_id,$ext_name){
		$ext_name_array = explode("---", $ext_name);
		$no_ext_name=$ext_name_array[0];
		$ext_name = str_replace("---", ".", $ext_name);
		$item_info= $this->Item->get_info($item_id);
		$image_folder='./images/f_images/img_catalogo/'.strtoupper(url_title($item_info->category,'_')).'/';
		if (!file_exists($image_folder))
			{
					mkdir($image_folder,0777,true);
					chmod($image_folder,0777);
			}
			$img_info=getimagesize($image_folder.$ext_name);
		/* comprobamos que sea una petición ajax */
			if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') 
			{
			/* obtenemos el archivo a subir */
			$files = $_FILES[$no_ext_name]['name'];
			$i = 0;
			/* comprobamos si el archivo ha subido */
			foreach($files as $file)
			{
				/* /"images/f_images/paginas/".$id."/img_banner_1.jpg" */
			if (move_uploaded_file($_FILES[$no_ext_name]['tmp_name'][$i],$image_folder.$ext_name))
			{
				echo "EL archivo ".$no_ext_name." ha subido correctamente<br>";
				$config['image_library'] = 'gd2';
				$config['source_image'] = $image_folder.$ext_name;
				$config['create_thumb'] = FALSE;
				$config['maintain_ratio'] = TRUE;
				$config['width']         = $img_info[0];
				$config['height']       = $img_info[1];
				$this->load->library('image_lib', $config);
				if ( ! $this->image_lib->resize())
				{
				echo $this->image_lib->display_errors($this->lang->line("common_img_resizefail"));
				}
				$i++;
			}
			}
			}else{
				echo $this->lang->line("common_uploadfail");  
			}
	}
	/*Obtiene un segmento de los items segun su ofsset*/
	function get_items(){
		$config['total_rows'] = $this->Item->count_all();
		$config['per_page'] = '20';
		$offset=$this->uri->segment(3, 0);
		$data['filtro']=$this->config_lib->get_filtro_items();
		$manage_table=get_items_manage_table($this->Item->get_all($data['filtro'],$config['per_page'], $offset),$this);
		echo json_encode( array('success'=>$manage_table));
	}
	/*Carga lo mismo que el index pero con cierto offset para paginar la tabla del modulo*/
	function page($offset=0){
		if($offset < 0)$offset=0;
		$data['notificacion_qty']=$this->Notificacion->get_all()->num_rows();
		$data['notificaciones']=notification_header($this->Notificacion->get_all());
		$language=$this->lang_lib->get_idioma();
		$this->lang_lib->switch_to($language);
		$data['controller_name']=strtolower(get_class());
		$config['first_link'] = $this->lang->line("common_first_link");
		$config['last_link'] = $this->lang->line("common_last_link");
		$config['total_rows'] = $this->Item->count_all();
		$config['per_page'] = '20';
		$config['base_url'] = base_url().'/index.php/'.$data['controller_name'].'/page/';
		$this->pagination->initialize($config);
		$filtro_uno=$this->config_lib->get_filtro_items();
		$barcode_config=$this->config_lib->get_barcode_config();
		if(empty($barcode_config)){
			$this->config_lib->set_barcode_config("24");
		}
		if(empty($filtro_uno)){
			$this->config_lib->set_filtro_items("name");
		}
		$data['filtro']=$this->config_lib->get_filtro_items();
		$manage_table=get_items_manage_table($this->Item->get_all($data['filtro'],$config['per_page'], $offset),$this);
		$data['filtro']=$this->config_lib->get_filtro_items();
		$data['config_info']=$this->Appconfig->get_info();
		$idiomas=array();
		$languages =get_dir_file_info(APPPATH.'language/');
		foreach($languages as $language){
			if($language["name"]!="index.html")$idiomas[$language["name"]]=$language["name"];
		}
		$data["idiomas"]=$idiomas;
		$data['idioma']=$this->lang_lib->get_content_idioma();
		$data['manage_table']=$manage_table;
		$this->load->view('cc_views/items/manage',$data);
	}
	/*Carga el formulario del contenido de los items*/
	function form_content($item_id){
		$idioma=$this->lang_lib->get_content_idioma();
		$data['idioma']=$idioma;
		$data['loading_txt']=$this->lang->line('common_uploadproccess');
		$data['lang']=$this->lang_lib->get_items_content_file($idioma,$item_id);
		$data['controller_name']=strtolower(get_class());
		$data['item_info']=$this->Item->get_info($item_id);
		$this->load->view("cc_views/items/form_content",$data);
	}
	/*Carga el formulario del contenido de la categorias*/
	function form_cat_content($item_id){
		$idioma=$this->lang_lib->get_content_idioma();
		$data['idioma']=$idioma;
		$data['loading_txt']=$this->lang->line('common_uploadproccess');
		$data['item_info']=$this->Item->get_info($item_id);
		$data['lang']=$this->lang_lib->get_cats_content_file($idioma,url_title($data['item_info']->category));
		$data['controller_name']=strtolower(get_class());
		$img_folder='./images/f_images/img_catalogo/'.strtoupper(url_title($data['item_info']->category,'_')).'/';
		$data['images']=get_filenames($img_folder, TRUE);
		$this->load->view("cc_views/items/form_cat_content",$data);
	}
	/*Guarda los cambios del form_content de los items*/
	function save_edit_content($item_id){
		$fields=$this->input->post(NULL, FALSE);
		$idioma=$this->lang_lib->get_content_idioma();
		$result=$this->lang_lib->save_items_edit_content($idioma,$item_id,$fields);
		$item_info=$this->Item->get_info($item_id);
		echo json_encode(array('success'=>'true','message'=>$this->lang->line('module_items_success_msj'),'id'=>$item_id));
	}
	/* Guarda los cambios del form_content de las categorias*/
	function save_edit_cat_content($item_id){
		$fields=$this->input->post(NULL, FALSE);
		$idioma=$this->lang_lib->get_content_idioma();
		$result=$this->lang_lib->save_cats_edit_content($idioma,$item_id,$fields);
		$item_info=$this->Item->get_info($item_id);
		echo json_encode(array('success'=>'true','message'=>$this->lang->line('module_items_success_msj'),'id'=>$item_id));
	}
	/*Guarda el filtro para mostrar idioma de los form de contenido*/
	function filtro_idioma(){
		$this->lang_lib->set_content_idioma($this->input->post("idioma"));
		$idioma=$this->lang_lib->get_content_idioma();
		echo json_encode(array('idioma'=>$idioma,'post'=>$this->input->post("idioma")));
	}
	/*Obtiene el ultimo item registrado por la importación*/
	function get_items_import(){
		
		$config['per_page'] = '20';
		$offset=$this->uri->segment(3, 0);
		$data['filtro']=$this->config_lib->get_filtro_items();
		$cantidad =$this->Item->get_list_items_import_num();
		$manage_table=get_items_manage_table_data_rows($this->Item->get_items_import($cantidad),$this);
		echo json_encode( array('success'=>true,'data'=>$manage_table));
	}
	/*Manual*/
	function help(){
		$language=$this->lang_lib->get_idioma();
		$this->lang_lib->switch_to($language);
		$this->lang->load('manual', $language);
		$data['controller_name']=strtolower(get_class());;
		$data['contenido']=get_table_manual($data['controller_name']);
		$this->load->view("cc_views/".$data['controller_name']."/manual",$data);
	}
}
?>