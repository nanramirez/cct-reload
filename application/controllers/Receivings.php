<?php
require_once ("Secure_area.php");
class Receivings extends Secure_area
{
	function __construct(){
		parent::__construct('receivings');
		$this->load->library('receiving_lib');
		$this->load->model('Receiving');
		$this->load->model('Recurso');
		$this->load->model('Gasto');
		$this->load->helper('receivings');
	}
	/*Carga el manage view del modulo*/
	function index(){
		$data['notificacion_qty']=$this->Notificacion->get_all()->num_rows();
		$data['notificaciones']=notification_header($this->Notificacion->get_all());
		$data['version']=get_cc_version();
		$language=$this->lang_lib->get_idioma();
		$this->lang_lib->switch_to($language);
		$data['config_info']=$this->Appconfig->get_info();
		$data['controller_name']=strtolower(get_class());
		$filtro_uno=$this->config_lib->get_filtro_receivings();
		if(empty($filtro_uno)){
			$this->config_lib->set_filtro_receivings("receiving_time");
		}
		$data['estado_selected']=$this->config_lib->get_estado_receivings();
		$data['filtro']=$this->config_lib->get_filtro_receivings();
		$data['selected_month']=$this->config_lib->get_mes_show();
		$config['total_rows'] = $this->Receiving->count_all($data['selected_month']);
		$config['per_page'] = 20;
		$config['base_url'] = base_url()."index.php/".$data['controller_name'].'/page/';
		$config['first_link'] = base_url().$data['controller_name'];
		if($config['total_rows']>$config['per_page'])$this->pagination->initialize($config);
		$data['manage_table']=get_entradas_manage_table($this->Receiving->get_all($config['per_page'],$this->input->get('per_page'),$data['selected_month'],$data['estado_selected']),$data['estado_selected']);
		if(empty($total_undone)){
			$data['total_undone'] =0;
		}
		$data['estado']=array(
			'Receive'=>$this->lang->line("common_realizadas"),
			'Pendiente'=>$this->lang->line("common_pendientes"),
			'Return'=>$this->lang->line("common_devolution")
				  );
		if(empty($total_done)){
			$data['total_done'] =0;
		}
		$meses=array(
		  '0'  => 'Todos',
		  '01'  => 'Enero',
		  '02'  => 'Febrero',
		  '03'  => 'Marzo',
		  '04'  => 'Abril',
		  '05'  => 'Mayo',
		  '06'  => 'Junio',
		  '07'  => 'julio',
		  '08'  => 'Agosto',
		  '09'  => 'Septiembre',
		  '10'  => 'Octubre',
		  '11'  => 'Noviembre',
		  '12'  => 'Diciembre');
		$data['meses'] =$meses;
		$data['controller_name']=strtolower(get_class());
		$this->load->view("cc_views/receivings/manage",$data);
	}
	/*Carga lo mismo que el index pero con cierto offset para paginar la tabla del modulo*/
	function page($offset=0){
		$data['notificacion_qty']=$this->Notificacion->get_all()->num_rows();
		$data['notificaciones']=notification_header($this->Notificacion->get_all());
		$language=$this->lang_lib->get_idioma();
		$this->lang_lib->switch_to($language);
		$data['config_info']=$this->Appconfig->get_info();
		$data['controller_name']=strtolower(get_class());
		$filtro_uno=$this->config_lib->get_filtro_receivings();
		if(empty($filtro_uno)){
			$this->config_lib->set_filtro_receivings("receiving_time");
		}
		$data['estado_selected']=$this->config_lib->get_estado_receivings();
		$data['filtro']=$this->config_lib->get_filtro_receivings();
		$data['selected_month']=$this->config_lib->get_mes_show();
		$config['total_rows'] = $this->Receiving->count_all($data['selected_month']);
		$config['per_page'] = 20;
		$config['base_url'] = base_url()."index.php/".$data['controller_name'].'/page/';
		$config['first_link'] = base_url().$data['controller_name'];
		if($config['total_rows']>$config['per_page'])$this->pagination->initialize($config);
		$data['manage_table']=get_entradas_manage_table($this->Receiving->get_all($config['per_page'],$this->input->get('per_page'),$data['selected_month'],$data['estado_selected']),$data['estado_selected']);
		if(empty($total_undone)){
			$data['total_undone'] =0;
		}
		$data['estado']=array(
			'Receive'	=>$this->lang->line("module_receiving"),
			'Pendiente'  =>$this->lang->line("common_pending"),
			'Return'    =>$this->lang->line("common_devolution"));
		if(empty($total_done)){
			$data['total_done'] =0;
		}
		$data['meses'] =get_full_months();
		$data['controller_name']=strtolower(get_class());
		$this->load->view("cc_views/receivings/manage",$data);
      /**/  
	}
	/*Filtrado de busqueda*/
	function filtro(){
		if($this->input->post("meses")!= null)$this->config_lib->set_mes_show($this->input->post("meses"));
		if($this->input->post("estado")!= null)$this->config_lib->set_estado_receivings($this->input->post("estado"));
		$state=$this->config_lib->get_estado_receivings();
		$data['manage_table']=get_entradas_manage_table($this->Receiving->get_all('20', $this->input->get('per_page'),$this->input->post("meses"),$state),$state);
		echo json_encode(array('success'=>'true','manage_table'=>$data['manage_table'],'estado'=>$this->input->post("estado")));
	}
	/*Establece el filtro para las busquedas*/
	function set_filter_search(){
		$filter=$this->input->post("filter");
		$this->config_lib->set_filtro_receivings($filter);
		echo json_encode( array('success'=>$this->config_lib->get_filtro_receivings()));
	}
	/*Carga el view del panel del registro de entradas*/
	function register($receiving_id=-1){
		$language=$this->lang_lib->get_idioma();
		$this->lang_lib->switch_to($language);
		$data['info_receiving']=$this->Receiving->get_info($receiving_id);
		/*$data['func']='is_valid_card';*/
		$supplier_id=$this->receiving_lib->get_supplier();
		if($receiving_id!=-1){
			$this->receiving_lib->clear_all();
			/*var_dump($info_items);*/
			$this->receiving_lib->set_supplier($data['info_receiving']->supplier_id);
			foreach($this->Receiving->get_items_info($receiving_id) as $item){
				$this->receiving_lib->add_item($item['item_id'],$item['quantity_purchased'],$xchange=1,$discount=0,$item['item_cost_price']);
				/*$this->add($item['item_id']);*/
			}
			$info=$this->Supplier->get_info($data['info_receiving']->supplier_id);
			$data['supplier']=$data['info_receiving']->supplier_id."|".$info->first_name." ".$info->last_name."(".$info->company.")";
			$data['supplier_email']=$info->email;
			$supplier_id=-1;
		}
		$data['config_info']=$this->Appconfig->get_info();
		$person_info = $this->Employee->get_logged_in_employee_info();
		$data['cart']=$this->receiving_lib->get_cart();
		$data['mode']=$this->receiving_lib->get_mode();
		$data['total']=$this->receiving_lib->get_total();
		$data['items_module_allowed'] = $this->Employee->has_permission('items', $person_info->person_id);
		$data['comment'] = $this->receiving_lib->get_comment();
		$data['modos']=array(
			'Receive'		=>$this->lang->line("module_receiving"),
			'Pendiente'  =>$this->lang->line("common_pendin"),
			'Return'    =>$this->lang->line("common_devolution"));
		$recursos=array(-1=>$this->lang->line("common_seleccionar").' '.$this->lang->line("module_recurso"));
		foreach($this->Recurso->get_all()->result_array() as $row){
		$recursos[$row['recurso_id'].'|'.$row['recurso']] =$row['nombre'].'|'.to_currency($row['recurso']);
		}
		$data['recursos'] = $recursos;
		$payment_options=$this->Recurso->get_all_formas_pago();
		foreach($payment_options as $payment){
			$options[$payment["formas_pago_id"]]=$payment["nombre"];
		}
		$data['payment_options']=$options;
		$items_module_allowed= $this->Employee->has_permission('items',$this->Employee->get_logged_in_employee_info()->person_id);
		if(count($data['cart'])==0){
			$data['table_receiving']="<div class='cover_result_register_psr' id='warning_message_register'><div class='warning_message'>".$this->lang->line('module_items_null')."</div></div>";
		}
		else{
			$data['table_receiving']=get_receivings_panel_manage_table_data_rows($data['cart'],false);
			/*var_dump($data['cart']);*/
		}
		if($supplier_id!=-1){
			$info=$this->Supplier->get_info($supplier_id);
			$data['supplier']=$supplier_id."|".$info->first_name." ".$info->last_name."(".$info->company.")";
			$data['supplier_email']=$info->email;
		}
		$data['facturar']=$this->receiving_lib->get_factura_mode();
		$data['controller_name']=strtolower(get_class());
		$data['payments_cover_total'] = $this->_payments_cover_total();
		$data['payments']=$this->receiving_lib->get_payments();
		$this->load->view("cc_views/receivings/receiving",$data);
	}
	/*Guarda un monto parcial y su metodo de pago en una coockie*/
	function add_payment(){
		$language=$this->lang_lib->get_idioma();
		$this->lang_lib->switch_to($language);
		$data=array();
		$config_info=$this->Appconfig->get_info();
		$payment_id=$this->input->post('payment_type');
		$payment_type=$payment_id;
		$payment_type=$this->Recurso->get_info_nombre_metodo($payment_id);
		$recurso_id=$this->Recurso->get_info_recurso_id($payment_type);
		$recurso=$this->Recurso->get_info($recurso_id);
		$show_coin=$config_info["coin"];
		$payment_id=$payment_type;
		if($config_info["xchange_show"]=="1")$show_coin=$config_info["coin2"];
		$coin_payment=$recurso->coin;
		if($show_coin== $coin_payment){
			$payment_amount=$this->input->post('amount_tendered');
			$amount_tendered=$this->input->post('amount_tendered');
		}
		elseif($show_coin!= $coin_payment && $config_info["xchange_show"]=="1"){
			$payment_amount=$this->input->post('amount_tendered');
			$payment_amount*=$config_info['xchange_rate'];
			$amount_tendered=$this->input->post('amount_tendered');
		}
		elseif($show_coin!= $coin_payment && $config_info["xchange_show"]=="0"){
			$payment_amount=$this->input->post('amount_tendered');
			$payment_amount/=$config_info['xchange_rate'];
			$amount_tendered=$this->input->post('amount_tendered');
		}
		if($payment_amount!=null){
			if(!$this->receiving_lib->add_payment( $payment_type, $payment_amount, $payment_id,$amount_tendered)){
				$data['error']='Unable to Add Payment! Please try again!';
			}
		}
		$payments=$this->receiving_lib->get_payments();
		$total_payments="";
		$total_amount=0;
		$delete=array("01","02","03","04");
		foreach($payments as $payment){
			$payment["payment_type"]=str_replace($delete, "", $payment["payment_type"]);
			$total_payments.='<span id="text_monto">'.to_currency($payment['payment_amount']).'</span><span id="text_tipo">'.$payment_type.'</span><a id="'.$payment_type.'" class="pagos"></a>';
			$total_amount+=$payment['payment_amount'];
		}
		$total=$this->receiving_lib->get_total();
		$config_info=$this->Appconfig->get_info();
		echo json_encode(array("payments"=>$total_payments,"total_amount"=>$total_amount,"total"=>$total,'coin_payment'=>$payment_type,'print_after_sale'=>$config_info['print_receipt']));
	}
	/*Borra un monto parcial y su metodo de pago de la coockie*/
	function delete_payment(){
		$payment_id=$this->input->post("payment_id");
		$this->receiving_lib->delete_payment($payment_id);
		$payments=$this->receiving_lib->get_payments();
		$total=$this->receiving_lib->get_total();
		$total_payments="";
		$total_amount=0;
		if(isset($payments)){
			foreach($payments as $payment){
				$total_payments.='<span>'.to_currency($payment['payment_amount']).'</span><span>'.$payment['payment_type'].'</span><a id="'.$payment['payment_type'].'" class="pagos"></a>';
				$total_amount+=$payment['payment_amount'];
			}
		}
		else{
			$this->receiving_lib->empty_payments();
		}
		echo json_encode(array("payments"=>$total_payments,"total_amount"=>$total_amount,"total"=>$total));
	}
	/*Guarda el valor del input de comentarios en una coockie*/
	function set_comment() {
 	  $this->receiving_lib->set_comment($this->input->post('comment'));
	  $comment=$this->receiving_lib->get_comment();
	  echo json_encode(array("comment"=>$comment));
	}
	/*Nos indica si los pagos superan el monto total de la entrada*/
	function _payments_cover_total(){
		$total_payments = 0;
		foreach($this->receiving_lib->get_payments() as $payment){
			$total_payments += $payment['payment_amount'];
		}
		/* Redondea el float*/
		if ( ( $this->receiving_lib->get_mode() == 'sale' ) && ( ( to_currency_no_money( $this->receiving_lib->get_total() ) - $total_payments ) > 1e-6 ) ){
			return false;
		}
		return true;
	}
	/*Nos muestra los detalles del provedor de la entrada*/
	function supplier_details($receiving_id=-1){
		$language=$this->lang_lib->get_idioma();
		$this->lang_lib->switch_to($language);
		$info=$this->Receiving->get_info($receiving_id);
		$info_supplier=$this->Supplier->get_info($info->supplier_id);
		$data['supplier']=$info_supplier->company.' | '.$info_supplier->first_name.' '.$info_supplier->last_name;
		$data['dir']=$info_supplier->address_1;
		$data['city']=$info_supplier->city;
		$data['zip']=$info_supplier->zip;
		$data['country']=$info_supplier->country;
		$data['email']=$info_supplier->email;
		$data['tel']=$info_supplier->phone_number;
		$this->load->view("cc_views/receivings/detail_supplier",$data);
	}
	/*Muestra un form con los detalles de los item de la entrada */
	function items_details($receiving_id=-1){
		$data['config_info']=$this->Appconfig->get_info();
		$language=$this->lang_lib->get_idioma();
		$this->lang_lib->switch_to($language);
		$info=$this->Receiving->get_info($receiving_id);
		$data['info']=$info;
		$data['facturada']=$this->lang->line("common_sinfacturar");
		if($info->facturada==1)$data['facturada']=$this->lang->line("common_facturada");
		$supplier_info=$this->Supplier->get_info($info->supplier_id);
		$data['supplier_info']=$supplier_info;
		$data['supplier']=$supplier_info->first_name.' '.$supplier_info->last_name;
		$data['company']=$supplier_info->company;
		$employee_info=$this->Employee->get_info($info->employee_id);
		$data['employee']=$employee_info->first_name.' '.$employee_info->last_name;
		$data['items']=$this->Receiving->get_items_info($receiving_id);
		$data['id']=$receiving_id;
		if($receiving_id==-1)
		{$data['id']="menosuno";}
		$data['image_properties'] = array(
		'src'   => base_url().'images/cc_images/receivings/'.$data['id'].'/1.jpg',
		'alt'   => 'No hay imagen para mostrar',
		'class' => 'post_images',
		'width' => '260',
		'height'=> '260',
		'title' => 'That was quite a night',
		'rel'   => 'lightbox'
		);		
		$data['receiving_id']=$receiving_id;
		$data['total']=$info->total;
		$this->load->view("cc_views/receivings/items_details",$data);
		/**/
	}
	/*Establece el modo del registro de la entrada en la cookie*/
	function change_mode(){
		$mode = $this->input->post("mode");
		$this->receiving_lib->set_mode($mode);
	}
	/*Agrega un item a la linea de registro de la entradas*/
	function add(){
		$msj="";
		$data=array();
		$mode = $this->receiving_lib->get_mode();
		$item_id_or_number_or_item_kit_or_receipt = $this->input->post("item");
		$quantity = 1;
		if($this->receiving_lib->is_valid_receipt($item_id_or_number_or_item_kit_or_receipt) && $mode=='return'){
			$this->receiving_lib->return_entire_receiving($item_id_or_number_or_item_kit_or_receipt);
		}
		elseif($this->receiving_lib->is_valid_item_kit($item_id_or_number_or_item_kit_or_receipt)){
			$this->receiving_lib->add_item_kit($item_id_or_number_or_item_kit_or_receipt);
		}
		elseif(!$this->receiving_lib->add_item($item_id_or_number_or_item_kit_or_receipt,$quantity)){
			$data['error']=$this->lang->line('recvs_unable_to_add_item');
		}
		$total=$this->receiving_lib->get_total();
		$cart = $this->receiving_lib->get_cart();
		$table_receiving=get_receivings_panel_manage_table_data_rows($cart);
		echo json_encode(array("table_receiving"=>$table_receiving,"total"=>$total,"msj"=>$msj));
	}
	/*Obtiene una fila actualizada del manage table*/
	function get_row(){
		$estado=$this->config_lib->get_estado_receivings();
		$receiving_id = $this->input->post("row_id");
		$data_row=get_entradas_data_row($this->Receiving->get_info($receiving_id),$estado);
		echo $data_row;
	}
	/*Edita la cantidad en la linea de un item agregado*/
	function edit_item($line){
		$config_info=$this->Appconfig->get_info();
		$coin=$config_info["coin"];
		if($config_info["xchange_show"]=="1")$coin=$config_info["coin2"];
		$language=$this->lang_lib->get_idioma();
		$this->lang_lib->switch_to($language);
		$msj="";
		$data= array();
		$quantity = $this->input->post("quantity");
		$price = $this->input->post("price");
		$this->receiving_lib->edit_item($line,$quantity,$price);
		$cart = $this->receiving_lib->get_cart();
        $table_receiving=get_receivings_panel_manage_table_data_rows($cart);
        $total=$this->receiving_lib->get_total();
        echo json_encode(array("table_receiving"=>$table_receiving,"total"=>$total,"msj"=>$msj));
	}
	/*Borra la linea de un item agregado en guardado en la cookie usada para registro de las entradas*/
	function delete_item($line){
		$this->receiving_lib->delete_item($line);
		$total=$this->receiving_lib->get_total();
		echo json_encode(array("total"=>$total));
	}
	/*Guarda el registro de las entradas en la BD */
	function save($receiving_id=-1){
		$config_info=$this->Appconfig->get_info();
		$language=$this->lang_lib->get_idioma();
		$this->lang_lib->switch_to($language);
		$facturar=$this->receiving_lib->get_factura_mode();
		$items=$this->receiving_lib->get_cart();
		$total=$this->receiving_lib->get_total();
		$data['receipt_title']=$this->lang->line('recvs_receipt');
		$data['transaction_time']= date('m/d/Y h:i:s a');
		$supplier_id=$this->receiving_lib->get_supplier();
		$employee_id=$this->Employee->get_logged_in_employee_info()->person_id;
		$comment =  $this->receiving_lib->get_comment();
		/*$recurso_id = $this->input->post('recurso');*/
		$modo = $this->input->post('modos');
		$emp_info=$this->Employee->get_info($employee_id);
		$data['payments']=$this->receiving_lib->get_payments();
		$payment_types='';
		foreach($data['payments'] as $payment_id=>$payment){
			$payment_types.=$payment['payment_type'].': '.to_currency($payment['payment_amount']).'<br>';
		}
		$notificar=$this->input->post('notificar')==null ? 0:$this->input->post('notificar');
		$data['employee']=$emp_info->first_name.' '.$emp_info->last_name;
		$suppl_info=$this->Supplier->get_info($supplier_id);
		$data['supplier']='('.$suppl_info->company.')'.$suppl_info->first_name.' '.$suppl_info->last_name;
		$coin=$config_info["coin"];
		if($config_info['xchange_show']=="1")$coin=$config_info["coin2"];
		$tax=0;
		if($facturar==1 && $config_info['default_tax_1_rate']!=0){
			$tax=($total)-($total/$config_info['default_tax_1_rate']);
		}
		$receivings_data = array(
		'supplier_id'=> $this->Supplier->exists($supplier_id) ? $supplier_id : null,
		'employee_id'=>$employee_id,
		'payment_type'=>$payment_types,
		'coin'=>$coin,
		'comment'=>$comment ? $comment : " ",
		'total'=>$total,
		'modo'=>$modo,
		'facturada'=>$facturar,
		'tax'=>number_format($tax,2)
		);
		if($this->Receiving->save($items,$receivings_data)){	
			$folio=$this->lang->line("module_receivings_id").': '.$receivings_data["receiving_id"].' '.$data['supplier'].' '.$comment;
			if($modo=="Receive")
			{
				foreach($data['payments'] as $payment_id=>$payment)
				{
					$recurso_id=$this->Recurso->get_info_recurso_id($payment["payment_type"]);
					$recurso=$this->Recurso->get_info($recurso_id);
					$gastos_data = array(
					"fecha"=>date('Y-m-d H:i:s'),
					"concepto"=>$folio,
					"category"=>$this->lang->line('module_receivings'),
					'coin'=>$recurso->coin,
					"estado"=>'Hecho',
					'deducible'=>$facturar,
					"cantidad"=>$payment['payment_amount'],
					'recurso_id'=>$recurso_id);
					if($this->Gasto->save($gastos_data)){
						$operation_data = array(
						"recurso_id"=>$recurso_id,
						"monto"=>$gastos_data['cantidad'],
						"coin"=>$recurso->coin,
						"concepto"=>"#".$gastos_data['gasto_id']."-".$folio,
						"saldo_anterior"=>$recurso->recurso,
						"saldo_restante"=>($recurso->recurso)-$gastos_data['cantidad']
						);
						if($this->Recurso->save_operation($operation_data)){
							$this->Recurso->rest_recurso($gastos_data['cantidad'],$gastos_data['recurso_id'],$recurso->coin);
							$notificacion_data=array(
							'time' => date('Y-m-d H:i:s'),
							'notificacion'=>$this->lang->line("module_receiving_new_notify")." ".$folio,
							'modulo'=>strtolower(get_class()),
							'prioridad'=>"medium"
							);
							$this->Notificacion->make_new($notificacion_data);
						}
					}
				}
			}
			elseif($modo=="Pendiente"){
					$notificacion_data=array(
					'time' => date('Y-m-d H:i:s'),
					'notificacion'=>$this->lang->line("module_receiving_pending_notify")." ".$folio,
					'modulo'=>strtolower(get_class()),
					'prioridad'=>"medium"
					);
					$this->Notificacion->make_new($notificacion_data);
			}
			if($receiving_id==-1)
			{
				$image_folder='./images/cc_images/users/receivings/'.$receivings_data["receiving_id"].'/';
				$image_default_file='./images/cc_images/users/receivings/menosuno/main.jpg';
				if (!file_exists($image_folder)&& file_exists($image_default_file)){
				mkdir($image_folder,0777,true);
				chmod($image_folder,0777);
				copy($image_default_file, $image_folder."/main.jpg");
				}
				echo json_encode(array('success'=>'true','message'=>$this->lang->line('module_receivings_success_msj'),'receiving_id'=>$receivings_data["receiving_id"]));
				/*if($notificar==1)
				{
					$this->load->helper('pdf_helper');
					$info=$this->Receiving->get_info($receiving_id);
					$items=$this->Receiving->get_items_info($receiving_id);
					receiving_pdf($info,$items,$total);
				//$this->notify($pedido_data['receiving_id']);
				}*/
			}
			else
			{
				echo json_encode(array('success'=>'true','message'=>$this->lang->line('module_receivings_update_msj'),'receiving_id'=>$receiving_id));
			}
			/**/
		}
		else{
			echo json_encode(array('error'=>true,'message'=>$this->lang->line('common_form_error')));
		}
		$this->receiving_lib->clear_all();
	}
	/*Borra entradas que estan péndientes en la BD*/
	function delete(){
		$entradas_to_delete=$this->input->post("ids")!=false ? $this->input->post("ids"):array();
		if($this->Receiving->delete_list($entradas_to_delete)){
			echo json_encode(array("success"=>"true","message"=>$this->lang->line("common_delete_succes")." ".
			count($entradas_to_delete)." ".$this->lang->line("module_receiving")."(s)"));
		}
		else{
			echo json_encode(array('success'=>"false",'message'=>$this->lang->line('common_form_error')));
		}
				/**/
	}
	/*Carga el view de los detalles de la entrada*/
	function receipt($receiving_id){
		$language=$this->lang_lib->get_idioma();
		$this->lang_lib->switch_to($language);
		$receiving_info = $this->Receiving->get_info($receiving_id)->row_array();
		$this->receiving_lib->copy_entire_receiving($receiving_id);
		$data['cart']=$this->receiving_lib->get_cart();
		$data['total']=$this->receiving_lib->get_total();
		$data['receipt_title']=$this->lang->line('recvs_receipt');
		$data['transaction_time']= date('m/d/Y h:i:s a', strtotime($receiving_info['receiving_time']));
		$supplier_id=$this->receiving_lib->get_supplier();
		$emp_info=$this->Employee->get_info($receiving_info['employee_id']);
		$data['payment_type']=$receiving_info['payment_type'];
		$data['employee']=$emp_info->first_name.' '.$emp_info->last_name;
		if($supplier_id!=-1){
			$supplier_info=$this->Supplier->get_info($supplier_id);
			$data['supplier']=$supplier_info->first_name.' '.$supplier_info->last_name;
		}
		$data['receiving_id']='RECV '.$receiving_id;
		$this->load->view("cc_views/receivings/receipt",$data);
		$this->receiving_lib->clear_all();
	}
	/*Borra todo el contenido de las cookies de la entrada en curso*/
	function cancel_receiving(){
    	$this->receiving_lib->clear_all();
		echo "<div class='cover_result_register_psr' id='warning_message_register'><div class='warning_message'>".$this->lang->line('module_sales_msj')."</div></div>";
    }
	/*Sugiere las busquedas en el manage table*/
	function suggest(){
		$filtro=$this->config_lib->get_filtro_receivings();
		$valores=$this->input->post("q");
		$suggestions = $this->Receiving->get_search_suggestions($valores,'',$filtro);
		echo json_encode($suggestions);
	}
	/*Busca el Receiving mediante un filtro específico de busqueda*/
	function search(){
		$search=$this->input->post('search');
		$data_rows=get_entradas_manage_table_data_rows($this->Receiving->search($search),$this);
		echo json_encode(array("data_rows"=>$data_rows));
	}
	/*Guarda el provedor seleccionado en el panel de registro*/
	function select_supplier(){	
		$supplier_id = $this->input->post("supplier");
		$this->receiving_lib->set_supplier($supplier_id);
		$info=$this->Supplier->get_info($supplier_id);
		$supplier=$info->first_name." ".$info->last_name;
		echo json_encode(array("supplier"=>$supplier));
	}
	/*Borra el provedor seleccionado en el panel de registro*/
	function remove_supplier(){
		$this->receiving_lib->remove_supplier();
	}
	/*Regresa los pagos via ajax guardados en la cookie*/
	function get_payments_ajax(){
		$payments=$this->receiving_lib->get_payments();
		$total_payments="";
		$total_amount=0;
		foreach($payments as $payment){
			$total_payments.='<span id="text_monto">'.to_currency($payment['payment_amount']).'</span><span id="text_tipo">'.$payment['payment_type'].'</span><a id="'.$payment['payment_type'].'" class="pagos"></a>';
			$total_amount+=$payment['payment_amount'];
		}
		$total=$this->receiving_lib->get_total();
		echo json_encode(array("payments"=>$total_payments,"total_amount"=>$total_amount,"total"=>$total));
	}
	/*Regresa el total via ajax guardado en la cookie*/
	function get_total(){
		$total = 0;
		$config_info=$this->CI->Appconfig->get_info();
		$iva=0;
		$tax=floatval($config_info['default_tax_1_rate']);
		$tax=1+($tax/100);
		foreach($this->get_cart() as $item){
            $total+=($item['price']*$item['quantity']-$item['price']*$item['quantity']*$item['discount']/100);
		}
		foreach($this->get_taxes() as $tax){
			$total+=$tax;
		}
		return to_currency_no_money($total);
	}
	/*Establece el modo deducible de una entrada en la cookie*/
	function set_factura_mode($mode){
		$this->receiving_lib->set_factura_mode($mode);
	}
	/*Manual*/
	function help(){
		$language=$this->lang_lib->get_idioma();
		$this->lang_lib->switch_to($language);
		$this->lang->load('manual', $language);
		$data['controller_name']=strtolower(get_class());;
		$data['contenido']=get_table_manual($data['controller_name']);
		$this->load->view("cc_views/".$data['controller_name']."/manual",$data);
	}
}
?>