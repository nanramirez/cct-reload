<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| Hooks
| -------------------------------------------------------------------------
| This file lets you define "hooks" to extend CI without hacking the core
| files.  Please see the user guide for info:
|
|	https://codeigniter.com/user_guide/general/hooks.html
|
*/
$hook['pre_controller'] = array(
        'class'    => 'url',
        'function' => 'url_title',
        'filename' => 'url_helper.php',
        'filepath' => 'hooks',
        'params'   => array($str=NULL, $separator = '-', $lowercase = FALSE)
);
