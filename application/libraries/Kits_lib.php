<?php
class Kits_lib
{
	var $CI;
  	function __construct()
	{
		$this->CI =& get_instance();
	}
	//	
	function get_items_in_kit()
	{
		if(!$this->CI->session->userdata('items_in_kit'))
			$this->set_items_in_kit(array());
		return $this->CI->session->userdata('items_in_kit');
	}
	//
	function set_items_in_kit($items_in_kit)
	{
		$this->CI->session->set_userdata('items_in_kit',$items_in_kit);
	}
	//
	function add_item_to_kit($item_id,$quantity=1)
	{
		 //Obtiene los items de la cockie que muestra los datos en el form
		$items = $this->get_items_in_kit();
		$items_to_save=array();
		$info=$this->CI->Item->get_info($item_id);
		if(is_string ( $quantity ))
		{
			$quantity = (double)filter_var($quantity, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
		}
		$quantity=number_format($quantity, 2, '.', '');
		$sihayitem=FALSE;
        $maxkey=0;                       //Highest key so far
        $itemalreadyinsale=FALSE;        //We did not find the item yet.
		$insertkey=0;                    //Variable para agregar una linea
		$updatekey=0;                    //Variable para actualizar(quantity)
		foreach ($items as $item)
		{
            //Cada ciclo se lo agregamos a maxkey comenzando con el valor 0 la primera vez.
            //Tambien guardamos las linea que coincide con la linea de item 
			if($maxkey <= $item['line'])
			{
				$maxkey = $item['line'];
			}
			 //Si el item ya esta actualizamos su linea($updatekey).
			if($item['item_id']==$item_id)
			{
				$itemalreadyinsale=TRUE;
				$updatekey=$item['line'];
			}
		}
		$insertkey=$maxkey+1;
		$item = array(($insertkey)=>
		array(
			'item_id'=>$item_id,
			'line'=>$insertkey,
			'name'=>$info->name,
			'quantity'=>$quantity
			)
		);
		if($itemalreadyinsale==true && ($info->is_serialized ==0))
		{
			$items[$updatekey]['quantity']+=$quantity;
		}
		else	
		{
			//echo'Nuevo en carro';
			$items+=$item;
		}		
		$this->set_items_in_kit($items);
		return true;
		/*
		//var_dump($items);
			if(count($items)==0)
			{
			//var_dump($items);
				$items[$item_id.'|'.$name]=1;
				$items_to_save+=$items;
			}
			else
			{
			foreach ($items as $key=>$item)
			{
				//var_dump($key);
				//var_dump($item_id);
				if($key==$item_id.'|'.$name)
				{
					//var_dump($key);
					$items[$key] +=1;				
					$items_to_save+=$items;
				}
				else
				{
					$items[$item_id.'|'.$name] =1;
					$items_to_save+=$items;						
				}
			}
			}
		$this->set_items_in_kit($items_to_save);*/
	}
	//
	function delete_items_in_kit()
	{
		$this->CI->session->unset_userdata('items_in_kit');
	}
	//
	function delete_item($line)
	{
			$items = $this->get_items_in_kit();
			foreach($items as $key=> $single_item)
			{
				if($single_item['item_id']==$line)
				{	
					unset($items[$key]);
				}
			}	
			//$this->set_data_save($items);
		//}	
			$this->set_items_in_kit($items);
			//$items = $this->get_items_in_kit();
			//var_dump($items);
	}
	//
	function edit_item($item_id,$quantity)
	{
			$items = $this->get_items_in_kit();
			//Actualizo la cantidad en la cokie que muestra datos en el register
			foreach($items as $key=> $single_item)
			{
				if($single_item['item_id']==$item_id)
				{	//echo'Se borro item';	
					//unset($items[$single_item['item_id']]);
					//var_dump($single_item);
					//unset($items[$key]);
					$items[$key]['quantity'] = $quantity;
				}
				//Busco, actualizo y guardo la cantidad equivalente de los items que coincidan con el kit				
			}
		$this->set_items_in_kit($items);	
	}
}
?>