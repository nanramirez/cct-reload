<?php
class Config_lib
{
	var $CI;
  	function __construct(){
		$this->CI =& get_instance();
	}
	/*Obtiene el filtro de busquedas del modulo empleados de su cookie*/
	function get_filtro_employees(){
		if(!$this->CI->session->userdata('filtro_employees'))$this->set_filtro_employees(array());
		return $this->CI->session->userdata('filtro_employees');
	}
	/*Guarda el filtro de busquedas del modulo empleados en su cookie*/
	function set_filtro_employees($filtro_data){
		$this->CI->session->set_userdata('filtro_employees',$filtro_data);
	}
	/*Obtiene el filtro de busquedas del modulo suppliers de su cookie*/
	function get_filtro_suppliers(){
		if(!$this->CI->session->userdata('filtro_suppliers'))$this->set_filtro_suppliers(array());
		return $this->CI->session->userdata('filtro_suppliers');
	}
	/*Guarda el filtro de busquedas del modulo suppliers en su cookie*/
	function set_filtro_suppliers($filtro_data){
		$this->CI->session->set_userdata('filtro_suppliers',$filtro_data);
	}
	/*Obtiene el filtro de busquedas del modulo items de su cookie*/
	function get_filtro_items(){
		if(!$this->CI->session->userdata('filtro_items'))$this->set_filtro_items(array());
		return $this->CI->session->userdata('filtro_items');
	}
	/*Guarda el filtro de busquedas del modulo items en su cookie*/
	function set_filtro_items($filtro_data){
		$this->CI->session->set_userdata('filtro_items',$filtro_data);
	}
	/*Obtiene la configuracion del tipo de código de barra*/
	function get_barcode_config(){
		if(!$this->CI->session->userdata('barcode_config'))$this->set_barcode_config(array());
		return $this->CI->session->userdata('barcode_config');
	}
	/*Guarda la configuracion del tipo de código de barra*/
	function set_barcode_config($config){
		$this->CI->session->set_userdata('barcode_config',$config);
	}
	/*Obtiene el filtro de busquedas del modulo recursos de su cookie*/
	function get_filtro_recursos(){
		if(!$this->CI->session->userdata('filtro_recursos'))$this->set_filtro_recursos(array());
		return $this->CI->session->userdata('filtro_recursos');
	}
	/*Guarda el filtro de busquedas del modulo recursos en su cookie*/
	function set_filtro_recursos($filtro_data){
		$this->CI->session->set_userdata('filtro_recursos',$filtro_data);
	}
	/*Obtiene el filtro del mes seleccionado de su cookie*/
	function get_mes_show(){
		if(!$this->CI->session->userdata('mes_show'))$this->set_mes_show(0);
		return $this->CI->session->userdata('mes_show');
	}
	/*Guarda el filtro del mes seleccionado en su cookie*/
	function set_mes_show($filtro_data){
		$this->CI->session->set_userdata('mes_show',$filtro_data);
	}
	/*Obtiene el filtro de busquedas del modulo gastos de su cookie*/
	function get_filtro_gastos(){
		if(!$this->CI->session->userdata('filtro_gastos'))$this->set_filtro_gastos(array());
		return $this->CI->session->userdata('filtro_gastos');
	}
	/*Guarda el filtro de busquedas del modulo gastos en su cookie*/
	function set_filtro_gastos($filtro_data){
		$this->CI->session->set_userdata('filtro_gastos',$filtro_data);
	}
	/*Obtiene el filtro del estado del modulo gastos*/
	function get_estado_gastos(){
		if(!$this->CI->session->userdata('estado_gastos'))$this->set_estado_gastos('Pendiente');
		return $this->CI->session->userdata('estado_gastos');
	}
	/*Guarda el filtro del estado del modulo gastos*/
	function set_estado_gastos($filtro_data){
		$this->CI->session->set_userdata('estado_gastos',$filtro_data);
	}
	/*Obtiene el filtro de busquedas del modulo receivings de su cookie*/
	function get_filtro_receivings(){
		if(!$this->CI->session->userdata('filtro_receivings'))$this->set_filtro_receivings(array());
		return $this->CI->session->userdata('filtro_receivings');
	}
	/*Guarda el filtro de busquedas del modulo receivings en su cookie*/
	function set_filtro_receivings($filtro_data){
		$this->CI->session->set_userdata('filtro_receivings',$filtro_data);
	}
	/*Obtiene el filtro del estado del modulo receivings*/
	function get_estado_receivings(){
		if(!$this->CI->session->userdata('estado_receivings'))$this->set_estado_receivings('Receive');
		return $this->CI->session->userdata('estado_receivings');
	}
	/*Guarda el filtro del estado del modulo receivings*/
	function set_estado_receivings($filtro_data){
		$this->CI->session->set_userdata('estado_receivings',$filtro_data);
	}
	//
	function get_receivings_show()
	{
		if(!$this->CI->session->userdata('receivings_show'))
			$this->set_receivings_show(array());
		return $this->CI->session->userdata('receivings_show');
	}
	//
	function set_receivings_show($receivings_show)
	{
		$this->CI->session->set_userdata('receivings_show',$receivings_show);
	}
	//
	function get_filtro_item_kits()
	{
		if(!$this->CI->session->userdata('filtro_item_kits'))
			$this->set_filtro_item_kits(array());
		return $this->CI->session->userdata('filtro_item_kits');
	}
	//
	function set_filtro_item_kits($filtro_data)
	{
		$this->CI->session->set_userdata('filtro_item_kits',$filtro_data);
	}
	
}
?>