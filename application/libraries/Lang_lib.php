<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Lang_lib  extends CI_Lang
{
	var $CI;
  	function __construct()
	{
		$this->CI =& get_instance();
	}
	/*Obtiene el idioma actual del CC*/
	function get_idioma()
	{
		$info=$this->CI->Appconfig->get_info();
		if(!$this->CI->session->userdata('idioma'))$this->set_idioma($info['language']);
		return $this->CI->session->userdata('idioma');
	}
	/*Cambia el idioma del CC*/
	function set_idioma($idioma)
	{
		$this->CI->session->set_userdata('idioma',$idioma);
	}
	/*comentarios*/
	function switch_to($idiom)
    {
        $CI =& get_instance();
		$files=array('common','module');
		$CI->lang->load($files,$idiom);
		
    }
	/*Cambia el idioma para editar el contenido en el modulo de paginas, items y kits*/
	function set_content_idioma($idioma)
	{
		$this->CI->session->set_userdata('content_idioma',$idioma);
	}
	/*Obtiene el idioma para editar el contenido en el modulo de paginas, items y kits*/
	function get_content_idioma()
	{
		if(!$this->CI->session->userdata('content_idioma'))$this->set_content_idioma($this->get_idioma());
		return $this->CI->session->userdata('content_idioma');
	}
	/**/
	function items_file_new($idioma,$id)
	{
		/*//////Generacion de archivo de lengua*/
		$this->CI->load->helper('file');
		$config_info=$this->CI->Appconfig->get_info();
		$archivo_lang =  read_file('./application/language/'.$idioma.'/items_lang.php');
		$caracteres = array('<?php','?>');
$archivo_lang = str_replace($caracteres ,"", $archivo_lang);
		$lineas=explode(";", $archivo_lang);
		$archivo_lang='';
$newdata ='$lang["item_'.$id.'_MetaLang"]="'.$idioma.'";
$lang["item_'.$id.'_MetaUrl"]="'.str_replace('"' ,"'", $this->CI->Item->get_info($id)->name)." ".$idioma.'";
$lang["item_'.$id.'_MetaType"]="";
$lang["item_'.$id.'_MetaTitle"]="'.str_replace('"' ,"'", $this->CI->Item->get_info($id)->name).'";
$lang["item_'.$id.'_MetaDescription"]="'.str_replace('"' ,"'", $this->CI->Item->get_info($id)->description).'";
$lang["item_'.$id.'_MetaAuthor"]="'.$config_info["company"].'";
$lang["item_'.$id.'_Keywords"]="";
$lang["item_'.$id.'_Name"]="'.str_replace('"' ,"'", $this->CI->Item->get_info($id)->name).'";
$lang["item_'.$id.'_Modelo"]="'.str_replace('"' ,"'", $this->CI->Item->get_info($id)->modelo).'";
$lang["item_'.$id.'_Description"]="'.str_replace('"' ,"'", $this->CI->Item->get_info($id)->description).'";
';
$a=array();
		foreach($lineas as $key=>$linea)
		{
				/*$piece_linea=explode("_", $linea[$key]);*/
				$a[]=$lineas[$key];
		}
		$str=implode(";",$a);
		$archivo_lang.=$str.$newdata;
			/*$archivo_lang.=$newdata;*/
$archivo_lang='<?php'.$archivo_lang.'?>';
		if ( ! write_file('./application/language/'.$idioma.'/items_lang.php', $archivo_lang))
		{
			/* echo 'Unable to write the file';*/
		}
		else
		{
			/* echo 'File written!'; */
		}
}
	/**/
	function get_items_content_file($idioma,$id)
	{
		$this->CI->load->helper('file');
		$archivo_lang =  read_file('./application/language/'.$idioma.'/items_lang.php');
		$caracteres = array('&lt;');
		$archivo_lang=str_replace($caracteres ,"<", $archivo_lang);
		$lineas=explode(";",$archivo_lang);
		$a=array();
		foreach($lineas as $key=>$linea)
		{
			$lin=explode("_", $lineas[$key]);
			/*ejemp:$lang["contenido_1_ciudades"]="Quintana Roo"; array($lang["contenido, 1, ciudades"]="Quintana Roo";);*/
			$line=explode("]=", $lineas[$key]);
		if(isset($lin[1]))
			{
				$indice=explode('"', $lineas[$key]);
				/*//ejemp:$lang["contenido_1_ciudades"]="Quintana Roo"; array($lang[, contenido_1_ciudades, ]="Quintana Roo";); */
				$slashes = array('\"',"\'");
				$field=str_replace($slashes,"'",$line[1]);
				$txt_linea=explode('"', $field);
				/*echo $lin[1];*/
					
				$txt_linea[1]=str_replace( '<br />',"\r", $txt_linea[1] );
				if($lin[1]==$id)
				{
					$a[$indice[1]]=''.$txt_linea[1].'';
				}
			}
		}
		return $a;
	}
	/**/
	function save_items_edit_content($idioma,$id,$update_content)
	{
		$this->CI->load->helper('file');
		$archivo_lang =  read_file('./application/language/'.$idioma.'/items_lang.php');
		if ( ! write_file('./application/language/'.$idioma.'/items_lang_backup.php', $archivo_lang))
		{
			/* echo 'Unable to write the file';*/
		}
		 $caracteres = array('?>','<?php');
		$archivo_lang= str_replace($caracteres ,"", $archivo_lang);
		$lineas=explode(";", $archivo_lang);
		$changes=array();
		/*var_dump($lineas);*/
		$archivo_lang_edit = "";
		foreach($lineas as $key=>$linea)
		{
			/*var_dump($linea);*/
				$indice=explode("=", $linea);
				$index=explode('"', $indice[0]);
				/*$chars = array('[','"',']','$lang',"'");*/
				/*$index=str_replace($chars,"",$indice[0]);*/
				/*var_dump($index[1]);*/
			if(isset($index[1]))
			{
				if(isset($update_content[$index[1]]))
				{
					/*$value=trim($update_content[$key]);
					$notallow_caracteres = array("'", '"', ";", ".'", '."', "<", ">", "''",'<?php','?>','/>','</','""');
					$value = str_replace($notallow_caracteres ,"", $value);
					$value=str_replace( '<br />', "\r", $value);*/
					/*$changes[$index[1]]=$update_content[$index[1]];*/
					$field=trim($update_content[$index[1]]);
					$field= str_replace('"' ,"'", $field);
					$field= nl2br($field);
					$field=addslashes($field);
					$archivo_lang_edit.='
$lang["'.$index[1].'"]="'.$field.'";';
				}
				else
				{
					$archivo_lang_edit.=$linea.';';
					/*var_dump($linea);*/
				}
			}
		}
		$archivo_lang_edit="<?php".
		$archivo_lang_edit."
?>";
		/*var_dump($changes);*/
	if ( ! write_file('./application/language/'.$idioma.'/items_lang.php', $archivo_lang_edit))
		{
			return 'Unable to write the file';
		}
		else
		{
			return 'File written!';
		}
	}
	/**/
	function content_cat_file_new($idioma,$category)
	{
		$this->CI->load->helper('file');
		$archivo_lang =  read_file('./application/language/'.$idioma.'/cats_lang.php');
		$caracteres = array('<?php','?>');
		$archivo_lang = str_replace($caracteres ,"", $archivo_lang);
		$lineas=explode(";", $archivo_lang);
		$human_category=url_title($category);
		$archivo_lang='';
$newdata ='$lang["cats_'.$human_category.'"]="'.str_replace('"' ,"'", $category)." ".$idioma.'";
';
$a=array();
		foreach($lineas as $key=>$linea)
		{
				/*$piece_linea=explode("_", $linea[$key]);*/
				$a[]=$lineas[$key];
		}
		$str=implode(";",$a);
		$archivo_lang.=$str.$newdata;
			/*$archivo_lang.=$newdata;*/
$archivo_lang='<?php'.$archivo_lang.'?>';
		if ( ! write_file('./application/language/'.$idioma.'/cats_lang.php', $archivo_lang))
		{
			/* echo 'Unable to write the file'; */
		}
		else
		{
			/* echo 'File written!'; */
		}
	}
	/**/
	function get_cats_content_file($idioma,$category)
	{
		$this->CI->load->helper('file');
		$archivo_lang =  read_file('./application/language/'.$idioma.'/cats_lang.php');
		$caracteres = array('<?php','?>');
		$chars = array('"',"'");
		$caracteres=str_replace($caracteres ,"", $caracteres);
		$lineas=explode(";", $archivo_lang);
		$a=array();
		;
		foreach($lineas as $key=>$linea)
		{
			$lin=explode("_", $lineas[$key]);
			/*ejemp:$lang["contenido_1_ciudades"]="Quintana Roo"; array($lang["contenido, 1, ciudades"]="Quintana Roo";);*/
			$line=explode("=", $lineas[$key]);
		if(isset($lin[1]))
			{
				$lin1=explode("]=", $lin[1]);
				$lin0=str_replace($chars ,"", $lin1[0]);
				$indice=explode('"', $lineas[$key]);
				/*//ejemp:$lang["contenido_1_ciudades"]="Quintana Roo"; array($lang[, contenido_1_ciudades, ]="Quintana Roo";);*/
				$slashes = array('\"',"\'");
				$field=str_replace($slashes,"'",$line[1]);
				$txt_linea=explode('"', $field);
				/*echo $lin[1];*/
					$txt_linea[1]=str_replace( '<br />', "
", $txt_linea[1] ); 
				if($lin0==$category)
				{
					$a[$indice[1]]=''.$txt_linea[1].'';
				}
			}
		}
		return $a;
	}
	/**/
	function save_cats_edit_content($idioma,$id,$update_content)
	{
		$this->CI->load->helper('file');
		$archivo_lang =  read_file('./application/language/'.$idioma.'/cats_lang.php');
		if ( ! write_file('./application/language/'.$idioma.'/cats_lang_backup.php', $archivo_lang))
		{
			/* echo 'Unable to write the file'; */
		}
		 $caracteres = array('?>','<?php');
		$archivo_lang= str_replace($caracteres ,"", $archivo_lang);
		$lineas=explode(";", $archivo_lang);
		$changes=array();
		/*var_dump($lineas); */
		$archivo_lang_edit = "";
		foreach($lineas as $key=>$linea)
		{
			/*var_dump($linea);*/
				$indice=explode("=", $linea);
				$index=explode('"', $indice[0]);
				/*$chars = array('[','"',']','$lang',"'");*/
				/*$index=str_replace($chars,"",$indice[0]);*/
				/*var_dump($index[1]);*/
			if(isset($index[1]))
			{
				if(isset($update_content[$index[1]]))
				{
					/*$value=trim($update_content[$key]);
					$notallow_caracteres = array("'", '"', ";", ".'", '."', "<", ">", "''",'<?php','?>','/>','</','""');
					$value = str_replace($notallow_caracteres ,"", $value);
					$value=str_replace( '<br />', "\r", $value);*/
					/*$changes[$index[1]]=$update_content[$index[1]];*/
					$field=trim($update_content[$index[1]]);
					$field=addslashes($field);
					$archivo_lang_edit.='
$lang["'.$index[1].'"]="'.$field.'";';
				}
				else
				{
					$archivo_lang_edit.=$linea.';';
					/* var_dump($linea); */
				}
			}
		}
		$archivo_lang_edit="<?php".
		$archivo_lang_edit."
?>";
		/*var_dump($changes);*/
	if ( ! write_file('./application/language/'.$idioma.'/cats_lang.php', $archivo_lang_edit))
		{
			return 'Unable to write the file';
		}
		else
		{
			return 'File written!';
		}
	}
	/**/
	function kits_file_new($idioma,$id){
		////////Generacion de archivo de lengua
		$this->CI->load->helper('file');
		$config_info=$this->CI->Appconfig->get_info();
		$archivo_lang =  read_file('./application/language/'.$idioma.'/kits_lang.php');
		$caracteres = array('<?php','?>');
$archivo_lang = str_replace($caracteres ,"", $archivo_lang);		
		$lineas=explode(";", $archivo_lang);
		$archivo_lang='';
$newdata = '$lang["kit_'.$id.'_MetaLang"]="'.$idioma.'";
$lang["kit_'.$id.'_MetaUrl"]="'.$this->CI->Item_kit->get_info($id)->name." ".$idioma.'";
$lang["kit_'.$id.'_MetaType"]="";
$lang["kit_'.$id.'_MetaTitle"]="'.$this->CI->Item_kit->get_info($id)->name.'";
$lang["kit_'.$id.'_MetaDescription"]="'.$this->CI->Item_kit->get_info($id)->description.'";
$lang["kit_'.$id.'_MetaAuthor"]="'.$config_info["company"].'";
$lang["kit_'.$id.'_Keywords"]="";
$lang["kit_'.$id.'_Name"]="'.$this->CI->Item_kit->get_info($id)->name.'";
$lang["kit_'.$id.'_Description"]="'.$this->CI->Item_kit->get_info($id)->description.'";
';
$a=array();
		foreach($lineas as $key=>$linea)
		{
				//$piece_linea=explode("_", $linea[$key]);
				$a[]=$lineas[$key];
		}
		$str=implode(";",$a);
		$archivo_lang.=$str.$newdata;
			//$archivo_lang.=$newdata; 
$archivo_lang='<?php'.$archivo_lang.'?>';
		if ( ! write_file('./application/language/'.$idioma.'/kits_lang.php', $archivo_lang))
		{
			// echo 'Unable to write the file';
		}
		else
		{
			// echo 'File written!';
		}
}
	/**/
	function get_kits_content_file($idioma,$id){
		$this->CI->load->helper('file');
		$archivo_lang =  read_file('./application/language/'.$idioma.'/kits_lang.php');
		$caracteres = array('<?php','?>');
		$caracteres=str_replace($caracteres ,"", $caracteres);
		$lineas=explode(";", $archivo_lang);
		$a=array();
		;
		foreach($lineas as $key=>$linea)
		{
			$lin=explode("_", $lineas[$key]);
			//ejemp:$lang["contenido_1_ciudades"]="Quintana Roo"; array($lang["contenido, 1, ciudades"]="Quintana Roo";);
			$line=explode("=", $lineas[$key]);
		if(isset($lin[1]))
			{
				$indice=explode('"', $lineas[$key]);
				///ejemp:$lang["contenido_1_ciudades"]="Quintana Roo"; array($lang[, contenido_1_ciudades, ]="Quintana Roo";);
				$slashes = array('\"',"\'");
				$field=str_replace($slashes,"'",$line[1]);
				$txt_linea=explode('"', $field);
				//echo $lin[1];
				$txt_linea[1]=str_replace( '<br />',"\r", $txt_linea[1] ); 
				if($lin[1]==$id)
				{
					$a[$indice[1]]=''.$txt_linea[1].'';
				}
			}
		}
		return $a;
	}
	/**/
	function save_item_kits_edit_content($idioma,$id,$update_content){
		$this->CI->load->helper('file');
		$archivo_lang =  read_file('./application/language/'.$idioma.'/kits_lang.php');
		if ( ! write_file('./application/language/'.$idioma.'/kits_lang_backup.php', $archivo_lang))
		{
			// echo 'Unable to write the file';
		}
		 $caracteres = array('?>','<?php');
		$archivo_lang= str_replace($caracteres ,"", $archivo_lang);
		$lineas=explode(";", $archivo_lang);
		$changes=array();
		//var_dump($lineas);
		$archivo_lang_edit = "";
		foreach($lineas as $key=>$linea)
		{
			//var_dump($linea);
				$indice=explode("=", $linea);
				$index=explode('"', $indice[0]);
				//$chars = array('[','"',']','$lang',"'");
				//$index=str_replace($chars,"",$indice[0]);
				//var_dump($index[1]);
			if(isset($index[1]))
			{
				if(isset($update_content[$index[1]]))
				{
					/*$value=trim($update_content[$key]);
					$notallow_caracteres = array("'", '"', ";", ".'", '."', "<", ">", "''",'<?php','?>','/>','</','""');
					$value = str_replace($notallow_caracteres ,"", $value);
					$value=str_replace( '<br />', "\r", $value);*/
					//$changes[$index[1]]=$update_content[$index[1]];
					$field=trim($update_content[$index[1]]);
					$field= str_replace('"' ,"'", $field);
					$field= nl2br($field);
					$field=addslashes($field);
					$archivo_lang_edit.='
$lang["'.$index[1].'"]="'.$field.'";';
				}
				else
				{
					$archivo_lang_edit.=$linea.';';
					//var_dump($linea);
				}
			}
		}
		$archivo_lang_edit="<?php".
		$archivo_lang_edit."
?>";
		//var_dump($changes);
	if ( ! write_file('./application/language/'.$idioma.'/kits_lang.php', $archivo_lang_edit))
		{
			return 'Unable to write the file';
		}
		else
		{
			return 'File written!';
		}
	}
}
?>