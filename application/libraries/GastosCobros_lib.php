<?php
class GastosCobros_lib
{
	var $CI;
  	function __construct()
	{
		$this->CI =& get_instance();
	}
	//Alain Multiple Payments
	function get_payments_total()
	{
		$subtotal = 0;
		foreach($this->get_payments() as $payments)
		{
		    $subtotal+=$payments['payment_amount'];
		}
		return to_currency_no_money($subtotal);
	}
	//Alain Multiple Payments
	function get_amount_due()
	{
		$amount_due=0;
		$payment_total = $this->get_payments_total();
		$sales_total=$this->get_total();
		$amount_due=to_currency_no_money($sales_total - $payment_total);
		if($amount_due<=0){
		$amount_due=to_currency_no_money($payment_total-$sales_total );
		}else{$amount_due=0;}
		return $amount_due;
	}
	//Obtenie el cliente de la SC 
	function get_customer()
	{
		if(!$this->CI->session->userdata('customer_cobros'))
			$this->set_customer(-1);
		return $this->CI->session->userdata('customer_cobros');
	}
	//Guarda el cliente en la SC 
	function set_customer($customer_id)
	{
		$this->CI->session->set_userdata('customer_cobros',$customer_id);
	}
	//Obtenie el proveedor de la SC 
	function get_supplier()
	{
		if(!$this->CI->session->userdata('supplier_gastos'))
			$this->set_supplier(-1);
		return $this->CI->session->userdata('supplier_gastos');
	}
	//Guarda el cliente en la SC de 
	function set_supplier($supplier_id)
	{
		$this->CI->session->set_userdata('supplier_gastos',$supplier_id);
	}
	//
	function get_mode()
	{
		if(!$this->CI->session->userdata('cobro_mode'))
			$this->set_mode('sale');
		return $this->CI->session->userdata('cobro_mode');
	}
	function set_mode($mode)
	{
		$this->CI->session->set_userdata('cobro_mode',$mode);
	}
	//
	function remove_customer()
	{
		$this->CI->session->unset_userdata('customer_cobros');
	}
	//
	function remove_supplier()
	{
		$this->CI->session->unset_userdata('supplier_gastos');
	}
	function clear_mode()
	{
		$this->CI->session->unset_userdata('cobro_mode');
	}
	function clear_all()
	{
		$this->clear_mode();
		$this->remove_customer();
		$this->remove_supplier();
	}
}
?>