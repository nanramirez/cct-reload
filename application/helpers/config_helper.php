<?php
/* lee el archivo version.txt, lo almacena y lo devuelve como resultado */
function get_cc_version(){
	$CI =& get_instance();
	$CI->load->helper('file');
	$version =  read_file('./version.txt');
	return $version;
}
/**/
function get_codebar_sheet($config){
	$CI =& get_instance();
	$sheet='<div id="cover_row_barcode_kit"><div class="box_field_row">';
	for($i=1;$i<=$config;$i++)
	{
		$sheet.='<div id="box_'.$i.'" class="codebar_sheet"></div>';
	}
	$sheet.='</div></div>';
	return $sheet;
}
?>