<?php
/*Genera la tabla del modulo de employees*/
function get_employees_manage_table($people,$controller){
	$CI =& get_instance();
	$table='<div class="cover_titles_table">';
	$headers = array('<input type="checkbox" id="select_all" />', 
	$CI->lang->line('module_employee_id'),
	$CI->lang->line('module_employee'),
	$CI->lang->line("common_phone_number"),
	$CI->lang->line('common_email'),
	$CI->lang->line("common_edition")
	);
	foreach($headers as $header)
	{
		$table.="<div class='titles_table_c'>$header</div>";
	}
	$table.='</div>';
	$table.='<section class="cover_result_table" id="cover_resultados_tabla">';
	$table.=get_employees_manage_table_data_rows($people,$controller);
	$table.='</section>';
	return $table;
}
/* Genera las filas de la tabla de empleados*/
function get_employees_manage_table_data_rows($people,$controller,$search=false){
	$CI =& get_instance();
	$table_data_rows='';
	$table_data_rows_array=array();
	foreach($people->result() as $person)
	{
		if($person->person_id!=1)
		{
			$table_data_rows.=get_employee_data_row($person,$controller);
			$table_data_rows_array[$person->person_id]=get_employee_data_row($person, $controller);
		}
	}
	if($people->num_rows()==0)
	{
		$table_data_rows.="<div class='files_result_table'><div class='warning_message' id='empty' >".$CI->lang->line('module_employees_null')."</div></div>";
		$table_data_rows_array[0]="<div class='files_result_table'><div class='warning_message' id='empty'>".$CI->lang->line('module_employees_null')."</div></div>";
	}
	if($search==true)
	{
		return $table_data_rows_array;
	}
	else
	{
		return $table_data_rows;
	}
}
/*Genera una fila de la tabla de empleados*/
function get_employee_data_row($person,$controller){
	$CI =& get_instance();
	$controller_name=strtolower(get_class($CI));
	$table_data_row='<div class="files_result_table" name="row_'.$person->person_id.'" id="row_'.$person->person_id.'">';
	$table_data_row.="<div class='box_result_table_c' id='seleccionar_".$person->person_id."'><div id='seleccionar_t_".$person->person_id."' class='soloVisibleResponsivamente'>".$CI->lang->line('common_seleccionar')."</div><input type='checkbox' class ='chksbxs' name ='chksbxs[]' id='chksbxs[$person->person_id]' value='".$person->person_id."' title='".$CI->lang->line('common_seleccionar')."'/></div>";
	$table_data_row.='<div id="col4'.$person->person_id.'" class="box_result_table_c"><div id="col4_t'.$person->person_id.'" class="soloVisibleResponsivamente">'.$CI->lang->line("module_employee_id").'</div>'.character_limiter($person->person_id,13).'</div>';	
	$table_data_row.='<div class="box_result_table_c" id="col2'.$person->person_id.'"><div id="col2_t'.$person->person_id.'" class="soloVisibleResponsivamente">'.$CI->lang->line("module_employee").' <span class="icon_more"></span> </br>'.$person->person_id.'</div>'.anchor($controller_name."/view_info/$person->person_id",character_limiter($person->first_name,13).' '.character_limiter($person->last_name,13),array('class'=>'info_form_ajax','title'=>$CI->lang->line('module_employee'))).'</div>';
	$table_data_row.='<div class="box_result_table_c" id="col1'.$person->person_id.'"><div id="col1_t'.$person->person_id.'" class="soloVisibleResponsivamente">'.$CI->lang->line("common_phone_number").'</div><a href="tel:'.$person->phone_number.'">'.$person->phone_number.'</a></div>';
	$table_data_row.='<div id="col3'.$person->person_id.'"class="box_result_table_c"><div id="col3_t'.$person->person_id.'" class="soloVisibleResponsivamente">'.$CI->lang->line('common_email').'</div>'.mailto($person->email,character_limiter($person->email,22)).'</div>';
	$table_data_row.='<div  id="col5'.$person->person_id.'" class="box_result_table_c"><div id="col5_t'.$person->person_id.'"  class="soloVisibleResponsivamente" >'.$CI->lang->line("common_edition").'</div>'.anchor($controller_name."/view/$person->person_id", $CI->lang->line('common_edit'),array('class'=>'ajax-popup-link','title'=>$CI->lang->line('common_edit'))).'</div>';
	$table_data_row.='</div>';
	return $table_data_row;
}
/* Genera la tabla del modulo de suppliers */
function get_suppliers_manage_table($suppliers,$controller){
	$CI =& get_instance();
	$table='<div class="cover_titles_table">';
	$headers = array('<input type="checkbox" id="select_all" />',
	$CI->lang->line('common_company'),
	$CI->lang->line('module_supplier'),
	$CI->lang->line("common_datos_fiscal"),
	$CI->lang->line('common_phone_number'),
	$CI->lang->line('common_email'),
	$CI->lang->line('common_edition')
	);
	foreach($headers as $header)
	{
	$table.="<div class='titles_table'>$header</div>";
	}
	$table.='</div>';
	$table.='<section class="cover_result_table" id="cover_resultados_tabla">';
	$table.=get_suppliers_manage_table_data_rows($suppliers,$controller);
	$table.='</section>';
	return $table;
}
/*  */
function get_suppliers_manage_table_data_rows($suppliers,$controller,$search=false){
	$CI =& get_instance();
	$table_data_rows='';
	$table_data_rows_array=array();
	foreach($suppliers->result() as $supplier)
	{
		$table_data_rows.=get_supplier_data_row($supplier,$controller);
		$table_data_rows_array[$supplier->person_id]=get_supplier_data_row($supplier, $controller);
	}
	if($suppliers->num_rows()==0)
	{
		$table_data_rows.="<div class='files_result_table'><div class='warning_message' id='empty' >".$CI->lang->line('module_suppliers_null')."</div></div>";
		$table_data_rows_array[0]="<div class='files_result_table'><div class='warning_message' id='empty'>".$CI->lang->line('module_suppliers_null')."</div></div>";
	}
	if($search==true)
	{
		return $table_data_rows_array;
	}
	else
	{
		return $table_data_rows;
	}
}
/**/
function get_supplier_data_row($supplier,$controller){
	$CI =& get_instance();
	$controller_name=strtolower(get_class($CI));
	$fiscal_info=$CI->Fiscal->get_info($supplier->person_id);
	$rfc="";
	if($fiscal_info->rfc!="")$rfc=anchor($controller_name."/view_info_rfc/$supplier->person_id", $fiscal_info->rfc,array('class'=>'info_form_ajax','title'=>$CI->lang->line('common_datos_fiscal')));
	$table_data_row='<div class="files_result_table" name="row_'.$supplier->person_id.'" id="row_'.$supplier->person_id.'">';
	$table_data_row.="<div id='seleccionar_".$supplier->person_id."' class='box_result_table'><div id='seleccionar_t_".$supplier->person_id."' class='soloVisibleResponsivamente'>".$CI->lang->line('common_seleccionar')."</div><input type='checkbox' class ='chksbxs' name ='chksbxs[]' id='chksbxs[$supplier->person_id]' value='".$supplier->person_id."'/></div>";
	$table_data_row.='<div id="col2'.$supplier->person_id.'" class="box_result_table"><div id="col2_t'.$supplier->person_id.'" class="soloVisibleResponsivamente">'.$CI->lang->line('common_company').' <span class="icon_more"></span> </br>'.$supplier->person_id.'</div>'.character_limiter($supplier->company,13).'</div>';
	$table_data_row.='<div id="col1'.$supplier->person_id.'" class="box_result_table"><div id="col1_t'.$supplier->person_id.'" class="soloVisibleResponsivamente">'.$CI->lang->line("module_supplier").'</div>'.anchor($controller_name."/view_info/$supplier->person_id",character_limiter($supplier->first_name,13).' '.character_limiter($supplier->last_name,13),array('class'=>'info_form_ajax','title'=>$CI->lang->line('module_supplier'))).'</div>';
	$table_data_row.='<div id="col3'.$supplier->person_id.'" class="box_result_table "><div id="col3_t'.$supplier->person_id.'"  class="soloVisibleResponsivamente">'.$CI->lang->line("common_datos_fiscal").'</div>'.$rfc.'</div>';
	$table_data_row.='<div id="col4'.$supplier->person_id.'" class="box_result_table"><div  id="col4_t'.$supplier->person_id.'" class="soloVisibleResponsivamente">'.$CI->lang->line('common_phone_number').'</div><a href="tel:'.$supplier->phone_number.'">'.$supplier->phone_number.'</a></div>';
	$table_data_row.='<div id="col5'.$supplier->person_id.'"  class="box_result_table"><div  id="col5_t'.$supplier->person_id.'" class="soloVisibleResponsivamente">'.$CI->lang->line('common_email').'</div>'.mailto($supplier->email,character_limiter($supplier->email,22)).'</div>';
	$table_data_row.='<div  id="col6'.$supplier->person_id.'" class="box_result_table"><div id="col6_t'.$supplier->person_id.'"  class="soloVisibleResponsivamente">'.$CI->lang->line("common_edition").'</div>'.anchor($controller_name."/view/$supplier->person_id", $CI->lang->line('common_edit'),array('class'=>'ajax-popup-link','title'=>$CI->lang->line('common_edit'))).'</div>';
	$table_data_row.='</div>';
	return $table_data_row;
}
/*Genera la tabla del modulo de items*/
function get_items_manage_table($items,$controller){
	$CI =& get_instance();
	$table='<div class="cover_titles_table" id="cover_titles_items">';
	$headers = array('<input type="checkbox" id="select_all" />', 
	$CI->lang->line('common_online'),
	$CI->lang->line('module_item'),
	$CI->lang->line('module_item_brand'),
	$CI->lang->line('common_category'),
	$CI->lang->line('common_cost').'/'.$CI->lang->line('common_price_sale'),
	$CI->lang->line('module_item_unit_stock'),
	$CI->lang->line('module_item_unit_stock_edit'),
	$CI->lang->line('common_edition'),
	$CI->lang->line('common_info')
	);
	foreach($headers as $header)
	{
		$table.="<div class='titles_table_b'>$header</div>\n";
	}
	$table.='</div>';
	$table.='<section class="cover_result_table" id="cover_resultados_tabla">';
	$table.=get_items_manage_table_data_rows($items,$controller);
	$table.='</section>';
	return $table;
}
/**/
function get_items_manage_table_data_rows($items,$controller,$search=false){
	$CI =& get_instance();
	$table_data_rows='';
	$table_data_rows_array=array();
	foreach($items->result() as $item)
	{
		$table_data_rows.=get_item_data_row($item,$controller);
		$table_data_rows_array[$item->item_id]=get_item_data_row($item, $controller);
	}
	if($items->num_rows()==0)
	{
		$table_data_rows.="<div class='files_result_table'><div class='warning_message' id='empty'>".$CI->lang->line('module_items_null')."</div></div>"."\n";
		$table_data_rows_array[0]="<div class='files_result_table'><div class='warning_message' id='empty'>".$CI->lang->line('module_items_null')."</div></div>";
	}
	if($search==true)
	{
		return $table_data_rows_array;
	}
	else
	{
		return $table_data_rows;
	}
}
/**/
function get_item_data_row($item,$controller){
	$CI =& get_instance();
	$item_tax_info=$CI->Item_taxes->get_info($item->item_id);
	$config=$CI->Appconfig->get_info();
	$tax_percents = '';
	foreach($item_tax_info as $tax_info)
	{
		$tax_percents.=$tax_info['percent']. '%, ';
	}
	$tax_percents=substr($tax_percents, 0, -2);
	$controller_name=strtolower(get_class($CI));
	$table_data_row='<div class="files_result_table" name="row_'.$item->item_id.'" id="row_'.$item->item_id.'">';
	$table_data_row.="<div id='seleccionar_".$item->item_id."' class='box_result_table_b'><div  id='seleccionar_t_".$item->item_id."' class='soloVisibleResponsivamente'>".$CI->lang->line('common_seleccionar')."</div><input type='checkbox' class ='chksbxs' name ='chksbxs[]' id='chksbxs[$item->item_id]' value='".$item->item_id."'/></div>"."\n";
	if($item->publicar==1){
	$table_data_row.='<div id="col1'.$item->item_id.'" class="box_result_table_b"><div id="col1_t'.$item->item_id.'"  class="soloVisibleResponsivamente">'.$CI->lang->line('common_online').'</div><img src="'.base_url().'images/cc_images/icon_cct/on_led.gif"></div>'."\n";
	}else{
	$table_data_row.='<div id="col1'.$item->item_id.'" class="box_result_table_b"><div id="col1_t'.$item->item_id.'"  class="soloVisibleResponsivamente">'.$CI->lang->line('common_online').'</div><img src="'.base_url().'images/cc_images/icon_cct/off_led.gif"></div>'."\n";
	}
	/*$table_data_row.='<div id="col2'.$item->item_id.'"class="box_result_table_b "><div  id="col2_t'.$item->item_id.'" class="soloVisibleResponsivamente">'.$CI->lang->line('module_item').' <span class="icon_more"></span></br>'.$item->item_id.' </div>'.$item->name.'</div>';*/
	$table_data_row.='<div id="col2'.$item->item_id.'" class="box_result_table_b"><div id="col2_t'.$item->item_id.'"  class="soloVisibleResponsivamente">'.$CI->lang->line('module_item').' <br>#'.$item->item_id.'</div>'.anchor($controller_name."/form_content/$item->item_id", '#'.$item->item_id.':'.character_limiter($item->name,24),array('class'=>'content_forms_ajax','title'=>$CI->lang->line('module_item'))).'</div>'."\n";
	$table_data_row.='<div id="col3'.$item->item_id.'" class="box_result_table_b"><div id="col3_t'.$item->item_id.'"  class="soloVisibleResponsivamente">'.$CI->lang->line('module_item_brand').'</div>'.$item->brand.'</div>'."\n";
	/*$table_data_row.='<div id="col4'.$item->item_id.'" class="box_result_table_b"><div  id="col4_t'.$item->item_id.'" class="soloVisibleResponsivamente">'.$CI->lang->line('common_category').'</div>'.$item->category.'</div>';*/
	$table_data_row.='<div id="col4'.$item->item_id.'" class="box_result_table_b"><div id="col4_t'.$item->item_id.'"class="soloVisibleResponsivamente">'.$CI->lang->line('common_category').'</div>'.anchor($controller_name."/form_cat_content/$item->item_id", $item->category,array('class'=>'content_forms_ajax','title'=>$CI->lang->line('common_category'))).'</div>'."\n";
	$table_data_row.='<div id="col5'.$item->item_id.'" class="box_result_table_b"><div id="col5_t'.$item->item_id.'"  class="soloVisibleResponsivamente">'.$CI->lang->line('common_cost').'/'.$CI->lang->line('common_price_sale').'</div>'.character_limiter(to_currency($item->cost_price),13).' '.$config['coin'].'<br><b>'.character_limiter(to_currency($item->unit_price),13).' '.$config['coin'].'</b></div>'."\n";
	$table_data_row.='<div id="col6'.$item->item_id.'" class="box_result_table_b"><div id="col6_t'.$item->item_id.'"  class="soloVisibleResponsivamente">'.$CI->lang->line('module_item_unit_stock').'</div>'.$item->quantity.'</div>'."\n";
	$table_data_row.='<div id="col7'.$item->item_id.'" class="box_result_table_b"><div id="col7_t'.$item->item_id.'"  class="soloVisibleResponsivamente">'.$CI->lang->line('module_item_unit_stock_edit').'</div>'.anchor($controller_name."/inventory/$item->item_id", $CI->lang->line('common_inv'),array('class'=>'inv_form_ajax','title'=>$CI->lang->line('module_item_unit_stock_edit'))).'</div>'."\n";
	$table_data_row.='<div  id="col8'.$item->item_id.'" class="box_result_table_b"><div  id="col8_t'.$item->item_id.'" class="soloVisibleResponsivamente">'.$CI->lang->line("common_edition").'</div>'.anchor($controller_name."/view/$item->item_id", $CI->lang->line('common_edit'),array('class'=>'ajax-popup-link','title'=>$CI->lang->line('common_edition'))).'</div>';
	/*Ramel Seguimiento de Inventario*/
	$table_data_row.='<div id="col9'.$item->item_id.'" class="box_result_table_b"><div  id="col9_t'.$item->item_id.'" class="soloVisibleResponsivamente">'.$CI->lang->line('common_info').'</div>'.anchor($controller_name."/count_details/$item->item_id", $CI->lang->line('common_det'),array('class'=>'inv_form_ajax','title'=>$CI->lang->line('common_info'))).'</div>'."\n";
	/*detalles del inventario*/
	$table_data_row.='</div>'."\n";
	return $table_data_row;
}
/*Aqui comienza el helper para los recursos */
function get_recursos_manage_table($recursos,$controller){
	$CI =& get_instance();
	$table='<div class="cover_titles_table">';
	$headers = array(
	'<input type="checkbox" id="select_all" />',
	$CI->lang->line('module_recursos_nombre_cuenta'),
	$CI->lang->line('common_quantity'),
	$CI->lang->line('common_edition')
	);
	
	foreach($headers as $header)
	{
		
		$table.="<div class='titles_table_f' >$header</div>";
	}
	$table.='</div>';
	$table.='<section class="cover_result_table" id="cover_resultados_tabla">';
	$table.=get_recursos_manage_table_data_rows($recursos,$controller);
	$table.='</section>';
	return $table;
}
/**/
function get_recursos_manage_table_data_rows($recursos,$controller,$search=false){
	$CI =& get_instance();
	$table_data_rows='';
	$table_data_rows_array=array();
	$obj=$recursos->result();
	/*foreach($recursos->result() as $recurso) /lo cambie asi por que me marcaba error en la condicion*/
	foreach($obj as $recurso)
	{

		/*$table_data_rows.=get_recursos_data_row($recurso,$controller);
		 $table_data_rows_array[$recurso->recurso_id]=get_recursos_data_row($recurso, $controller);*/
		 /* con esta condicion el ultimo elemento no tendra los header sobrantes */
	if ($recurso === end($obj)) {
		 $table_data_rows.=get_recursos_data_row($recurso,$controller,1);
		 $table_data_rows_array[$recurso->recurso_id]=get_recursos_data_row($recurso, $controller,1);
    }

   else{
		 $table_data_rows.=get_recursos_data_row($recurso,$controller);
		 $table_data_rows_array[$recurso->recurso_id]=get_recursos_data_row($recurso, $controller);
    }
		
	}
	if($recursos->num_rows()==0)
	{
		$table_data_rows.="<div class='files_result_table'><div class='warning_message' id='empty' >".$CI->lang->line('module_recursos_null')."</div></div>";
		$table_data_rows_array[0]="<div class='files_result_table'><div class='warning_message' id='empty'>".$CI->lang->line('module_recursos_null')."</div></div>";
	}
	if($search==true)
	{
		return $table_data_rows_array;
	}
	else
	{
		return $table_data_rows;
	}
}
/**/
function get_recursos_data_row($recurso,$controller,$diferencia=0){
	$CI =& get_instance();
	$controller_name=strtolower(get_class($CI));
	$clase="montos_blue";
	if($recurso->recurso<0)$clase="montos_red";
	$table_data_row='<div class="files_result_table" name="row_'.$recurso->recurso_id.'" id="row_'.$recurso->recurso_id.'">';
	$table_data_row.="<div id='seleccionar_".$recurso->recurso_id."' class='box_result_table_f'><div id='seleccionar_t_".$recurso->recurso_id."' class='soloVisibleResponsivamente'>".$CI->lang->line('common_seleccionar')."</div><input type='checkbox' class ='chksbxs' name='chksbxs[]'id='chksbxs[$recurso->recurso_id]' value='".$recurso->recurso_id."'/></div>";
	$table_data_row.='<div id="col2'.$recurso->recurso_id.'" class="box_result_table_f"><div id="col2_t'.$recurso->recurso_id.'" class="soloVisibleResponsivamente">'.$CI->lang->line('module_recursos_nombre_cuenta').'<span class="icon_more"></span></br></div><a id="'.$recurso->recurso_id.'" class="show_hidde_operation">'.$recurso->nombre.'</a></div>';
	$table_data_row.='<div id="col1'.$recurso->recurso_id.'" class="box_result_table_f"><div id="col1_t'.$recurso->recurso_id.'" class="soloVisibleResponsivamente">'.$CI->lang->line('common_quantity').'</div>'.anchor($controller_name."/new_operation/$recurso->recurso_id",'<span id="span'.$recurso->recurso_id.'" class="'.$clase.'">'.to_currency(floatval($recurso->recurso)).' '.$recurso->coin."</span>",array('class'=>'operations-popup-link','title'=>to_currency(floatval($recurso->recurso)).' '.$recurso->coin)).'</div>';
	$table_data_row.='<div  id="col3'.$recurso->recurso_id.'" class="box_result_table_f"><div id="col3_t'.$recurso->recurso_id.'" class="soloVisibleResponsivamente">'.$CI->lang->line("common_edition").'</div>'.anchor($controller_name."/view/$recurso->recurso_id", $CI->lang->line('common_edit'),array('class'=>'ajax-popup-link','title'=>$CI->lang->line('common_edit').' '.$recurso->nombre)).'</div>';
	$table_data_row.='</div>';
	if($CI->Recurso->get_all_operations(1000,0,$CI->config_lib->get_mes_show(),$recurso->recurso_id)->num_rows()!=0){
	$table_data_row.='<div class="cover_subtitles_table" id="cover_subtitles_table_'.$recurso->recurso_id.'">';
	$headers = array(
	$CI->lang->line('common_date'),
	$CI->lang->line('common_concepto'),
	$CI->lang->line('common_monto'),
	$CI->lang->line('module_recursos_operation_before_update')
	);
	
	foreach($headers as $header)
	{
		
		$table_data_row.="<div class='subtitles_table' id='subtitles_table_".$recurso->recurso_id."'>$header</div>";
		
	}
	$table_data_row.='</div>';
	$table_data_row.='<div class="files_result_subtable" id="files_result_subtable_'.$recurso->recurso_id.'">';
	foreach($CI->Recurso->get_all_operations(1000,0,$CI->config_lib->get_mes_show(),$recurso->recurso_id)->result() as $operation){
		$clase="montos_green";
		$clase2="remaining_up";
		$monto="+".to_currency($operation->monto);
		if($operation->monto<0&&$operation->saldo_restante>0){
			$clase="montos_red";
			$monto=to_currency($operation->monto);
			$clase2="remaining_down";
			
		}elseif($operation->monto>0&&$operation->saldo_restante>0){
			$clase2="remaining_up";
		}
		if($operation->saldo_restante<0)$clase2="remaining_red_up";
		$table_data_row.='<div class="files_result_table" name="row_'.$operation->operation_id.'" id="row_'.$operation->operation_id.'">';
		$table_data_row.="<div id='subcol1".$operation->operation_id."' class='box_result_subtable_f'><div id='col".$operation->operation_id."' class='soloVisibleResponsivamente'>".$CI->lang->line('common_date')."</div>".$operation->operation_time."</div>";
		$table_data_row.='<div id="subcol2'.$operation->operation_id.'" class="box_result_subtable_f"><div id="col1_t'.$operation->operation_id.'" class="soloVisibleResponsivamente">'.$CI->lang->line('common_concepto').'<span class="icon_more"></span></br></div>'.$operation->concepto.'</div>';
		$table_data_row.='<div id="subcol3'.$operation->operation_id.'" class="box_result_subtable_f"><div id="col2_t'.$operation->operation_id.'" class="soloVisibleResponsivamente">'.$CI->lang->line('common_monto').'<span class="icon_more"></span></br></div><span  class="'.$clase.'">'.$monto.'</span></div>';
		$table_data_row.='<div id="subcol4'.$operation->operation_id.'" class="box_result_subtable_f"><div id="col3_t'.$operation->operation_id.'" class="soloVisibleResponsivamente">'.$CI->lang->line('module_recursos_operation_before_update').'<span class="icon_more"></span></br></div><span class="previous">'.to_currency($operation->saldo_anterior).'</span> <span class="'.$clase2.'">'.to_currency($operation->saldo_restante).'</span></div>';
		$table_data_row.='</div>';
	}
	$table_data_row.='</div>';
	if($diferencia ==0){
	$table_data_row.='<div class="cover_titles_table_hidde" id="cover_titles_table_hidde_'.$recurso->recurso_id.'">';
	$headers = array(
	'<input type="checkbox" id="select_all" />',
	$CI->lang->line('module_recursos_nombre_cuenta'),
	$CI->lang->line('common_quantity'),
	$CI->lang->line('common_edition')
	);
	foreach($headers as $header)
	{

		$table_data_row.="<div class='titles_table_f'>$header</div>";
	}
	$table_data_row.='</div>';}
	}
	return $table_data_row;
}
/******************Aqui comienza el helper para los gastos****************/
function get_gastos_manage_table($gastos,$controller){
	$CI =& get_instance();
	$table='<div class="cover_titles_table">';
	$headers = array('<input type="checkbox" id="select_all" />',
	$CI->lang->line('common_date'),
	$CI->lang->line('common_monto'),
	$CI->lang->line('common_concepto'),
	$CI->lang->line('common_category'),
	$CI->lang->line('common_payments_methods'),
	$CI->lang->line('common_edition')
	);
	$headers_done = array(
	$CI->lang->line('common_date'),
	$CI->lang->line('common_monto'),
	$CI->lang->line('common_category'),
	$CI->lang->line('common_concepto'),
	$CI->lang->line('common_payments_methods')
	);
	if($controller=="Pendiente")
	{
		foreach($headers as $header)
		{
			$table.="<div class='titles_table'>$header</div>";
		}
	}
	elseif ($controller=="Hecho")
	{
		foreach($headers_done as $header)
		{
			$table.="<div class='titles_table_d'>$header</div>";
		}
	}
	$table.='</div>';
	$table.='<section class="cover_result_table" id="cover_resultados_tabla">';
	$table.=get_gastos_manage_table_data_rows($gastos,$controller);
	$table.='</section>';
	return $table;
}
/**/
function get_gastos_manage_table_data_rows($gastos,$controller,$search=false){
	$CI =& get_instance();
	$table_data_rows='';
	$table_data_rows_array=array();
	foreach($gastos->result() as $gasto)
	{
		$table_data_rows.=get_gastos_data_row($gasto,$controller);
		$table_data_rows_array[$gasto->gasto_id]=get_gastos_data_row($gasto, $controller);
	}
	if($gastos->num_rows()==0)
	{
		$table_data_rows.="<div class='files_result_table'><div class='warning_message' id='empty' >".$CI->lang->line('module_gastos_null')."</div></div>";
		$table_data_rows_array[0]="<div class='files_result_table'><div class='warning_message' id='empty'>".$CI->lang->line('module_gastos_null')."</div></div>";
	}
	if($search==true)
	{
		return $table_data_rows_array;
	}
	else
	{
		return $table_data_rows;
	}
}
/**/
function get_gastos_data_row($gasto,$controller){
	$CI =& get_instance();
	$controller_name=strtolower(get_class($CI));
	$hoy=date('Y').'-'.date('n').'-'.date('d');
	$fecha=explode(' ', $gasto->fecha);
	$fecha_split=explode('-', $fecha[0]);
	$fresh_gasto_info=$CI->Gasto->get_info($gasto->gasto_id);
	$payment_type=$CI->Recurso->get_info_nombre_metodo($gasto->recurso_id);
	if($controller=="Pendiente" && date('Y')==$fecha_split[0] && date('n')==$fecha_split[1] && date('d')==$fecha_split[2])
	{	/*cuando los gastos expiran el dia de hoy*/
		$table_data_row='<div class="files_result_table" name="row_'.$gasto->gasto_id.'" id="row_'.$gasto->gasto_id.'">';
		$table_data_row.='<div id="seleccionar_'.$gasto->gasto_id.'" class="box_result_table   box_result_table_hoy"><div id="seleccionar_t_'.$gasto->gasto_id.'" class="soloVisibleResponsivamente">'.$CI->lang->line('common_seleccionar').'</div><input type="checkbox" class ="chksbxs" name="chksbxs[]"id="chksbxs[$gasto->gasto_id]" value="'.$gasto->gasto_id.'"/></div>';
		$table_data_row.='<div id="col1'.$gasto->gasto_id.'"  class="box_result_table   box_result_table_hoy"><div id="col1_t'.$gasto->gasto_id.'" class="soloVisibleResponsivamente">'.$CI->lang->line('common_date').'</div>'.character_limiter($gasto->fecha,13).'</div>';
		$table_data_row.='<div id="col3'.$gasto->gasto_id.'" class="box_result_table   box_result_table_hoy"><div id="col3_t'.$gasto->gasto_id.'" class="soloVisibleResponsivamente">'.$CI->lang->line('common_monto').'</div>'.to_currency($fresh_gasto_info->cantidad).' '.$fresh_gasto_info->coin.'</div>';
		$table_data_row.='<div id="col2'.$gasto->gasto_id.'"  class="box_result_table   box_result_table_hoy"><div id="col2_t'.$gasto->gasto_id.'" class="soloVisibleResponsivamente">'.$CI->lang->line('common_concepto').'<span class="icon_more"></span></br></div>'.character_limiter($gasto->concepto,25).'</div>';
		$table_data_row.='<div id="col4'.$gasto->gasto_id.'" class="box_result_table   box_result_table_hoy"><div id="col4_t'.$gasto->gasto_id.'"  class="soloVisibleResponsivamente">'.$CI->lang->line('common_category').'</div>'.character_limiter($gasto->category,13).'</div>';
		$table_data_row.='<div id="col5'.$gasto->gasto_id.'"  class="box_result_table   box_result_table_hoy"><div id="col5_t'.$gasto->gasto_id.'"  class="soloVisibleResponsivamente">'.$CI->lang->line('common_payments_methods').'</div>'.character_limiter($payment_type,13).'</div>';
		$table_data_row.='<div  id="col6'.$gasto->gasto_id.'"  class="box_result_table   box_result_table_hoy"><div id="col6_t'.$gasto->gasto_id.'"  class="soloVisibleResponsivamente">'.$CI->lang->line("common_edition").'</div>'.anchor($controller_name."/view/$gasto->gasto_id", $CI->lang->line('common_edit'),array('class'=>'ajax-popup-link','title'=>$CI->lang->line($controller_name.'_update'))).'</div>';
		$table_data_row.='</div>';
	}
	elseif($controller=="Pendiente" && date('Y')==$fecha_split[0] && date('n')>=$fecha_split[1] && date('d')>$fecha_split[2])
	{	/*cuando los gastos ya expiraron y son del mismo mes*/
		$table_data_row='<div class="files_result_table" name="row_'.$gasto->gasto_id.'" id="row_'.$gasto->gasto_id.'">';
		$table_data_row.='<div id="seleccionar_'.$gasto->gasto_id.'"  class="box_result_table   box_result_table_exp"><div id="seleccionar_t_'.$gasto->gasto_id.'" class="soloVisibleResponsivamente">'.$CI->lang->line('common_seleccionar').'</div><input type="checkbox" class ="chksbxs" name="chksbxs[]"id="chksbxs[$gasto->gasto_id]" value="'.$gasto->gasto_id.'"/></div>';
		$table_data_row.='<div id="col1'.$gasto->gasto_id.'" class="box_result_table   box_result_table_exp"><div id="col1_t'.$gasto->gasto_id.'" class="soloVisibleResponsivamente">'.$CI->lang->line('common_date').'</div>'.character_limiter($gasto->fecha,13).'</div>';
		$table_data_row.='<div id="col3'.$gasto->gasto_id.'" class="box_result_table   box_result_table_exp"><div id="col3_t'.$gasto->gasto_id.'"  class="soloVisibleResponsivamente">'.$CI->lang->line('common_monto').'</div>'.to_currency($gasto->cantidad).' '.$fresh_gasto_info->coin.'</div>';
		$table_data_row.='<div id="col2'.$gasto->gasto_id.'" class="box_result_table   box_result_table_exp"><div id="col2_t'.$gasto->gasto_id.'"  class="soloVisibleResponsivamente">'.$CI->lang->line('common_concepto').'<span class="icon_more"></span></br></div>'.character_limiter($gasto->concepto,25).'</div>';
		$table_data_row.='<div id="col4'.$gasto->gasto_id.'" class="box_result_table   box_result_table_exp"><div id="col4_t'.$gasto->gasto_id.'"  class="soloVisibleResponsivamente">'.$CI->lang->line('common_category').'</div>'.character_limiter($gasto->category,13).'</div>';
		$table_data_row.='<div id="col5'.$gasto->gasto_id.'" class="box_result_table   box_result_table_exp"><div id="col5_t'.$gasto->gasto_id.'"  class="soloVisibleResponsivamente">'.$CI->lang->line('common_payments_methods').'</div>'.character_limiter($payment_type,13).'</div>';
		$table_data_row.='<div  id="col6'.$gasto->gasto_id.'" class="box_result_table   box_result_table_exp"><div id="col6_t'.$gasto->gasto_id.'"  class="soloVisibleResponsivamente">'.$CI->lang->line("common_edition").'</div>'.anchor($controller_name."/view/$gasto->gasto_id", $CI->lang->line('common_edit'),array('class'=>'ajax-popup-link','title'=>$CI->lang->line($controller_name.'_update'))).'</div>';
		$table_data_row.='</div>';
	}
	elseif($controller=="Pendiente" && date('Y')==$fecha_split[0] && date('n')==$fecha_split[1] && date('d')<$fecha_split[2])
	{	/*cuando los gastos ya expiraron y son de meses anteriores*/
		$table_data_row='<div class="files_result_table" name="row_'.$gasto->gasto_id.'" id="row_'.$gasto->gasto_id.'">';
		$table_data_row.='<div id="seleccionar_'.$gasto->gasto_id.'" class="box_result_table"><div id="seleccionar_t_'.$gasto->gasto_id.'"class="soloVisibleResponsivamente">'.$CI->lang->line('common_seleccionar').'</div><input type="checkbox" class ="chksbxs" name="chksbxs[]"id="chksbxs[$gasto->gasto_id]" value="'.$gasto->gasto_id.'"/></div>';
		$table_data_row.='<div id="col1'.$gasto->gasto_id.'" class="box_result_table"><div id="col1_t'.$gasto->gasto_id.'"  class="soloVisibleResponsivamente">'.$CI->lang->line('common_date').'</div>'.character_limiter($gasto->fecha,13).'</div>';
		$table_data_row.='<div id="col3'.$gasto->gasto_id.'"class="box_result_table"><div id="col3_t'.$gasto->gasto_id.'"  class="soloVisibleResponsivamente">'.$CI->lang->line('common_monto').'</div>'.to_currency($gasto->cantidad).' '.$fresh_gasto_info->coin.'</div>';
		$table_data_row.='<div id="col2'.$gasto->gasto_id.'" class="box_result_table"><div id="col2_t'.$gasto->gasto_id.'"  class="soloVisibleResponsivamente">'.$CI->lang->line('common_concepto').'<span class="icon_more"></span></br></div>'.character_limiter($gasto->concepto,25).'</div>';
		$table_data_row.='<div id="col4'.$gasto->gasto_id.'" class="box_result_table"><div id="col4_t'.$gasto->gasto_id.'"  class="soloVisibleResponsivamente">'.$CI->lang->line('common_category').'</div>'.character_limiter($gasto->category,13).'</div>';
		$table_data_row.='<div id="col5'.$gasto->gasto_id.'"class="box_result_table"><div id="col5_t'.$gasto->gasto_id.'"  class="soloVisibleResponsivamente">'.$CI->lang->line('common_payments_methods').'</div>'.character_limiter($payment_type,13).'</div>';
		$table_data_row.='<div  id="col6'.$gasto->gasto_id.'"class="box_result_table"><div id="col6_t'.$gasto->gasto_id.'"  class="soloVisibleResponsivamente">'.$CI->lang->line("common_edition").'</div>'.anchor($controller_name."/view/$gasto->gasto_id", $CI->lang->line('common_edit'),array('class'=>'ajax-popup-link','title'=>$CI->lang->line($controller_name.'_update'))).'</div>';
		$table_data_row.='</div>';
	}
	elseif($controller=="Pendiente" && date('Y')==$fecha_split[0] && $fecha_split[1]<date('n'))
	{	/*Cuando los gastos no estan vencidos*/
		$table_data_row='<div class="files_result_table" name="row_'.$gasto->gasto_id.'" id="row_'.$gasto->gasto_id.'">';
		$table_data_row.='<div id="seleccionar_'.$gasto->gasto_id.'"  class="box_result_table   box_result_table_exp"><div id="seleccionar_t_'.$gasto->gasto_id.'" class="soloVisibleResponsivamente">'.$CI->lang->line('common_seleccionar').'</div><input type="checkbox" class ="chksbxs" name="chksbxs[]"id="chksbxs[$gasto->gasto_id]" value="'.$gasto->gasto_id.'"/></div>';
		$table_data_row.='<div id="col1'.$gasto->gasto_id.'" class="box_result_table   box_result_table_exp"><div id="col1_t'.$gasto->gasto_id.'" class="soloVisibleResponsivamente">'.$CI->lang->line('common_date').'</div>'.character_limiter($gasto->fecha,13).'</div>';
		$table_data_row.='<div id="col3'.$gasto->gasto_id.'" class="box_result_table   box_result_table_exp"><div id="col3_t'.$gasto->gasto_id.'"  class="soloVisibleResponsivamente">'.$CI->lang->line('common_monto').'</div>'.to_currency($gasto->cantidad).' '.$fresh_gasto_info->coin.'</div>';
		$table_data_row.='<div id="col2'.$gasto->gasto_id.'" class="box_result_table   box_result_table_exp"><div id="col2_t'.$gasto->gasto_id.'"  class="soloVisibleResponsivamente">'.$CI->lang->line('common_concepto').'<span class="icon_more"></span></br></div>'.character_limiter($gasto->concepto,25).'</div>';
		$table_data_row.='<div id="col4'.$gasto->gasto_id.'" class="box_result_table   box_result_table_exp"><div id="col4_t'.$gasto->gasto_id.'"  class="soloVisibleResponsivamente">'.$CI->lang->line('common_category').'</div>'.character_limiter($gasto->category,13).'</div>';
		$table_data_row.='<div id="col5'.$gasto->gasto_id.'" class="box_result_table   box_result_table_exp"><div id="col5_t'.$gasto->gasto_id.'"  class="soloVisibleResponsivamente">'.$CI->lang->line('common_payments_methods').'</div>'.character_limiter($payment_type,13).'</div>';
		$table_data_row.='<div  id="col6'.$gasto->gasto_id.'" class="box_result_table   box_result_table_exp"><div id="col6_t'.$gasto->gasto_id.'"  class="soloVisibleResponsivamente">'.$CI->lang->line("common_edition").'</div>'.anchor($controller_name."/view/$gasto->gasto_id", $CI->lang->line('common_edit'),array('class'=>'ajax-popup-link','title'=>$CI->lang->line($controller_name.'_update'))).'</div>';
		$table_data_row.='</div>';
	}
	elseif($controller=="Pendiente" && date('Y')==$fecha_split[0] && date('n')<$fecha_split[1])
	{
		$table_data_row='<div class="files_result_table" name="row_'.$gasto->gasto_id.'" id="row_'.$gasto->gasto_id.'">';
		$table_data_row.='<div id="seleccionar_'.$gasto->gasto_id.'" class="box_result_table"><div id="seleccionar_t_'.$gasto->gasto_id.'"class="soloVisibleResponsivamente">'.$CI->lang->line('common_seleccionar').'</div><input type="checkbox" class ="chksbxs" name="chksbxs[]"id="chksbxs[$gasto->gasto_id]" value="'.$gasto->gasto_id.'"/></div>';
		$table_data_row.='<div id="col1'.$gasto->gasto_id.'" class="box_result_table"><div id="col1_t'.$gasto->gasto_id.'"  class="soloVisibleResponsivamente">'.$CI->lang->line('common_date').'</div>'.character_limiter($gasto->fecha,13).'</div>';
		$table_data_row.='<div id="col3'.$gasto->gasto_id.'"class="box_result_table"><div id="col3_t'.$gasto->gasto_id.'"  class="soloVisibleResponsivamente">'.$CI->lang->line('common_monto').'</div>'.to_currency($gasto->cantidad).' '.$fresh_gasto_info->coin.'</div>';
		$table_data_row.='<div id="col2'.$gasto->gasto_id.'" class="box_result_table"><div id="col2_t'.$gasto->gasto_id.'"  class="soloVisibleResponsivamente">'.$CI->lang->line('common_concepto').'<span class="icon_more"></span></br></div>'.character_limiter($gasto->concepto,25).'</div>';
		$table_data_row.='<div id="col4'.$gasto->gasto_id.'" class="box_result_table"><div id="col4_t'.$gasto->gasto_id.'"  class="soloVisibleResponsivamente">'.$CI->lang->line('common_category').'</div>'.character_limiter($gasto->category,13).'</div>';
		$table_data_row.='<div id="col5'.$gasto->gasto_id.'"class="box_result_table"><div id="col5_t'.$gasto->gasto_id.'"  class="soloVisibleResponsivamente">'.$CI->lang->line('common_payments_methods').'</div>'.character_limiter($payment_type,13).'</div>';
		$table_data_row.='<div  id="col6'.$gasto->gasto_id.'"class="box_result_table"><div id="col6_t'.$gasto->gasto_id.'"  class="soloVisibleResponsivamente">'.$CI->lang->line("common_edition").'</div>'.anchor($controller_name."/view/$gasto->gasto_id", $CI->lang->line('common_edit'),array('class'=>'ajax-popup-link','title'=>$CI->lang->line($controller_name.'_update'))).'</div>';
		$table_data_row.='</div>';
	}
	elseif($controller=="Hecho")
	{	/*Cuando los gastos ya estan hechos*/
		$table_data_row='<div class="files_result_table" name="row_'.$gasto->gasto_id.'" id="row_'.$gasto->gasto_id.'"><input type="hidden" class ="chksbxs" name="chksbxs[]"id="chksbxs[$gasto->gasto_id]" value="'.$gasto->gasto_id.'"/>';
		$table_data_row.='<div id="col1'.$gasto->gasto_id.'"class="box_result_table_d"><div class="soloVisibleResponsivamente">'.$CI->lang->line('common_date').'</div>'.character_limiter($gasto->fecha,13).'</div>';
		$table_data_row.='<div id="col3'.$gasto->gasto_id.'"class="box_result_table_d"><div id="col3_t'.$gasto->gasto_id.'" class="soloVisibleResponsivamente">'.$CI->lang->line('common_monto').'</div>'.to_currency($gasto->cantidad).' '.$fresh_gasto_info->coin.'</div>';
		$table_data_row.='<div id="col4'.$gasto->gasto_id.'" class="box_result_table_d"><div id="col4_t'.$gasto->gasto_id.'" class="soloVisibleResponsivamente">'.$CI->lang->line('common_category').'+ </div>'.character_limiter($gasto->category,13).'</div>';
		$table_data_row.='<div  id="col2'.$gasto->gasto_id.'" class="box_result_table_d"><div id="col2_t'.$gasto->gasto_id.'"class="soloVisibleResponsivamente">'.$CI->lang->line("common_concepto").'<span class="icon_more"></span></br></div>'.anchor($controller_name."/details/$gasto->gasto_id",character_limiter($gasto->concepto,25),array('class'=>'info_form_ajax','title'=>$CI->lang->line($controller_name.'_update'))).'</div>';
		$table_data_row.='<div id="col5'.$gasto->gasto_id.'" class="box_result_table_d"><div id="col5_t'.$gasto->gasto_id.'"class="soloVisibleResponsivamente">'.$CI->lang->line('common_payments_methods').'</div>'.character_limiter($payment_type,13).'</div>';
		$table_data_row.='</div>';
	}
	else
	{
		/*Cuando los gastos no estan vencidos*/
		$table_data_row='<div class="files_result_table" name="row_'.$gasto->gasto_id.'" id="row_'.$gasto->gasto_id.'">';
		$table_data_row.='<div id="seleccionar_'.$gasto->gasto_id.'" class="box_result_table"><div id="seleccionar_t_'.$gasto->gasto_id.'"class="soloVisibleResponsivamente">'.$CI->lang->line('common_seleccionar').'</div><input type="checkbox" class ="chksbxs" name="chksbxs[]"id="chksbxs[$gasto->gasto_id]" value="'.$gasto->gasto_id.'"/></div>';
		$table_data_row.='<div id="col1'.$gasto->gasto_id.'" class="box_result_table"><div id="col1_t'.$gasto->gasto_id.'"  class="soloVisibleResponsivamente">'.$CI->lang->line('common_date').'</div>'.character_limiter($gasto->fecha,13).'</div>';
		$table_data_row.='<div id="col3'.$gasto->gasto_id.'"class="box_result_table"><div id="col3_t'.$gasto->gasto_id.'"  class="soloVisibleResponsivamente">'.$CI->lang->line('common_monto').'</div>'.to_currency($gasto->cantidad).' '.$fresh_gasto_info->coin.'</div>';
		$table_data_row.='<div id="col2'.$gasto->gasto_id.'" class="box_result_table"><div id="col2_t'.$gasto->gasto_id.'"  class="soloVisibleResponsivamente">'.$CI->lang->line('common_concepto').'<span class="icon_more"></span></br></div>'.character_limiter($gasto->concepto,25).'</div>';
		$table_data_row.='<div id="col4'.$gasto->gasto_id.'" class="box_result_table"><div id="col4_t'.$gasto->gasto_id.'"  class="soloVisibleResponsivamente">'.$CI->lang->line('common_category').'</div>'.character_limiter($gasto->category,13).'</div>';
		$table_data_row.='<div id="col5'.$gasto->gasto_id.'"class="box_result_table"><div id="col5_t'.$gasto->gasto_id.'"  class="soloVisibleResponsivamente">'.$CI->lang->line('common_payments_methods').'</div>'.character_limiter($payment_type,13).'</div>';
		$table_data_row.='<div  id="col6'.$gasto->gasto_id.'"class="box_result_table"><div id="col6_t'.$gasto->gasto_id.'"  class="soloVisibleResponsivamente">'.$CI->lang->line("common_edition").'</div>'.anchor($controller_name."/view/$gasto->gasto_id", $CI->lang->line('common_edit'),array('class'=>'ajax-popup-link','title'=>$CI->lang->line($controller_name.'_update'))).'</div>';
		$table_data_row.='</div>';
	}
	return $table_data_row;
}
/*Aqui comienza el helper para las Entradas */
function get_entradas_manage_table($entradas,$controller){
	$CI =& get_instance();
	$table='<div class="cover_titles_table">';
	$headers_pending = array(
	'<input type="checkbox" id="select_all" />',
	 $CI->lang->line('common_date'),
	$CI->lang->line('module_receivings_id'),
	$CI->lang->line('module_supplier'),
	$CI->lang->line('module_items'),
	$CI->lang->line('common_edition')
	);
	$headers = array(
	$CI->lang->line('common_date'),
	$CI->lang->line('module_receivings_id'),
	$CI->lang->line('module_supplier'),
	$CI->lang->line('module_items'),
	$CI->lang->line('common_modes_register')
	);
	$table.='';
	if($controller=="Pendiente")
	{
		foreach($headers_pending as $header)
		{
			$table.="<div class='titles_table_c'>$header</div>";
		}
	}
	else
	{
		foreach($headers as $header)
		{
			$table.="<div class='titles_table_d'>$header</div>";
		}
	}
	$table.='</div>';
	$table.='<section class="cover_result_table" id="cover_resultados_tabla">';
	$table.=get_entradas_manage_table_data_rows($entradas,$controller);
	$table.='</section>';
	return $table;
}
/**/
function get_entradas_manage_table_data_rows($entradas,$controller,$search=false){
	$CI =& get_instance();
	$table_data_rows='';
	$table_data_rows_array=array();
	foreach($entradas->result() as $entrada)
	{
		$table_data_rows.=get_entradas_data_row($entrada,$controller);
		$table_data_rows_array[$entrada->receiving_id]=get_entradas_data_row($entrada, $controller);
	}
	if($entradas->num_rows()==0)
	{
		$table_data_rows.="<div class='files_result_table'><div class='warning_message' id='empty' >".$CI->lang->line('module_receivings_null')."</div></div>";
		$table_data_rows_array[0]="<div class='files_result_table'><div class='warning_message' id='empty'>".$CI->lang->line('module_receivings_null')."</div></div>";
	}
	if($search==true)
	{
		return $table_data_rows_array;
	}
	else
	{
		return $table_data_rows;
	}
}
/**/
function get_entradas_data_row($entrada,$controller){
	$CI =& get_instance();
	$controller_name=strtolower(get_class($CI));
	$supplier_info=$CI->Supplier->get_info($entrada->supplier_id);
	if($entrada->modo=="Receive"){$modo=$CI->lang->line("module_receiving");}
	elseif($entrada->modo=="Return"){$modo=$CI->lang->line("common_devolution");}
	if($controller=="Pendiente")
	{
	$table_data_row='<div class="files_result_table" name="row_'.$entrada->receiving_id.'" id="row_'.$entrada->receiving_id.'">';
	$table_data_row.='<div id="seleccionar_'.$entrada->receiving_id.'" class="box_result_table_c"><div id="seleccionar_t_'.$entrada->receiving_id.'" class="soloVisibleResponsivamente">'.$CI->lang->line('common_seleccionar').'</div><input type="checkbox" class ="chksbxs" name="chksbxs[]"id="chksbxs[$entrada->receiving_id]" value="'.$entrada->receiving_id.'"/></div>';
	$table_data_row.='<div id="col2'.$entrada->receiving_id.'" class="box_result_table_c"><div id="col2_t'.$entrada->receiving_id.'" class="soloVisibleResponsivamente">'.$CI->lang->line('common_date').'<span class="icon_more"></span></br>#'.$entrada->receiving_id.'</div>'.character_limiter($entrada->receiving_time,13).'</div>';
	$table_data_row.='<div id="col3'.$entrada->receiving_id.'" class="box_result_table_c"><div id="col3_t'.$entrada->receiving_id.'" class="soloVisibleResponsivamente">'.$CI->lang->line('module_receivings_id').'</div>'.character_limiter($entrada->receiving_id,13).'</div>';
	$table_data_row.='<div id="col4'.$entrada->receiving_id.'" class="box_result_table_c"><div id="col4_t'.$entrada->receiving_id.'" class="soloVisibleResponsivamente">'.$CI->lang->line('module_supplier').
	'</div>'.anchor($controller_name."/supplier_details/$entrada->receiving_id",character_limiter($supplier_info->company,20),array('class'=>'info_form_ajax','title'=>$CI->lang->line($controller_name.'_update'))).'</div>';
	$table_data_row.='<div id="col5'.$entrada->receiving_id.'" class="box_result_table_c"><div id="col5_t'.$entrada->receiving_id.'" class="soloVisibleResponsivamente">'.$CI->lang->line('module_items').'</div>'.anchor($controller_name."/items_details/$entrada->receiving_id", $CI->lang->line('common_details').' '.$CI->lang->line('module_items'),array('class'=>'ajax-popup-receivings','title'=>$CI->lang->line($controller_name.'_update'))).'</div>';
	$table_data_row.='<div id="col1'.$entrada->receiving_id.'" class="box_result_table_c"><div id="col1_t'.$entrada->receiving_id.'" class="soloVisibleResponsivamente">'.$CI->lang->line('common_edition').'</div>'.anchor($controller_name."/register/".$entrada->receiving_id, $CI->lang->line('common_edit'),array('class'=>'ajax-popup-receivings','title'=>$CI->lang->line('common_edit'))).'</div>';
	$table_data_row.='</div>';
	}
	elseif($controller=="Receive")
	{
	$table_data_row='<div class="files_result_table" name="row_'.$entrada->receiving_id.'" id="row_'.$entrada->receiving_id.'"><input type="hidden" class ="chksbxs" name="chksbxs[]"id="chksbxs[$pedido->pedido_id]" value="'.$entrada->receiving_id.'"/>';
	$table_data_row.='<div id="col2'.$entrada->receiving_id.'" class="box_result_table_d"><div id="col2_t'.$entrada->receiving_id.'" class="soloVisibleResponsivamente">'.$CI->lang->line('common_date').'<span class="icon_more"></span></br>#'.$entrada->receiving_id.'</div>'.character_limiter($entrada->receiving_time,13).'</div>';
	$table_data_row.='<div id="col1'.$entrada->receiving_id.'" class="box_result_table_d"><div id="col1_t'.$entrada->receiving_id.'" class="soloVisibleResponsivamente">'.$CI->lang->line('module_receivings_id').'</div>'.character_limiter($entrada->receiving_id,13).'</div>';
	$table_data_row.='<div id="col3'.$entrada->receiving_id.'" class="box_result_table_d"><div id="col3_t'.$entrada->receiving_id.'" class="soloVisibleResponsivamente">'.$CI->lang->line('module_supplier').
	'</div>'.anchor($controller_name."/supplier_details/$entrada->receiving_id",character_limiter($supplier_info->company,20),array('class'=>'info_form_ajax','title'=>$CI->lang->line($controller_name.'_update'))).'</div>';
	$table_data_row.='<div id="col4'.$entrada->receiving_id.'" class="box_result_table_d"><div id="col4_t'.$entrada->receiving_id.'" class="soloVisibleResponsivamente">'.$CI->lang->line('module_items').'</div>'.anchor($controller_name."/items_details/$entrada->receiving_id", $CI->lang->line('common_details').' '.$CI->lang->line('module_items'),array('class'=>'ajax-popup-receivings','title'=>$CI->lang->line($controller_name.'_update'))).'</div>';
	$table_data_row.='<div id="col5'.$entrada->receiving_id.'" class="box_result_table_d"><div id="col5_t'.$entrada->receiving_id.'" class="soloVisibleResponsivamente">'.$CI->lang->line('common_modes_register').'</div>'.$modo.'</div>';
	$table_data_row.='</div>';
	}
	elseif($controller=="Return")
	{
	$table_data_row='<div class="files_result_table" name="row_'.$entrada->receiving_id.'" id="row_'.$entrada->receiving_id.'"><input type="hidden" class ="chksbxs" name="chksbxs[]"id="chksbxs[$entrada->receiving_id]" value="'.$entrada->receiving_id.'"/>';
	$table_data_row.='<div id="col2'.$entrada->receiving_id.'"  class="box_result_table_d"><div id="col2_t'.$entrada->receiving_id.'" class="soloVisibleResponsivamente">'.$CI->lang->line('common_date').'<span class="icon_more"></span></br>#'.$entrada->receiving_id.'</div>'.character_limiter($entrada->receiving_time,13).'</div>';
	$table_data_row.='<div id="col1'.$entrada->receiving_id.'" class="box_result_table_d"><div id="col1_t'.$entrada->receiving_id.'" class="soloVisibleResponsivamente">'.$CI->lang->line('module_receivings_id').'</div>'.character_limiter($entrada->receiving_id,13).'</div>';
	$table_data_row.='<div id="col3'.$entrada->receiving_id.'" class="box_result_table_d"><div id="col3_t'.$entrada->receiving_id.'" class="soloVisibleResponsivamente">'.$CI->lang->line('module_supplier').
	'</div>'.anchor($controller_name."/supplier_details/$entrada->receiving_id",character_limiter($supplier_info->company,20),array('class'=>'info_form_ajax','title'=>$CI->lang->line($controller_name.'_update'))).'</div>';
	$table_data_row.='<div id="col4'.$entrada->receiving_id.'" class="box_result_table_d"><div id="col4_t'.$entrada->receiving_id.'" class="soloVisibleResponsivamente">'.$CI->lang->line('module_items').'</div>'.anchor($controller_name."/items_details/$entrada->receiving_id", $CI->lang->line('common_details').' '.$CI->lang->line('module_items'),array('class'=>'ajax-popup-receivings','title'=>$CI->lang->line($controller_name.'_update'))).'</div>';
	$table_data_row.='<div id="col5'.$entrada->receiving_id.'" class="box_result_table_d"><div id="col5_t'.$entrada->receiving_id.'" class="soloVisibleResponsivamente">'.$CI->lang->line('common_modes_register').'</div>'.$modo.'</div>';
	$table_data_row.='</div>';
	}
	return $table_data_row;
}
/*Aqui comienza el helper para los item_kits*/
function get_item_kits_manage_table( $item_kits, $controller ){
	$CI =& get_instance();
	$table='<div class="cover_titles_table" id="cover_titles_item_kits">';
	$headers = array('<input type="checkbox" id="select_all" />', 
	$CI->lang->line('module_item_kit'),
	$CI->lang->line('common_category'),
	$CI->lang->line('common_price'),
	$CI->lang->line('module_item_kit_id'),
	$CI->lang->line('common_edition'),
	);
	foreach($headers as $header)
	{
		$table.="<div class='titles_table_c'>$header</div>";
	}
	$table.='</div>';
	$table.='<section class="cover_result_table" id="cover_resultados_tabla">';
	$table.=get_item_kits_manage_table_data_rows( $item_kits, $controller );
	$table.='</section>';
	return $table;
}
/**/
function get_item_kits_manage_table_data_rows($item_kits, $controller,$search=false ){
	$CI =& get_instance();
	$table_data_rows='';
	$table_data_rows_array=array();
	foreach($item_kits->result() as $item_kit)
	{
		$table_data_rows.=get_item_kit_data_row( $item_kit, $controller );
		$table_data_rows_array[$item_kit->item_kit_id]=get_item_kit_data_row( $item_kit, $controller );
	}
	if($item_kits->num_rows()==0)
	{
		$table_data_rows.="<div class='files_result_table'><div class='warning_message' id='empty' >".$CI->lang->line("module_item_kit_null")."</div></div>";
		$table_data_rows_array[0]="<div class='files_result_table'><div class='warning_message' id='empty'>".$CI->lang->line("module_item_kit_null")."</div></div>";;
	}
	if($search==true)
	{
		return $table_data_rows_array;
	}
	else
	{
		return $table_data_rows;
	}
}
/**/
function get_item_kit_data_row($item_kit,$controller){
	$CI =& get_instance();
	$controller_name=strtolower(get_class($CI));
	$config_info=$CI->Appconfig->get_info();
	$coin=$config_info["coin"];
	if($config_info['xchange_show']=="1")$coin=$config_info["coin2"];
	$table_data_row='<div class="files_result_table" name="row_'.$item_kit->item_kit_id.'" id="row_'.$item_kit->item_kit_id.'">';
	$table_data_row.="<div id='seleccionar_".$item_kit->item_kit_id."' class='box_result_table_c'><div id='seleccionar_t_".$item_kit->item_kit_id."'  class='soloVisibleResponsivamente'>".$CI->lang->line('common_seleccionar')."</div><input type='checkbox' class ='chksbxs' name ='chksbxs[]' id='chksbxs[$item_kit->item_kit_id]' value='".$item_kit->item_kit_id."'/></div>";
	/*$table_data_row.='<div id="col2'.$item_kit->item_kit_id.'" class="box_result_table_c"><div  id="col2_t'.$item_kit->item_kit_id.'" class="soloVisibleResponsivamente">'.$CI->lang->line('module_item_kit').' <span class="icon_more"></span></br>'.$item_kit->item_kit_id.'</div>'.character_limiter($item_kit->name,13).'</div>';*/
	$table_data_row.='<div id="col2'.$item_kit->item_kit_id.'" class="box_result_table_c"><div id="col7_t'.$item_kit->item_kit_id.'"  class="soloVisibleResponsivamente">'.$CI->lang->line('module_item_kit').'</div>'.anchor($controller_name."/form_content/$item_kit->item_kit_id", $item_kit->name,array('class'=>'content_forms_ajax','title'=>$CI->lang->line('module_item_kit'))).'</div>';
	/*$table_data_row.='<div id="col3'.$item_kit->item_kit_id.'" class="box_result_table_c"><div id="col3_t'.$item_kit->item_kit_id.'" class="soloVisibleResponsivamente">'.$CI->lang->line('common_category').'</div>'.character_limiter($item_kit->category,13).'</div>';*/
	$table_data_row.='<div id="col3'.$item_kit->item_kit_id.'" class="box_result_table_c"><div id="col3_t'.$item_kit->item_kit_id.'" class="soloVisibleResponsivamente">'.$CI->lang->line('common_category').'</div>'.anchor($controller_name."/form_cat_content/$item_kit->item_kit_id", character_limiter($item_kit->category,13),array('class'=>'content_forms_ajax','title'=>$CI->lang->line('common_category'))).'</div>';
	$table_data_row.='<div id="col1'.$item_kit->item_kit_id.'" class="box_result_table_c"><div  id="col1_t'.$item_kit->item_kit_id.'" class="soloVisibleResponsivamente">'.$CI->lang->line('common_price').'</div><b>'.character_limiter(to_currency($item_kit->kit_price),13).' '.$coin.'</b></div>';
	$table_data_row.='<div id="col4'.$item_kit->item_kit_id.'" class="box_result_table_c"><div  id="col4_t'.$item_kit->item_kit_id.'" class="soloVisibleResponsivamente">'.$CI->lang->line('module_item_kit_id').'</div>'.$item_kit->item_kit_id.'</div>';
	$table_data_row.='<div  id="col5'.$item_kit->item_kit_id.'" class="box_result_table_c"><div  id="col5_t'.$item_kit->item_kit_id.'" class="soloVisibleResponsivamente">'.$CI->lang->line("common_edition").'</div>'.anchor($controller_name."/view/$item_kit->item_kit_id", $CI->lang->line('common_edit'),array('class'=>'ajax-popup-link','title'=>$CI->lang->line($controller_name.'_update'))).'</div>';
	$table_data_row.='</div>';
	return $table_data_row;
}
/**/
function get_table_manual($controller_name){
	$CI =& get_instance();
	$e=0;
	$i=0;
	$contenido="";
	while ($i <= 5) 
		{
			$e =$e+1;
			if(!$CI->lang->line("manual_".$controller_name."_lista".$e))
				{
					$i=6;
				}
			else
			{
				$contenido .="<li title=".$CI->lang->line("manual_".$controller_name."_TitleLista".$e)." ".$CI->lang->line("manual_".$controller_name."_lista".$e)."><span>".$CI->lang->line("manual_".$controller_name."_TitleLista".$e).":</span>".$CI->lang->line("manual_".$controller_name."_lista".$e)."</li>";
			}
		}
	return $contenido;
}

?>