<?php
/*Regresa un array con rangos de tiempo especifico como: mes, la semana pasada etc.*/
function get_simple_date_ranges(){
	$CI =& get_instance();
	$today =  date('Y-m-d');
	$yesterday = date('Y-m-d', mktime(0,0,0,date("m"),date("d")-1,date("Y")));
	$six_days_ago = date('Y-m-d', mktime(0,0,0,date("m"),date("d")-6,date("Y")));
	$start_of_this_month = date('Y-m-d', mktime(0,0,0,date("m"),1,date("Y")));
	$end_of_this_month = date('Y-m-d',strtotime('-1 second',strtotime('+1 month',strtotime(date('m').'/01/'.date('Y').' 00:00:00'))));
	$start_of_last_month = date('Y-m-d', mktime(0,0,0,date("m")-1,1,date("Y")));
	$end_of_last_month = date('Y-m-d',strtotime('-1 second',strtotime('+1 month',strtotime((date('m') - 1).'/01/'.date('Y').' 00:00:00'))));
	$start_of_this_year =  date('Y-m-d', mktime(0,0,0,1,1,date("Y")));
	$end_of_this_year =  date('Y-m-d', mktime(0,0,0,12,31,date("Y")));
	$start_of_last_year =  date('Y-m-d', mktime(0,0,0,1,1,date("Y")-1));
	$end_of_last_year =  date('Y-m-d', mktime(0,0,0,12,31,date("Y")-1));
	$start_of_time =  date('Y-m-d', 0);
	return array(
		0=>$CI->lang->line('module_reports_select_period'),
		$today. '/' . $today 								=> $CI->lang->line('module_reports_hoy'),
		$yesterday. '/' . $yesterday						=> $CI->lang->line('module_reports_ayer'),
		$six_days_ago. '/' . $today 						=> $CI->lang->line('module_reports_semana'),
		$start_of_this_month . '/' . $end_of_this_month		=> $CI->lang->line('module_reports_mes'),
		$start_of_last_month . '/' . $end_of_last_month		=> $CI->lang->line('module_reports_mes_anterior'),
		$start_of_this_year . '/' . $end_of_this_year	 	=> $CI->lang->line('module_reports_year'),
		$start_of_last_year . '/' . $end_of_last_year		=> $CI->lang->line('module_reports_last_year'),
		$start_of_time . '/' . 	$today						=> $CI->lang->line('module_reports_todo'));
}
/*Regresa un array con los meses sin abreviar más un indice para indicar todos*/
function get_full_months(){
	$CI =& get_instance();
	$meses=array(
		'0'  => $CI->lang->line("common_all"),
		'01'  => $CI->lang->line("common_ene"),
		'02'  => $CI->lang->line("common_feb"),
		'03'  => $CI->lang->line("common_mar"),
		'04'  => $CI->lang->line("common_abr"),
		'05'  => $CI->lang->line("common_may"),
		'06'  => $CI->lang->line("common_jun"),
		'07'  => $CI->lang->line("common_jul"),
		'08'  => $CI->lang->line("common_ago"),
		'09'  => $CI->lang->line("common_sep"),
		'10'  => $CI->lang->line("common_oct"),
		'11'  => $CI->lang->line("common_nov"),
		'12'  => $CI->lang->line("common_dic"));
		return $meses;
}
/*Regresa un array con los meses abreviados*/
function get_months(){
	$CI =& get_instance();
	$config_info=$CI->Appconfig->get_info();
	$months = array();
	for($k=1;$k<=12;$k++){
		$cur_month = mktime(0, 0, 0, $k, 1, 2000);
		$mes=date("M",$cur_month);
		if($config_info["language"]=="ES"){
			switch ($mes) {
			case "Jan":
			$mes="Ene";
			break;
			case "Apr":
			$mes="Abr";
			break;
			case "Aug":
			$mes="Ago";
			break;
			case "Dec":
			$mes="Dic";
			break;
			default:
			}
		}
		$months[date("m", $cur_month)] = $mes;
	}
	return $months;
}
/*Regresa un array con los dias del mes*/
function get_days(){
	$days = array();
	for($k=1;$k<=31;$k++){
		$cur_day = mktime(0, 0, 0, 1, $k, 2000);
		$days[date('d',$cur_day)] = date('j',$cur_day);
	}
	return $days;
}
/*Regresa un array con los siguientes diez años*/
function get_years(){
	$years = array();
	for($k=0;$k<10;$k++){
		$years[date("Y")-$k] = date("Y")-$k;
	}
	return $years;
}
/*Regresa un array con las horas del día*/
function get_hours(){
	$hours = array();
	for($k=1;$k<=24;$k++){
		$cur_hour = mktime($k, 0, 0, 0, 0, 0);
		$hours[date("G", $cur_hour)] = date("G",$cur_hour);
	}
	return $hours;
}
/*Regresa un array con los minutos de una hora*/
function get_minutes(){
	$minutes = array();
	for($k=1;$k<=60;$k++){
		$cur_minute = mktime(0, $k, 0, 0, 0, 0);
		$minutes[date("i", $cur_minute)] = date("i",$cur_minute);
	}
	return $minutes;
}
/*Regresa un array de colores hexadecimales aleatorios de longitud solicitada*/
function get_random_colors($how_many){
	$colors = array();
	for($k=0;$k<$how_many;$k++){
		$colors[] = '#'.random_color();
	}
	return $colors;
}
/*Regresa un color hexadecimal aleatorio*/
function random_color(){
    mt_srand((double)microtime()*1000000);
    $c = '';
    while(strlen($c)<6){
        $c .= sprintf("%02X", mt_rand(0, 255));
    }
    return $c;
}
/*Regresa el minuto de la hora actual del dia*/
function selected_minute(){
	$CI =& get_instance();
	$config_info=$CI->Appconfig->get_info();
	date_default_timezone_set("UTC"); 
	$timestamp=time();
	$timezone  = $config_info['timezone'];
	$hora=gmt_to_local($timestamp, $timezone);
	$fecha_hora_actual=unix_to_human($hora,false,'mx');
	$minuto = substr($fecha_hora_actual, -2);
	return $minuto;
}
/*Regresa la hora actual del dia*/
function selected_hour(){
	$CI =& get_instance();
	$config_info=$CI->Appconfig->get_info();
	date_default_timezone_set("UTC"); 
	$timestamp=time();
	$timezone  = $config_info['timezone'];
	$hora=gmt_to_local($timestamp, $timezone);
	$fecha_hora_actual=unix_to_human($hora,false,'mx');
	$hora = substr($fecha_hora_actual, -5,2);
	return $hora;
}
/*Regresa el dia actual*/
function selected_day(){
	$CI =& get_instance();
	$config_info=$CI->Appconfig->get_info();
	date_default_timezone_set("UTC"); 
	$timestamp=time();
	$timezone  = $config_info['timezone'];
	$hora=gmt_to_local($timestamp, $timezone);
	$fecha_hora_actual=unix_to_human($hora,false,'mx');
	$hora = substr($fecha_hora_actual, -8,2);
	return $hora;
}
/*Regresa el mes actual*/
function selected_month(){
	$CI =& get_instance();
	$config_info=$CI->Appconfig->get_info();
	date_default_timezone_set("UTC"); 
	$timestamp=time();
	$timezone  = $config_info['timezone'];
	$hora=gmt_to_local($timestamp, $timezone);
	$fecha_hora_actual=unix_to_human($hora,false,'mx');
	$hora = substr($fecha_hora_actual, -11,2);
	return $hora;
}
/*Regresa el año actual*/
function selected_year(){
	$CI =& get_instance();
	$config_info=$CI->Appconfig->get_info();
	date_default_timezone_set("UTC"); 
	$timestamp=time();
	$timezone  = $config_info['timezone'];
	$hora=gmt_to_local($timestamp, $timezone);
	$fecha_hora_actual=unix_to_human($hora,false,'mx');
	$hora = substr($fecha_hora_actual, -16,4);
	return $hora;
}
?>