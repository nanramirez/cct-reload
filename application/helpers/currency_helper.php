<?php
function to_currency($number){
	$number=vsprintf ("%.10f",$number);
	$split_n=explode('.', $number);
	$decimales=strlen(rtrim($split_n[1], '0'));
	
	 $number = $number=$split_n[0].".".rtrim($split_n[1], '0');
	 $number=number_format($number,$decimales, '.', ',');
	if($split_n[1]==0)$number=number_format($split_n[0], 2, '.', ',');
	
	$CI =& get_instance();
	$currency_symbol = $CI->config->item('currency_symbol') ? $CI->config->item('currency_symbol') : '$';
	return $currency_symbol.$number;
    
}
function to_currency_no_money($number){
	return number_format($number, 2, '.', ',');
}
?>