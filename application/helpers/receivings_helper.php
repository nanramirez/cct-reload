<?php
/**/
function get_receivings_panel_manage_table_data_rows($cart,$update=false){
	$CI =& get_instance();
	$table_data_rows_array=array();
	$table_data_rows='';
	if($update==false)
	{
		foreach(array_reverse($cart, true) as $line=>$single_item)
		{
			$table_data_rows.=get_receivings_panel_data_row($single_item);
		}
	}
	return $table_data_rows;
}
function get_receivings_panel_data_row($item){
	$CI =& get_instance();
	$controller_name=strtolower(get_class($CI));
	$item_id= $item["item_id"];
	$cur_item_info = $CI->Item->get_info($item_id);
	$table_data_row='<div class="cover_result_register_psr" name="row_'.$item['line'].'" id="row_'.$item['line'].'">';
	if($item["is_serialized"]==1)
	{
		$table_data_row.='<div class="filas_items_reg_rec"><div class="soloVisibleResponsivamente_psr">'.$CI->lang->line("common_quantity").'</div><span>'.$item["quantity"].'</span></div>';
	}
	else
	{
		$table_data_row.='<div class="filas_items_reg_rec" id="cantidad_sel"><div class="soloVisibleResponsivamente_psr">'.$CI->lang->line("common_quantity").'</div><span>'.form_input(array('name'=>'quantity','class'=>'edit_item_receiving','id'=>$item['line'],'value'=>$item["quantity"])).'</span></div>';
	}
	$table_data_row.='<div class="filas_items_reg_rec" id="producto_sel"><div class="soloVisibleResponsivamente_psr">'.$CI->lang->line("module_item").'</div><span id="item_name_span">'.character_limiter($item["name"],30).'</span><span id="item_stock_span">['.$cur_item_info->quantity.' '.$CI->lang->line('common_stocks').']</span></div>';
	$table_data_row.='<div class="filas_items_reg_rec"><div class="soloVisibleResponsivamente_psr">'.$CI->lang->line("common_quantity").'</div><span>'.form_input(array('name'=>'price_receiving','class'=>'price_receiving','id'=>$item['line'],'value'=>to_currency($item['price']))).'</span></div>';
	$table_data_row.='<div class="filas_items_reg_rec""><div class="soloVisibleResponsivamente_psr">'.$CI->lang->line("common_quantity").'</div><span id="item_subtotal_span">'.to_currency($item["price"]*$item["quantity"]-$item["price"]*$item["quantity"]*$item["discount"]/100).'</span></div>';
	$table_data_row.='<div  id="opc-edit" class="filas_items_reg_rec"><div class="soloVisibleResponsivamente_psr">'.$CI->lang->line("common_edition").'</div><span id="item_delete_span"><a class="quit" id="'.$item['line'].'" name="quit-item[]" "title"="'.$CI->lang->line('common_quit').'">'.$CI->lang->line('common_quit').'</a></span></div><input type="hidden" id="item_line['.$item['line'].']" value="'.$item['line'].'" name="item_line[]"/>';
	$table_data_row.='</div>';
	return $table_data_row;
}
?>