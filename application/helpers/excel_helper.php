<?php
/**/
function items_to_excel($db_fields){
	error_reporting(E_ALL);
	ini_set('display_errors', TRUE);
	ini_set('display_startup_errors', TRUE);
	define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
	$CI =& get_instance();
	/*$CI->load->library('excel_lib');*/
	/* Estableciendo las propiedades del documento*/
	$CI->excel_lib->getProperties()->setCreator("Brontobyte Team")
	->setLastModifiedBy("Brontobyte Team")
	->setTitle($CI->lang->line('common_candc'))
	->setSubject($CI->lang->line('module_items_export_title'));
get_excel_lines($db_fields,$CI);
/* Glifos misceláneos, UTF-8*/
$CI->excel_lib->getActiveSheet()->getRowDimension(16)->setRowHeight(0);
$CI->excel_lib->getActiveSheet()->getStyle('A1')->getAlignment()->setWrapText(true);
$CI->excel_lib->getActiveSheet()->setTitle($CI->lang->line("module_items"));
/* Establezca el índice de hoja activa en la primera hoja, de modo que Excel lo abra como la primera hoja */
$CI->excel_lib->setActiveSheetIndex(0);
$objWriter = PHPExcel_IOFactory::createWriter($CI->excel_lib, 'Excel2007');
$objWriter->save(str_replace('.php', '.xlsx', __FILE__));
/*echo date('H:i:s') , " File written to " , str_replace('.php', '.xls', pathinfo(__FILE__, PATHINFO_BASENAME)) , EOL;*/
/* Echo memory usage */
/*echo date('H:i:s') , ' Current memory usage: ' , (memory_get_usage(true) / 1024 / 1024) , " MB" , EOL; */
/* Echo memory peak usage*/
/*echo date('H:i:s') , " Peak memory usage: " , (memory_get_peak_usage(true) / 1024 / 1024) , " MB" , EOL; */
/* Echo done*/
/*echo date('H:i:s') , " Done writing files" , EOL;*/
/*echo 'Files have been created in ' , getcwd() , EOL;*/
}
/**/
function get_excel_lines($db_fields,$CI){
	/*$CI =& get_instance();*/
	/*$CI->load->library('excel_lib');*/
	get_excel_headers($CI);
	$i=1;
	foreach($db_fields->result() as $key=>$line)
	{
		get_excel_line($line,$i,$CI);
		$i++;
	}
}
/**/
function get_excel_line($line,$key,$CI){
	/*$CI =& get_instance();*/
	/*$CI->load->library('excel_lib');*/
	$key+=1;
	$CI->excel_lib->setActiveSheetIndex(0)
            ->setCellValue('A'.$key.'', $line->item_id)
			->setCellValue('B'.$key.'', $line->name)
			->setCellValue('C'.$key.'', $line->modelo)
			->setCellValue('D'.$key.'', $line->brand)
			->setCellValue('E'.$key.'', $line->category)
			->setCellValue('F'.$key.'', $line->cost_price)
			->setCellValue('G'.$key.'', $line->unit_price)
			->setCellValue('H'.$key.'', $line->other_price)
			->setCellValue('I'.$key.'', $line->quantity)
			->setCellValue('J'.$key.'', $line->reorder_level)
			->setCellValue('K'.$key.'', $line->item_number." ")
			->setCellValue('L'.$key.'', $line->location)
			->setCellValue('M'.$key.'', $line->description);
}
/**/
function get_excel_headers($CI){
			/*$CI =& get_instance();*/
			/*$CI->load->library('excel_lib');*/
			$CI->excel_lib->setActiveSheetIndex(0)
            ->setCellValue('A1', $CI->lang->line("module_item_id"))
            ->setCellValue('B1', $CI->lang->line("module_item"))
			->setCellValue('C1', $CI->lang->line("module_item_modelo"))
            ->setCellValue('D1', $CI->lang->line("module_item_brand"))
			->setCellValue('E1', $CI->lang->line("common_category"))
			->setCellValue('F1', $CI->lang->line("common_cost"))
			->setCellValue('G1', $CI->lang->line("common_price"))
			->setCellValue('H1', $CI->lang->line("common_price_special"))
			->setCellValue('I1', $CI->lang->line("module_item_unit_stock"))
			->setCellValue('J1', $CI->lang->line("module_item_stock_minimum"))
			->setCellValue('K1', $CI->lang->line("common_barcode"))
			->setCellValue('L1', $CI->lang->line("common_location"))
			->setCellValue('M1', $CI->lang->line("common_description"));
}
?>