-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 13-09-2018 a las 03:21:54
-- Versión del servidor: 10.1.35-MariaDB
-- Versión de PHP: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `cct-reload`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cc_app_config`
--

CREATE TABLE `cc_app_config` (
  `key` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cc_app_config`
--

INSERT INTO `cc_app_config` (`key`, `value`) VALUES
('address', 'Avenida siempre viva #SN'),
('coin', 'MXN'),
('coin2', 'USD'),
('company', 'Cash and Control'),
('language', 'EN'),
('notify_level', '3'),
('phone', '322 5454584'),
('print_receipt', '0'),
('timezone', 'UM6'),
('user_cfdi', ''),
('xchange_rate', '17.7'),
('xchange_show', '0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cc_employees`
--

CREATE TABLE `cc_employees` (
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `person_id` int(10) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cc_employees`
--

INSERT INTO `cc_employees` (`username`, `password`, `person_id`, `deleted`) VALUES
('NaN', '21232f297a57a5a743894a0e4a801fc3', 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cc_fiscales`
--

CREATE TABLE `cc_fiscales` (
  `person_id` int(10) NOT NULL,
  `rfc` varchar(20) DEFAULT NULL,
  `razon` varchar(255) DEFAULT NULL,
  `ce` varchar(255) DEFAULT NULL,
  `noExterior` varchar(10) DEFAULT NULL,
  `noInterior` varchar(255) DEFAULT NULL,
  `colonia` varchar(255) DEFAULT NULL,
  `localidad` varchar(255) DEFAULT NULL,
  `municipio` varchar(255) DEFAULT NULL,
  `estado` varchar(255) DEFAULT NULL,
  `pais` varchar(255) DEFAULT NULL,
  `codigoPostal` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cc_fiscales`
--

INSERT INTO `cc_fiscales` (`person_id`, `rfc`, `razon`, `ce`, `noExterior`, `noInterior`, `colonia`, `localidad`, `municipio`, `estado`, `pais`, `codigoPostal`) VALUES
(100, 'XAXX010101000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cc_formas_pago`
--

CREATE TABLE `cc_formas_pago` (
  `formas_pago_id` int(255) UNSIGNED NOT NULL,
  `recurso_id` int(10) DEFAULT NULL,
  `nombre` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cc_formas_pago`
--

INSERT INTO `cc_formas_pago` (`formas_pago_id`, `recurso_id`, `nombre`) VALUES
(1, 1, 'Efectivo'),
(2, 2, 'Efectivo dlls');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cc_gastos`
--

CREATE TABLE `cc_gastos` (
  `gasto_id` int(255) NOT NULL,
  `fecha` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `concepto` varchar(255) NOT NULL,
  `recurso_id` int(10) NOT NULL,
  `estado` varchar(15) NOT NULL DEFAULT 'Pendiente',
  `cantidad` double(20,10) NOT NULL DEFAULT '0.0000000000',
  `tipo` varchar(15) NOT NULL DEFAULT 'Efectivo',
  `category` varchar(255) NOT NULL,
  `receiving_id` int(10) NOT NULL DEFAULT '-1',
  `coin` varchar(20) NOT NULL,
  `deducible` tinyint(4) NOT NULL DEFAULT '0',
  `tax` double(20,10) NOT NULL DEFAULT '0.0000000000',
  `suscrito` varchar(255) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cc_inventory`
--

CREATE TABLE `cc_inventory` (
  `trans_id` int(11) NOT NULL,
  `trans_items` int(11) NOT NULL DEFAULT '0',
  `trans_user` int(11) NOT NULL DEFAULT '0',
  `trans_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `trans_comment` text NOT NULL,
  `trans_inventory` varchar(11) NOT NULL DEFAULT '0',
  `trans_type` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cc_items`
--

CREATE TABLE `cc_items` (
  `name` varchar(255) NOT NULL,
  `category` varchar(255) NOT NULL,
  `supplier_id` int(11) DEFAULT '100',
  `item_number` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `cost_price` double(20,10) NOT NULL DEFAULT '0.0000000000',
  `unit_price` double(20,10) NOT NULL DEFAULT '0.0000000000',
  `other_price` double(20,10) NOT NULL DEFAULT '0.0000000000',
  `quantity` double(15,2) NOT NULL DEFAULT '0.00',
  `reorder_level` double(15,2) NOT NULL DEFAULT '0.00',
  `location` varchar(255) NOT NULL DEFAULT 'Principal',
  `item_id` int(10) NOT NULL,
  `allow_alt_description` tinyint(1) NOT NULL DEFAULT '0',
  `is_serialized` tinyint(1) DEFAULT '0',
  `deleted` int(1) NOT NULL DEFAULT '0',
  `publicar` tinyint(1) NOT NULL DEFAULT '0',
  `destacar` tinyint(4) DEFAULT '0',
  `promocionar` tinyint(4) DEFAULT '0',
  `show_price` tinyint(4) DEFAULT '1',
  `show_stock` tinyint(4) DEFAULT '1',
  `brand` varchar(255) DEFAULT NULL,
  `modelo` varchar(255) DEFAULT NULL,
  `img_qty` int(2) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cc_items_cfdi`
--

CREATE TABLE `cc_items_cfdi` (
  `item_id` int(10) NOT NULL,
  `c_ClaveUnidad` varchar(255) NOT NULL,
  `c_ClaveProdServ` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cc_items_contenido`
--

CREATE TABLE `cc_items_contenido` (
  `name_element` varchar(255) NOT NULL,
  `cont_element` text NOT NULL,
  `lang_element` varchar(255) NOT NULL,
  `item_id` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cc_items_taxes`
--

CREATE TABLE `cc_items_taxes` (
  `item_id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `percent` double(20,10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cc_item_kits`
--

CREATE TABLE `cc_item_kits` (
  `item_kit_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `kit_number` varchar(255) DEFAULT NULL,
  `category` varchar(255) NOT NULL,
  `kit_price` double(20,10) NOT NULL DEFAULT '0.0000000000',
  `other_price` double(20,10) NOT NULL DEFAULT '0.0000000000',
  `publicar` tinyint(4) NOT NULL DEFAULT '0',
  `show_price` tinyint(4) DEFAULT '1',
  `promocionar` tinyint(4) DEFAULT '0',
  `destacar` tinyint(4) DEFAULT '0',
  `deleted` int(1) NOT NULL DEFAULT '0',
  `expiracion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `servicio` tinyint(4) NOT NULL DEFAULT '0',
  `img_qty` int(2) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cc_item_kits_cfdi`
--

CREATE TABLE `cc_item_kits_cfdi` (
  `item_kit_id` int(10) NOT NULL,
  `c_ClaveUnidad` varchar(255) NOT NULL,
  `c_ClaveProdServ` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cc_item_kit_items`
--

CREATE TABLE `cc_item_kit_items` (
  `item_kit_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `quantity` double(15,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cc_kits_contenido`
--

CREATE TABLE `cc_kits_contenido` (
  `name_element` varchar(255) NOT NULL,
  `cont_element` text NOT NULL,
  `lang_element` varchar(255) NOT NULL,
  `kit_id` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cc_locations`
--

CREATE TABLE `cc_locations` (
  `nombre_location` varchar(255) NOT NULL,
  `item_id` int(20) NOT NULL,
  `cantidad` double(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cc_modules`
--

CREATE TABLE `cc_modules` (
  `name_lang_key` varchar(255) NOT NULL,
  `desc_lang_key` varchar(255) NOT NULL,
  `sort` int(10) NOT NULL,
  `module_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cc_modules`
--

INSERT INTO `cc_modules` (`name_lang_key`, `desc_lang_key`, `sort`, `module_id`) VALUES
('module_employees', 'module_employees_desc', 80, 'employees'),
('module_gastos', 'module_gastos_desc', 130, 'gastos'),
('module_items', 'module_items_desc', 20, 'items'),
('module_item_kits', 'module_item_kits_desc', 30, 'item_kits'),
('module_receivings', 'module_receivings_desc', 60, 'receivings'),
('module_recursos', 'module_recursos_desc', 90, 'recursos'),
('module_suppliers', 'module_suppliers_desc', 40, 'suppliers');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cc_notificaciones`
--

CREATE TABLE `cc_notificaciones` (
  `notificacion_id` int(11) NOT NULL,
  `time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `notificacion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `modulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `prioridad` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'medium',
  `visto` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cc_people`
--

CREATE TABLE `cc_people` (
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `phone_number` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `address_1` text NOT NULL,
  `address_2` text,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `zip` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `comments` text NOT NULL,
  `person_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cc_people`
--

INSERT INTO `cc_people` (`first_name`, `last_name`, `phone_number`, `email`, `address_1`, `address_2`, `city`, `state`, `zip`, `country`, `comments`, `person_id`) VALUES
('Nan', 'Ramirez', '555-555-5555', 'nan@brontobytemx.com', 'Address 1', '', '', '', '', '', '', 1),
('Persona', 'Genérica', '', '', '', '', '', '', '', '', '', 100);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cc_permissions`
--

CREATE TABLE `cc_permissions` (
  `module_id` varchar(255) NOT NULL,
  `person_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cc_permissions`
--

INSERT INTO `cc_permissions` (`module_id`, `person_id`) VALUES
('employees', 1),
('gastos', 1),
('items', 1),
('item_kits', 1),
('receivings', 1),
('recursos', 1),
('suppliers', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cc_receivings`
--

CREATE TABLE `cc_receivings` (
  `receiving_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `supplier_id` int(10) DEFAULT NULL,
  `employee_id` int(10) NOT NULL DEFAULT '0',
  `comment` text NOT NULL,
  `receiving_id` int(10) NOT NULL,
  `payment_type` varchar(512) DEFAULT NULL,
  `coin` varchar(20) NOT NULL,
  `total` double(20,10) NOT NULL,
  `modo` varchar(20) NOT NULL DEFAULT 'Receive',
  `facturada` tinyint(4) DEFAULT '0',
  `tax` double(20,10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cc_receivings_items`
--

CREATE TABLE `cc_receivings_items` (
  `receiving_id` int(10) NOT NULL DEFAULT '0',
  `item_id` int(10) NOT NULL DEFAULT '0',
  `description` varchar(30) DEFAULT NULL,
  `serialnumber` varchar(30) DEFAULT NULL,
  `line` int(3) NOT NULL,
  `quantity_purchased` int(10) NOT NULL DEFAULT '0',
  `item_cost_price` double(20,10) NOT NULL,
  `item_unit_price` double(20,10) NOT NULL,
  `discount_percent` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cc_receivings_items_taxes`
--

CREATE TABLE `cc_receivings_items_taxes` (
  `receiving_id` int(10) NOT NULL,
  `item_id` int(10) NOT NULL,
  `line` int(3) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `percent` double(15,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cc_recursos`
--

CREATE TABLE `cc_recursos` (
  `recurso_id` int(255) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `coin` varchar(20) NOT NULL,
  `recurso` double(20,10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cc_recursos`
--

INSERT INTO `cc_recursos` (`recurso_id`, `nombre`, `coin`, `recurso`) VALUES
(1, 'Pesos', 'MXN', 0.0000000000),
(2, 'Dolares', 'USD', 0.0000000000);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cc_recursos_operaciones`
--

CREATE TABLE `cc_recursos_operaciones` (
  `operation_id` int(255) NOT NULL,
  `recurso_id` int(255) NOT NULL,
  `operation_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `coin` varchar(20) NOT NULL,
  `monto` double(20,10) NOT NULL,
  `saldo_anterior` double(20,10) NOT NULL,
  `saldo_restante` double(20,10) NOT NULL,
  `concepto` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cc_sessions`
--

CREATE TABLE `cc_sessions` (
  `id` varchar(128) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cc_suppliers`
--

CREATE TABLE `cc_suppliers` (
  `person_id` int(10) NOT NULL,
  `company` varchar(255) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `accounts_number` tinytext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cc_suppliers`
--

INSERT INTO `cc_suppliers` (`person_id`, `company`, `deleted`, `accounts_number`) VALUES
(100, 'Generic Company', 0, NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cc_app_config`
--
ALTER TABLE `cc_app_config`
  ADD PRIMARY KEY (`key`);

--
-- Indices de la tabla `cc_employees`
--
ALTER TABLE `cc_employees`
  ADD PRIMARY KEY (`person_id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indices de la tabla `cc_fiscales`
--
ALTER TABLE `cc_fiscales`
  ADD UNIQUE KEY `rfc` (`rfc`),
  ADD KEY `person_id` (`person_id`);

--
-- Indices de la tabla `cc_formas_pago`
--
ALTER TABLE `cc_formas_pago`
  ADD PRIMARY KEY (`formas_pago_id`);

--
-- Indices de la tabla `cc_gastos`
--
ALTER TABLE `cc_gastos`
  ADD PRIMARY KEY (`gasto_id`);

--
-- Indices de la tabla `cc_inventory`
--
ALTER TABLE `cc_inventory`
  ADD PRIMARY KEY (`trans_id`),
  ADD KEY `cc_inventory_ibfk_1` (`trans_items`),
  ADD KEY `cc_inventory_ibfk_2` (`trans_user`);

--
-- Indices de la tabla `cc_items`
--
ALTER TABLE `cc_items`
  ADD PRIMARY KEY (`item_id`),
  ADD UNIQUE KEY `item_number` (`item_number`),
  ADD KEY `cc_items_ibfk_1` (`supplier_id`);

--
-- Indices de la tabla `cc_items_cfdi`
--
ALTER TABLE `cc_items_cfdi`
  ADD PRIMARY KEY (`item_id`);

--
-- Indices de la tabla `cc_items_taxes`
--
ALTER TABLE `cc_items_taxes`
  ADD PRIMARY KEY (`item_id`,`name`,`percent`);

--
-- Indices de la tabla `cc_item_kits`
--
ALTER TABLE `cc_item_kits`
  ADD PRIMARY KEY (`item_kit_id`),
  ADD UNIQUE KEY `kit_number` (`kit_number`);

--
-- Indices de la tabla `cc_item_kits_cfdi`
--
ALTER TABLE `cc_item_kits_cfdi`
  ADD PRIMARY KEY (`item_kit_id`);

--
-- Indices de la tabla `cc_item_kit_items`
--
ALTER TABLE `cc_item_kit_items`
  ADD PRIMARY KEY (`item_kit_id`,`item_id`,`quantity`),
  ADD KEY `cc_item_kit_items_ibfk_2` (`item_id`);

--
-- Indices de la tabla `cc_modules`
--
ALTER TABLE `cc_modules`
  ADD PRIMARY KEY (`module_id`),
  ADD UNIQUE KEY `desc_lang_key` (`desc_lang_key`),
  ADD UNIQUE KEY `name_lang_key` (`name_lang_key`);

--
-- Indices de la tabla `cc_notificaciones`
--
ALTER TABLE `cc_notificaciones`
  ADD PRIMARY KEY (`notificacion_id`);

--
-- Indices de la tabla `cc_people`
--
ALTER TABLE `cc_people`
  ADD PRIMARY KEY (`person_id`),
  ADD UNIQUE KEY `cc_people` (`email`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indices de la tabla `cc_permissions`
--
ALTER TABLE `cc_permissions`
  ADD PRIMARY KEY (`module_id`,`person_id`),
  ADD KEY `person_id` (`person_id`);

--
-- Indices de la tabla `cc_receivings`
--
ALTER TABLE `cc_receivings`
  ADD PRIMARY KEY (`receiving_id`),
  ADD KEY `supplier_id` (`supplier_id`),
  ADD KEY `employee_id` (`employee_id`);

--
-- Indices de la tabla `cc_receivings_items`
--
ALTER TABLE `cc_receivings_items`
  ADD PRIMARY KEY (`receiving_id`,`item_id`,`line`),
  ADD KEY `item_id` (`item_id`);

--
-- Indices de la tabla `cc_receivings_items_taxes`
--
ALTER TABLE `cc_receivings_items_taxes`
  ADD PRIMARY KEY (`receiving_id`,`item_id`,`line`,`name`,`percent`),
  ADD KEY `item_id` (`item_id`);

--
-- Indices de la tabla `cc_recursos`
--
ALTER TABLE `cc_recursos`
  ADD PRIMARY KEY (`recurso_id`);

--
-- Indices de la tabla `cc_recursos_operaciones`
--
ALTER TABLE `cc_recursos_operaciones`
  ADD PRIMARY KEY (`operation_id`);

--
-- Indices de la tabla `cc_sessions`
--
ALTER TABLE `cc_sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cc_sessions_timestamp` (`timestamp`);

--
-- Indices de la tabla `cc_suppliers`
--
ALTER TABLE `cc_suppliers`
  ADD PRIMARY KEY (`person_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cc_formas_pago`
--
ALTER TABLE `cc_formas_pago`
  MODIFY `formas_pago_id` int(255) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `cc_gastos`
--
ALTER TABLE `cc_gastos`
  MODIFY `gasto_id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cc_inventory`
--
ALTER TABLE `cc_inventory`
  MODIFY `trans_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cc_items`
--
ALTER TABLE `cc_items`
  MODIFY `item_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cc_item_kits`
--
ALTER TABLE `cc_item_kits`
  MODIFY `item_kit_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cc_notificaciones`
--
ALTER TABLE `cc_notificaciones`
  MODIFY `notificacion_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cc_people`
--
ALTER TABLE `cc_people`
  MODIFY `person_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;

--
-- AUTO_INCREMENT de la tabla `cc_receivings`
--
ALTER TABLE `cc_receivings`
  MODIFY `receiving_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cc_recursos`
--
ALTER TABLE `cc_recursos`
  MODIFY `recurso_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `cc_recursos_operaciones`
--
ALTER TABLE `cc_recursos_operaciones`
  MODIFY `operation_id` int(255) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cc_employees`
--
ALTER TABLE `cc_employees`
  ADD CONSTRAINT `cc_employees_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `cc_people` (`person_id`);

--
-- Filtros para la tabla `cc_fiscales`
--
ALTER TABLE `cc_fiscales`
  ADD CONSTRAINT `cc_fiscales_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `cc_people` (`person_id`);

--
-- Filtros para la tabla `cc_inventory`
--
ALTER TABLE `cc_inventory`
  ADD CONSTRAINT `cc_inventory_ibfk_1` FOREIGN KEY (`trans_items`) REFERENCES `cc_items` (`item_id`),
  ADD CONSTRAINT `cc_inventory_ibfk_2` FOREIGN KEY (`trans_user`) REFERENCES `cc_employees` (`person_id`);

--
-- Filtros para la tabla `cc_items`
--
ALTER TABLE `cc_items`
  ADD CONSTRAINT `cc_items_ibfk_1` FOREIGN KEY (`supplier_id`) REFERENCES `cc_suppliers` (`person_id`);

--
-- Filtros para la tabla `cc_items_cfdi`
--
ALTER TABLE `cc_items_cfdi`
  ADD CONSTRAINT `cc_items_cfdi_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `cc_items` (`item_id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `cc_items_taxes`
--
ALTER TABLE `cc_items_taxes`
  ADD CONSTRAINT `cc_items_taxes_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `cc_items` (`item_id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `cc_item_kit_items`
--
ALTER TABLE `cc_item_kit_items`
  ADD CONSTRAINT `cc_item_kit_items_ibfk_1` FOREIGN KEY (`item_kit_id`) REFERENCES `cc_item_kits` (`item_kit_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `cc_item_kit_items_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `cc_items` (`item_id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `cc_permissions`
--
ALTER TABLE `cc_permissions`
  ADD CONSTRAINT `cc_permissions_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `cc_employees` (`person_id`),
  ADD CONSTRAINT `cc_permissions_ibfk_2` FOREIGN KEY (`module_id`) REFERENCES `cc_modules` (`module_id`);

--
-- Filtros para la tabla `cc_receivings`
--
ALTER TABLE `cc_receivings`
  ADD CONSTRAINT `cc_receivings_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `cc_employees` (`person_id`),
  ADD CONSTRAINT `cc_receivings_ibfk_2` FOREIGN KEY (`supplier_id`) REFERENCES `cc_suppliers` (`person_id`);

--
-- Filtros para la tabla `cc_receivings_items`
--
ALTER TABLE `cc_receivings_items`
  ADD CONSTRAINT `cc_receivings_items_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `cc_items` (`item_id`),
  ADD CONSTRAINT `cc_receivings_items_ibfk_2` FOREIGN KEY (`receiving_id`) REFERENCES `cc_receivings` (`receiving_id`);

--
-- Filtros para la tabla `cc_receivings_items_taxes`
--
ALTER TABLE `cc_receivings_items_taxes`
  ADD CONSTRAINT `cc_receivings_items_taxes_ibfk_1` FOREIGN KEY (`receiving_id`) REFERENCES `cc_receivings_items` (`receiving_id`),
  ADD CONSTRAINT `cc_receivings_items_taxes_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `cc_items` (`item_id`);

--
-- Filtros para la tabla `cc_suppliers`
--
ALTER TABLE `cc_suppliers`
  ADD CONSTRAINT `cc_suppliers_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `cc_people` (`person_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
