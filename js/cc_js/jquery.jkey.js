/*
	Copyright (c) 2011 Oscar Godson ( http://oscargodson.com ) and Sebastian Nitu ( http://sebnitu.com )
	
	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:
	
	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.
	
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
	
	More infomation on http://oscargodson.com/labs/jkey
	or fork it at https://github.com/OscarGodson/jKey
	
	Special thanks to Macy Abbey
*/
(function($) {
	$.fn.jkey = function(keyCombo,options,callback) {

  /*Verificar si el elemento seleccionado fue la ventana y hacerlo documento*/
  /*Hacemos esto porque IE fallará si selecciona ventana ya que no puede adjuntar*/
  /*pulsa la tecla en la ventana en IE*/
  var $this = this;
  if(this[0] && !this[0].parentNode){ $this = document; }

		/* Guarde los códigos de tecla en el objeto JSON */
		var keyCodes = { 
    /* iniciar las teclas a-z */
    'a' : 65,
    'b' : 66,
    'c' : 67,
    'd' : 68,
    'e' : 69,
    'f' : 70,
    'g' : 71,
    'h' : 72,
    'i' : 73,
    'j' : 74,
    'k' : 75,
    'l' : 76,
    'm' : 77,
    'n' : 78,
    'o' : 79,
    'p' : 80,
    'q' : 81,
    'r' : 82,
    's' : 83,
    't' : 84,
    'u' : 85,
    'v' : 86,
    'w' : 87,
    'x' : 88,
    'y' : 89,
    'z' : 90,
    /* comenzar las teclas numéricas */
    '0' : 48,
    '1' : 49,
    '2' : 50,
    '3' : 51,
    '4' : 52,
    '5' : 53,
    '6' : 54,
    '7' : 55,
    '8' : 56,
    '9' : 57,
    /* inicia las teclas f */
    'f1' : 112,
    'f2' : 113,
    'f3' : 114,
    'f4' : 115,
    'f5' : 116,
    'f6' : 117,
    'f7' : 118,
    'f8' : 119,
    'f9' : 120,
    'f10': 121,
    'f11': 122,
    'f12': 123,
    /* inicia las teclas modificadoras */
    'shift' : 16,
    'ctrl' : 17,
    'control' : 17,
    'alt' : 18,
    'option' : 18, /*Tecla Mac OS*/
    'opt' : 18, /*Tecla Mac OS*/
    'cmd' : 224, /*Tecla Mac OS*/
    'command' : 224, /*Tecla Mac OS*/
    'fn' : 255, /*probado en Lenovo ThinkPad*/
    'function' : 255, /*probado en Lenovo ThinkPad*/
    /* Misc. Keys */
    'backspace' : 8,
    'osxdelete' : 8, /*Versión de Mac OS de retroceso*/
    'enter' : 13,
    'return' : 13, /*Versión de Mac OS de "enter"*/
    'space':32,
    'spacebar':32,
    'esc':27,
    'escape':27,
    'tab':9,
    'capslock':20,
    'capslk':20,
    'super':91,
    'windows':91,
    'insert':45,
    'delete':46, /*NO LA TECLA DE ELIMINACIÓN DE OS X!*/
    'home':36,
    'end':35,
    'pgup':33,
    'pageup':33,
    'pgdn':34,
    'pagedown':34,
    /* Teclas de flecha */
    'left' : 37,
    'up'   : 38,
    'right': 39,
    'down' : 40,
    /* teclas especiales */
    '!':49,
    '@':50,
    '#':51,
    '$':52,
    '%':53,
    '^':54,
    '&':55,
    '*':56,
    '(':57,
    ')':48,
    '`':96,
    '~':96,
    '-':45,
    '_':45,
    '=':187,
    '+':187,
    '[':219,
    '{':219,
    ']':221,
    '}':221,
    '\\':220, /*en realidad es un \ pero hay dos para escapar del original*/
    '|':220,
    ';':59,
    ':':59,
    "'":222,
    '"':222,
    ',':188,
    '<':188,
    '.':190,
    '>':190,
    '/':191,
    '?':191
		};

		var x = '';
		var y = '';
		if(typeof options == 'function' && typeof callback == 'undefined'){
			callback = options;
			options = false;
		}

		/* IE tiene problemas aquí ... entonces, "convertimos" a String () :( */
		if(keyCombo.toString().indexOf(',') > -1){ /* Si se seleccionan varias teclas */
			var keySplit = keyCombo.match(/[a-zA-Z0-9]+/gi);
		}
		else { /* De lo contrario, almacena esta única tecla */
			var keySplit = [keyCombo];
		}
		for(x in keySplit){ /*Para cada tecla en la matriz ...*/
			if(!keySplit.hasOwnProperty(x)) { continue; }
			/* Lo mismo que arriba para toString () e IE */
			if(keySplit[x].toString().indexOf('+') > -1){
				/* La selección de teclas por usuario es una combinación de teclas */
				/* Crear un conjunto combinado y dividir la combinación de teclas */ 
				var combo = [];
				var comboSplit = keySplit[x].split('+');
				/* Guarde los códigos de tecla para cada elemento en la combinación de teclas */ 
				for(y in comboSplit){
					combo[y] = keyCodes[ comboSplit[y] ];
				} 
				keySplit[x] = combo;
			}
			else {
				/* De lo contrario, es solo un comando de tecla normal y único */
				keySplit[x] = keyCodes[ keySplit[x] ];
			}
		}
			
		function swapJsonKeyValues(input) {
			var one, output = {};
			for (one in input) {
				if (input.hasOwnProperty(one)) {
					output[input[one]] = one;
				}
			}
			return output;
		}
			
		var keyCodesSwitch = swapJsonKeyValues(keyCodes);
			
		return this.each(function() {
			$this = $(this);
			
			/*Crear matriz de teclas activas*/ 
			/* Esta matriz almacenará todas las teclas que se están presionando actualmente */
			var activeKeys = [];
			$this.bind('keydown.jkey',function(e){
			/* Guarde la tecla presionada */
			activeKeys[ e.keyCode ] = e.keyCode;
	
			if($.inArray(e.keyCode, keySplit) > -1){ /* Si la tecla que el usuario presionó coincide con cualquier tecla, el desarrollador estableció un código de tecla con ... */
				if(typeof callback == 'function'){ /* y proporcionaron una función de devolución de llamada */
					callback.call(this, keyCodesSwitch[e.keyCode], e ); /* activar la devolución de llamada y ... */
					if(options === false){
						e.preventDefault(); /* cancelar la normal */
					}
				}
			}
			else { /* De lo contrario, la tecla no coincide, lo que significa que es una combinación de teclas o simplemente no existe */ 
				/* Verifique si los elementos individuales en el combo de tecla coinciden con lo que se presionó */
				for(x in keySplit){
					if($.inArray(e.keyCode, keySplit[x]) > -1){
						/* Inicia la variable activa */ 
						var active = 'unchecked';
						
						/* Todas las teclas individuales en el combo con las teclas que se están presionando actualmente */
						for(y in keySplit[x]) {
							if(active != false) {
								if($.inArray(keySplit[x][y], activeKeys) > -1){
									active = true;
								}
								else {
									active = false;
								}
							}
						}
						/* Si se presionan todas las teclas del combo, activo será igual a verdadero */ 
						if(active === true){
							if(typeof callback == 'function'){ /* y proporcionaron una función de devolución de llamada */
								
								var activeString = '';
								
								for(var z in activeKeys) {
									if (activeKeys[z] != '') {
										activeString += keyCodesSwitch[ activeKeys[z] ] + '+';
									}
								}
								activeString = activeString.substring(0, activeString.length - 1);
								callback.call(this, activeString, e); /* activar la devolución de llamada y ... */
								if(options === false){
									e.preventDefault(); /* cancelar la normal */
								}
							}
						}
					}
				}
			} /* fin de si en matriz */ 
			}).bind('keyup.jkey',function(e) {
				/* Eliminar la tecla actual presionar */
				activeKeys[ e.keyCode ] = '';
			});
		});
	}
})(jQuery);