/* */
function search_module(base_url,module){
	/* Busca elementos segun el modudulo y su filtro */
	$( "#search" ).autocomplete({
	source: function (request, response) {
		$.ajax({
			type: "POST",
			url:base_url+"index.php/"+module+"/suggest/",
			data: {q:$('#search').val()},
			success: response,
			dataType: 'json',
			minLength: 2,
			/* delay: 100, */
			cache: false
		}).done(function(data)
		{
			if($(".ui-autocomplete" ).css("height").replace(/[a-z]/g, '')>201)
			{
				$(".ui-autocomplete" ).css("height", "200px")
			}
			else
			{
				$(".ui-autocomplete" ).css("height", "auto")
			}
		});
	},
	response: function( event, ui ) 
	{
		$.ajax(
		{
			type: "POST",
			url:base_url+"index.php/"+module+"/search/",
			data: {search:$('#search').val()},
			dataType: 'json',
			minLength: 3,
			/* delay: 100, */
			cache: false
		}).done(function(data)
		{
			$( ".files_result_table" ).remove();
			$( "#cover_resultados_tabla" ).fadeIn(500,function()
			{
				jQuery.each( data.data_rows, function( i, val ) 
				{
					var parshtml=$.parseHTML(val);	
					$("#cover_resultados_tabla").add().prepend(parshtml);
					$("#cover_resultados_tabla").prepend(parshtml);
				});
				info_forms(module,base_url);
				form_ajax(module,base_url);
				content_forms(module,base_url);
				items_forms(module,base_url);
			});
		});
	},
	select: function( event, ui )
	{
		$( "#search" ).autocomplete( "close" );
		$.ajax(
		{
			type: "POST",
			url:base_url+"index.php/"+module+"/search/",
			data: {search:$('#search').val()},
			dataType: 'json',
			minLength: 3,
			/* delay: 100, */
			cache: false
		}).done(function(data)
		{
			$( ".files_result_table" ).remove();
			$( "#cover_resultados_tabla" ).fadeIn(500,function()
			{
				jQuery.each( data.data_rows, function( i, val )
				{
					var parshtml=$.parseHTML(val);
					$("#cover_resultados_tabla").add().prepend(parshtml);
					$("#cover_resultados_tabla").prepend(parshtml);
				});
				info_forms(module,base_url);
				form_ajax(module,base_url);
				content_forms(module,base_url);
				items_forms(module,base_url);
			});
		});
	}
	});
	/*  */
	$("#search").click(function()
	{
		$("#search").select();
	});
	$("#search").keypress("autocompleteresponse", function(e)
	{
		if(e.which == 13)
		{
			$( "#search" ).autocomplete( "close" );
			$.ajax(
			{
				type: "POST",
				url:base_url+"index.php/"+module+"/search/",
				data: {search:$('#search').val()},
				dataType: 'json',
				minLength: 3,
				/* delay: 100, */
				cache: false
			}).done(function(data)
			{
				$( ".files_result_table" ).remove();
				$( "#cover_resultados_tabla" ).fadeIn(500,function(){
				jQuery.each( data.data_rows, function( i, val ) 
				{
					var parshtml=$.parseHTML(val);	
					$("#cover_resultados_tabla").add().prepend(parshtml);
					$("#cover_resultados_tabla").prepend(parshtml);
				});
				info_forms(module,base_url);
				form_ajax(module,base_url);
				content_forms(module,base_url);
				items_forms(module,base_url);
				});
			});
			/* alert('Enter'); */
		}
	});
}
/* Para los modulos que usan fecha y estados en el filtrado */
function set_filtro_manage(module,base_url){
	$(".filtro_changer").change( function (evt) {
	var filtro_data={
	meses:$("#meses").val(),
	estado:$("#estado").val()
	}
	if(module==='cobros' || module==='gastos')
	{
		if(filtro_data.meses!="0")
		{
		$(".box-pagination").css("display","none");
		}
	}
	$.ajax(
	{
		type: "POST",
		url:base_url+"index.php/"+module+"/filtro/",
		data: filtro_data,
		dataType: 'json',
		minLength: 3,
		/* delay: 100, */
		cache: false
	}).done(function(data){
		$(".cover_titles_table").remove();
		$("#cover_resultados_tabla").remove();
		$( document.body ).scrollTop(200);
		$( "#table_holder" ).fadeIn(3000,function()
		{
		$("#table_holder").add().prepend(data.manage_table);
		});
		enable_select_all(module,base_url);
		info_forms(module,base_url);
		form_ajax(module,base_url);
	});
	});
}
/*Para los modulos que usan forms de contenido en varios idiomas */
function language_filter(module,base_url){
	$(".idioma_changer").change( function (evt) {
		var idioma_data={
		idioma:$("#idioma").val()
		}
			$.ajax(
			{
			type: "POST",
			url:base_url+"index.php/"+module+"/filtro_idioma",
			data:idioma_data,
			dataType: 'html',
			minLength: 3,
			delay: 100,
			cache: false
			});
	});
}
/* */
function search_category(base_url){
	$( "#category" ).autocomplete({
	source: function (request, response) 
	{
	},
	response: function( event, ui ) 
	{
	}
	});
}
/* Para los modulos que sus tablas no se ordenan por fecha o estado */
function show_hide_search_filter(base_url,filtro,module){
	$("#"+filtro+"").prop("checked", true);
	$('#imageDivLink').click(function(){
	if ($('#search_filter_section').css( "display")==="none") {
	$('#search_filter_section').css( "display", "block" );
	$('#f_options').replaceWith('<img src="'+base_url+'images/cc_images/menubar/minus.png" id="f_options">');
	}
	else
	{
	$('#search_filter_section').css( "display", "none" );
	$('#f_options').replaceWith('<img src="'+base_url+'images/cc_images/menubar/plus.png" id="f_options">');
	}
	});
	$('.sf').click(function()
	{
		$('input[class="sf"]').prop("checked", false);
		var id=this.id;	
		$("#"+id+"").prop("checked", true);
		var filter_id={filter:id}
		$.post( base_url+"index.php/"+module+"/set_filter_search/", filter_id).done(function( data ) 
		{
			$.ajax(
			{
				type: "POST",
				url:base_url+"index.php/"+module+"/search/",
				data: {search:$('#search').val()},
				dataType: 'json',
				minLength: 3,
				delay: 100,
				cache: false
			}).done(function(data)
			{
				$( ".files_result_table" ).remove();
				$( document.body ).scrollTop(200);
				$( "#cover_resultados_tabla" ).fadeIn(500,function()
				{
					jQuery.each( data.data_rows, function( i, val ) 
					{
						var parshtml=$.parseHTML(val);	
						$("#cover_resultados_tabla").add().prepend(parshtml);
						$("#cover_resultados_tabla").prepend(parshtml);
					});
					enable_select_all(module,base_url);
					info_forms(module,base_url);
					form_ajax(module,base_url);
				});
			});
		});
	});
}
/*  */
function enable_select_all(module,base_url){ 
	$('input[id="select_all"]').prop("checked", false);
	$('#select_all').off('click').click(function()
	{
		var r_id=$(this).val();
		if ($('input[id="select_all"]').prop("checked")==true)
		{
			$('input[class="chksbxs"]').prop("checked", true);
			$("input[name='chksbxs[]']:checked").each(function( i, e ) {
			$("#row_"+r_id+"").css("background-color","#e8ebf1");
			});
		}
		else
		{
			$('input[class="chksbxs"]').prop("checked", false);
			$("input[name='chksbxs[]']").each(function( i, e ) {
			$("#row_"+e.value+"").css("background-color","#FFF");
			});
		}
	});
	$("input[name='chksbxs[]']").off('click').click(function()
	{
		var r_id=$(this).val();
		if ($("input[id='chksbxs["+r_id+"]']").prop("checked")==true)
		{
			$("#row_"+r_id+"").css("background-color","#e8ebf1");
		}
		else if($("input[id='chksbxs["+r_id+"]']").prop("checked")==false)
		{
			$("#row_"+r_id+"").css("background-color","#FFF");
		}
	});
	$('#delete').off('click').click(function(){
		var inputs_vals=[];
		var ids_to_del={ids:[]}
		if ($("input[name='chksbxs[]']:checked").prop("checked")== true){
			if(confirm("Seguro de borrar los elementos seleccionados")){
				$("input[name='chksbxs[]']:checked").each(function (i, e){	
					inputs_vals['ids']= e.value;
					 ids_to_del.ids.push(inputs_vals['ids']);
					 $( "#row_"+e.value+"" ).remove();
				});
				
				$.post( base_url+"index.php/"+module+"/delete/", ids_to_del).done(function( data ) 
				{
					var first_p=$( document.body ).scrollTop();
					jobj=jQuery.parseJSON(data);
					if(module==='cobros' || module==='gastos'){
						var undone=jobj.total_undone;
						var done=jobj.total_done;
						if(done == null){done=0;}
						if(undone == null){undone=0;}
						$('#undone').remove();
						$('#done').remove();
						$( "#txt_gastos_int_bar_undone" ).append( "<span id='undone'>$"+undone+"</span>" );
						$( "#txt_gastos_int_bar_done" ).append( "<span id='done'>$"+done+"</span>" );
					}
					$( document.body ).scrollTop(200);
					$( "#cover_resultados_tabla" ).fadeIn(1000,function()
					{
						$( "#cover_interaction_bar" ).append( "<div id='msj_manages'><span id='msg'>"+jobj.message+"</span></div>" );
						
					});
					$( '#msj_manages' ).fadeOut( 3000, function()
					{
						$( document.body ).scrollTop(first_p);
						
						$('#msj_manages').remove();
					});
				
				});
			}
			else
			{
				/* alert("nel"); */
			}
		}
		else
		{
			alert("No ha seleccionado nada para borrar");
		}
	});
}
/* */
function show_hidde_responsive_manage(){
	var mediaquery = window.matchMedia("(max-width: 640px)");
	if(mediaquery.matches)
	{
		$("input[name='chksbxs[]']").each(function( i, e ) {
			$("#seleccionar_"+e.value+"").css("display","none");
			$("#seleccionar_t_"+e.value+"").css("display","none");
			$("#col1"+e.value+"").css("display","none");
			$("#col1_t"+e.value+"").css("display","none");
			$("#col3"+e.value+"").css("display","none");
			$("#col3_t"+e.value+"").css("display","none");
			$("#col4"+e.value+"").css("display","none");
			$("#col4_t"+e.value+"").css("display","none");
			$("#col5"+e.value+"").css("display","none");
			$("#col5_t"+e.value+"").css("display","none");
			$("#col6"+e.value+"").css("display","none");
			$("#col6_t"+e.value+"").css("display","none");
			$("#col7"+e.value+"").css("display","none");
			$("#col7_t"+e.value+"").css("display","none");
			$("#col8"+e.value+"").css("display","none");
			$("#col8_t"+e.value+"").css("display","none");
			$("#col9"+e.value+"").css("display","none");
			$("#col9_t"+e.value+"").css("display","none");
		});
		$(".soloVisibleResponsivamente").click(function()
		{
			var aidi =this.id;
			aidi = aidi.split("t", 2);
			aidi = parseInt(aidi[1]);
			if ($("#col1"+aidi+"").css( "display")==="block")
			{
				$("#row_"+aidi+"").css({"border":"0px solid #c9c9c9", "border-top": "0px solid #c9c9c9"});
				$("#seleccionar_"+aidi+"").slideUp(500);
				$("#seleccionar_t_"+aidi+"").slideUp(500);
				$("#col1"+aidi+"").slideUp(500);
				$("#col1_t"+aidi+"").slideUp(500);
				$("#col3"+aidi+"").slideUp(500);
				$("#col3_t"+aidi+"").slideUp(500);
				$("#col4"+aidi+"").slideUp(500);
				$("#col4_t"+aidi+"").slideUp(500);
				$("#col5"+aidi+"").slideUp(500);
				$("#col5_t"+aidi+"").slideUp(500);
				$("#col6"+aidi+"").slideUp(500);
				$("#col6_t"+aidi+"").slideUp(500);
				$("#col7"+aidi+"").slideUp(500);
				$("#col7_t"+aidi+"").slideUp(500);
				$("#col8"+aidi+"").slideUp(500);
				$("#col8_t"+aidi+"").slideUp(500);
				$("#col9"+aidi+"").slideUp(500);
				$("#col9_t"+aidi+"").slideUp(500);
			}
			else
			{
			$("#row_"+aidi+"").css({"border":"1px solid #89BFCC"});
		    $("#seleccionar_"+aidi+"").slideDown(500);
			$("#seleccionar_t_"+aidi+"").slideDown(500);
			$("#col1"+aidi+"").slideDown(500);
			$("#col1_t"+aidi+"").slideDown(500);
			$("#col3"+aidi+"").slideDown(500);
			$("#col3_t"+aidi+"").slideDown(500);
			$("#col4"+aidi+"").slideDown(500);
			$("#col4_t"+aidi+"").slideDown(500);
			$("#col5"+aidi+"").slideDown(500);
			$("#col5_t"+aidi+"").slideDown(500);
			$("#col6"+aidi+"").slideDown(500);
			$("#col6_t"+aidi+"").slideDown(500);
			$("#col7"+aidi+"").slideDown(500);
			$("#col7_t"+aidi+"").slideDown(500);
			$("#col8"+aidi+"").slideDown(500);
			$("#col8_t"+aidi+"").slideDown(500);
			$("#col9"+aidi+"").slideDown(500);
			$("#col9_t"+aidi+"").slideDown(500);
			}
			/* $("#seleccionar_t").css("display","block"); */
		});
	/* $(".soloVisibleResponsivamente").css("display","none"); */
	/* $(".box_result_table").css("display","none"); */
	/* $("#show_title").css("display","block"); */
	/* $("#show_line").css("display","block"); */
	}
	else
	{
	}
}
/* */
function show_hidde_operations(){
	$(".show_hidde_operation").each(function( i, e ) {
/*con off evitamos que se genere mas de un mismo evento*/
		$("#"+e.id).off("click").on("click",function(){
			/*alert(e.id);*/
			if($("#cover_subtitles_table_"+e.id).css('display')=='none'){
			$('#cover_subtitles_table_'+e.id).css( "display", "block" );
			$('#files_result_subtable_'+e.id).css( "display", "block" );
			$('#cover_titles_table_hidde_'+e.id).css( "display", "block" );
			$('#row_'+e.id).css({ "border":"1px solid #01233a"});
			$(this).css({ "color":"#fff"});
			$('#span'+e.id).css({ "color":"#fff"});
			$('#seleccionar_'+e.id).css({ "background-color":"#01233a","color":"#fff","border":"1px solid #01233a","border-bottom":"1px solid #01233a" });
			$('#col1'+e.id).css({ "background-color":"#01233a","color":"#fff","border":"1px solid #01233a","border-bottom":"1px solid #01233a" });
			$('#col2'+e.id).css({ "background-color":"#01233a","color":"#fff","border":"1px solid #01233a","border-bottom":"1px solid #01233a" });
			$('#col3'+e.id).css({ "background-color":"#01233a","color":"#fff","border":"1px solid #01233a","border-bottom":"1px solid #01233a" });
			}
			else{
				$('#cover_subtitles_table_'+e.id).css( "display", "none" );
				$('#files_result_subtable_'+e.id).css( "display", "none" );
				$('#cover_titles_table_hidde_'+e.id).css( "display", "none" );
				$('#row_'+e.id).css({ "border":"none"});
				$(this).css({ "color":"#0f6fa5"});
				$('#span'+e.id).css({ "color":"#0f6fa5"});
				$('#seleccionar_'+e.id).css({ "background-color":"transparent","color":"#fff","border":"none" });
				$('#seleccionar_'+e.id).css({ "background-color":"transparent","color":"#fff","border-bottom":"1px solid #c9c9c9" });
			$('#col1'+e.id).css({ "background-color":"transparent","color":"#fff","border":"none"});
			$('#col1'+e.id).css({ "background-color":"transparent","color":"#fff","border-left":"1px solid #c9c9c9","border-bottom":"1px solid #c9c9c9"});
			$('#col2'+e.id).css({ "background-color":"transparent","color":"#fff","border":"none"});
			$('#col2'+e.id).css({ "background-color":"transparent","color":"#fff","border-left":"1px solid #c9c9c9","border-bottom":"1px solid #c9c9c9"});
			$('#col3'+e.id).css({ "background-color":"transparent","color":"#fff","border":"none"});
			$('#col3'+e.id).css({ "background-color":"transparent","color":"#fff","border-left":"1px solid #c9c9c9","border-bottom":"1px solid #c9c9c9","border-right":"1px solid #c9c9c9"});
			}
		});
	});
}