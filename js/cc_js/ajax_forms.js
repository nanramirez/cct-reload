/* Todos los forms que carga el ancla de nuevo en los modulos */
function form_ajax(module,base_url){
   /*if(module==="items")
	{
		content_forms(module,base_url);
		items_forms(module,base_url);
	}*/
	$('.ajax-popup-link').magnificPopup({
		type: 'ajax',
		alignTop: true,
		ajax:
		{
			settings: null, /* Ajax objeto de configuración  */ 
			cursor: 'mfp-ajax-cur', 
			/* Clase CSS que se agregará al cuerpo durante la carga (agrega cursor de "progreso") */
			tError: '<a href="%url%">The content</a> could not be loaded.',
			 /* Mensaje de error, puede contener etiquetas% curr% y% total% si la galería está habilitada */
			 tLoading: '<img class="load_gif_form" src="../images/cc_images/icon_cct/load.gif"/>'
		},
		callbacks: 
		{
			/* var header=load('<?php $this->load->view("cc_views/partial/header_forms"); ?>'); */
			parseAjax: function(mfpResponse) 
			{
			/*  mfpResponse.data es un objeto de "datos" de ajax "éxito" de devolución de llamada */
			/* para archivo HTML simple, será solo String */
			/* Puede modificarlo para cambiar el contenido de la ventana emergente */
			/* Por ejemplo, para mostrar solo # algún elemento: */ 
			mfpResponse.data =$(mfpResponse.data).find(":first");
			/* mfpResponse.data debe ser un elemento String o DOM (jQuery) */ 
			},
			ajaxContentAdded: function() 
			{
				/* El contenido de Ajax se carga y se agrega al DOM */ 
				/* Funciones especiales del modulo employees */
				if( module==='employees'){
					load_image(module,base_url);
					autozip();
				}
				/* Funciones especiales del modulo suppliers */
				else if(module==='suppliers'){
					load_image(module,base_url);
					show_fiscal_form();
					autozip();
				}
				/*Funciones especiales del modulo items*/
				else if(module==='items'){
					load_image(module,base_url);
					suggest_category(module,base_url);
					suggest_brand(module,base_url);
					quantity_input();
				}
				/*Funciones especiales del modulo recursos*/
				else if(module==='recursos'){
					coins();
					formas_pago(base_url);
				}
				/*Funciones especiales del modulo gastos*/
				else if(module==='gastos'){
					load_image(module,base_url);
					gastos_recurrentes(base_url);
					suggest_suppliers(base_url);
					suggest_category(module,base_url);
				 }
				/*Funciones especiales del modulo items_kits*/
				else if(module==='item_kits'){
					load_image(module,base_url);
					suggest_category(module,base_url);
					quantity_input();
					items_en_kits(module,base_url);
				}
				submit_forms(module,base_url);
			}
		}
	});
}
/* Sumbit del forms que no es submit en realidad*/
function submit_forms(module,base_url){
	var jsonerrors=jQuery.parseJSON($("#jsonerrors").val());
	$('.submit_button_pop').click(function (){
		if(module==='employees'){
			var respuesta=validar_new_employees(jsonerrors);
			if(respuesta== true)
			{
				submit_employees_form(module,base_url);
			}
		}
		else if(module==='suppliers'){
			var respuesta=validar_new_suppliers(jsonerrors);
			if(respuesta== true)
			{
				submit_suppliers_form(module,base_url);
			}
		}
		else if(module==='items'){
			var respuesta=validar_new_items(jsonerrors);
			if(respuesta== true){
				submit_items_form(module,base_url);
			}
		}
		else if(module==='recursos'){
			var respuesta=validar_new_recursos(jsonerrors);
			if(respuesta== true){
				submit_recursos_form(module,base_url);
			}
		}
		else if(module==='gastos'){
			var respuesta=validar_new_gastos(jsonerrors);
			if (respuesta==true){
				submit_gastos_form(module,base_url);
			}
		}
		else if(module==='item_kits'){
			var respuesta=validar_new_item_kits(jsonerrors);
			if (respuesta==true){
				submit_item_kits_form(module,base_url);
			}
		}
	});
}
/*Carga imagenes en los forms */
function load_image(module,base_url){
	var loading_txt =$("#loading_txt").val();
	var upload_image="upload_image";
	if($("#cat_form").val()!="")upload_image=$("#cat_form").val();
	
	$(":input").each(function(index){
		var img_src=document.getElementById("src_"+this.id);
		if(img_src!=null){
			var picture=document.getElementById("content_thumbail_"+this.id);
			var width_picture =$("#size_img_w"+this.id).val();
			var height_picture =$("#size_img_h"+this.id).val();
			$('#content_thumbail_'+this.id).replaceWith('<img id="content_thumbail_'+this.id+'" src="'+img_src.value+'" alt="'+this.id+'" >');
			jQuery('#content_thumbail_'+this.id).attr('src',jQuery('#content_thumbail_'+this.id).attr('src')+ '?' + (new Date()).getTime());
			$("#content_thumbail_"+this.id).css({"width":"100%","height":"100%"});
			var no_ext_name =this.id;
			if(this.type=="file" ){
			var input = document.getElementById(this.id); 
			formdata = false;
			function showUploadedItem (source) {
					var ori_img =document.getElementById("content_thumbail_"+no_ext_name);
					var list = document.getElementById("image-list_"+no_ext_name);
					var li = document.getElementById("first_li_"+no_ext_name);
					list.removeChild(li);
					li   = document.createElement("li");
					img  = document.createElement("img");
					img.width = width_picture;
					img.height = height_picture;
					img.src = source;
					img.id="content_thumbail_"+no_ext_name;
					var response = document.getElementById("response_"+no_ext_name);
					response.style.display = "block";
					li.appendChild(img);
					list.appendChild(li);
					$("#content_thumbail_"+this.id).css({"width":"100%","height":"100%"});
				}   
				if (window.FormData) {
				formdata = new FormData();
				}
				var ext_name =$("#ext_name_"+this.id).val();
				ext_name =ext_name.replace(".", "---");
				$('#'+this.id).change( function (evt){
					var hidde_id =$("#id_input").val();
					document.getElementById("response_"+no_ext_name).innerHTML = loading_txt
					var i = 0, len = this.files.length, img, reader, file;
					for ( ; i < len; i++ ){
						file = this.files[i];
						if (!!file.type.match("image.*")){
							if ( window.FileReader ) {
								reader = new FileReader();
								reader.onloadend = function (e){ 
									showUploadedItem(e.target.result, file.fileName);
								};
								reader.readAsDataURL(file);
							}
							if (formdata){
								formdata.append(no_ext_name+"[]", file);
							}
						}
					}
					if (formdata){
						$.ajax({
						url: base_url+"index.php/"+module+"/"+upload_image+"/"+hidde_id+"/"+ext_name,
						type: "POST",
						data: formdata,
						processData: false,
						contentType: false,
						success: function (res) {
							document.getElementById("response_"+no_ext_name).innerHTML = res;
						}});
					}
				});
				/* Inicia el Js para cargar imagenes en los forms */
				var drop = document.getElementById("drop_"+no_ext_name);
				function handleDrop(e){
					e.stopPropagation();
					e.preventDefault();
					var files = e.dataTransfer.files;
					var f = files[0];
					var hidde_id =$("#id_input").val();
					if(module==='item_kits')hidde_id =$("#id_input_kits").val();
					if(hidde_id == ""){
						hidde_id=-1;
					}
					/* alert(hidde_id); */
					document.getElementById("response_"+no_ext_name).innerHTML = loading_txt;
					var i = 0, len = files.length, img, reader, file;
					for ( ; i < len; i++ ){
						file = files[i];
						if (!!file.type.match("image.*")){
							if ( window.FileReader ){
								reader = new FileReader();
								reader.onloadend = function (e) { 
									showUploadedItem(e.target.result, file.fileName);
								};
								reader.readAsDataURL(file);
							}
							if (formdata) {
								formdata.append(no_ext_name+"[]", file);
							}
						}
					}
					if (formdata){
						$.ajax({
						url: base_url+"index.php/"+module+"/upload_image/"+hidde_id+"/"+ext_name,
						type: "POST",
						data: formdata,
						processData: false,
						contentType: false,
						success: function (res) {
							document.getElementById("response_"+no_ext_name).innerHTML = res;
						}});
					}
				}
				function handleDragover(e){
					e.stopPropagation();
					e.preventDefault();
					e.dataTransfer.dropEffect = 'copy';
				}
				if(drop.addEventListener) {
					drop.addEventListener('dragenter', handleDragover, false);
					drop.addEventListener('dragover', handleDragover, false);
					drop.addEventListener('drop', handleDrop, false);
				}
			}
		}
	});
}
/* Carga los forms que solo son informativos solo cargan una imagen */
function info_forms(module,base_url){
	$('.info_form_ajax').magnificPopup(
	{
		type: 'ajax',
		alignTop: true,
		ajax:{
				settings: null, /* Ajax objeto de configuración */ 
				cursor: 'mfp-ajax-cur', 
				/* Clase CSS que se agregará al cuerpo durante la carga (agrega cursor de "progreso") */
				tError: '<a href="%url%">The content</a> could not be loaded.' 
				 /* Mensaje de error, puede contener etiquetas% curr% y% total% si la galería está habilitada */
			},
			callbacks: 
			{
				/* var header=load('<?php $this->load->view("cc_views/partial/header_forms"); ?>'); */
				parseAjax: function(mfpResponse) 
				{
					/*  mfpResponse.data es un objeto de "datos" de ajax "éxito" de devolución de llamada */
					/* para archivo HTML simple, será solo String */
					/* Puede modificarlo para cambiar el contenido de la ventana emergente */
					/* Por ejemplo, para mostrar solo #algún_elemento: */ 
					mfpResponse.data =$(mfpResponse.data).find(":first");
					/* mfpResponse.data debe ser un elemento String o DOM (jQuery) */ 
				},
				ajaxContentAdded: function() 
				{	
					var img_src=document.getElementById("src_main");
						
					if(img_src!=null){
						$('#main_img').replaceWith('<img id="main_img" src="'+img_src.value+'" alt="'+this.id+'" >');
						jQuery('#main_img').attr('src',jQuery('#main_img').attr('src')+ '?' + (new Date()).getTime());
						$("#main_img").css({"width":"100%","height":"100%"});
						//console.log(img_src);
					}
					$("#box_print").click(function(){
							window.print();
						});
					if(module==='cobros'|| module==='gastos'){
						deducible_ajax(base_url,module);
						$("#drop").css("display","none");
						var monto=$("#monto").val();
						var coin=$("#coin").val();
						var centavos=monto.toString();
						centavos=centavos.substr(-2, 2);
						var monto_en_letras=NumeroALetras(monto);
						$("#cover_field_row_popup_info").append('<div class="box_field_row "><label class="items_category">('+monto_en_letras+'<small>'+centavos+'/100</small> '+coin+')</label></div>');
						$("#cover_field_row_popup_info").append('<div class="box_field_row "><img id="logo_recibo"src="'+base_url+'images/f_images/logo/logotipo.png"></div></div>');
					}
					$('#button_close').click(function ()
					{
						$('.ajax-popup-pedido').magnificPopup('close');
					});
					/*Inicia el Js para cargar imagenes en los forms*/
					load_image(module,base_url);
					/* Fin de el Js para cargar imagenes en los forms */
				}
			}
	});
}
/* Forms extras del modulo de productos */
function items_forms(module,base_url){
	$('.inv_form_ajax').magnificPopup(
	{
		type: 'ajax',
		alignTop: true,
		ajax:{
				settings: null, /* Objeto de configuración Ajax */
				cursor: 'mfp-ajax-cur', 
				/* Clase CSS que se agregará al cuerpo durante la carga (agrega cursor de "progreso") */
				tError: '<a href="%url%">The content</a> could not be loaded.' 
				 /* Mensaje de error, puede contener etiquetas %curr% y %total% si la galería está habilitada */
			},
			callbacks: 
			{
				/* var header=load('<?php $this->load->view("cc_views/partial/header_forms"); ?>'); */
				parseAjax: function(mfpResponse) 
				{
				/* mfpResponse.data es un objeto de "datos" de ajax "éxito" de devolución de llamada */
				/* para archivo HTML simple, será solo String */
				/* Puede modificarlo para cambiar el contenido de la ventana emergente */
				/* Por ejemplo, para mostrar solo #algún-elemento: */
				mfpResponse.data =$(mfpResponse.data).find(":first");
				/* mfpResponse.data debe ser un elemento String o DOM (jQuery) */
				},
				ajaxContentAdded: function() 
				{		
						var img_src=document.getElementById("src_main");
						
						if(img_src!=null){
							$('#main_img').replaceWith('<img id="main_img" src="'+img_src.value+'" alt="'+this.id+'" >');
							jQuery('#main_img').attr('src',jQuery('#main_img').attr('src')+ '?' + (new Date()).getTime());
							$("#main_img").css({"width":"100%","height":"100%"});
						}
						var respuesta=false;
						if($("#item_number").val().length > 2)
						{
						$("#barcode").barcode($("#item_number").val(), "ean13");
						}
						$( ".detail_item" ).each(function( index ) {
						$(".ver_mas_"+index).click(function()
						{
							var renglon =$("#detalle_input_"+index).val();
							if($("#result_razon_detalle_"+index).css('display')=='none')
							{$("#result_razon_detalle_"+index).css('display','block');}	
							else{$("#result_razon_detalle_"+index).css('display','none');}
						});
						});
						$('.submit_button_inventory').click(function ()
						{
						var hidde_id =$("#id_input").val();
						var base_url =$("#base_url").val();
						if($("#json_msj").val())
						{
						var json_msjs=jQuery.parseJSON($("#json_msj").val());
						var respuesta=validar_inventory_form(json_msjs);
						}
						if(respuesta== true)
						{
							$.ajax(
								{
									type: "POST",
									url:base_url+"index.php/items/save_inventory/"+hidde_id,
									data: {trans_comment:$("#trans_comment").val(),
									newquantity:$("#newquantity").val()},
									dataType: 'json',
									minLength: 3,
									delay: 100,
									cache: false
								}).done(function( data )
								{
									jobj=data;
									var row={row_id:hidde_id}
									/* $.post( location.href+"/get_row/", row)*/
									if(jobj.success=="true")
									{
											var first_p=$( document.body ).scrollTop();
											$( document.body ).scrollTop(200);
											$( "#row_"+hidde_id+"" ).remove();
											$.ajax(
											{
												type: "POST",
												url:base_url+"index.php/items/get_row/",
												data: {row_id:hidde_id},
												dataType: 'html',
												minLength: 3,
												delay: 100,
												cache: false
											}).done(function( data )
											{
												$( document.body ).scrollTop(200);
												$( "#cover_resultados_tabla" ).fadeIn(1000,function()
												{
													$( "#cover_interaction_bar" ).append( "<div id='msj_manages'><span id='msg'>"+jobj.message+"</span></div>" );
													$("#cover_resultados_tabla").add().prepend(data);
													$("#row_"+hidde_id+"" ).css({"background-color":" rgb(194, 255, 195)"});
												});
												$( '#msj_manages' ).fadeOut( 3000, function()
												{
													$( document.body ).scrollTop(first_p);
													$( "#row_"+jobj.item_id+"" ).css({"background-color":"rgb(240, 249, 240)","border": "none"});
													$( '#msj_manages'  ).remove();
												});
												info_forms(module,base_url);
												form_ajax(module,base_url);
												content_forms(module,base_url);
												items_forms(module,base_url);
											});
									}
									else if(jobj.success=="false")
									{
										$( document.body ).scrollTop(200);
										$( "#cover_interaction_bar" ).fadeIn(1000,function()
										{
											$( "#cover_interaction_bar" ).append( "<div id='msj_error_manages'><span id='msg'>"+jobj.message+"</span></div>" );
										});
										$( '#msj_error_manages' ).fadeOut( 4500, function()
										{
											$( '#msj_error_manages'  ).remove();
										});
									}
								});
								$('.inv_form_ajax').magnificPopup('close');
						}
					});
				}
			}
	});
	$('#barcodes').magnificPopup(
	{
		type: 'ajax',
		alignTop: true,
		ajax:
		{
			settings: null, /* Objeto de configuración Ajax */
			cursor: 'mfp-ajax-cur', 
			/* Clase CSS que se agregará al cuerpo durante la carga (agrega cursor de "progreso") */
			tError: '<a href="%url%">The content</a> could not be loaded.' 
			 /* Mensaje de error, puede contener etiquetas %curr% y %total% si la galería está habilitada */
		},
		callbacks: 
		{
			parseAjax: function(mfpResponse) 
			{
			/* mfpResponse.data es un objeto de "datos" de ajax "éxito" de devolución de llamada */
			/* para archivo HTML simple, será solo String */
			/* Puede modificarlo para cambiar el contenido de la ventana emergente */
			/* Por ejemplo, para mostrar solo #algún-elemento: */
			mfpResponse.data =$(mfpResponse.data).find(":first");
			/* mfpResponse.data debe ser un elemento String o DOM (jQuery) */
			},
			ajaxContentAdded: function() 
			{
				var inputs_vals=[];
				var ids_to_barcode={ids:[]}
				if ($("input[name='chksbxs[]']:checked").prop("checked")== true && $("input[name='chksbxs[]']:checked").length == 1)
				{
					if(confirm("Generar un código de barras para el elemento seleccionado?"))
					{
						var id_to_barcode=$("input[name='chksbxs[]']:checked").val();
						$.ajax(
						{
							type: "POST",
							url:location.href+"/generate_barcodes/"+id_to_barcode,
							data: {ids_to_barcode},
							dataType: 'json',
							minLength: 3,
							delay: 100,
							cache: false
						}).done(function( data ) 
						{
						var codebar=data.item_number;
						if(data.item_number==undefined)codebar=data.kit_number;
						if(codebar!=null)
						{
							var config=$("#config").val();
							$(".codebar_sheet").barcode(codebar, "ean13",{barWidth:1, barHeight:60});
							for(i=0; i<config+1; i++)
							{
								$("#box_"+i).prepend('<label id="'+i+'" class="codebar_sheet"><span>'+data.name+'</span></label>');
								/*$("#box_"+i).css({"width":"","height": "","padding":"","":""});*/
							}
							$("#box_print").click(function()
							{
								window.print();
							});
						}
						else
						{
							$("#cover_row_barcode_kit").prepend("<span id='barcode_error'>"+$("#barcodes_null").val()+"</span>");
						}
						info_forms(module,base_url);
						form_ajax(module,base_url);
						});
					}
					else
					{
					/*alert("nel");*/
						$('#generate_barcodes').magnificPopup('close');
					}
				}
				else
				{
					alert("Seleccione un elemento a la vez para generar su código de barras");
					$('#generate_barcodes').magnificPopup('close');
				}
			}
		}
	});
	$('#bulk_edit').magnificPopup(
	{
		type: 'ajax',
		alignTop: true,
		ajax:
		{
			settings: null, /* Objeto de configuración Ajax */
			cursor: 'mfp-ajax-cur', 
			/* Clase CSS que se agregará al cuerpo durante la carga (agrega cursor de "progreso") */
			tError: '<a href="%url%">The content</a> could not be loaded.' 
			/* Mensaje de error, puede contener etiquetas %curr% y %total% si la galería está habilitada */
		},
		callbacks: 
		{
		parseAjax: function(mfpResponse) 
		{
			/* mfpResponse.data es un objeto de "datos" de ajax "éxito" de devolución de llamada */
			/* para archivo HTML simple, será solo String */
			/* Puede modificarlo para cambiar el contenido de la ventana emergente */
			/* Por ejemplo, para mostrar solo #algún-elemento: */
			mfpResponse.data =$(mfpResponse.data).find(":first");
			/* mfpResponse.data debe ser un elemento String o DOM (jQuery) */
		},
		ajaxContentAdded: function() 
		{
			$( "#category" ).autocomplete({
					appendTo: "#ui-widget-category",
					autoFocus: true,
					source: function (request, response) {
					$.ajax(
					{
						type: "POST",
						url:location.href+"/suggest_category/",
						data: {q:$('#category').val()},
						success: response,
						dataType: 'json',
						minLength: 2,
						delay: 1,
						cache: false
					});
					},
					response: function( event, ui ) {
					 },
					change: function( event, ui ) 
					{},
					select: function( event, ui ) {
					}});
					$( "#brand" ).autocomplete({
					appendTo: "#ui-widget-brand",
					autoFocus: true,
					source: function (request, response) {
					$.ajax(
					{
					type: "POST",
					url:location.href+"/suggest_brand/",
					data: {q:$('#brand').val()},
					success: response,
					dataType: 'json',
					minLength: 2,
					delay: 1,
					cache: false
					});
					},
					response: function( event, ui ) {
					},
					change: function( event, ui ) 
					{},
					select: function( event, ui ) {
					}});	
			if ($("input[name='chksbxs[]']:checked").prop("checked")== true  )
			{	
				$('.submit_button_pop_2').click(function ()
				{
					if(module==='items')
					{
					var inputs_vals=[];
					var item_data=
					{
						brand:$("#brand").val(),
						category:$("#category").val(),
						supplier_id:$("#supplier_id").val(),
						cost_price:$("#cost_price").val(),
						unit_price:$("#unit_price").val(),
						other_price:$("#other_price").val(),
						reorder_level:$("#reorder_level").val(),
						tax_names:[],
						tax_percents:[],
						location:$("#location").val(),
						publicar:$("#publicar").val(),
						destacar:$("#destacar").val(),
						show_price:$("#show_price").val(),
						show_stock:$("#show_stock").val(),
						promocionar:$("#promocionar").val(),
						allow_alt_description:$("#allow_alt_description").val(),
						is_serialized:$("#is_serialized").val(),
						c_ClaveUnidad:$("#ClaveUnidad").val(),
						c_ClaveProdServ:$("#ClaveProdServ").val(),
						img_qty:$("#img_qty").val(),
						item_ids:[]
					}
					$("input[name='chksbxs[]']:checked").each(function (i, e)
					{	
						inputs_vals['item_ids']= e.value;
						item_data.item_ids.push(inputs_vals['item_ids']);
					});
					/*var id_to_barcode=$("input[name='chksbxs[]']:checked").val();*/
					var jsonerrors=jQuery.parseJSON($("#jsonerrors").val());
					var respuesta=validar_form_bulk(jsonerrors);
					if(respuesta== true)
					{
						var tax_names_array=[];
						$("input[name='tax_names[]']").each(function (i, e)
						{
							tax_names_array['tax_names']= e.value;
						});
						item_data.tax_names.push(tax_names_array);
						var tax_percents_array=[];
						$("input[name='tax_percents[]']").each(function (i, e)
						{
							tax_percents_array['tax_percents']= e.value;
						});
						item_data.tax_percents.push(tax_percents_array);
						$.ajax(
								{
								type: "POST",
								url:base_url+"index.php/"+module+"/bulk_update/",
								dataType: 'json',
								data:item_data,
								minLength: 2,
								delay: 1,
								cache: false
								}).done(function( data ){
							jobj=data;
							if(jobj.success=="true")
							{
							$.each(item_data.item_ids, function(i, e) 
							{ 
								$( "#row_"+e+"" ).remove();
								$.ajax(
								{
									type: "POST",
									url:base_url+"index.php/items/get_row/",
									data: {row_id:e},
									dataType: 'html',
									minLength: 3,
									delay: 100,
									cache: false
								}).done(function( data ) 
								{	
									$( document.body ).scrollTop(200);
									$( "#cover_resultados_tabla" ).fadeIn(1000,function()
									{
										$("#cover_resultados_tabla").add().prepend(data);
										$( "#cover_interaction_bar" ).append( "<div id='msj_manages'><span id='msg'>"+jobj.message+"</span></div>" );
										$("#row_"+e+"" ).css({"background-color":" rgb(194, 255, 195)"});
									info_forms(module,base_url);
									form_ajax(module,base_url);
									content_forms(module,base_url);
									items_forms(module,base_url);
									});
									$( '#msj_manages' ).fadeOut( 3000, function()
									{
										$( "#row_"+e+"" ).css({"background-color":"rgb(240, 249, 240)","border": "none"});
										$( '#msj_manages'  ).remove();
									});
								});
							});
							}
							else if(jobj.success=="false")
							{
								$( document.body ).scrollTop(200);
								$( "#cover_interaction_bar" ).fadeIn(1000,function()
								{
									$( "#cover_interaction_bar" ).append( "<div id='msj_error_manages'><span id='msg'>"+jobj.message+"</span></div>" );
								});
								$( '#msj_error_manages' ).fadeOut( 4500, function()
								{
									$( '#msj_error_manages'  ).remove();
								});
							}
						});
						$('#bulk_edit').magnificPopup('close');
					}
				}
				else if(module==='item_kits')
				{
					var inputs_vals=[];
					var kit_data=
					{
						kit_price:$("#kit_price").val(),
						other_price:$("#other_price").val(),
						category:$("#category").val(),
						description:$("#description").val(),
						publicar:$("#publicar").val(),
						show_price:$("#show_price").val(),
						destacar:$("#destacar").val(),
						promocionar:$("#promocionar").val(),
						servicio:$("#service").val(),
						c_ClaveUnidad:$("#ClaveUnidad").val(),
						c_ClaveProdServ:$("#ClaveProdServ").val(),
						img_qty:$("#img_qty").val(),
						kit_ids:[]
					}
					$("input[name='chksbxs[]']:checked").each(function (i, e)
					{
						inputs_vals['kit_ids']= e.value;
						kit_data.kit_ids.push(inputs_vals['kit_ids']);
					});
					var jsonerrors=jQuery.parseJSON($("#jsonerrors").val());
					var respuesta=validar_form_kit_bulk(jsonerrors);
					if(respuesta== true)
					{
						$.ajax(
						{
						type: "POST",
						url:base_url+"index.php/"+module+"/bulk_update/",
						dataType: 'json',
						data:kit_data,
						minLength: 2,
						delay: 1,
						cache: false
						}).done(function( data )
						{
							if(data.success=="true")
							{
								$.each(kit_data.kit_ids, function(i, e) 
								{
									$( "#row_"+e+"" ).remove();
									$.ajax(
									{
									type: "POST",
									url:base_url+"index.php/item_kits/get_row/",
									data: {row_id:e},
									dataType: 'html',
									minLength: 3,
									delay: 100,
									cache: false
									}).done(function( data ) 
									{	
										$( document.body ).scrollTop(200);
										$( "#cover_resultados_tabla" ).fadeIn(1000,function()
										{
										$("#cover_resultados_tabla").add().prepend(data);
										$("#row_"+e+"" ).css({"background-color":" rgb(194, 255, 195)"});
										});
										$( '#msj_manages' ).fadeOut( 3000, function()
										{
										$( "#row_"+e+"" ).css({"background-color":"rgb(240, 249, 240)","border": "none"});
										});
												info_forms(module,base_url);
												form_ajax(module,base_url);
												content_forms(module,base_url);
									});
								});
								$( document.body ).scrollTop(200);
								$( "#cover_resultados_tabla" ).fadeIn(1000,function()
								{
								$( "#cover_interaction_bar" ).append( "<div id='msj_manages'><span id='msg'>"+data.message+"</span></div>" );
								});
								$( '#msj_manages' ).fadeOut( 3000, function()
								{
								$( '#msj_manages'  ).remove();
								});
							}
							else if(data.success=="false")
							{
								$( document.body ).scrollTop(200);
								$( "#cover_interaction_bar" ).fadeIn(1000,function()
								{
									$( "#cover_interaction_bar" ).append( "<div id='msj_error_manages'><span id='msg'>"+data.message+"</span></div>" );
								});
								$( '#msj_error_manages' ).fadeOut( 4500, function()
								{
									$( '#msj_error_manages'  ).remove();
								});
							}
							$('#bulk_edit').magnificPopup('close');
						});
					}
				}
				});
			}
			else
			{
					alert("No has seleccionado elementos para editar");
					$('#bulk_edit').magnificPopup('close');
			}
			/* */
			jQuery('<div class="quantity-nav"><div class="quantity-button quantity-up">+</div><div class="quantity-button quantity-down">-</div></div>').insertAfter('.quantity input');
			jQuery('.quantity').each(function() {
			var spinner = jQuery(this),
			input = spinner.find('input[type="number"]'),
			btnUp = spinner.find('.quantity-up'),
			btnDown = spinner.find('.quantity-down'),
			min = input.attr('min'),
			max = input.attr('max');
			btnUp.click(function() {
			if(input.val()=="")input.val(0);
			var oldValue = parseFloat(input.val());
			if (oldValue >= max) {
			var newVal = oldValue;
			} else {
			var newVal = oldValue + 1;
			}
			spinner.find("input").val(newVal);
			spinner.find("input").trigger("change");
			});
			btnDown.click(function() {
			if(input.val()=="")input.val(0);
			var oldValue = parseFloat(input.val());
			if (oldValue <= min) {
			var newVal = oldValue;
			} else {
			var newVal = oldValue - 1;
			}
			spinner.find("input").val(newVal);
			spinner.find("input").trigger("change");
			});
			});
		}
		}
	});
}
/*Funcion que transforma un archivo xlsx a obj para enviarlo via ajax y hacer una inserccion masiva de productos ***OJO solo importa archivos*/
function excel(base_url,module){
/*se agrego module*/
	$('#excel').magnificPopup(
	{
		type: 'ajax',
		alignTop: true,
		ajax:
		{
			settings: null, /* Objeto de configuración Ajax */
			cursor: 'mfp-ajax-cur', 
			/* Clase CSS que se agregará al cuerpo durante la carga (agrega cursor de "progreso") */
			tError: '<a href="%url%">The content</a> could not be loaded.' 
			 /* Mensaje de error, puede contener etiquetas %curr% y %total% si la galería está habilitada */
		},
		callbacks: 
		{
			parseAjax: function(mfpResponse) 
			{
			/* mfpResponse.data es un objeto de "datos" de ajax "éxito" de devolución de llamada */
			/* para archivo HTML simple, será solo String */
			/* Puede modificarlo para cambiar el contenido de la ventana emergente */
			/* Por ejemplo, para mostrar solo #algún-elemento: */
			mfpResponse.data =$(mfpResponse.data).find(":first");
			/* mfpResponse.data debe ser un elemento String o DOM (jQuery) */
			},
			ajaxContentAdded: function() 
			{
				dragXlsx(base_url);
				var fileExtension = "";
					/*función que observa los cambios del campo file y obtiene información*/
					$('#xlf').change(function()
					{
						/*obtenemos un array con los datos del archivo*/
						var file = $("#xlf")[0].files[0];
						/*obtenemos el nombre del archivo*/
						var fileName = file.name;
						/*obtenemos la extensión del archivo*/
						fileExtension = fileName.substring(fileName.lastIndexOf('.') + 1);
						/*obtenemos el tamaño del archivo*/
						var fileSize = file.size;
						/*obtenemos el tipo de archivo image/png ejemplo*/
						var fileType = file.type;
						/*mensaje con la información del archivo*/
						/*alert("Archivo para subir: "+fileName+", peso total: "+fileSize+" bytes");*/
					});
			$.cookie.json = true;
			$(".submit_import_button").click(function() 
			{
				var JsonObj;
				var excel_sheet=$("#xlsxsheet").val();
				var error_msj=$("#error_msj").val();
				if(excel_sheet!=""){
				/*Cortamos el nombre de la sheet para dejar solo lo que se necesita del CSV*/
				excel_sheet=excel_sheet.slice(18);
				/*Remplazamos las cabeceras por la string del nombre de columna de la bd */
				var bdheaders=["item_id","name","modelo","brand","category","cost_price","unit_price","other_price","quantity","reorder_level","item_number","location","description"];
				excel_sheet=excel_sheet.replace(/.*/,bdheaders);
				var JsonObj=csvJSON(excel_sheet);
				/*hacemos la petición ajax  */
					$.ajax({
					type:"POST",
					/*se acomodo la url*/
                    url: base_url+"index.php/"+module+"/excel_import/",  
                    data: {xlsxsheet:JsonObj},
                    dataType: 'json',
                    cache: false,
					success  : function(msg){
						$.ajax(
						{
						type: "POST",
						/*se acomodo la url*/
						url:base_url+"index.php/"+module+"/get_items_import/",
						/*url:location.href+"/get_items/",*/
						dataType: 'json',
						minLength: 3,
						delay: 100,
						cache: false
						}).done(function( data ) 
						{
							$('#cover_resultados_tabla').replaceWith('<section id="cover_resultados_tabla">'+data.data+'</div>');
							$( document.body ).scrollTop(200);
							$( document.body ).scrollTop(200);
							/*se agrego para volver a cargar evenetos*/
							info_forms(module,base_url);
							form_ajax(module,base_url);
							content_forms(module,base_url);
							items_forms(module,base_url);
						});
					},
					error : function(msg) {
					alert('Failed');
					},
				});
				$('#excel').magnificPopup('close');
				}
				else{
					alert(error_msj);
				}
			});
				var inputs_vals=[];
				var ids_to_excel={ids:[]}
				var all_selected;
				if ($("input[name='chksbxs[]']:checked").prop("checked")== true  )
				{	
					$(".submit_export_button").click(function()
					{
						$("input[name='chksbxs[]']:checked").each(function (i, e)
						{
							inputs_vals['ids']= e.value;
							ids_to_excel.ids.push(inputs_vals['ids']);
						});
						/*alert(ids_to_excel.ids);*/
						all_selected=false;
						$.ajax(
						{
						type: "POST",
						url:base_url+"index.php/"+module+"/excel_export/"+all_selected,
						/*url:base_url+"index.php/"+module+"/excel_export/"+all_selected,*/
						data: ids_to_excel,
						dataType: 'json',
						minLength: 2,
						delay: 1,
						cache: false
						}).done(function( data )
						{
							var reader = new FileReader();
							reader.onload = function (event) {
							var save = document.createElement('a');
							save.href = base_url+"formats/excel_helper.xlsx";
							save.target = '_blank';
							save.download = "productos_exportados.xlsx"|| "excel_helper.xlsx";
							var clicEvent = new MouseEvent('click', {
							'view': window,
							'bubbles': true,
							'cancelable': true
							});
							save.dispatchEvent(clicEvent);
							(window.URL || window.webkitURL).revokeObjectURL(save.href);
							};
							var blob = new Blob([this.response], {type: 'application/vnd.ms-excel'});
							reader.readAsDataURL(blob);
						});
						$('#excel').magnificPopup('close');
					});
				}
				else
				{
					$(".submit_export_button").click(function() 
					{
						/*alert('unchecked');*/
						all_selected=true;
						$.ajax(
						{
						type: "POST",
						url:base_url+"index.php/"+module+"/excel_export/"+all_selected,
						data: ids_to_excel,
						dataType: 'json',
						minLength: 2,
						delay: 1,
						cache: false
						}).done(function( data )
						{
							/*jobj=jQuery.parseJSON(data);*/
							var reader = new FileReader();
							reader.onload = function (event) {
							var save = document.createElement('a');
							save.href = base_url+"formats/excel_helper.xlsx";
							save.target = '_self';
							save.download = "productos_exportados.xlsx"|| "excel_helper.xlsx";
							var clicEvent = new MouseEvent('click', {
							'view': window,
							'bubbles': true,
							'cancelable': true
							});
							save.dispatchEvent(clicEvent);
							(window.URL || window.webkitURL).revokeObjectURL(save.href);
							};
							var blob = new Blob([this.response], {type: 'application/vnd.ms-excel'});
							reader.readAsDataURL(blob);
						});
						$('#excel').magnificPopup('close');
					});
				}
			}
		}
	});
}
/*Librerias para arrastrar archivos en los forms usada incialmente por la funcion de excel*/
function dragXlsx(base_url){
var X = XLSX;
var XW = {
	/* mensaje de trabajador */
	msg: 'xlsx',
	/* scripts de trabajador */
	rABS: base_url+'js/cc_js/xlsxworker2.js',
	norABS: base_url+'js/cc_js/xlsxworker1.js',
	noxfer: base_url+'js/cc_js/xlsxworker.js'
};
var rABS = typeof FileReader !== "undefined" && typeof FileReader.prototype !== "undefined" && typeof FileReader.prototype.readAsBinaryString !== "undefined";
if(!rABS) {
	document.getElementsByName("userabs")[0].disabled = true;
	document.getElementsByName("userabs")[0].checked = false;
}
var use_worker = typeof Worker !== 'undefined';
if(!use_worker) {
	document.getElementsByName("useworker")[0].disabled = true;
	document.getElementsByName("useworker")[0].checked = false;
}
var transferable = use_worker;
if(!transferable) {
	document.getElementsByName("xferable")[0].disabled = true;
	document.getElementsByName("xferable")[0].checked = false;
}
var wtf_mode = false;
function fixdata(data) {
	var o = "", l = 0, w = 10240;
	for(; l<data.byteLength/w; ++l) o+=String.fromCharCode.apply(null,new Uint8Array(data.slice(l*w,l*w+w)));
	o+=String.fromCharCode.apply(null, new Uint8Array(data.slice(l*w)));
	return o;
}
function ab2str(data) {
	var o = "", l = 0, w = 10240;
	for(; l<data.byteLength/w; ++l) o+=String.fromCharCode.apply(null,new Uint16Array(data.slice(l*w,l*w+w)));
	o+=String.fromCharCode.apply(null, new Uint16Array(data.slice(l*w)));
	return o;
}
function s2ab(s) {
	var b = new ArrayBuffer(s.length*2), v = new Uint16Array(b);
	for (var i=0; i != s.length; ++i) v[i] = s.charCodeAt(i);
	return [v, b];
}
function xw_noxfer(data, cb) {
	var worker = new Worker(XW.noxfer);
	worker.onmessage = function(e) {
		switch(e.data.t) {
			case 'ready': break;
			case 'e': console.error(e.data.d); break;
			case XW.msg: cb(JSON.parse(e.data.d)); break;
		}
	};
	var arr = rABS ? data : btoa(fixdata(data));
	worker.postMessage({d:arr,b:rABS});
}
function xw_xfer(data, cb) {
	var worker = new Worker(rABS ? XW.rABS : XW.norABS);
	worker.onmessage = function(e) {
		switch(e.data.t) {
			case 'ready': break;
			case 'e': console.error(e.data.d); break;
			default: xx=ab2str(e.data).replace(/\n/g,"\\n").replace(/\r/g,"\\r"); console.log("done"); cb(JSON.parse(xx)); break;
		}
	};
	if(rABS) {
		var val = s2ab(data);
		worker.postMessage(val[1], [val[1]]);
	} else {
		worker.postMessage(data, [data]);
	}
}
function xw(data, cb) {
	transferable = true;
	if(transferable) xw_xfer(data, cb);
	else xw_noxfer(data, cb);
}
function get_radio_value( radioName ) {
	var radios = document.getElementsByName( radioName );
	for( var i = 0; i < radios.length; i++ ) {
		if( radios[i].checked || radios.length === 1 ) {
			return radios[i].value;
		}
	}
}
function to_json(workbook) {
	var result = {};
	workbook.SheetNames.forEach(function(sheetName) {
		var roa = X.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
		if(roa.length > 0){
			result[sheetName] = roa;
		}
	});
	return result;
}
function to_csv(workbook) {
	var result = [];
	workbook.SheetNames.forEach(function(sheetName) {
		var csv = X.utils.sheet_to_csv(workbook.Sheets[sheetName]);
		if(csv.length > 0){
			result.push("SHEET: " + sheetName);
			result.push("");
			result.push(csv);
		}
	});
	var input = document.createElement("input");
	$("#xlsxsheet").val(result);;
	return result.join("\n");
}
function to_formulae(workbook) {
	var result = [];
	workbook.SheetNames.forEach(function(sheetName) {
		var formulae = X.utils.get_formulae(workbook.Sheets[sheetName]);
		if(formulae.length > 0){
			result.push("SHEET: " + sheetName);
			result.push("");
			result.push(formulae.join("\n"));
		}
	});
	return result.join("\n");
}
var tarea = document.getElementById('b64data');
function b64it() {
	if(typeof console !== 'undefined') console.log("onload", new Date());
	var wb = X.read(tarea.value, {type: 'base64',WTF:wtf_mode});
	process_wb(wb);
}
function process_wb(wb) {
	var output = "";
	switch(get_radio_value("format")) {
		case "json":
			output = JSON.stringify(to_json(wb), 2, 2);
			break;
		case "form":
			output = to_formulae(wb);
			break;
		default:
		output = to_csv(wb);
	}
		/*Se carga en excel_sheet el csv a enviar*/
	if(out.innerText === undefined) out.textContent = output;
	else out.innerText = output;
	if(typeof console !== 'undefined') console.log("output", new Date());
}
var drop = document.getElementById('drop');
function handleDrop(e) {
	e.stopPropagation();
	e.preventDefault();
	rABS = true;
	use_worker = true;
	var files = e.dataTransfer.files;
	var f = files[0];
	{
		var reader = new FileReader();
		var name = f.name;
		reader.onload = function(e) {
			if(typeof console !== 'undefined') console.log("onload", new Date(), rABS, use_worker);
			var data = e.target.result;
			if(use_worker) {
				xw(data, process_wb);
			} else {
				var wb;
				if(rABS) {
					wb = X.read(data, {type: 'binary'});
				} else {
				var arr = fixdata(data);
					wb = X.read(btoa(arr), {type: 'base64'});
				}
				process_wb(wb);
			}
		};
		if(rABS) reader.readAsBinaryString(f);
		else reader.readAsArrayBuffer(f);
	}
	$( ".fieldset_form_popup" ).css({"width":"150%","margin-left": "-25.5%"});
}
function handleDragover(e) {
	e.stopPropagation();
	e.preventDefault();
	e.dataTransfer.dropEffect = 'copy';
}
if(drop.addEventListener) {
	drop.addEventListener('dragenter', handleDragover, false);
	drop.addEventListener('dragover', handleDragover, false);
	drop.addEventListener('drop', handleDrop, false);
}
var xlf = document.getElementById('xlf');
function handleFile(e) {
	rABS = true;
	use_worker = true;
	var files = e.target.files;
	var f = files[0];
	{
		var reader = new FileReader();
		var name = f.name;
		reader.onload = function(e) {
			if(typeof console !== 'undefined') console.log("onload", new Date(), rABS, use_worker);
			var data = e.target.result;
			if(use_worker) {
				xw(data, process_wb);
			} else {
				var wb;
				if(rABS) {
					wb = X.read(data, {type: 'binary'});
				} else {
				var arr = fixdata(data);
					wb = X.read(btoa(arr), {type: 'base64'});
				}
				process_wb(wb);
			}
		};
		if(rABS) reader.readAsBinaryString(f);
		else reader.readAsArrayBuffer(f);
	}
	$( ".fieldset_form_popup" ).css({"width":"150%","margin-left": "-25.5%"});
}
if(xlf.addEventListener) xlf.addEventListener('change', handleFile, false);
}
/*Librerias requeridas por la funcion de excel para el manejo de archivos csv */
function csvJSON(csv){
  var lines=csv.split("\n");
  var result = [];
  var headers=lines[0].split(",");
  for(var i=1;i<lines.length;i++){
	  var obj = {};
	  var currentline=lines[i].split(",");
	  for(var j=0;j<headers.length;j++){
		  obj[headers[j]] = currentline[j];
	  }
	  result.push(obj);
  }
  /*return result;*/ 
  /*JavaScript object*/
  return JSON.stringify(result); /*JSON*/
}
/*Form content del modulo de items y item_kits */
function content_forms(module,base_url){
	$('.content_forms_ajax').magnificPopup(
	{
		type: 'ajax',
		alignTop: true,
		ajax:{
				settings: null, /* Objeto de configuración Ajax */
				cursor: 'mfp-ajax-cur', 
				/* Clase CSS que se agregará al cuerpo durante la carga (agrega cursor de "progreso") */
				tError: '<a href="%url%">The content</a> could not be loaded.', 
				 /* Mensaje de error, puede contener etiquetas %curr% y %total% si la galería está habilitada */
				 tLoading: '<img class="load_gif_form" src="../images/cc_images/icon_cct/load.gif"/>'
			},
			callbacks: 
			{
				/* var header=load('<?php $this->load->view("cc_views/partial/header_forms"); ?>'); */
				parseAjax: function(mfpResponse) 
				{
				/* mfpResponse.data es un objeto de "datos" de ajax "éxito" de devolución de llamada */
				/* para archivo HTML simple, será solo String */
				/* Puede modificarlo para cambiar el contenido de la ventana emergente */
				/* Por ejemplo, para mostrar solo #algún-elemento: */
				},
				ajaxContentAdded: function() 
				{
					/* El contenido de Ajax se carga y se agrega al DOM */
					load_image(module,base_url);
					$('.submit_content_button').click(function ()
					{
						var hidde_id =$("#id_input").val();
						var txt_vals={};
                        var file_vals=[];
                        $(":input").each(function(index){
                            if(this.type=="textarea")
                            {
                                txt_vals[this.name]=this.value;
                            }    
                            else if(this.type=="file" )
                            {
                                file_vals[this.id]=this.value;
                            }
                        });
                        $.ajax({
                        type: "POST",
                        url:base_url+"index.php/"+module+"/save_edit_content/"+hidde_id,
						data:txt_vals,
						dataType: 'json',
						minLength: 2,
						delay: 1,
						cache: false
						}).done(function(data)
						{
							jobj=data;
									var row={row_id:jobj.id}
										if(jobj.success=="true")
										{   /*alert("true");*/
												$( document.body ).scrollTop(200);
												$( "#row_"+jobj.id+"" ).remove();
												$.ajax(
													{
														url:base_url+"index.php/"+module+"/get_row/",
														type: "POST",
														data: row,
														dataType: 'html',
														minLength: 3,
														delay: 100,
														cache: false
													}).done(function( data )
												{
													$( "#empty" ).remove();
													$( document.body ).scrollTop(200);
													$( "#cover_resultados_tabla" ).fadeIn(1000,function()
													{
														$( "#cover_interaction_bar" ).append( "<div id='msj_manages'><span id='msg'>"+jobj.message+"</span></div>" );
														$("#cover_resultados_tabla").add().prepend(data);
														$("#row_"+jobj.id+"" ).css({"background-color":" rgb(194, 255, 195)"});
													});
													$( '#msj_manages' ).fadeOut( 3000, function()
													{
														$( "#row_"+jobj.id+"" ).css({"background-color":"rgb(240, 249, 240)","border": "none"});
														$( '#msj_manages'  ).remove();
													});
												info_forms(module,base_url);
												form_ajax(module,base_url);
												content_forms(module,base_url);
												items_forms(module,base_url);
												});
										}
										else if(jobj.success=="false")
										{
											$( document.body ).scrollTop(200);
											$( "#cover_interaction_bar" ).fadeIn(1000,function()
											{
											$( "#cover_interaction_bar" ).append( "<div id='msj_error_manages'><span id='msg'>"+jobj.message+"</span></div>" );
											});
											$( '#msj_error_manages' ).fadeOut( 4500, function()
											{
											$( '#msj_error_manages'  ).remove();
											});
										}
										$('.content_forms_ajax').magnificPopup('close');
						});
						$('.content_forms_ajax').magnificPopup('close');
					});
					/* */
					$('.submit_cat_button').click(function (){
						/*alert("yeah");*/
						var hidde_id =$("#id_input").val();
						var txt_vals={};
                        var file_vals=[];
                        $(":input").each(function(index){
                            if(this.type=="textarea")
                            {
                                txt_vals[this.name]=this.value;
                            }
                            else if(this.type=="file" )
                            {
                                file_vals[this.id]=this.value;
                            }
                        });
                        $.ajax({
                        type: "POST",
                        url:base_url+"index.php/"+module+"/save_edit_cat_content/"+hidde_id,
						data:txt_vals,
						dataType: 'json',
						minLength: 2,
						delay: 1,
						cache: false
						}).done(function(data)
						{
							jobj=data;
									var row={row_id:jobj.id}
										if(jobj.success=="true")
										{   /*alert("true");*/
												$( document.body ).scrollTop(200);
												$( "#row_"+jobj.id+"" ).remove();
												$.ajax(
													{
														url:base_url+"index.php/"+module+"/get_row/",
														type: "POST",
														data: row,
														dataType: 'html',
														minLength: 3,
														delay: 100,
														cache: false
													}).done(function( data )
												{
													$( "#empty" ).remove();
													$( document.body ).scrollTop(200);
													$( "#cover_resultados_tabla" ).fadeIn(1000,function()
													{
														$( "#cover_interaction_bar" ).append( "<div id='msj_manages'><span id='msg'>"+jobj.message+"</span></div>" );
														$("#cover_resultados_tabla").add().prepend(data);
														$("#row_"+jobj.id+"" ).css({"background-color":" rgb(194, 255, 195)"});
													});
													$( '#msj_manages' ).fadeOut( 3000, function()
													{
														$( "#row_"+jobj.id+"" ).css({"background-color":"rgb(240, 249, 240)","border": "none"});
														$( '#msj_manages'  ).remove();
													});
													info_forms(module,base_url);
													form_ajax(module,base_url);
													content_forms(module,base_url);
													items_forms(module,base_url);
												});
										}
										else if(jobj.success=="false")
										{
											$( document.body ).scrollTop(200);
											$( "#cover_interaction_bar" ).fadeIn(1000,function()
											{
											$( "#cover_interaction_bar" ).append( "<div id='msj_error_manages'><span id='msg'>"+jobj.message+"</span></div>" );
											});
											$( '#msj_error_manages' ).fadeOut( 4500, function()
											{
											$( '#msj_error_manages'  ).remove();
											});
										}
										$('.content_forms_ajax').magnificPopup('close');
						});
						$('.content_forms_ajax').magnificPopup('close');
					});
				}
			}
	});
}
/*Popup del form de operaciones en el modulo de recuros */
function recursos_operation(module,base_url){
	$('.operations-popup-link').magnificPopup({
				type: 'ajax',
		alignTop: true,
		ajax:
		{
			settings: null,
			cursor: 'mfp-ajax-cur',
			tError: '<a href="%url%">The content</a> could not be loaded.',
			tLoading: '<img class="load_gif_form" src="../images/cc_images/icon_cct/load.gif"/>'
		},
		callbacks: 
		{
			parseAjax: function(mfpResponse) 
			{
				mfpResponse.data =$(mfpResponse.data).find(":first");
			},
			ajaxContentAdded: function() 
			{
				var jsonerrors=jQuery.parseJSON($("#jsonerrors").val());
				$('.submit_button_pop').click(function (){
					var respuesta=validar_operation_form(jsonerrors);
					if(respuesta== true)
					{
						submit_operations_form(module,base_url);
					}
				});
				$('#operation_manual').click(function (){
				if ($('input[id="operation_manual"]').prop("checked")==true){
				$('input[id="operation_manual"]').prop("checked", true);
				$("#manual_op").css("display","block");
				$('input[id="transfer"]').prop("checked", false);
				$("#trans").css("display","none");
				$("#xchange_rate_row").css("display","none");
				}
				});
				$('#transfer').click(function (){
				if ($('input[id="transfer"]').prop("checked")==true){
				$('input[id="operation_manual"]').prop("checked", true);
				$("#trans").css("display","block");
				$('input[id="operation_manual"]').prop("checked", false);
				$("#manual_op").css("display","none");
				if($("#recursos").val()!="-1")get_coin(base_url);
				}
				});
				$("#recursos").change(function(){
					get_recurso_info(base_url);
				});
			}
		}
	});
}
/*Obtiene la informacion de un recurso via ajax y la inserta como inputs hidde en el form */
function get_recurso_info(base_url){
	$.ajax(
	{
	type: "POST",
	url:base_url+"index.php/recursos/get_recurso_info/",
	data: {recurso_id:$("#recursos").val()},
	dataType: 'json',
	minLength: 3,
	delay: 100,
	cache: false
	}).done(function(data) 
	{
		$( ".fieldset_form_popup" ).append( "<input type='hidden' name='other_resource_recurso' id='other_resource_recurso' value='"+data.recurso+"'>" );
		$( ".fieldset_form_popup" ).append( "<input type='hidden' name='other_resource_coin' id='other_resource_coin' value='"+data.coin+"'>" );
		$( ".fieldset_form_popup" ).append( "<input type='hidden' name='other_resource_nombre' id='other_resource_nombre' value='"+data.nombre+"'>" );
		if(data.coin!=$("#coin").val())$("#xchange_rate_row").css("display","block");
	});	
}
/*Sugiere los proveedores existentes en el input de proveedores del form de gastos */
function suggest_suppliers(base_url){
	$( "#search_suppliers" ).autocomplete({
	appendTo: "#ui-widget-suppliers",
	autoFocus: true,
	source: function (request, response){
		$.ajax(
		{
		type: "POST",
		url:base_url+"index.php/suppliers/suggest/s",
		data: {q:$('#search_suppliers').val()},
		success: response,
		dataType: 'json',
		minLength: 2,
		delay: 1,
		cache: false
		}).done(function(data){
			if($(".ui-autocomplete" ).css("height").replace(/[a-z]/g, '')>201){
				$(".ui-autocomplete" ).css("height", "200px")
			}
			else{
				$(".ui-autocomplete" ).css("height", "auto")
			}
		});
	}});
}
/*Sugiere las categorias existentes en el input de category del form de items */
function suggest_category(module,base_url){
	$( "#category" ).autocomplete({
					appendTo: "#ui-widget-category",
					autoFocus: true,
					source: function (request, response) {
					$.ajax(
					{
						type: "POST",
						url:base_url+"index.php/"+module+"/suggest_category/",
						data: {q:$('#category').val()},
						success: response,
						dataType: 'json',
						minLength: 2,
						delay: 1,
						cache: false
					});
					},
					response: function( event, ui ) {
					 },
					change: function( event, ui ) 
					{},
					select: function( event, ui ) {
					}});
}
/*Sugiere las marcas existentes en el input de brand del form de items */
function suggest_brand(module,base_url){
	$( "#brand" ).autocomplete({
	appendTo: "#ui-widget-brand",
	autoFocus: true,
	source: function (request, response) {
	$.ajax(
	{
	type: "POST",
	url:location.href+"/suggest_brand/",
	data: {q:$('#brand').val()},
	success: response,
	dataType: 'json',
	minLength: 2,
	delay: 1,
	cache: false
	});
	},
	response: function( event, ui ) {
	},
	change: function( event, ui ) 
	{},
	select: function( event, ui ) {
	}});
}
/*Hace que el input de imagenes adicionales solo acepte numeros y agrega dos botones de + y - para aumentar o disminuir la cantidad */
function quantity_input(){
	jQuery('<div class="quantity-nav"><div class="quantity-button quantity-up">+</div><div class="quantity-button quantity-down">-</div></div>').insertAfter('.quantity input');
	jQuery('.quantity').each(function() {
		var spinner = jQuery(this),
		input = spinner.find('input[type="number"]'),
		btnUp = spinner.find('.quantity-up'),
		btnDown = spinner.find('.quantity-down'),
		min = input.attr('min'),
		max = input.attr('max');
		btnUp.click(function() {
			var oldValue = parseFloat(input.val());
			if (oldValue >= max) {
			var newVal = oldValue;
			} else {
			var newVal = oldValue + 1;
			}
			spinner.find("input").val(newVal);
			spinner.find("input").trigger("change");
		});
		btnDown.click(function() {
			var oldValue = parseFloat(input.val());
			if (oldValue <= min) {
				var newVal = oldValue;
			} else {
				var newVal = oldValue - 1;
			}
			spinner.find("input").val(newVal);
			spinner.find("input").trigger("change");
		});
	});
}
/* Muestra los campos fiscales en los forms fiscales */
function show_fiscal_form(){
	if ($('input[id="taxable"]').prop("checked")==true){
		$('input[id="taxable"]').prop("checked", true);
		$(".fiscales").css("display","block");
	}
	$('#taxable').click(function (){
		if ($('input[id="taxable"]').prop("checked")==true){
			$('input[id="taxable"]').prop("checked", true);
			$(".fiscales").css("display","block");
		}
		else{
			$('input[id="taxable"]').prop("checked", false);
			$(".fiscales").css("display","none");
		}
	});
	$('#same_address').click(function (){
		if($("#ce").val()=="" || $("#municipio").val()=="" || $("#estado").val()=="" || $("#pais").val()=="" || $("#cp").val()=="")
		{
			$("#ce").val($("#address_1").val());
			$("#colonia").val($("#address_1").val());
			$("#localidad").val($("#city").val());
			$("#municipio").val($("#city").val());
			$("#estado").val($("#state").val());
			$("#pais").val($("#country").val());
			$("#cp").val($("#zip").val());
		}
		else
		{
			$("#ce").val("");
			$("#colonia").val("");
			$("#municipio").val("");
			$("#estado").val("");
			$("#pais").val("");
			$("#cp").val("");
			$("#localidad").val("");
		}
	});
}
/* Función para los gastos recurrentes*/
function gastos_recurrentes(base_url){
	var gasto_recurrente =$("#gasto_recurrente").val();
	var suscrito_box =$("#suscrito_box").val();
	/*$("#cantidad").css("width","49%");*/
	/*$("#search_suppliers").css("width","98%");*/
	var gasto_id=$("#id_input").val();
	if(suscrito_box!=0){
		$("#category" ).css('background-color','#dddddd');
		$("#category" ).attr('disabled','disabled');
		if(gasto_id==""){
			$("#estado" ).css('background-color','#dddddd');
			$("#estado" ).attr('disabled','disabled');
		}
		$('#gasto_fijo').change( function (evt) {
			if(confirm("Ya no será un gasto periodico y se borraran todos los futuros gastos recurrentes iguales")==true){
				$("#suscrito_box").val(0);
				$.ajax(
				{
				type: "POST",
				url:base_url+"index.php/gastos/cancelar_gastos_recurrentes/",
				dataType: 'json',
				data:{gasto_id:gasto_id},
				minLength: 2,
				delay: 1,
				cache: false
				});
			}
			else
			{
				$('#gasto_fijo').prop("checked",'checked');
			}
		});
	}
	$('#suscrito_box').change( function (evt) {
		gasto_recurrente =$("#gasto_recurrente").val();
		suscrito_box =$("#suscrito_box").val();
		if(suscrito_box==0){
			$("#category").val("");
			$("#category" ).css('background-color','#FFF');
			$("#category" ).removeAttr('disabled');
			$("#estado" ).css('background-color','#FFF');
			$("#estado" ).removeAttr('disabled');
		}
		else{
			$("#category").val(gasto_recurrente);
			$("#category" ).css('background-color','#dddddd');
			$("#category" ).attr('disabled','disabled');
			$("#estado" ).css('background-color','#dddddd');
			$("#estado" ).attr('disabled','disabled');
		}
	});	
}
/* Funciones para agregar formas de pago en el form de recuros */
function formas_pago(base_url){
	$("#formas_pago").change(function(){
		if($("#formas_pago").val()=="-2")$("#formas_pago").replaceWith("<input type='text' id='formas_pago' name='formas_pago' value=''/>");
		$("#formas_pago").focus();
	});
	$( ".delete" ).each(function (i, e){
		var elem_to_del=e.id;
		var id_split= elem_to_del.split("_");
		var row_id = parseInt(id_split[1]);
		$( '#'+e.id+'' ).click(function()
		{ 
		/*alert(row_id);*/
		$.ajax({
		type: "POST",
		url:base_url+"index.php/recursos/delete_forma_pago/"+row_id,
		dataType: 'json',
		minLength: 2,
		delay: 1,
		cache: false
		}).done(function(data)
		{
		if(data.success==true)$( '#result_name_recursos_pagos_'+row_id+'' ).remove();
		});
		});
	});
}
/* Funciones para agregar, editar y borrar los items en el form de items_kit */
function items_en_kits(module,base_url){
	$( ".delete" ).each(function (i, e){
		var elem_to_del=e.id;
		var id_split= elem_to_del.split("_");
		var row_id = parseInt(id_split[1]);
		$( '#'+e.id+'' ).click(function(){
			/*alert(row_id);*/
			$( '#result_name_kit_'+row_id+'' ).remove();
			$.ajax({
			type: "POST",
			url:base_url+"index.php/item_kits/delete_item/"+row_id,
			success: response,
			dataType: 'json',
			minLength: 2,
			delay: 1,
			cache: false
			});
		});
		$('#'+row_id+'').change(function(evt){
			/*alert($('#'+row_id+'').val());*/
			$.ajax({
			type: "POST",
			url:base_url+"index.php/item_kits/edit_item/"+row_id,
			success: response,
			data:{quantity:$('#'+row_id+'').val()},
			dataType: 'json',
			minLength: 2,
			delay: 1,
			cache: false
			});
		});
		$('#'+row_id+'').on("click",function(){
		$('#'+row_id+'').select();
		});
	});
	/**/
	$("#search_item").jkey('enter',function(){
		$("input[name*='search_item']").autocomplete( "close" );
		var item =$("input[name*='search_item']").val();
		item = item.split("|", 1);
		var item_id = parseInt(item);
		if(!isNaN(item_id)){
			$.ajax(
			{
			type: "POST",
			url:base_url+"index.php/item_kits/items_in_kit/"+item_id,
			dataType: 'json',
			minLength: 2,
			delay: 1,
			cache: false
			}).done(function(data){

			$( ".detail_kit" ).remove();
			$.each( data, function(key,value){

				var quantity=parseFloat(value.quantity);
				var item_id = parseInt(value.item_id);
				var row_id = item_id;
				/*alert(row_id);*/
				/* var name=key_split[1];*/
				$("#cover_add_result_kit").append("<div class='file_add_result_kit' id='result_name_kit_"+item_id+"'><div class='detail_kit' id='result_name_kit'><span>"+value.name+"</span></div><div class='detail_kit'><span><input class='quantity' id='"+item_id+"' type='text' size='3' name=item_kit_item["+item_id+"] value='"+quantity+"'/></span></div><div class='detail_kit'><span><a class='delete' id='del_"+item_id+"'>X</a></span></div></div>");
				$( '#del_'+item_id+'' ).click(function(){ 
					$( '#result_name_kit_'+row_id+'' ).remove();
				});
				$('#'+row_id+'').change(function(evt){
					/*alert($('#'+row_id+'').val());*/
					$.ajax({
					type: "POST",
					url:base_url+"index.php/item_kits/edit_item/"+row_id,
					success: response,
					data:{quantity:$('"#'+row_id+'"').val()},
					dataType: 'json',
					minLength: 2,
					delay: 1,
					cache: false
					});
				});
				$('#'+row_id+'').on("click",function(){
					$('#'+row_id+'').select();
				});
			});
				$("input[name*='search_item']").val("");
				$("input[name*='search_item']").focus();
			});
		}
		else{
			$("input[name*='search_item']").val("");
			$("input[name*='search_item']").focus();
		}
	});
	/**/
	$( "input[name*='search_item']" ).autocomplete({
		appendTo: "#ui-widget-items",
		autoFocus: true,
		source: function (request, response){
			$.ajax({
			type: "POST",
			url:base_url+"index.php/items/items_only_search/",
			data: {q:$("input[name*='search_item']").val()},
			success: response,
			dataType: 'json',
			minLength: 2,
			delay: 1,
			cache: false
			});
		},
		change: function (event, ui){
			var item =event.target.value;
			var item_split = item.split("|", 1);
			var item_id = parseInt(item_split[0]);
			if(!isNaN(item_id)){
				$.ajax(
				{
				type: "POST",
				url:base_url+"index.php/item_kits/items_in_kit/"+item_id,
				dataType: 'json',
				minLength: 2,
				delay: 1,
				cache: false
				}).done(function(data){
					$( ".detail_kit" ).remove();
					$.each( data, function( key, value ) {

						var quantity=parseFloat(value.quantity);
						var item_id = parseInt(value.item_id);
						var row_id = item_id;
						/*alert(row_id);*/

						quantity =	quantity.toFixed(2);  	
						/* var name=key_split[1];*/
						$("#cover_add_result_kit").append("<div class='file_add_result_kit' id='result_name_kit_"+item_id+"'><div class='detail_kit' id='result_name_kit'><span>"+value.name+"</span></div><div class='detail_kit'><span><input class='quantity' id='"+item_id+"' type='text' size='3' name=item_kit_item["+item_id+"] value='"+quantity+"'/></span></div><div class='detail_kit'><span><a class='delete' id='del_"+item_id+"'>X</a></span></div></div>");
						$( '#del_'+item_id+'' ).click( function(){ 
							$( '#result_name_kit_'+row_id+'' ).remove();
						});
						$('#'+row_id+'').change( function(evt){
							/*alert($('#'+row_id+'').val());*/
							$.ajax({
							type: "POST",
							url:base_url+"index.php/item_kits/edit_item/"+row_id,
							data:{quantity:$("#"+row_id).val()},
							dataType: 'json',
							minLength: 2,
							delay: 1,
							cache: false
							});
						});
						$('#'+row_id+'').on("click", function () {
							$('#'+row_id+'').select();
						});
					});
					$("input[name*='search_item']").val("");
					$("input[name*='search_item']").focus();
				});
			}
			else{
				$("input[name*='search_item']").val("");
				$("input[name*='search_item']").focus();
			}
		},
		select: function( event, ui ){
			$("input[name*='search_item']").val("");
			$("input[name*='search_item']").focus();
			try{	

				var item =event.toElement.innerText;
				var item_split= item.split("|");
				var item_id = parseInt(item_split[0]);
				$.ajax(
				{
				type: "POST",
				url:base_url+"index.php/item_kits/items_in_kit/"+item_id,
				dataType: 'json',
				minLength: 2,
				delay: 1,
				cache: false
				}).done(function(data){

					$( ".detail_kit" ).remove();
					$.each( data, function( key, value ) {
						/*var key_split= key.split("|");*/
						var row_id = parseInt(item_id);
						var item_id = parseInt(value.item_id);
						var quantity=parseFloat(value.quantity);
						quantity =	quantity.toFixed(2);  
						$("#cover_add_result_kit").append("<div class='file_add_result_kit' id='result_name_kit_"+item_id+"'><div class='detail_kit' id='result_name_kit'><span>"+value.name+"</span></div><div class='detail_kit'><span><input class='quantity' id='"+item_id+"' type='text' size='3' name=item_kit_item["+item_id+"] value='"+quantity+"'/></span></div><div class='detail_kit'><span><a class='delete' id='del_"+item_id+"'>X</a></span></div></div>");
						$( '#del_'+item_id+'' ).click(function(){ 
							$( '#result_name_kit_'+item_id+'' ).remove();
						});
						$('#'+item_id+'' ).change( function (evt) {
							/*alert($('#'+item_id+'' ).val());*/
							$.ajax({
							type: "POST",
							url:base_url+"index.php/item_kits/edit_item/"+row_id,
							success: response,
							data:{quantity:$('#'+item_id+'' ).val()},
							dataType: 'json',
							minLength: 2,
							delay: 1,
							cache: false
							});
						});
						$('#'+item_id+'').on("click", function () {
							$('#'+item_id+'').select();
						});
					});
					$("input[name*='search_item']").val("");
					$("input[name*='search_item']").focus();
				});
			}
			catch(e){
				$("input[name*='search_item']").val("");
				$("input[name*='search_item']").focus();
			}
		}
	});
}
/* Guarda los datos del formulario del modulo de empleados */
function submit_employees_form(module,base_url){
	var hidde_id =$("#id_input").val();
	var person_data={
		first_name:$("#first_name").val(),
		last_name:$("#last_name").val(),
		phone_number:$("#phone_number").val(),
		email:$("#email").val(),
		address_1:$("#address_1").val(),
		address_2:$("#address_2").val(),
		city:$("#city").val(),
		state:$("#state").val(),
		zip:$("#zip").val(),
		country:$("#country").val(),
		comments:$("#comments").val(),
		username:$("#username").val(),
		password:$("#password").val(),
		permissions:[]
	}
	var permission_array=[];
	$("input[name='permissions[]']:checked").each(function (i, e){
		permission_array['permissions']= e.value;
		person_data.permissions.push(permission_array['permissions']);
	});
	$.ajax(
	{
	type: "POST",
	url:base_url+"index.php/employees/save/"+hidde_id,
	dataType: 'json',
	data:person_data,
	minLength: 2,
	delay: 1,
	cache: false
	}).done(function( data ){
		jobj=data;
		var row={row_id:jobj.person_id}
		if(jobj.success=="true"){
			$('#empty').remove();
			if(jobj.person_id==-1){
				$.ajax(
				{
				type: "POST",
				url:base_url+"index.php/employees/get_row/",
				data: {row_id:jobj.person_id},
				dataType: 'json',
				minLength: 3,
				delay: 100,
				cache: false
				}).done(function( data ) 
				{
					$( "#empty" ).remove();
					$( document.body ).scrollTop(200);
					$( "#cover_resultados_tabla" ).fadeIn(1000,function(){
						$( "#cover_interaction_bar" ).append( "<div id='msj_manages'><span id='msg'>"+jobj.message+"</span></div>" );
						$("#cover_resultados_tabla").add().prepend(data);
						$("#row_"+jobj.person_id+"" ).css({"background-color":" rgb(194, 255, 195)"});
					});
					$( '#msj_manages' ).fadeOut( 3000, function(){
						$( "#row_"+jobj.person_id+"" ).css({"background-color":"rgb(240, 249, 240)","border": "none"});
						$( '#msj_manages'  ).remove();
					});
					info_forms(module,base_url);
					form_ajax(module,base_url);
				});
			}
			else{
				var first_p=$( document.body ).scrollTop();
				$( document.body ).scrollTop(200);
				$( "#row_"+jobj.person_id+"" ).remove();
				$.ajax(
				{
				type: "POST",
				url:base_url+"index.php/employees/get_row/",
				data: {row_id:jobj.person_id},
				dataType: 'html',
				minLength: 3,
				delay: 100,
				cache: false
				}).done(function( data )
				{
					$( document.body ).scrollTop(200);
					$( "#cover_resultados_tabla" ).fadeIn(1000,function()
					{
						$( "#cover_interaction_bar" ).append( "<div id='msj_manages'><span id='msg'>"+jobj.message+"</span></div>" );
						$("#cover_resultados_tabla").add().prepend(data);
						$("#row_"+jobj.person_id+"" ).css({"background-color":" rgb(194, 255, 195)"});
					});
					$( '#msj_manages' ).fadeOut( 3000, function()
					{
						$( document.body ).scrollTop(first_p);
						$( "#row_"+jobj.person_id+"" ).css({"background-color":"rgb(240, 249, 240)","border": "none"});
						$('#msj_manages').remove();
					});
					info_forms(module,base_url);
					form_ajax(module,base_url);
				});
			}
		}
		else if(jobj.success=="false")
		{
			$( document.body ).scrollTop(200);
			$( "#cover_interaction_bar" ).fadeIn(1000,function(){
				$( "#cover_interaction_bar" ).append( "<div id='msj_error_manages'><span id='msg'>"+jobj.message+"</span></div>" );
			});
			$('#msj_error_manages').fadeOut( 4500, function(){
				$('#msj_error_manages').remove();
			});
		}
	});
	$('.ajax-popup-link').magnificPopup('close');
}
/* Guarda los datos del formulario del modulo de proveedores */
function submit_suppliers_form(module,base_url){
	var ans=true;
	var fiscalerrors=jQuery.parseJSON($("#json_fiscal_errors").val());
	var hidde_id =$("#id_input").val();
	if ($('input[id="taxable"]').prop("checked")==true){
		ans=validar_form_fiscal(fiscalerrors);
		var person_data={
			first_name:$("#first_name").val(),
			last_name:$("#last_name").val(),
			phone_number:$("#phone_number").val(),
			email:$("#email").val(),
			address_1:$("#address_1").val(),
			address_2:$("#address_2").val(),
			city:$("#city").val(),
			state:$("#state").val(),
			zip:$("#zip").val(),
			country:$("#country").val(),
			comments:$("#comments").val(),
			accounts_number:$("#accounts_number").val(),
			company:$("#company").val(),
			rfc:$("#rfc").val(),
			razon_social:$("#razon_social").val(),
			ce:$("#ce").val(),
			email:$("#email").val(),
			no_exterior:$("#no_exterior").val(),
			no_interior:$("#no_interior").val(),
			colonia:$("#colonia").val(),
			localidad:$("#localidad").val(),
			municipio:$("#municipio").val(),
			estado:$("#estado").val(),
			pais:$("#pais").val(),
			cp:$("#cp").val()
		}
	}
	else{
		var person_data={
			first_name:$("#first_name").val(),
			last_name:$("#last_name").val(),
			phone_number:$("#phone_number").val(),
			email:$("#email").val(),
			address_1:$("#address_1").val(),
			address_2:$("#address_2").val(),
			city:$("#city").val(),
			state:$("#state").val(),
			zip:$("#zip").val(),
			country:$("#country").val(),
			comments:$("#comments").val(),
			accounts_number:$("#accounts_number").val(),
			company:$("#company").val(),
		}
	}
	if(ans){
		$.ajax(
		{
		type: "POST",
		url:base_url+"index.php/suppliers/save/"+hidde_id,
		dataType: 'json',
		data:person_data,
		minLength: 2,
		delay: 1,
		cache: false
		}).done(function( data ){
			jobj=data;
			var row={row_id:jobj.person_id}
			if(jobj.success=="true")
			{  
				if(jobj.person_id==-1){	
					$.ajax(
					{
					type: "POST",
					url:base_url+"index.php/suppliers/get_row/",
					data: {row_id:jobj.person_id},
					dataType: 'json',
					minLength: 3,
					delay: 100,
					cache: false
					}).done(function(data) 
					{
						$('#empty').remove();
						$( document.body ).scrollTop(200);
						$( "#cover_resultados_tabla" ).fadeIn(1000,function(){
							$( "#cover_interaction_bar" ).append( "<div id='msj_manages'><span id='msg'>"+jobj.message+"</span></div>" );
							$("#cover_resultados_tabla").add().prepend(data);
							$("#row_"+jobj.person_id+"" ).css({"background-color":" rgb(210, 255, 210)"});
						});
						$( '#msj_manages' ).fadeOut( 3000, function(){
							$( "#row_"+jobj.person_id+"" ).css({"background-color":"rgb(240, 249, 240)","border": "none"});
							$( '#msj_manages'  ).remove();
						});
						info_forms(module,base_url);
						form_ajax(module,base_url);
					});
				}
				else
				{
				var first_p=$( document.body ).scrollTop();
				$( document.body ).scrollTop(200);
				$( "#row_"+jobj.person_id+"" ).remove();
				$.ajax(
				{
				type: "POST",
				url:base_url+"index.php/suppliers/get_row/",
				data: {row_id:jobj.person_id},
				dataType: 'html',
				minLength: 3,
				delay: 100,
				cache: false
				}).done(function( data ) {
					$( document.body ).scrollTop(200);
					$( "#cover_resultados_tabla" ).fadeIn(1000,function(){
						$( "#cover_interaction_bar" ).append( "<div id='msj_manages'><span id='msg'>"+jobj.message+"</span></div>" );
						$("#cover_resultados_tabla").add().prepend(data);
						$("#row_"+jobj.person_id+"" ).css({"background-color":" rgb(194, 255, 195)"});
					});
					$( '#msj_manages' ).fadeOut( 3000, function(){
						$( document.body ).scrollTop(first_p);
						$( "#row_"+jobj.person_id+"" ).css({"background-color":"rgb(240, 249, 240)","border": "none"});
						$( '#msj_manages'  ).remove();
					});
					info_forms(module,base_url);
					form_ajax(module,base_url);
				});
				}
			}
			else if(jobj.success=="false"){
				$( document.body ).scrollTop(200);
				$( "#cover_interaction_bar" ).fadeIn(1000,function(){
					$( "#cover_interaction_bar" ).append( "<div id='msj_error_manages'><span id='msg'>"+jobj.message+"</span></div>" );
				});
				$( '#msj_error_manages' ).fadeOut( 4500, function(){
					$( '#msj_error_manages'  ).remove();
				});
			}
		});
		$('.ajax-popup-link').magnificPopup('close');
	}
}
/* Guarda los datos del formulario del modulo de items */
function submit_items_form(module,base_url){
	var hidde_id =$("#id_input").val();
	var publish=0,destacar=0,show_price=0,show_stock=0,promocionar=0,allow_alt_description=0,is_serialized=0;
	if($("#publish").prop("checked")==true)publish=1;
	if($("#destacar").prop("checked")==true)destacar=1;
	if($("#show_price").prop("checked")==true)show_price=1;
	if($("#show_stock").prop("checked")==true)show_stock=1;
	if($("#promocionar").prop("checked")==true)promocionar=1;
	if($("#allow_alt_description").prop("checked")==true)allow_alt_description=1;
	if($("#is_serialized").prop("checked")==true)is_serialized=1;
	var item_data={
		name:$("#name").val(),
		brand:$("#brand").val(),
		modelo:$("#modelo").val(),
		category:$("#category").val(),
		supplier:$("#supplier").val(),
		cost_price:$("#cost_price").val(),
		unit_price:$("#unit_price").val(),
		other_price:$("#other_price").val(),
		quantity:$("#quantity").val(),
		reorder_level:$("#reorder_level").val(),
		item_number:$("#item_number").val(),
		tax_names:[],
		tax_percents:[],
		location:$("#location").val(),
		description:$("#description").val(),
		publish:publish,
		destacar:destacar,
		show_price:show_price,
		show_stock:show_stock,
		promocionar:promocionar,
		allow_alt_description:allow_alt_description,
		is_serialized:is_serialized,
		c_ClaveUnidad:$("#ClaveUnidad").val(),
		c_ClaveProdServ:$("#ClaveProdServ").val(),
		img_qty:$("#img_qty").val()
	}
	$("input[name='tax_names[]']").each(function (i, e){
		item_data.tax_names.push(e.value);
	});
	$("input[name='tax_percents[]']").each(function (i, e){
		item_data.tax_percents.push(e.value);
	});
	$.ajax(
	{
	type: "POST",
	url:base_url+"index.php/items/save/"+hidde_id,
	dataType: 'json',
	data:item_data,
	minLength: 2,
	delay: 1,
	cache: false
	}).done(function( data ){
	jobj=data;
	var row={row_id:jobj.item_id}
	if(jobj.success=="true"){
		$('#empty').remove();
		if(jobj.item_id==-1){
			$.ajax(
			{
			type: "POST",
			url:base_url+"index.php/items/get_row/",
			data: {row_id:jobj.item_id},
			dataType: 'html',
			minLength: 3,
			delay: 100,
			cache: false
			}).done(function(data){
				$( document.body ).scrollTop(200);
				$( "#cover_resultados_tabla" ).fadeIn(1000,function(){
					$( "#cover_interaction_bar" ).append( "<div id='msj_manages'><span id='msg'>"+jobj.message+"</span></div>" );
					$("#cover_resultados_tabla").add().prepend(data);
					$("#row_"+jobj.item_id+"" ).css({"background-color":" rgb(194, 255, 195)"});
				});
				$( '#msj_manages' ).fadeOut( 3000, function(){
					$( "#row_"+jobj.item_id+"" ).css({"background-color":"rgb(240, 249, 240)","border": "none"});
					$('#msj_manages').remove();
				});
			});
		}
		else
		{
			var first_p=$( document.body ).scrollTop();
			$( document.body ).scrollTop(200);
			$( "#row_"+jobj.item_id+"" ).remove();
			$.ajax(
			{
			type: "POST",
			url:base_url+"index.php/"+module+"/get_row/",
			data: {row_id:jobj.item_id},
			dataType: 'html',
			minLength: 3,
			delay: 100,
			cache: false
			}).done(function( data ){
				$( document.body ).scrollTop(200);
				$( "#cover_resultados_tabla" ).fadeIn(1000,function(){
					$( "#cover_interaction_bar" ).append( "<div id='msj_manages'><span id='msg'>"+jobj.message+"</span></div>" );
					$("#cover_resultados_tabla").add().prepend(data);
					$("#row_"+jobj.item_id+"" ).css({"background-color":" rgb(194, 255, 195)"});
				});
				$( '#msj_manages' ).fadeOut( 3000, function(){
					$( document.body ).scrollTop(first_p);
					$( "#row_"+jobj.item_id+"" ).css({"background-color":"rgb(240, 249, 240)","border": "none"});
					$( '#msj_manages'  ).remove();
				});
				info_forms(module,base_url);
				form_ajax(module,base_url);
				content_forms(module,base_url);
				items_forms(module,base_url);
			});
		}
	}
	else if(jobj.success=="false"){
		$( document.body ).scrollTop(200);
		$( "#cover_interaction_bar" ).fadeIn(1000,function(){
			$( "#cover_interaction_bar" ).append( "<div id='msj_error_manages'><span id='msg'>"+jobj.message+"</span></div>" );
		});
		$( '#msj_error_manages' ).fadeOut( 4500, function(){
			$('#msj_error_manages').remove();
		});
	}
	});
	$('.ajax-popup-link').magnificPopup('close');
}
/* Guarda los datos del formulario del modulo de recursos */
function submit_recursos_form(module,base_url){
	var hidde_id =$("#id_input").val();
	var f_pago=[];
	$('#formas_pago').each(function (i, e){
		f_pago[e.id]= e.value;
	});
	var formas_pago =$('#formas_pago').val();
	if(formas_pago=="-1")formas_pago="";
	var recursos_data={
		nombre_cuenta:$("#nombre_cuenta").val(),
		cantidad:$("#cantidad").val(),
		coin:$("#coin").val(),
		formas_pago:formas_pago
	}
	$.ajax(
	{
	type: "POST",
	url:base_url+"index.php/"+module+"/save/"+hidde_id,
	dataType: 'json',
	data:recursos_data,
	minLength: 2,
	delay: 1,
	cache: false
	}).done(function (data){
		jobj=data;
		var row={row_id:jobj.recurso_id}
		if(jobj.success=="true")
		{   /*alert("true");*/
			if(jobj.recurso_id==-1)
			{	
				$.ajax(
				{
				type: "POST",
				url:base_url+"index.php/"+module+"/get_row/",
				data: row,
				dataType: 'html',
				minLength: 3,
				delay: 100,
				cache: false
				}).done(function( data )
				{
					$( "#empty" ).remove();
					$( document.body ).scrollTop(200);
					$( "#cover_resultados_tabla" ).fadeIn(1000,function(){
						$( "#cover_interaction_bar" ).append( "<div id='msj_manages'><span id='msg'>"+jobj.message+"</span></div>" );
						$("#cover_resultados_tabla").add().prepend(data);
						$("#row_"+jobj.recurso_id+"" ).css({"background-color":" rgb(194, 255, 195)"});
					});
					$( '#msj_manages' ).fadeOut( 3000, function(){
						$( "#row_"+jobj.recurso_id+"" ).css({"background-color":"rgb(240, 249, 240)","border": "none"});
						$( '#msj_manages'  ).remove();
					});
				});
			}
			else
			{
				$( document.body ).scrollTop(200);
				$( "#row_"+jobj.recurso_id+"" ).remove();
				$.ajax(
				{
				type: "POST",
				url:base_url+"index.php/"+module+"/get_row/",
				data: row,
				dataType: 'html',
				minLength: 3,
				delay: 100,
				cache: false
				}).done(function( data ){
					$( "#empty" ).remove();
					$( document.body ).scrollTop(200);
					$( "#cover_resultados_tabla" ).fadeIn(1000,function(){
						$( "#cover_interaction_bar" ).append( "<div id='msj_manages'><span id='msg'>"+jobj.message+"</span></div>" );
						$("#cover_resultados_tabla").add().prepend(data);
						$("#row_"+jobj.recurso_id+"" ).css({"background-color":" rgb(194, 255, 195)"});
					});
					$( '#msj_manages' ).fadeOut( 3000, function(){
						$( "#row_"+jobj.recurso_id+"" ).css({"background-color":"rgb(240, 249, 240)","border": "none"});
						$( '#msj_manages'  ).remove();
					});
					 form_ajax(module,base_url);
                    recursos_operation(module,base_url);
				});
			}
		}
		else if(jobj.success=="false")
		{
			$( document.body ).scrollTop(200);
			$( "#cover_interaction_bar" ).fadeIn(1000,function(){
				$( "#cover_interaction_bar" ).append( "<div id='msj_error_manages'><span id='msg'>"+jobj.message+"</span></div>" );
			});
			$( '#msj_error_manages' ).fadeOut( 4500, function(){
				$( '#msj_error_manages'  ).remove();
			});
		}
		$('.ajax-popup-link').magnificPopup('close');
	});
}
/* Guarda los datos del formulario de operaciones en el modulo de recursos */
function submit_operations_form(module,base_url){
	var hidde_id =$("#id_input").val();
	/*var saldo_restante=(parseFloat($("#recurso").val()))+(parseFloat($("#monto").val())*xchange_rate);*/
	/*con esta condicion verirficamos el tipo de transacion que se desea hacer*/
	if($("#transfer").prop('checked')==true)
	{
		var saldo_restante=(parseFloat($("#recurso").val()))-(parseFloat($("#monto").val()));
		var operation_from_data=
		{
			coin:$("#coin").val(),
			monto:parseFloat($("#monto").val())*-1,
			concepto:$("#concept_transfer_from").val()+" "+$("#other_resource_nombre").val(),
			saldo_anterior:$("#recurso").val(),
			saldo_restante:saldo_restante
		}
		$.ajax(
		{
		type: "POST",
		url:base_url+"index.php/"+module+"/save_operation/"+hidde_id,
		dataType: 'json',
		data:operation_from_data,
		minLength: 2,
		delay: 1,
		cache: false
		}).done(function (data){
		var jobj= data;
		var row={'row_id':data.recurso_id};
		if(data.success== "true")
		{
			var xchange_rate=$("#xchange_rate").val();
			if(xchange_rate=="")xchange_rate=1;
			/*Se Parsea a float despues de checar que no sea una string vacia*/
			var monto =parseFloat($("#monto").val())*xchange_rate
			xchange_rate=parseFloat(xchange_rate);
			saldo_restante=(parseFloat($("#other_resource_recurso").val()))+monto;
			var operation_destiny_data=
			{
				coin:$("#other_resource_coin").val(),
				monto:monto,
				concepto:$("#concept_transfer_from").val()+" "+$("#recurso_name").val(),
				saldo_anterior:$("#other_resource_recurso").val(),
				saldo_restante:saldo_restante,
			}
			$.ajax(
			{
			type: "POST",
			url:base_url+"index.php/"+module+"/save_operation/"+$("#recursos").val(),
			dataType: 'json',
			data:operation_destiny_data,
			minLength: 2,
			delay: 1,
			cache: false
			});
			$( document.body ).scrollTop(200);
			$( "#row_"+jobj.recurso_id+"" ).remove();
			$.ajax(
			{
			type: "POST",
			url:base_url+"index.php/"+module+"/get_row/",
			data: row,
			dataType: 'html',
			minLength: 3,
			delay: 100,
			cache: false
			}).done(function( data ){
			$( "#empty" ).remove();
			$( document.body ).scrollTop(200);
			$( "#cover_resultados_tabla" ).fadeIn(1000,function(){
			$( "#cover_interaction_bar" ).append( "<div id='msj_manages'><span id='msg'>"+jobj.message+"</span></div>" );
			$("#cover_resultados_tabla").add().prepend(data);
			$("#row_"+jobj.recurso_id+"" ).css({"background-color":" rgb(194, 255, 195)"});
			});
			$( '#msj_manages' ).fadeOut( 3000, function(){
			$( "#row_"+jobj.recurso_id+"" ).css({"background-color":"rgb(240, 249, 240)","border": "none"});
			$( '#msj_manages'  ).remove();
			});
			form_ajax(module,base_url);
			recursos_operation(module,base_url);
			});
		}
		else if(data.success=="false")
		{
			$( document.body ).scrollTop(200);
			$( "#cover_interaction_bar" ).fadeIn(1000,function(){
			$( "#cover_interaction_bar" ).append( "<div id='msj_error_manages'><span id='msg'>"+jobj.message+"</span></div>" );
			});
			$( '#msj_error_manages' ).fadeOut( 4500, function(){
			$( '#msj_error_manages'  ).remove();
			form_ajax(module,base_url);	
			info_forms(module,base_url);
			});
		}
		$('.ajax-popup-link').magnificPopup('close');
		});
	}
	else
	{
		var saldo_restante=(parseFloat($("#recurso").val()))+(parseFloat($("#monto").val()));
		/*if($("#recurso").val()==0)saldo_restante=-1*saldo_restante;*/
		var operation_data={
		coin:$("#coin").val(),
		monto:$("#monto").val(),
		concepto:$("#concepto").val(),
		saldo_anterior:$("#recurso").val(),
		saldo_restante:saldo_restante
		}
		$.ajax(
		{
		type: "POST",
		url:base_url+"index.php/"+module+"/save_operation/"+hidde_id,
		dataType: 'json',
		data:operation_data,
		minLength: 2,
		delay: 1,
		cache: false
		}).done(function (data){
		var jobj= data;
		var row={'row_id':data.recurso_id};
		if(data.success== "true")
		{
		$( document.body ).scrollTop(200);
		$( "#row_"+jobj.recurso_id+"" ).remove();
		$.ajax(
		{
		type: "POST",
		url:base_url+"index.php/"+module+"/get_row/",
		data: row,
		dataType: 'html',
		minLength: 3,
		delay: 100,
		cache: false
		}).done(function( data ){
		$( "#empty" ).remove();
		$( document.body ).scrollTop(200);
		$( "#cover_resultados_tabla" ).fadeIn(1000,function(){
		$( "#cover_interaction_bar" ).append( "<div id='msj_manages'><span id='msg'>"+jobj.message+"</span></div>" );
		$("#cover_resultados_tabla").add().prepend(data);
		$("#row_"+jobj.recurso_id+"" ).css({"background-color":" rgb(194, 255, 195)"});
		});
		$( '#msj_manages' ).fadeOut( 3000, function(){
		$( "#row_"+jobj.recurso_id+"" ).css({"background-color":"rgb(240, 249, 240)","border": "none"});
		$( '#msj_manages'  ).remove();
		});
		form_ajax(module,base_url);
		recursos_operation(module,base_url);
		});
		}
		else if(data.success=="false")
		{
		$( document.body ).scrollTop(200);
		$( "#cover_interaction_bar" ).fadeIn(1000,function(){
		$( "#cover_interaction_bar" ).append( "<div id='msj_error_manages'><span id='msg'>"+jobj.message+"</span></div>" );
		});
		$( '#msj_error_manages' ).fadeOut( 4500, function(){
		$( '#msj_error_manages'  ).remove();
		form_ajax(module,base_url);	
		info_forms(module,base_url);
		});
		}
		$('.ajax-popup-link').magnificPopup('close');
		});
	}
}
/* Guarda los datos del formulario del modulo de gastos */
function submit_gastos_form(module,base_url){
	var hidde_id =$("#id_input").val();
	var suppliers="";
	if($("#search_suppliers").val()!="")
	{
		suppliers=" "+$("#search_suppliers").val();
	}	
	var gastos_data={
		year:$("#year").val(),
		month:$("#month").val(),
		day:$("#day").val(),
		hour:$("#hour").val(),
		minute:$("#minute").val(),
		concepto:$("#concepto").val()+suppliers,
		category:$("#category").val(),
		estado:$("#estado").val(),
		cantidad:$("#cantidad").val(),
		modo:$("#modo").val(),
		suscrito_box:$('#suscrito_box').val()
	}
	$.ajax(
	{
	type: "POST",
	url:base_url+"index.php/gastos/save/"+hidde_id,
	dataType: 'json',
	data:gastos_data,
	minLength: 2,
	delay: 1,
	cache: false
	}).done(function (data){
		jobj=data;
		if(jobj.success=="true"){
			$( "#empty" ).remove();
			$("#total_undone_mes_coin").replaceWith('<span id="total_undone_mes_coin">'+data.total_undone_mes_coin+'</span>');
			$("#total_undone_mes_coin2").replaceWith('<span id="total_undone_mes_coin2">'+data.total_undone_mes_coin2+'</span>');
			$("#total_done_mes_coin").replaceWith('<span id="total_done_mes_coin">'+data.total_done_mes_coin+'</span>');
			$("#total_done_mes_coin2").replaceWith('<span id="total_done_mes_coin2">'+data.total_done_mes_coin2+'</span>');
			$("#total_undone_coin").replaceWith('<span id="total_undone_coin">'+data.total_undone_coin+'</span>');
			$("#total_undone_coin2").replaceWith('<span id="total_undone_coin2">'+data.total_undone_coin2+'</span>');
			$("#total_done_coin").replaceWith('<span id="total_done_coin">'+data.total_done_coin+'</span>');
			$("#total_done_coin2").replaceWith('<span id="total_done_coin2">'+data.total_done_coin2+'</span>');
			if(jobj.gasto_id==-1 && gastos_data.suscrito_box==0){
				$.ajax(
				{
				type: "POST",
				url:base_url+"index.php/gastos/get_row/",
				data: {row_id:jobj.gasto_id},
				dataType: 'html',
				minLength: 3,
				delay: 100,
				cache: false
				}).done(function( data ){
					$( document.body ).scrollTop(200);
					$( "#cover_resultados_tabla" ).fadeIn(1000,function(){
						$("#cover_interaction_bar").append( "<div id='msj_manages'><span id='msg'>"+jobj.message+"</span></div>" );
						$("#cover_resultados_tabla").add().prepend(data);
						$("#row_"+jobj.gasto_id+"" ).css({"background-color":" rgb(194, 255, 195)"});
					});
					$( '#msj_manages' ).fadeOut( 4000, function(){
						$( "#row_"+jobj.gasto_id+"" ).css({"background-color":"rgb(240, 249, 240)","border": "none"});
						$( '#msj_manages'  ).remove();
					});	
				});
				form_ajax(module,base_url);
			}
			else if(jobj.estado==jobj.estado_filtro && gastos_data.suscrito_box==0)
			{
				$( document.body ).scrollTop(200);
				$.ajax(
				{
				type: "POST",
				url:base_url+"index.php/gastos/get_row/",
				data: {row_id:jobj.gasto_id},
				dataType: 'html',
				minLength: 3,
				delay: 100,
				cache: false
				}).done(function( data ){
					$( document.body ).scrollTop(200);
					$( "#cover_resultados_tabla" ).fadeIn(1000,function(){
						$("#row_"+jobj.gasto_id+"" ).remove();
						$( "#cover_interaction_bar" ).append( "<div id='msj_manages'><span id='msg'>"+jobj.message+"</span></div>" );
						$("#cover_resultados_tabla").add().prepend(data);
						$("#row_"+jobj.gasto_id+"" ).css({"background-color":" rgb(194, 255, 195)"});
					});
					$( '#msj_manages' ).fadeOut( 3000, function(){
						$( "#row_"+jobj.gasto_id+"" ).css({"background-color":"rgb(240, 249, 240)","border": "none"});
						$( '#msj_manages'  ).remove();
					});
					info_forms(module,base_url);
					form_ajax(module,base_url);
				});
			}
			else if(jobj.estado!= jobj.estado_filtro){
				var first_p=$( document.body ).scrollTop();
				$( document.body ).scrollTop(200);
				$( "#row_"+jobj.gasto_id+"" ).remove();
				$( "#cover_resultados_tabla" ).fadeIn(1000,function(){
					$( "#cover_interaction_bar" ).append( "<div id='msj_manages'><span id='msg'>"+jobj.message+"</span></div>" );
				});
				$( '#msj_manages' ).fadeOut( 3000, function(){
					$( document.body ).scrollTop(first_p);
					$( '#msj_manages'  ).remove();
				});
				form_ajax(module,base_url);
				info_forms(module,base_url);
			}
		}
		else if(jobj.success=="false"){
			$( document.body ).scrollTop(200);
				$( "#cover_interaction_bar" ).fadeIn(1000,function(){
				$( "#cover_interaction_bar" ).append( "<div id='msj_error_manages'><span id='msg'>"+jobj.message+"</span></div>" );
			});
			$( '#msj_error_manages' ).fadeOut( 4500, function(){
				$( '#msj_error_manages'  ).remove();
			});
		}
	});
	$('.ajax-popup-link').magnificPopup('close');
}
/* Guarda los datos del formulario del modulo de items_kits */
function submit_item_kits_form(module,base_url){
	var hidde_id =$("#id_input").val();
	var publish=0,destacar=0,promocionar=0,show_price=0,servicio=0;
	if($("#promocionar").prop("checked")==true)promocionar=1;
	if($("#destacar").prop("checked")==true)destacar=1;
	if($("#publish").prop("checked")==true)publish=1;
	if($("#show_price").prop("checked")==true)show_price=1;
	if($("#service").prop("checked")==true)servicio=1;
	var item_kit_item=[];
	$("input[name*='item_kit_item']").each(function (i, e){
		item_kit_item[e.id]= e.value;
	});
	var item_kits_data={
		kit_name:$("#kit_name").val(),
		description:$("#description").val(),
		kit_number:$("#kit_number").val(),
		kit_category:$("#category").val(),
		kit_price:$("#kit_price").val(),
		other_price:$("#other_price").val(),
		publish:publish,
		show_price:show_price,
		destacar:destacar,
		promocionar:promocionar,
		servicio:servicio,
		item_kit_item:item_kit_item,
		c_ClaveUnidad:$("#ClaveUnidad").val(),
		c_ClaveProdServ:$("#ClaveProdServ").val(),
		img_qty:$("#img_qty").val()
	}
	$.ajax(
	{
	type: "POST",
	url:base_url+"index.php/item_kits/save/"+hidde_id,
	dataType: 'json',
	data:item_kits_data,
	minLength: 2,
	delay: 1,
	cache: false
	}).done(function( data ){
		jobj=data;
		var row={row_id:jobj.item_kit_id}
		if(jobj.success=="true"){
			$('#empty').remove();
			if(jobj.item_kit_id==-1){
				$.ajax(
				{
				type: "POST",
				url:base_url+"index.php/item_kits/get_row/",
				data: {row_id:jobj.item_kit_id},
				dataType: 'json',
				minLength: 3,
				delay: 100,
				cache: false
				}).done(function( data ){
					$( document.body ).scrollTop(200);
					$( "#cover_resultados_tabla" ).	fadeIn(1000,function(){
						$( "#cover_interaction_bar" ).append( "<div id='msj_manages'><span id='msg'>"+jobj.message+"</span></div>" );
						$("#cover_resultados_tabla").add().prepend(data);
						$("#row_"+jobj.item_kit_id+"" ).css({"background-color":" rgb(194, 255, 195)"});
					});
					info_forms(module,base_url);
					form_ajax(module,base_url);
					content_forms(module,base_url);
					items_forms(module,base_url);
				});
			}	
			else{
				var first_p=$( document.body ).scrollTop();
				$( document.body ).scrollTop(200);
				$( "#row_"+jobj.item_kit_id+"" ).remove();
				$.ajax(
				{
				type: "POST",
				url:base_url+"index.php/item_kits/get_row/",
				data: {row_id:jobj.item_kit_id},
				dataType: 'html',
				minLength: 3,
				delay: 100,
				cache: false
				}).done(function( data ){
					$( document.body ).scrollTop(200);
					$( "#cover_resultados_tabla" ).fadeIn(1000,function(){
						$( "#cover_interaction_bar" ).append( "<div id='msj_manages'><span id='msg'>"+jobj.message+"</span></div>" );
						$("#cover_resultados_tabla").add().prepend(data);
						$("#row_"+jobj.item_kit_id+"" ).css({"background-color":" rgb(194, 255, 195)"});
					});
					$( '#msj_manages' ).fadeOut( 3000, function(){
						$( document.body ).scrollTop(first_p);
						$( "#row_"+jobj.item_kit_id+"" ).css({"background-color":"rgb(240, 249, 240)","border": "none"});
						$( '#msj_manages'  ).remove();
					});
					info_forms(module,base_url);
					form_ajax(module,base_url);
					content_forms(module,base_url);
					items_forms(module,base_url);
				});
			}
		}
		else if(jobj.success=="false"){
			$( document.body ).scrollTop(200);
			$( "#cover_interaction_bar" ).fadeIn(1000,function(){
				$( "#cover_interaction_bar" ).append( "<div id='msj_error_manages'><span id='msg'>"+jobj.message+"</span></div>" );
			});
			$( '#msj_error_manages' ).fadeOut( 4500, function(){
				$( '#msj_error_manages'  ).remove();
			});
		}
	});
	$('.ajax-popup-link').magnificPopup('close');
}
/* */
function autozip(){
	$('#zip').keyup(function(e){
	    var cp = $('#zip').val();
	    mexico(cp);
    });
}
/* */
function eliminar_Forma_de_pago(module,base_url){
	$(".delete").click(function(){
	var id= ($(this).attr("id")).replace("del_", "");
		$.ajax(
		{
			type: "POST",
			url:base_url+"index.php/"+module+"/delete_forma_pago/"+id,
			dataType: 'text',
			data: id,
			minLength: 2,
			delay: 1,
			cache: false
		}).done(function (data){
			$("#result_name_recursos_pagos_"+id).fadeOut( 1000, function()
				{
					$("#result_name_recursos_pagos_"+id).remove();
				});
		});
	});
}
/*Funciones que transforman una cantidad numerica a letras 
***********************************************************
*/
/* Regresa las unidades literales*/
function Unidades(num){
    switch(num)
    {
        case 1: return "Un";
        case 2: return "Dos";
        case 3: return "Tres";
        case 4: return "Cuatro";
        case 5: return "Cinco";
        case 6: return "Seis";
        case 7: return "Siete";
        case 8: return "Ocho";
        case 9: return "Nueve";
    }
    return "";
}/*Unidades()*/
/* Regresa las decenas literales si las unidades son = 0 */
function Decenas(num){
    decena = Math.floor(num/10);
    unidad = num -(decena * 10);
    switch(decena)
    {
        case 1:
            switch(unidad)
            {
                case 0: return "Diez";
                case 1: return "Once";
                case 2: return "Doce";
                case 3: return "Trece";
                case 4: return "Catorce";
                case 5: return "Quince";
                default: return "Dieci" + Unidades(unidad);
            }
        case 2:
            switch(unidad)
            {
                case 0: return "Veinte";
                default: return "Veinti" + Unidades(unidad);
            }
        case 3: return DecenasY("Treinta", unidad);
        case 4: return DecenasY("Cuarenta", unidad);
        case 5: return DecenasY("Cincuenta", unidad);
        case 6: return DecenasY("Sesenta", unidad);
        case 7: return DecenasY("Setenta", unidad);
        case 8: return DecenasY("Ochenta", unidad);
        case 9: return DecenasY("Noventa", unidad);
        case 0: return Unidades(unidad);
    }
}/*Unidades()*/
/* Regresa las decenas literales si las unidades son != 0  */
function DecenasY(strSin, numUnidades) {
    if (numUnidades > 0)
    return strSin + " Y " + Unidades(numUnidades)
    return strSin;
}/*DecenasY()*/
/* Regresa las centenas literales*/
function Centenas(num) {
    centenas = Math.floor(num / 100);
    decenas = num -(centenas * 100);
    switch(centenas)
    {
        case 1:
            if (decenas > 0)
                return "Ciento " + Decenas(decenas);
            return "Cien";
        case 2: return "Doscientos " + Decenas(decenas);
        case 3: return "Trescientos " + Decenas(decenas);
        case 4: return "Cuatrocientos " + Decenas(decenas);
        case 5: return "Quinientos " + Decenas(decenas);
        case 6: return "Seiscientos " + Decenas(decenas);
        case 7: return "Setecientos " + Decenas(decenas);
        case 8: return "Ochocientos " + Decenas(decenas);
        case 9: return "Novecientos " + Decenas(decenas);
    }
    return Decenas(decenas);
}/*Centenas()*/
/* Regresa la string numerica en singular o plurar seguna los casos*/
function Seccion(num, divisor, strSingular, strPlural) {
    cientos = Math.floor(num / divisor)
    resto = num -(cientos * divisor)
    letras = "";
    if (cientos > 0)
        if (cientos > 1)
            letras = Centenas(cientos) + " " + strPlural;
        else
            letras = strSingular;
    if (resto > 0)
        letras += "";
    return letras;
}/*Seccion()*/
/* Regresa las miles literales*/
function Miles(num) {
    divisor = 1000;
    cientos = Math.floor(num / divisor)
    resto = num -(cientos * divisor)
    strMiles = Seccion(num, divisor, "Un Mil", "Mil");
    strCentenas = Centenas(resto);
    if(strMiles == "")
        return strCentenas;
    return strMiles + " " + strCentenas;
}/*Miles()*/
/* Regresa las millones literales*/
function Millones(num) {
    divisor = 1000000;
    cientos = Math.floor(num / divisor)
    resto = num - (cientos * divisor)
    strMillones = Seccion(num, divisor, "Un Millon De", "Millones De");
    strMiles = Miles(resto);
    if(strMillones == "")
        return strMiles;
    return strMillones + " " + strMiles;
}/*Millones()*/
/* Esta funcion transforman una cantidad numerica a letras usando todas las funciones anteriores */
function NumeroALetras(num) {
    var data = {
        numero: num,
        enteros: Math.floor(num),
        centavos: (((Math.round(num * 100)) - (Math.floor(num) * 100))),
        letrasCentavos: "",
        letrasMonedaPlural: '',/*"PESOS", 'Dólares', 'Bolívares', 'etcs'*/
        letrasMonedaSingular: '', /*"PESO", 'Dólar', 'Bolivar', 'etc'*/
        letrasMonedaCentavoPlural: "Centavos",
        letrasMonedaCentavoSingular: "Centavo"
    };
    if (data.centavos > 0) {
        data.letrasCentavos = "CON " + (function (){
            if (data.centavos == 1)
                return Millones(data.centavos) + " " + data.letrasMonedaCentavoSingular;
            else
                return Millones(data.centavos) + " " + data.letrasMonedaCentavoPlural;
            })();
    };
    if(data.enteros == 0)
        return "CERO " + data.letrasMonedaPlural + " " + data.letrasCentavos;
    if (data.enteros == 1)
        return Millones(data.enteros) + " " + data.letrasMonedaSingular + " " + data.letrasCentavos;
    else
        return Millones(data.enteros) + " " + data.letrasMonedaPlural + " " + data.letrasCentavos;
}/*NumeroALetras()*/
/*FIN de las funciones que transforman una cantidad numerica a letras 
***********************************************************
*/
/* */
function recargar_eventos_inicio(module,base_url)
{
	if(module==="items")
	{
		content_forms(module,base_url);
		items_forms(module,base_url);
		info_forms(module,base_url);
		load_image(module,base_url);
		suggest_category(module,base_url);
		suggest_brand(module,base_url);
		quantity_input();
	}
	else if(module==='recursos'){
		show_hidde_operations()
	}
}
/*codigo de deducible*/
function deducible_ajax(base_url,module){
	$("#deducible").on('click', function(){
	var hidde_id =$("#id_input").val();
	var gastos_data={
		deducible:+$("#deducible").prop('checked')
	}
$.ajax(
	{
	type: "POST",
	url:base_url+"index.php/gastos/deducibles/"+hidde_id,
	dataType: 'json',
	data:gastos_data,
	minLength: 2,
	delay: 1,
	cache: false
	}).done(function (data){ alert("se cambio deducibles"); /*}).fail(function(data) {*/
  })});
}