/* Valida los inputs del form del modulo de empleados */
function validar_new_employees(jsonerrors){
	var emailreg = /^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/;
	var telreg = /^([0-9\-])*$/;
	if($("#first_name").val() == "")
	{
		$("#first_name").focus();
		$('#first_name_label').replaceWith('<label name="first_name" id="first_name_label" class="required_error">'+jsonerrors.error_first_name+'</label>');
		return false;
	}
	else if($("#last_name").val() == "")
	{
		$("#last_name").focus();
		$('#last_name_label').replaceWith('<label name="last_name" id="last_name_label" class="required_error">'+jsonerrors.error_last_name+'</label>');
		return false;
	}
	else if($("#email").val() == "" || !emailreg.test($("#email").val()))
	{
		$("#email").focus();
		$('#email_label').replaceWith('<label name="email" id="email_label" class="required_error">'+jsonerrors.error_email+'</label>');
		return false;
	}
	else if(validar_mail($("#email").val()) && $("#id_input").val()=="")
	{
		$("#email").focus();
		$('#email_label').replaceWith('<label name="email" id="email_label" class="required_error">'+jsonerrors.error_email_exists+'</label>');
		return false;
	}
	else if($("#username").val() == "" || $("#username").val().length < 3)
	{
		$("#username").focus();
		$('#username_label').replaceWith('<label name="username" id="username_label" class="required_error">'+jsonerrors.error_user+'</label>');
		return false;
	}/*;*/
	else if($("#password").val() == "" || $("#password").val().length < 5)
	{
		$("#password").focus();
		$('#password_label').replaceWith('<label name="password" id="password_label" class="required_error">'+jsonerrors.error_pass+'</label>');
		return false;
	}
	else if($("#repeat_password").val() == "" || $("#password").val().localeCompare($("#repeat_password").val())!=0)
	{
		$("#repeat_password").focus();
		$('#repeat_password_label').replaceWith('<label name="repeat_password" id="repeat_password_label" class="required_error">'+jsonerrors.error_pass_rpt+'</label>');
		return false;
	}
	else
	{
		return true;
	}
}
/* Valida los inputs del form del modulo de proveedores */
function validar_new_suppliers(jsonerrors){
	var emailreg = /^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/;

	var telreg = /^([0-9\-])*$/;
	if($("#first_name").val() == "")
	{
		$("#first_name").focus();
		$('#first_name_label').replaceWith('<label name="first_name" id="first_name_label" class="required_error">'+jsonerrors.error_first_name+'</label>');
		return false;
	}
	else if($("#last_name").val() == "")
	{
		$("#last_name").focus();
		$('#last_name_label').replaceWith('<label name="last_name" id="last_name_label" class="required_error">'+jsonerrors.error_last_name+'</label>');
		return false;
		
		
	}
	else if($("#email").val() == "" || !emailreg.test($("#email").val()))
	{
		$("#email").focus();
		$('#email_label').replaceWith('<label name="email" id="email_label" class="required_error">'+jsonerrors.error_email+'</label>');
		return false;
	}
	else if(validar_mail($("#email").val()) && $("#id_input").val()=="")
	{
		$("#email").focus();
		$('#email_label').replaceWith('<label name="email" id="email_label" class="required_error">'+jsonerrors.error_email_exists+'</label>');
		return false;
	}
	else
	{
		return true;
	}
}
/* Valida los inputs de los forms fiscales */
function validar_form_fiscal(fiscalerrors){
	var rfcreg = /^(([A-Z]|[a-z]|\s){1})(([A-Z]|[a-z]){3})([0-9]{6})((([A-Z]|[a-z]|[0-9]){3}))/;
	var rfc =$("#rfc").val();
	if(rfc.length==12)rfcreg=/^(([A-Z]|[a-z]|\s){1})(([A-Z]|[a-z]){2})([0-9]{6})((([A-Z]|[a-z]|[0-9]){3}))/
	rfc=rfc.toUpperCase();
	$("#rfc").val(rfc);
	var cp =$("#cp").val();
	
	/* || rfc.length < 11 || rfc.length > 13 */
	if(rfc == ""  || !rfcreg.test(rfc))
	{
		$("#rfc").focus();
		$('#rfc_label').replaceWith('<label name="rfc" id="rfc_label" class="required_error">'+fiscalerrors.error_rfc+'</label>');
		return false;
	}
	else if($("#razon_social").val() == "")
	{
		$("#razon_social").focus();
		$('#razon_social_label').replaceWith('<label name="razon_social" id="razon_social_label" class="required_error">'+$("#field_razon_social").val()+' '+fiscalerrors.error_field+'</label>');
		
		return false;
	}
	else if($("#ce").val() == "")
	{
		$("#ce").focus();
		$('#ce_label').replaceWith('<label name="ce_label" id="ce_label" class="required_error">'+$("#field_ce").val()+' '+fiscalerrors.error_field+'</label>');
		
		return false;
	}
	else if($("#no_exterior").val() == "")
	{
		$("#no_exterior").focus();
		$('#no_exterior_label').replaceWith('<label name="no_exterior_label" id="no_exterior_label" class="required_error">'+$("#field_no_exterior").val()+' '+fiscalerrors.error_field+'</label>');
		
		return false;
	}
	else if($("#colonia").val() == "")
	{
		$("#colonia").focus();
		$('#colonia_label').replaceWith('<label name="colonia_label" id="colonia_label" class="required_error">'+$("#field_colonia").val()+' '+fiscalerrors.error_field+'</label>');
		
		return false;
	}
	else if($("#municipio").val() == "")
	{
		$("#municipio").focus();
		$('#municipio_label').replaceWith('<label name="municipio_label" id="municipio_label" class="required_error">'+$("#field_municipio").val()+' '+fiscalerrors.error_field+'</label>');
		
		return false;
	}
	else if($("#estado").val() == "")
	{
		$("#estado").focus();
		$('#estado_label').replaceWith('<label name="estado_label" id="estado_label" class="required_error">'+$("#field_estado").val()+' '+fiscalerrors.error_field+'</label>');
		
		return false;
	}
	else if($("#pais").val() == "")
	{
		$("#pais").focus();
		$('#pais_label').replaceWith('<label name="pais_label" id="pais_label" class="required_error">'+$("#field_pais").val()+' '+fiscalerrors.error_field+'</label>');
		
		return false;
	}
	else if( cp== "" || isNaN(cp) || cp.length !=5)
	{
		$("#cp").focus();
		$('#cp_label').replaceWith('<label name="cp_label" id="cp_label" class="required_error">'+fiscalerrors.error_zip+'</label>');
		
		return false;
	}
	else
	{
		return true;
	}
	
	
}
/* Valida los inputs del form del modulo de items */
function validar_new_items(jsonerrors){
	
	var pricereg = /^[0-9]/;
	
	if($("#name").val() == "")
	{
		$("#name").focus();
		$('#items_name_label').replaceWith('<label name="name" id="items_name_label" class="required_error">'+jsonerrors.error_items_name+'</label>');
		return false;
	}
	else if($("#brand").val() == "")
	{
		$("#brand").focus();
		$('#brand_label').replaceWith('<label name="brand" id="brand_label" class="required_error">'+jsonerrors.error_brand+'</label>');
		return false;
	}
	else if($("#modelo").val() == "")
	{
		$("#modelo").focus();
		$('#modelo_label').replaceWith('<label name="modelo" id="modelo_label" class="required_error">'+jsonerrors.error_modelo+'</label>');
		return false;
	}
	else if($("#category").val() == "")
	{
		$("#category").focus();
		$('#category_label').replaceWith('<label name="category" id="category_label" class="required_error">'+jsonerrors.error_category+'</label>');
		return false;
	}
	else if($("#cost_price").val() == "" || !pricereg.test($("#cost_price").val()))
	{
		$("#cost_price").focus();
		$('#cost_price_label').replaceWith('<label name="cost_price" id="cost_price_label" class="required_error">'+jsonerrors.error_cost_price+'</label>');
		return false;
	}
	else if($("#unit_price").val() == "" || !pricereg.test($("#unit_price").val()))
	{
		$("#unit_price").focus();
		$('#unit_price_label').replaceWith('<label name="unit_price" id="unit_price_label" class="required_error">'+jsonerrors.error_price+'</label>');
		return false;
	}
	else if($("#reorder_level").val() == "" || !pricereg.test($("#reorder_level").val()))
	{
		$("#reorder_level").focus();
		$('#reorder_level_label').replaceWith('<label name="reorder_level" id="reorder_level_label" class="required_error">'+jsonerrors.error_reorder_level+'</label>');
		return false;
	}
	else if($("#other_price").val()!="")
	{
		if(!pricereg.test($("#other_price").val()))
		{
			$("#other_price").focus();
			$('#other_price_label').replaceWith('<label name="other_price" id="other_price_label" class="required_error">'+jsonerrors.error_other_price+'</label>');
			return false;
		}
		else
		{
			return true;
		}
	}
	else if($("#item_number").val()!="")
	{
	
		if(!pricereg.test($("#item_number").val()))
		{
			$("#item_number").focus();
			$('#item_number_label').replaceWith('<label name="item_number" id="item_number_label" class="required_error">'+jsonerrors.error_numeric+'</label>');
			return false;
		}
		else
		{
			return true;
		}
		
	}
	else
	{
		return true;
	}
}
/* Valida los inputs del form de edición multiple en el modulo de items */
function validar_form_bulk(json_msjs){
	var pricereg = /^([0-9\-])*$/;
	if($("#brand").val() == "" && $("#category").val() == "" && $("#cost_price").val() == "" && $("#unit_price").val() == "" && $("#other_price").val() == "" && $("#reorder_level").val() == "" && $("#tax_name_1").val() == "" && $("#tax_percents_1").val() == "" && $("#tax_name_2").val() == "" && $("#tax_percents_2").val() == "" && $("#location").val() == "" && $("#publish").val() == "No modificar" && $("#promocionar").val() == "No modificar" && $("#allow_alt_description").val() == "No modificar" && $("#is_serialized").val() == "No modificar")
	{
		
		$('.box_field_row').append('<label class="required_error">'+json_msjs.error_items_form_bulk+'</label>');
		return false;
	}
	else if($("#cost_price").val()!="" && !pricereg.test($("#cost_price").val()))
	{
		
		$("#cost_price").focus();
		$('#cost_price_label').replaceWith('<label name="cost_price" id="cost_price_label" class="required_error">'+json_msjs.error_cost_price+'</label>');
		return false;
		
	}
	else if($("#unit_price").val()!="" && !pricereg.test($("#unit_price").val()))
	{
		
		$("#unit_price").focus();
		$('#unit_price_label').replaceWith('<label name="unit_price" id="unit_price_label" class="required_error">'+json_msjs.error_price+'</label>');
		return false;
	}
	else if($("#other_price").val()!="" && !pricereg.test($("#other_price").val()))
	{
		
		$("#other_price").focus();
		$('#other_price_label').replaceWith('<label name="other_price" id="other_price_label" class="required_error">'+json_msjs.error_other_price+'</label>');
		return false;
		
	}
	else if($("#reorder_level").val()!="" && !pricereg.test($("#reorder_level").val()))
	{
		
		$("#reorder_level").focus();
		$('#reorder_level_label').replaceWith('<label name="reorder_level" id="reorder_level_label" class="required_error">'+json_msjs.error_numeric+'</label>');
		return false;
		
	}
	else if($("#tax_percents_1").val()!="" && !pricereg.test($("#tax_percents_1").val()))
	{
		
		$("#tax_percents_1").focus();
		$('#tax_percent_name_1_label').replaceWith('<label name="tax_percents_1" id="tax_percent_name_1_label" class="required_error">'+json_msjs.error_numeric+'</label>');
		return false;
	}
	else if($("#tax_percents_2").val()!="" && !pricereg.test($("#tax_percents_2").val()))
	{
		$("#tax_percents_2").focus();
		$('#tax_percent_name_2_label').replaceWith('<label name="tax_percents_2" id="tax_percent_name_2_label" class="required_error">'+json_msjs.error_numeric+'</label>');
		return false;
		
	}
	else
	{
		return true;
	}

}
/* Valida los inputs del form de inventario en el modulo de items */
function validar_inventory_form(json_msjs){
	var pricereg = /^([0-9\-])*$/;
	if($("#newquantity").val() == ""|| !pricereg.test($("#newquantity").val())||$("#newquantity").val() == "0")
	{
		$("#newquantity").focus();
		$('#newquantity_label').replaceWith('<label name="newquantity" id="newquantity_label" class="required_error">'+json_msjs.error_inventory_stock+'</label>');
		return false;
	}
	else if($("#trans_comment").val() == "")
	{
		$("#trans_comment").focus();
		$('#justify_label').replaceWith('<label name="trans_comment" id="justify_label" class="required_error">'+json_msjs.error_inventory_razon+'</label>');
		return false;
	}
	else
	{

		return true;

	}

}
/* Valida los inputs del form del modulo de recursos */
function validar_new_recursos(jsonerrors){
	var pricereg = /^([0-9])*$/;
	if($("#nombre_cuenta").val() == "" )
	{
		$("#nombre_cuenta").focus();
		$("#nombre_cuenta_label").replaceWith('<label name="nombre_cuenta_label" id="nombre_cuenta_label" class="required_error">'+jsonerrors.error_nombre_cuenta+'</label>');
		return false;
	}
	else if($("#cantidad").val() == "" || !pricereg.test($("#cantidad").val()))
	{
		$("#cantidad").focus();
		$("#cantidad_label").replaceWith('<label name="cantidad" id="cantidad_label" class="required_error">'+jsonerrors.error_cantidad+'</label>');
		return false;
	}
	else if($("#coin").val() == "-1" )
	{
		$("#coin").focus();
		$("#coin_label").replaceWith('<label name="coin_label" id="coin_label" class="required_error">'+jsonerrors.error_coin+'</label>');
		return false;
	}
	else
	{

		return true;

	}

}
/* Valida los inputs del form de operaciones en el modulo de recursos */
function validar_operation_form(jsonerrors){
	var montoreg = /^([0-9\-\.])*$/;
	/*
	if($("#monto").val() == ""|| !montoreg.test($("#monto").val())||$("#monto").val() == "0")
	{
		$("#monto").focus();
		$('#operation_monto').replaceWith('<label name="operation_monto" id="operation_monto" class="required_error">'+jsonerrors.error_numeric+'</label>');
		return false;
	}
	else if($("#concepto").val() == "")
	{
		$("#concepto").focus();
		$('#concepto_label').replaceWith('<label name="concepto_label" id="concepto_label" class="required_error">'+jsonerrors.error_concepto+'</label>');
		return false;
	}
	else
	{

		return true;

	}*/

	/* este codigo era por que no sabia que habia un input hidden con el valor de recursos
     var cantidad = $("#recurso_label").text().length;
     var cadena = $("#recurso_label").text();
     var cantidad_y_tipo_moneda = cadena.split(" ");
     cantidad_y_tipo_moneda[0]= cantidad_y_tipo_moneda[0].substring(1);*/
     var xchange =($("#monto").val()*$("#xchange_rate").val());
     /*cantidad_y_tipo_moneda guarda el tipo de moned y la cantidad para su validacion*/
     
     var monto = parseFloat($("#monto").val());
     
	/* Esta validacion es cuando no es una tranferencia*/
	if($("#operation_manual").prop('checked')==true)
	{
		if($("#concepto").val() == "")
		{
			$("#concepto").focus();
			$('#concepto_label').replaceWith('<label name="concepto_label" id="concepto_label" class="required_error">'+jsonerrors.error_concepto+'</label>');
			return false;
		}
		else if($("#monto").val() == ""|| !montoreg.test($("#monto").val())||$("#monto").val() == "0" )
		{
			$("#monto").focus();
			$('#operation_monto_label').replaceWith('<label name="operation_monto_label" id="operation_monto_label" class="required_error">'+jsonerrors.error_numeric+'</label>');
			return false;
		}
		else{
			return true;
		}
	}
	else{
		if($('#recursos').val()=="-1")
		{
			$("#recursos").focus();
			$('#nombre_cuenta_label').replaceWith('<label name="nombre_cuenta_label" id="nombre_cuenta_label" class="required_error">'+jsonerrors.error_select_recurso+'</label>');
		}
		else if(($('#xchange_rate').val()!="" && !montoreg.test($("#xchange_rate").val())) || ($('#xchange_rate').val()!="" && $("#xchange_rate").val()=="0") || ($('#xchange_rate').val()!="" && $("#xchange_rate").val()<0))
		{
			$("#xchange_rate").focus();
			$('#xchange_rate_label').replaceWith('<label name="xchange_rate_label" id="xchange_rate_label" class="required_error">'+jsonerrors.error_numeric+'</label>');
		}
		else if(parseFloat($('#recurso').val())<parseFloat($("#monto").val()))
		{

			$("#monto").focus();
			$('#operation_monto_label').replaceWith('<label name="operation_monto_label" id="operation_monto_label" class="required_error">'+jsonerrors.error_less_resource+'</label>');
			return false;
	
		}
		else if($("#monto").val()== "" || !montoreg.test($("#monto").val())||$("#monto").val() == "0" )
		{
			$("#monto").focus();
			$('#operation_monto_label').replaceWith('<label name="operation_monto_label" id="operation_monto_label" class="required_error">'+jsonerrors.error_numeric+'</label>');
			return false;
		}
		else if(parseFloat($("#monto").val())<0)
		{

			$("#monto").focus();
			$('#operation_monto_label').replaceWith('<label name="operation_monto_label" id="operation_monto_label" class="required_error">'+jsonerrors.error_less_resource+'</label>');
			return false;
	
		}
		else
		{
			return true;
		}

	}
	/*else if($("#operation_manual").prop('checked'))
	{

		if($("#monto").val() == ""|| !montoreg.test($("#monto").val())||$("#monto").val() == "0")
		{
			$("#monto").focus();
			$('#operation_monto').replaceWith('<label name="operation_monto" id="operation_monto" class="required_error">'+jsonerrors.error_numeric+'</label>');
			return false;
		}
		else if($("#concepto").val() == "")
		{
			$("#concepto").focus();
			$('#concepto_label').replaceWith('<label name="concepto_label" id="concepto_label" class="required_error">'+jsonerrors.error_concepto+'</label>');
			return false;
		}
		else
		{
			return true;
		}

	}*/

}
/* Valida los inputs del form del modulo de gastos */
function validar_new_gastos(jsonerrors){
	var pricereg = /^[0-9]/;
	var d = new Date();
	if($("#cantidad").val() == ""|| !pricereg.test($("#cantidad").val()))
	{
		$("#cantidad").focus();
		$('#cantidad_label').replaceWith('<label name="cantidad" id="cantidad_label" class="required_error">'+jsonerrors.error_cantidad+'</label>');
		return false;
	}
	else if($("#concepto").val() == "")
	{
		$("#concepto").focus();
		$('#concepto_label').replaceWith('<label name="concepto" id="concepto_label" class="required_error">'+jsonerrors.error_concepto+'</label>');
		return false;
	}
	else if($("#category").val() == "")
	{
		$("#category").focus();
		$('#category_label').replaceWith('<label name="category" id="category_label" class="required_error">'+jsonerrors.error_category+'</label>');
		return false;
	}
	else if($("#estado").val() == "Pendiente" && $("#suscrito_box").val() == 0 && $("#month").val()<=d.getMonth() && $("#day").val()<=d.getDate()&& $("#year").val()<=d.getFullYear())
	{
		$("#estado").focus();
		$('#estado_label').replaceWith('<label name="estado" id="estado_label" class="required_error">'+jsonerrors.error_date+'</label>');
		return false;
	}
	else
	{

		return true;

	}
	
	
}
/* Valida los inputs del form del modulo de items_kits */
function validar_new_item_kits(jsonerrors){
	
	var pricereg = /^[0-9]/;
	
	if($("#kit_name").val() == "")
	{
		$("#kit_name").focus();
		$('#kit_name_label').replaceWith('<label name="kit_name" id="kit_name_label" class="required_error">'+jsonerrors.error_kit_name+'</label>');
		return false;
	}
	else if($("#kit_category").val() == "")
	{
		$("#kit_category").focus();
		$('#category_label').replaceWith('<label name="kit_category" id="category_label" class="required_error">'+jsonerrors.error_category+'</label>');
		return false;
	}
	else if($("#kit_price").val() == "" || !pricereg.test($("#kit_price").val()))
	{
		$("#kit_price").focus();
		$('#kit_price_label').replaceWith('<label name="kit_price" id="kit_price_label" class="required_error">'+jsonerrors.error_kit_price+'</label>');
		return false;
	}
	else
	{

		return true;

	}
	
}
/* Valida los inputs del form de edición multiple en el modulo de items_kits */
function validar_form_kit_bulk(json_msjs){
	var pricereg = /^[0-9\-]/;
	if($("#category").val() == "" && $("#kit_price").val() == "" && $("#description").val() == "" && $("#publish").val() == "No modificar" && $("#service").val() == "No modificar")
	{
		
		$('.box_field_row').append('<label class="required_error">'+json_msjs.error_items_form_bulk+'</label>');
		return false;
	}
	else if($("#kit_price").val()!="" && !pricereg.test($("#kit_price").val()))
	{
		
			$("#kit_price").focus();
			$('#kit_price_label').replaceWith('<label name="kit_price" id="kit_price_label" class="required_error">'+json_msjs.error_cost_price+'</label>');
			return false;
		
	}
	else
	{

		return true;

	}

}
/*Valida si el correo existe en la bd*/
function validar_mail(email){
	var response=false;
	$.ajax(
	{
	type: "POST",
	url:location.href+"/validar_mail/",
	data: {email:email},
	dataType: 'json',
	minLength: 2,
	async: false,
	delay: 1,
	cache: false,
	}).done(function(data){
		response=data.response;
	});
	return response;
	
	
}
