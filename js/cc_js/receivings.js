/*Ajax del panel de entradas*/
function register_receivings(module,base_url){
	
	$('.ajax-popup-receivings').magnificPopup(
	{
		type: 'ajax',
		alignTop: true,
		
		ajax:{
				
				settings: null, /* Ajax settings object */
				cursor: 'mfp-ajax-cur', 
				/* CSS class that will be added to body during the loading (adds "progress" cursor)*/
				tError: '<a href="%url%">The content</a> could not be loaded.' 
				 /*  Error message, can contain %curr% and %total% tags if gallery is enabled*/
			},
			callbacks: 
			{
				/*var header=load('<?php $this->load->view("cc_views/partial/header_forms"); ?>');*/
				parseAjax: function(mfpResponse) 
				{
				/* mfpResponse.data is a "data" object from ajax "success" callback*/
				/* for simple HTML file, it will be just String*/
				/* You may modify it to change contents of the popup*/
				/* For example, to show just #some-element:*/
				
				mfpResponse.data =$(mfpResponse.data).find(":first");
							
				/* mfpResponse.data must be a String or a DOM (jQuery) element*/
				
				},
				ajaxContentAdded: function() 
				{
					
					/*event.stopPropagation();*/
					/*Verifica si ya hubo algun registro en caso de haberlo adjunta posibles formas de pagos y sus funciones*/
					var total=$("#hidde_total").val();
					total=parseFloat(total);
						/*Adjunta la accion de Suspender ventas si el modo cambia a pendiente*/
					$("#modos").change( function (evt) 
					{
						modo=$("#modos").val();
						
						if(modo=="Pendiente")
						{
							r=confirm("Guardar pedido como pendiente?");
							if(r)
							{
							complete_receiving(true);
							}
							else
							{
								$("#modos").val('Receive');
							}
						}

					});
					
					if(total > 0)
					{
						get_receivings_payments_ajax();
						$(window).jkey('f9',function()
						{

							$("#pay_recived").focus();
							$("#pay_recived").select();
						});
					}
					
					if($("#comment").val()!="")
					{
						$('#comment_containner').css( "display", "block" );
					}
					var hidde_id =$("#id_input").val();
					/*Habilita el input para recibir pago */
				
					/*Si hay un proveedor seleccionado Carga el estilo de #search_supplier al cargar el panel de entradas */
					if($("#search_item" ).attr('disabled')=='disabled')
					{
						$("input[name*='search_suppliers']").focus();
						$("input[name*='search_item']" ).css('background-color','#dddddd');
						
						
					}
					else
					{
						$("#search_item" ).focus();
					}
					/* Muestra u oculta comentarios */
					$('#comment_display').click(function(){

						if ($('#comment_containner').css( "display")==="none") {
						$('#comment_containner').css( "display", "block" );


						}
						else
						{
						$('#comment_containner').css( "display", "none" );
						
						}
					});
					/*Cancela la entrada en curso*/
					$('#cancel_button').click(function()
					{
						cancel_receiving();
						
					});
					/*Busqueda de productos en el panel de entradas*/
					$( "input[name*='search_item']" ).autocomplete(
					{
						appendTo: "#ui-widget-items",
						autoFocus: true,
						source: function (request, response) 
						{
							$.ajax({
							type: "POST",
							url:base_url+"index.php/items/items_only_search/",
							data: {q:$("input[name*='search_item']").val()},
							success: response,
							dataType: 'json',
							minLength: 2,
							delay: 1,
							cache: false
							}).done(function(data)
							{

							});
						},
						change: function (event, ui)
						{
						
							var item =event.target.value;
								item = item.split("|", 1);
								var item_id = parseInt(item[0]);
								var verificador = /^[0-9]+\.[0-9]+\./;
								if(item_id != "")
								{	
									$("input[name*='search_item']").val("");
									$("input[name*='search_item']").focus();
									$.ajax(
									{
										type: "POST",
										url:location.href+"/add/",
										data: {item:item_id},
										dataType: 'json',
										minLength: 2,
										delay: 1,
										cache: false
										}).done(function(data)
										{
											event.stopPropagation();
											get_receivings_payments_ajax();
											
											
											$( "#cover_table_register_psr" ).fadeIn(500,function()
											{ 
												
												
												if(total==0)
												{	
												
												$(".cover_result_register_psr").replaceWith("<div class='cover_result_register_psr'>"+data.table_receiving+"</div>");
												}
												else
												{
													$(".cover_result_register_psr").replaceWith("<div class='cover_result_register_psr'>"+data.table_receiving+"</div>");
													
												}
											});
											$("input[name*='search_item']").val("");
											$("input[name*='search_item']").focus();
									
										});
									
								}
						},
						select: function( event, ui )
						{
							
						
							try
							{	
								
								var item =event.toElement.innerText;
								item = item.split("|", 1);
								
								var item_id = parseInt(item[0]);
								var verificador = /^[0-9]+\.[0-9]+\./;
								
								$("input[name*='search_item']").val("");
								$("input[name*='search_item']").focus();
								$.ajax(
								{
									type: "POST",
									url:location.href+"/add/",
									data: {item:item_id},
									dataType: 'json',
									minLength: 2,
									delay: 1,
									cache: false
								}).done(function(data)
								{
									event.stopPropagation();
									get_receivings_payments_ajax();
									$("#pay_recived").val(total.toFixed(2));
										$( "#cover_table_register_psr" ).fadeIn(500,function()
										{ 
											
											var total = parseFloat(data.total);
										$("#total").replaceWith("<div id='total'><span  class='span_text_payment'>Total:</span><span class='by_pay_total'> $ "+total.toFixed(2)+"</span></div><input type='hidden' id='hidde_total' value='"+total.toFixed(2)+"' name='hidde_total'/> ");
											if(total==0)
											{	
											
											$(".cover_result_register_psr").replaceWith("<div class='cover_result_register_psr'>"+data.table_receiving+"</div>");
											}
											else
											{
												$(".cover_result_register_psr").replaceWith("<div class='cover_result_register_psr'>"+data.table_receiving+"</div>");
												
											}
										});
									$("input[name*='search_item']").val("");
									$("input[name*='search_item']").focus();
									
								});
							}
							catch(e)
							{
								
					
							}
							
						}

					});
					/**/
					$("#search_item").jkey('enter',function()
					{

					$("input[name*='search_item']").autocomplete( "close" );
					var item =$("input[name*='search_item']").val();
							/*alert(item);*/
								item = item.split("|", 1);
								var item_id = parseInt(item[0]);
								var verificador = /^[0-9]+\.[0-9]+\./;
								if(item_id != "")
								{	
									$("input[name*='search_item']").val("");
									$("input[name*='search_item']").focus();
									$.ajax(
									{
										type: "POST",
										url:location.href+"/add/",
										data: {item:item_id},
										dataType: 'json',
										minLength: 2,
										delay: 1,
										cache: false
										}).done(function(data)
										{
											$("input[name*='search_item']").val("");
											$("input[name*='search_item']").focus();
											/*event.stopPropagation();*/
											get_receivings_payments_ajax();
											
											$("#pay_recived").val(total.toFixed(2));
											$( "#cover_table_register_psr" ).fadeIn(500,function()
											{ 
												
												$("#pay_recived").val(total.toFixed(2));
												$("#total").replaceWith("<div id='total'><span  class='span_text_payment'>Total:</span><span class='by_pay_total'> $ "+total.toFixed(2)+"</span></div><input type='hidden' id='hidde_total' value='"+total.toFixed(2)+"' name='hidde_total'/> ");
												if(total==0)
												{	
												
												$(".cover_result_register_psr").replaceWith("<div class='cover_result_register_psr'>"+data.table_receiving+"</div>");
												}
												else
												{
													$(".cover_result_register_psr").replaceWith("<div class='cover_result_register_psr'>"+data.table_receiving+"</div>");
													
												}
											});
											
									
										});
									
								}
								});
					/*Evento enter en busqueda de proveedores*/
					$("#search_suppliers").jkey('enter',function()
					{
						/*$("#search_suppliers").autocomplete( "close" );*/
						var supplier =$("#search_suppliers").val();
							supplier = supplier.split("|", 1);
							supplier = parseInt(supplier);
							if(!isNaN(supplier))
							{
							$.ajax(
							{
							type: "POST",
							url:location.href+"/select_supplier/",
							data: {supplier:supplier},
							dataType: 'json',
							minLength: 3,
							delay: 100,
							cache: false
							}).done(function(data)
							{
								$(".box_first_register").append('<span id="supplier_print">'+data.supplier+'</span>');
								$('#ui-widget-suppliers').click(function ()
								{
								$.ajax(
								{
								type: "POST",
								url:location.href+"/remove_supplier/",
								dataType: 'html',
								minLength: 3,
								delay: 100,
								cache: false
								});
								/*alert("dsgdfg");*/
								var css={
								'background-color':'#FFF'
								}
								$("#search_suppliers" ).css(css);
								$("#search_suppliers" ).removeAttr('disabled');
								$("#search_suppliers").val("");
								$("#search_suppliers").focus();
								$(".disabled" ).css('background-color','#dddddd');
								$(".disabled" ).attr('disabled','disabled');
								});
								/*alert('jghhgg');*/
							});
							
							$("#search_suppliers" ).attr('disabled','disabled');
							var css={
							'background-color':'#dddddd',
							"background-image":"url(../images/cc_images/icon_cct/icono-garbage-small.png)"
							}

							$("#search_suppliers" ).css(css);
							$(".disabled" ).css('background-color','#FFF');
							$(".disabled" ).removeAttr('disabled');
							$("#search_item" ).focus();
							}
						
					});
					/*Busqueda de provedores en el panel de entradas*/
					$( "#search_suppliers" ).autocomplete({
					appendTo: "#ui-widget-suppliers",
					autoFocus: true,
					source: function (request, response) 
					{
						$.ajax({
						type: "POST",
						url:base_url+"index.php/suppliers/suggest/s",
						data: {q:$('#search_suppliers').val()},
						success: response,
						dataType: 'json',
						minLength: 2,
						delay: 1,
						cache: false
						});
					},
					change: function( event, ui ) 
					{
							
							
							var supplier =event.target.value;
							supplier = supplier.split("|", 1);
							supplier = parseInt(supplier);
							
							if(!isNaN(supplier))
							{
						
								
								$.ajax(
								{
								type: "POST",
								url:location.href+"/select_supplier/",
								data: {supplier:supplier},
								dataType: 'json',
								minLength: 3,
								delay: 100,
								cache: false
								}).done(function(data)
								{
									$(".box_first_register").append('<span id="supplier_print">'+data.supplier+'</span>');
									$('#ui-widget-suppliers').click(function ()
									{
										$.ajax(
										{
										type: "POST",
										url:location.href+"/remove_supplier/",
										dataType: 'html',
										minLength: 3,
										delay: 100,
										cache: false
										});
										/*alert("dsgdfg");*/
										var css={
										'background-color':'#FFF'
										}
										$("#search_suppliers" ).css(css);
										$("#search_suppliers" ).removeAttr('disabled');
										$("#search_suppliers").val("");
										$("#search_suppliers").focus();
										$(".disabled" ).css('background-color','#dddddd');
										$(".disabled" ).attr('disabled','disabled');
									});
									
									/*alert('jghhgg');*/
								});
								$("#search_suppliers" ).attr('disabled','disabled');
								var css={
								'background-color':'#dddddd',
								"background-image":"url(../images/cc_images/icon_cct/icono-garbage-small.png)"
								}
								
							$("#search_suppliers" ).css(css);
								$(".disabled" ).css('background-color','#FFF');
								$(".disabled" ).removeAttr('disabled');
								$("#search_item" ).focus();
								
								
								
							}
											
					},
					select: function( event, ui ) {
						try
						{	
							var supplier =event.toElement.innerText;
							supplier = supplier.split("|", 1);
							supplier = parseInt(supplier);
							$.ajax(
							{
							type: "POST",
							url:location.href+"/select_supplier/",
							data: {supplier:supplier},
							dataType: 'json',
							minLength: 3,
							delay: 100,
							cache: false
							}).done(function(data)
							{
								$(".box_first_register").append('<span id="supplier_print">'+data.supplier+'</span>');
								$('#ui-widget-suppliers').click(function ()
								{
								$.ajax(
								{
								type: "POST",
								url:location.href+"/remove_supplier/",
								dataType: 'html',
								minLength: 3,
								delay: 100,
								cache: false
								});
								/*alert("dsgdfg");*/
								var css={
								'background-color':'#FFF'
								}
								$("#search_suppliers" ).css(css);
								$("#search_suppliers" ).removeAttr('disabled');
								$("#search_suppliers").val("");
								$("#search_suppliers").focus();
								$(".disabled" ).css('background-color','#dddddd');
								$(".disabled" ).attr('disabled','disabled');
								});
								/*alert('jghhgg');*/
							});
							
							$("#search_suppliers" ).attr('disabled','disabled');
							var css={
							'background-color':'#dddddd',
							"background-image":"url(../images/cc_images/icon_cct/icono-garbage-small.png)"
							}

							$("#search_suppliers" ).css(css);
							$(".disabled" ).css('background-color','#FFF');
							$(".disabled" ).removeAttr('disabled');
							$("#search_item" ).focus();
							
						}
						catch(e)
						{
							/*$('#ui-widget-customers').click(function ()
							{
								$.ajax(
								{
								type: "POST",
								url:location.href+"/remove_customer/",
								dataType: 'html',
								minLength: 3,
								delay: 100,
								cache: false
								});
								//alert("dsgdfg");
								var css={
								'background-color':'#FFF',
								"background-image":"url(../images/cc_images/icon_cct/icon-lupa.png)"
								}
								$("#search_customer" ).css(css);
								$("#search_customer" ).removeAttr('disabled');
								$("#search_customer").val("");
								$("#search_customer").focus();
								$(".disabled" ).css('background-color','#dddddd');
								$(".disabled" ).attr('disabled','disabled');
							});	*/
						}
						
					}
					});
					/*Si el provedore ya esta seleccionado*/
					if($("#search_suppliers" ).attr('disabled')=='disabled')
					{
						/*alert("Ws");*/
						
						var css={
						'background-color':'#dddddd',
						"background-image":"url(../images/cc_images/icon_cct/icono-garbage-small.png)"
						}
						$("#search_suppliers" ).css(css);
						$(".disabled" ).css('background-color','#FFF');
						$(".disabled" ).removeAttr('disabled');
						/*Borra el cliente del pedido en curso*/
						$('#ui-widget-suppliers').on('click',function ()
						{
							$.ajax(
							{
							type: "POST",
							url:location.href+"/remove_supplier/",
							dataType: 'html',
							minLength: 3,
							delay: 1,
							cache: false
							});
							var css=
							{
								'background-color':'#FFF',
								"background-image":"url(../images/cc_images/icon_cct/icon-lupa.png)"
							}
							$("#search_suppliers" ).css(css);
							$("#search_suppliers" ).removeAttr('disabled');
							$("#search_suppliers").val("");
							$("#search_suppliers").focus();
							$(".disabled" ).css('background-color','#dddddd');
							$(".disabled" ).attr('disabled','disabled');
						});
						/*Limpia y posciciona el cursor en el input de #item para facilitar agregar productos*/
						$("input[name*='search_item']").focus();
						$("input[name*='search_item']").focus(function()
						{
							$("input[name*='search_item']").val("");
						});
						$("input[name*='search_item']").click(function()
						{
							$("input[name*='search_item']").val("");
						});
						
					}
					/**/
					$("#print_btn").click(function()
					{
					window.print();
					});
					/**/
					$("#pay_recived").jkey('enter',function()
					{
						var supplier=$("#search_suppliers").val();
						if(supplier=="")
						{
						alert("Seleccione un proveedor");
						}
						else
						{
						event.stopImmediatePropagation();
						var recived =$("#pay_recived").val();
						var payment_type=$("#payment_options").val();
						recived = parseFloat(recived);
						recived=recived.toFixed(2);
						add_payment_receiving(payment_type,recived);
						}
				
					});
					/**/
					$("#comment").change( function (evt) 
					{
						set_comment_receiving();
					});
					/*Completaq el registro de la entrada*/
					$("#btn_register_receivings").click(function(){
						var supplier=$("#search_suppliers").val();
						if(supplier=="")
						{
						alert("Seleccione un proveedor");
						}
						else
						{
						r=confirm("Agregar pago?");
						if(r)
						{
						
						/*event.stopImmediatePropagation();*/
						var recived =$("#pay_recived").val();
						var payment_type=$("#payment_options").val();
						recived = parseFloat(recived);
						recived=recived.toFixed(2);
						add_payment_receiving(payment_type,recived);
						}
						}
					});
					/* */
					$("#facturar").click(function(){
						
						if($("#facturar").prop("checked")==true)
						{
								mode=1;
								$("#facturar").prop("checked",true);
								set_factura_mode(mode);
						}
						else if($("#facturar").prop("checked")==false)
						{
							
							
								mode=0;
								$("#facturar").prop("checked",false);
								set_factura_mode(mode);
							
						
						}
						else
						{
							
						}
					});
				}
		}
	});/*Fin del contenido de la magnificPopup*/
}
/**/
function get_receivings_payments_ajax()
{
	/*event.stopImmediatePropagation();*/
	$.ajax({
	type: "POST",
	url:location.href+"/get_payments_ajax/",
	dataType: 'json',
	minLength: 2,
	delay: 1,
	cache: false
	}).done(function(data)
	{

		var total_amount=data.total_amount;
		var total=parseFloat(data.total);
		var cambio=total_amount-total;
		$("#hidde_total").val(total);
		$("#total").replaceWith("<div id='total'><span  class='span_text_payment'>Total:</span><span class='by_pay_total'> $"+total.toFixed(2)+"</span></div>");
		$("#cover_complete_sale_pay").append('<input type="hidden" id="hidde_by_payment" val"'+cambio.toFixed(2)+'">');
		if(total_amount>= total )
		{
			$("#pay_recived").val(0.00);
			$("#recived_quantity" ).css('display','block');
			$('#amount_tendered').replaceWith('<output id="amount_tendered">'+data.payments+'</output>');

			$('#amount_change').replaceWith('<output id="amount_change">$'+cambio.toFixed(2)+'</output>');
			$("#change" ).css('display','block');
			$('#restante').css('display','none');

		}
		else
		{

			cambio=total_amount-total;
			cambio=cambio*-1;
			if($("#pay_recived").attr('disabled')=='disabled')
			{
				$("#pay_recived").removeAttr('disabled');
							$("#pay_recived" ).css('background-color','#FFF');
			}
			$("#pay_recived").val(cambio.toFixed(2));
			$("#recived_quantity" ).css('display','block');
			$('#amount_tendered').replaceWith('<output id="amount_tendered">'+data.payments+'</output>');
			$("#restante" ).css('display','block');
			$('#by_payment').replaceWith('<output id="by_payment">$'+cambio.toFixed(2)+'</output>');
			
		}
		$(".pagos").click(function()
		{
			/*alert(this.id);*/
			delete_payment(this.id,total);
		});
		$("#payment_options").change( function (evt) 
		{
			payment_type=$("#payment_options").val()
			if(payment_type=="credit")
			{
			alert(payment_type);
			}
			else
			{
			$("#pay_recived" ).css('background-color','#FFF');
			$("#pay_recived" ).removeAttr('disabled');
			$("#pay_recived").val(cambio.toFixed(2));
			$("#pay_recived").focus();
			$("#pay_recived").select();
			}

		});
		/*Selecciona el valor del input a editar*/
		$(".edit_item_receiving").click(function()
		{
			
				$(this).select();
			
		});
		/*Selecciona el valor del input a editar*/
		$(".price_receiving").click(function()
		{
			$(this).select();
			
			
		});
	
		/*Habilita el editar la cantidad de algun item en el panel de registro de ventas*/
		$(".edit_item_receiving").jkey('enter',function()
		{
				var res = $(".price_receiving").val();
				var res2 =res.replace("$", "");
				var precio =res2.replace(",", "");
				/*var edit_p=res.split("$");*/
				/*alert(this.id+$(this).val()+$(".price").val()+$(".discount").val());*/
				edit_item_receiving(this.id,$(this).val(),precio);
				$("input[name*='search_item']").focus();
			
		});
		/*Habilita el editar la cantidad de algun item en el panel de registro de ventas*/
		$(".price_receiving").jkey('enter',function()
		{
			var res = $(this).val();
			var res2 =res.replace("$", "");
			var precio =res2.replace(",", "");
				
			/*alert(this.id+$(this).val()+$(".edit_item").val()+$(".discount").val());*/
			edit_item_receiving(this.id,$(".edit_item_receiving").val(),precio);
			$("input[name*='search_item']").focus();
			
		});
		/*Habilita el editar la cantidad de algun item en el panel de registro de ventas*/
		$(".edit_item_receiving").change( function (evt) 
		{
				var res = $(".price_receiving").val();
				var res2 =res.replace("$", "");
				var precio =res2.replace(",", "");
				/*var edit_p=res.split("$");*/
				/*alert(this.id+$(this).val()+$(".price").val()+$(".discount").val());*/
				edit_item_receiving(this.id,$(this).val(),precio);
				$("input[name*='search_item']").focus();
			
		});
		/*Habilita el editar la cantidad de algun item en el panel de registro de ventas*/
		$(".price_receiving").change( function (evt) 
		{
			var res = $(this).val();
			var res2 =res.replace("$", "");
			var precio =res2.replace(",", "");
				
			/*alert(this.id+$(this).val()+$(".edit_item").val()+$(".discount").val());*/
			edit_item_receiving(this.id,$(".edit_item_receiving").val(),precio);
			$("input[name*='search_item']").focus();
			
		});
		
		/*Elimina productos individualmente en el panel de pedidos*/
		$(".quit" ).click(function(){
		
			delete_item_receiving(this.id);



		});
			$(window).jkey('f9',function()
					{
					
						/*event.stopPropagation();*/
						/*event.stopPropagation();*/
						
							
							$("#pay_recived").focus();
							$("#pay_recived").select();
							
							/*alert(total);*/
						
							
						
							
							
						
					
									
					});
	
	});
	
}
/**/
function add_payment_receiving(payment_type,recived)
{
	/*event.stopImmediatePropagation();*/
	var payments_data=
	{
	payment_type:payment_type,
	amount_tendered:recived
	}
	
	$.ajax({
	type: "POST",
	url:location.href+"/add_payment/",
	data:payments_data,
	dataType: 'json',
	minLength: 2,
	delay: 1,
	cache: false
	}).done(function(data)
	{
	

			var total_amount=data.total_amount;
			var total=parseFloat(data.total);
			var cambio=total_amount-total;
			$("#cover_complete_sale_pay").append('<input type="hidden" id="hidde_by_payment" val"'+cambio.toFixed(2)+'">');
			if(total_amount>= total )
			{	

				$("#recived_quantity" ).css('display','block');
				$('#amount_tendered').replaceWith('<output id="amount_tendered">'+data.payments+'</output>');

				$('#amount_change').replaceWith('<output id="amount_change">$'+cambio.toFixed(2)+'</output>');
				$("#change" ).css('display','block');
				$('#restante').css('display','none');
				
				complete_receiving();
				if(data.print_after_sale=="1")
				{	
				window.print();
				}

			}
			else
			{

				cambio=total_amount-total;
				cambio=cambio*-1;
				$("#recived_quantity" ).css('display','block');
				$('#amount_tendered').replaceWith('<output id="amount_tendered">'+data.payments+'</output>');
				$("#restante" ).css('display','block');
				$('#by_payment').replaceWith('<output id="by_payment">$'+cambio.toFixed(2)+'</output>');
				get_receivings_payments_ajax();
				
			}
		
			
		
		

			
	}).fail(function() {
    alert( "error agregando el pago" );
  });

}
/**/
function delete_payment(id,total)
{
	/*event.stopImmediatePropagation();*/
		$.ajax({
		type: "POST",
		url:location.href+"/delete_payment/",
		dataType: 'json',
		data:{payment_id:id},
		minLength: 2,
		delay: 1,
		cache: false
		}).done(function(data)
		{
		cambio=data.total_amount-total;
		cambio=cambio*-1;
		$("#recived_quantity" ).css('display','block');
		$('#amount_tendered').replaceWith('<output id="amount_tendered">'+data.payments+'</output>');
		$("#restante" ).css('display','block');
		$('#by_payment').replaceWith('<output id="by_payment">$'+cambio.toFixed(2)+'</output>');
		});
	/*alert( this.id );*/
}

/**/
function complete_receiving(pendiente=false)
{
	var hidde_id =$("#id_input").val();
	var supplier=$("#search_suppliers").val();
	var modos=$("#modos").val();
	if(pendiente==true)
	{
		modos="Pendiente";
	}	
	if(supplier=="")
	{
		alert("Seleccione un proveedor");
	}
	else
	{
	/*event.stopPropagation();*/
	$.ajax({
	type: "POST",
	url:location.href+"/save/"+hidde_id,
	data:{modos:modos},
	dataType: 'json',
	minLength: 2,
	delay: 1,
	cache: false
	}).done(function(data)
	{
		var css=
		{
		'background-color':'#FFF',
		"background-image":"url(../images/cc_images/icon_cct/icon-lupa.png)"
		}
		$('.label_field_sale').fadeOut( 2000, function()
		{
			$("#total").replaceWith("<div id='total'><span  class='span_text_payment'>Total:</span><span class='by_pay_total'> $0.00</span></div>");

			$("#pay_recived" ).val("");
			$("#change" ).css('display','none');
			$("#recived_quantity" ).css('display','none');
			$("#restante" ).css('display','none');
		});
		$("#search_suppliers" ).removeAttr('disabled');
		$("#pay_recived" ).css('background-color','#dddddd');
		$("#pay_recived" ).attr('disabled','disabled');
		$("#search_suppliers" ).css(css);
		$( ".cover_result_register_psr" ).replaceWith("<div class='cover_result_register_psr' id='warning_message_register'><div class='warning_message'>No hay productos</div></div>");
		$("#search_item").val("");
		$("#search_item" ).attr('disabled','disabled');
		$("#search_item" ).css('background-color','#dddddd');
		$("#search_suppliers").val("");
		$("#search_suppliers").focus();
		$('#cover_complete_sale').css( "height", "20px" );
		$('#cover_complete_sale').css( "height", "20px" );
		$("#facturar").prop("checked",false);
	
	}).fail(function(data) {
    alert( "error1" ); 
  });
	}
}
/**/
function cancel_receiving()
{
	/*event.stopPropagation();*/
	$.ajax(
	{
	type: "POST",
	url:location.href+"/cancel_receiving/",
	dataType: 'html',
	minLength: 2,
	delay: 1,
	cache: false,
	success: function(data)
	{
		$("#cover_register_sales_proccess").append(data);
		var css=
		{
		'background-color':'#FFF',
		"background-image":"url(../images/cc_images/icon_cct/icon-lupa.png)"
		}
		$("#total").replaceWith("<div id='total'><span  class='span_text_payment'>Total:</span><span class='by_pay_total'> $0.00</span></div>");
		$("#pay_recived" ).val("");
		$("#change" ).css('display','none');
		$("#recived_quantity" ).css('display','none');
		$("#restante" ).css('display','none');
		$("#search_suppliers" ).removeAttr('disabled');
		$("#pay_recived" ).css('background-color','#dddddd');
		$("#pay_recived" ).attr('disabled','disabled');
		$('#comment_containner').css( "display", "none" );
		$("#search_suppliers" ).css(css);
		$( ".cover_result_register_psr" ).remove();
		$( "#cover_table_register_psr" ).append("<div class='cover_result_register_psr' id='warning_message_register'><div class='warning_message'>No hay productos</div></div>");
		$("#search_item").val("");
		$("#search_item" ).css('background-color','#dddddd');
		$("#search_item" ).attr('disabled','disabled');
		$("#search_suppliers").val("");
		$("#search_suppliers").focus();
		$('#cover_complete_sale').css( "height", "20px" );
		$('#cover_complete_sale').css( "height", "20px" );
		$("#facturar").prop("checked",false);

	}
	});
}
/**/
function delete_item_receiving(line)
{
	/*event.stopPropagation();*/
	$.ajax({
	type: "POST",
	url:location.href+"/delete_item/"+line,
	dataType: 'json',
	minLength: 2,
	delay: 1,
	cache: false
	}).done(function(data)
	{

		$("input[name*='search_item']").focus();
		var total = parseFloat(data.total);
		$( "#cover_table_register_psr" ).fadeIn(500,function()
		{
		$("#total").replaceWith("<div id='total'><span  class='span_text_payment'>Total:</span><span class='by_pay_total'> $ "+total.toFixed(2)+"</span></div>");

		if(total===0)
		{
			$( ".cover_result_register_psr" ).replaceWith("<div class='cover_result_register_psr' id='warning_message_register'><div class='warning_message'>No hay productos</div></div>");

		}
		else
		{
			$( "#row_"+line ).remove();
		}
	});
	/* alert('jghhgg');*/
	});
	
}
/**/
function edit_item_receiving(line,quantity,price)
{
	/*event.stopImmediatePropagation();*/
	var edit_data=
	{
		quantity:parseFloat(quantity),
		price:parseFloat(price),
	}
	$.ajax({
	type: "POST",
	url:location.href+"/edit_item/"+line,
	data:edit_data,
	dataType: 'json',
	minLength: 2,
	delay: 1,
	cache: false
	}).done(function(data)
	{
		$( "#cover_table_register_psr" ).fadeIn(500,function()
		{ 

		var total = parseFloat(data.total);
		$("#total").replaceWith("<div id='total'><span  class='span_text_payment'>Total:</span><span class='by_pay_total'> $ "+total.toFixed(2)+"</span></div>");

		if(total==0)
		{
		
		$(".cover_result_register_psr").replaceWith("<div class='cover_result_register_psr'>"+data.table_receiving+"</div>");
		}
		else
		{
		$( ".cover_result_register_psr" ).remove();
		$("#cover_table_register_psr").append("<div class='cover_result_register_psr'>"+data.table_receiving+"</div>");

		}
		});
	
		get_receivings_payments_ajax();
	});
}
/**/
function set_comment_receiving()
{
	/*event.stopPropagation();*/
	$.ajax({
	type: "POST",
	url:location.href+"/set_comment/",
	dataType: 'json',
	data:{comment:$("#comment").val()},
	minLength: 2,
	delay: 1,
	cache: false
	}).done(function(data)
	{

	});
	
}
/**/
function set_factura_mode(mode)
{
	/*event.stopPropagation();*/
	$.ajax({
	type: "POST",
	url:location.href+"/set_factura_mode/"+mode,
	dataType: 'json',
	minLength: 2,
	delay: 1,
	cache: false
	}).done(function(data)
	{
		
		/*get_payments_ajax();*/
		/*$('.by_pay_total').replaceWith('<span class="by_pay_total">'+data.total+' '+data.coin+'</span>');*/
	});
}











